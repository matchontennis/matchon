//
//  MainViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 11/22/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MainViewController : UITabBarController {
    UIView *noteView;
}


@property (nonatomic, strong) IBOutlet UILabel *badge;

- (void)loadNewMessages;
//- (void)loadNewScores;

@end
