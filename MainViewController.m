//
//  MainViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 11/22/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "MainViewController.h"
#import "Constants.h"

@interface MainViewController () <UITabBarControllerDelegate>

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.delegate = self;
    [self.tabBar setTranslucent:NO];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPopup:) name:@"NotePopup" object:nil];
    
    noteView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    noteView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.5f];
    [noteView setHidden:YES];
    
    UIView *subNote = [[UIView alloc] initWithFrame:CGRectMake(30, noteView.frame.size.height / 2 - 120, noteView.frame.size.width - 60, 240)];
    subNote.backgroundColor = [UIColor whiteColor];
    UITextView *txtNote = [[UITextView alloc] initWithFrame:CGRectMake(10, 10, subNote.frame.size.width - 20, subNote.frame.size.height - 60)];
    txtNote.tag = 1000;
    NSString *noteStr = [NSString stringWithFormat:@"HOST NOTES: %@",@"Neque porro quisquamest, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. porro uisquamest, qui dolorem ipsum quia Neque porro quisquamest, qui dolorem ipsum quia dolor sitamet, onsectetur, adip"];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:noteStr];
    [attrStr addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Bold" size:15.0f]} range:NSMakeRange(0, 11)];
    [attrStr addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:15.0f]} range:NSMakeRange(11, noteStr.length - 11)];
    txtNote.attributedText = attrStr;
    UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(subNote.frame.size.width / 2 - 30, subNote.frame.size.height - 65, 60, 60)];
    [btnClose setBackgroundImage:[UIImage imageNamed:@"btn_close"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(hidePopup) forControlEvents:UIControlEventTouchUpInside];
    [subNote addSubview:txtNote];
    [subNote addSubview:btnClose];
    [noteView addSubview:subNote];
    [self.view addSubview:noteView];
    
}

-(void)showPopup:(NSNotification *)notification
{
    NSString *note = [notification.userInfo objectForKey:@"note"];
    UITextView *txtNote = (UITextView *)[noteView viewWithTag:1000];
    NSString *noteStr = [NSString stringWithFormat:@"HOST NOTES: %@",note];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:noteStr];
    [attrStr addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Bold" size:15.0f]} range:NSMakeRange(0, 11)];
    [attrStr addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:15.0f]} range:NSMakeRange(11, noteStr.length - 11)];
    txtNote.attributedText = attrStr;
    noteView.hidden = NO;
}

-(void)hidePopup
{
    noteView.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self loadNewMessages];
    //[self loadNewScores];

}

-(void) tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    if(tabBarController.selectedIndex != 2) {
        [self loadNewMessages];
        //[self loadNewScores];
    }
}


#pragma mark - Query helpers

- (void)loadNewMessages {
    if(![PFUser currentUser])return;
    
    PFQuery *query = [PFQuery queryWithClassName:@"Message"];
    [query whereKey:@"recipients" equalTo:[PFUser currentUser]];
    [query whereKey:@"read" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"active" notEqualTo:@NO];
    [query whereKey:@"deleted" notEqualTo:[PFUser currentUser]];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *notifications = [[NSNumber numberWithInt:number] stringValue];
            [self.badge removeFromSuperview];
            self.badge = nil;
            if(number > 0){
                self.badge=[[UILabel alloc]init];
                self.badge.text = notifications;
                self.badge.hidden = NO;
                self.badge.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12.0f];
                self.badge.textAlignment=NSTextAlignmentCenter;
                self.badge.frame=CGRectMake(self.view.frame.size.width-24.0f, 4.0f, 16.0f, 16.0f);
                self.badge.layer.cornerRadius=8;
                self.badge.clipsToBounds = YES;
                self.badge.textColor=[UIColor whiteColor];
                self.badge.backgroundColor= THEME_COLOR;
                UINavigationController *navigationController = (UINavigationController *)self.selectedViewController;
                [navigationController.navigationBar addSubview:self.badge];
            }
        });
    }];
}

/*- (void)loadNewScores {
    if(![PFUser currentUser])return;
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    [query orderByDescending:@"startTime"];
    [query whereKey:@"startTime" lessThan:[NSDate date]];
    [query includeKey:@"recipients"];
    [query includeKey:@"user"];
    [query includeKey:@"score"];
    [query includeKey:@"scorer"];
    [query includeKey:@"firstTeam"];
    [query includeKey:@"secondTeam"];
    [query includeKey:@"winner"];
    [query includeKey:@"loser"];
    [query includeKey:@"reviewer"];
    [query whereKey:@"recipients" equalTo:[PFUser currentUser]];
    [query whereKey:@"matchOn" equalTo:@YES];
    [query whereKey:@"active" equalTo:@YES];
    [query whereKey:@"approved" notEqualTo:@YES];
    [query whereKey:@"scorers" notEqualTo:[PFUser currentUser]];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
        NSString *notifications = [[NSNumber numberWithInt:number] stringValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            if(number>0) {
                [[self.tabBar.items objectAtIndex:3] setBadgeValue:notifications];
            } else {
                [[self.tabBar.items objectAtIndex:3] setBadgeValue:nil];
            }
        });
    }];
    
}*/

@end
