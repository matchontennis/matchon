//
//  ChallengePointModel.m
//  MatchOn
//
//  Created by Sol on 6/20/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "ChallengePointModel.h"
#import "ChallengeModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation ChallengePointModel

@dynamic challenge;
@dynamic user;
@dynamic point;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"ChallengePoint";
}

@end
