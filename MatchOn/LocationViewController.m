//
//  LocationViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/1/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "LocationViewController.h"
#import "PlayViewController.h"
#import "AppDelegate.h"
#import "ActivityAnnotation.h"

@interface MKMapView (ZoomLevel)
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;

-(double) getZoomLevel;
@end



@implementation MKMapView (ZoomLevel)

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel animated:(BOOL)animated {
    MKCoordinateSpan span = MKCoordinateSpanMake(0, 360/pow(2, zoomLevel)*self.frame.size.width/256);
    [self setRegion:MKCoordinateRegionMake(centerCoordinate, span) animated:animated];
}


-(double) getZoomLevel {
    return log2(360 * ((self.frame.size.width/256) / self.region.span.longitudeDelta));
}

@end


@interface LocationViewController () <CLLocationManagerDelegate, UIGestureRecognizerDelegate, UISearchDisplayDelegate, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) UISearchDisplayController *searchController;


@end

@implementation LocationViewController
@synthesize courtName;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.searchBar.placeholder = @"Where are you playing?";
    self.searchController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
    self.searchDisplayController.searchResultsTableView.tableHeaderView.frame = self.searchBar.frame;
    self.searchController.searchResultsDataSource = self;
    self.searchController.searchResultsDelegate = self;
    self.searchController.delegate = self;
    self.objects = [NSMutableArray array];
    self.searchDisplayController.searchResultsTableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    self.searchDisplayController.searchResultsTableView.separatorColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
    
    self.centerLocation = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 100.0f)];
    self.centerLocation.center = self.mapView.center;
    //[self.view addSubview:self.centerLocation];
}

-(void)gotoMyLocation
{
    //currentLocation = [[CLLocation alloc] initWithLatitude:40.0f longitude:100.0f];
    if (currentLocation != nil) {
        
        if ([self.mapView getZoomLevel] < 10) {
            [self.mapView setCenterCoordinate:currentLocation.coordinate zoomLevel:10 animated:YES];
        }else {
            [self.mapView setCenterCoordinate:currentLocation.coordinate animated:YES];
        }
        annotation.title = @"Match Location";
        annotation.subtitle = nil;
        courtName = nil;
        [annotation setCoordinate:currentLocation.coordinate];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    //[self.mapView setShowsUserLocation:YES];
    UIButton *btnMyLocation = [[UIButton alloc] initWithFrame:CGRectMake(self.mapView.frame.size.width - 40, self.mapView.frame.size.height - 50, 30, 30)];
    [btnMyLocation setBackgroundImage:[UIImage imageNamed:@"mylocation"] forState:UIControlStateNormal];
    [btnMyLocation addTarget:self action:@selector(gotoMyLocation) forControlEvents:UIControlEventTouchUpInside];
    [self.mapView addSubview:btnMyLocation];
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if(self.geopt) {
            NSLog(@"We have a geopoint");
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake(self.geopt.latitude, self.geopt.longitude);
            [self.mapView setCenterCoordinate:location animated:YES];
            
            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, 2000, 2000);
            MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
            [self.mapView setRegion:adjustedRegion animated:NO];
            
            if (annotation == nil) {
                annotation = [[MKPointAnnotation alloc] init];
                annotation.title = @"Match Location";
                [self.mapView addAnnotation:annotation];
            }
            [annotation setCoordinate:location];
            
        } else {
            if (!error) {
                CLLocationCoordinate2D location = CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude);
                [self.mapView setCenterCoordinate:location animated:YES];
                
                MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, 2000, 2000);
                MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
                [self.mapView setRegion:adjustedRegion animated:NO];
                if (annotation == nil) {
                    annotation = [[MKPointAnnotation alloc] init];
                    annotation.title = @"Match Location";
                    [self.mapView addAnnotation:annotation];
                }
                [annotation setCoordinate:location];
            }
        }
        for (int i = 0; i < self.mapView.annotations.count; i++) {
            if (![[self.mapView.annotations objectAtIndex:i] isKindOfClass:[ActivityAnnotation class]]) {
                continue;
            }
            ActivityAnnotation *courtAnnotation = (ActivityAnnotation *)[self.mapView.annotations objectAtIndex:i];
            if (self.courtName != nil && [self.courtName isEqualToString:courtAnnotation.title]) {
                annotation.title = courtAnnotation.title;
                annotation.subtitle = courtAnnotation.subtitle;
                annotation.coordinate = courtAnnotation.coordinate;
                [self.mapView viewForAnnotation:courtAnnotation].hidden = YES;
                break;
            }
        }
    }];
    
    self.centerLocation.layer.borderColor = [UIColor blueColor].CGColor;
    self.centerLocation.layer.borderWidth = 2.0f;
    self.centerLocation.layer.cornerRadius = self.centerLocation.frame.size.width/2.0f;
    
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    for (int i = 0; i < delegate.courts.count; i++) {
        NSDictionary *court = [delegate.courts objectAtIndex:i];
        ActivityAnnotation *courtAnnotation = [[ActivityAnnotation alloc] init];
        courtAnnotation.coordinate = CLLocationCoordinate2DMake([[court objectForKey:@"latitude"] doubleValue], [[court objectForKey:@"longitude"] doubleValue]);
        courtAnnotation.title = [court objectForKey:@"name"];
        courtAnnotation.subtitle = [court objectForKey:@"full_address"];
        if (annotation != nil) {
            if (annotation.coordinate.latitude == courtAnnotation.coordinate.latitude && annotation.coordinate.longitude == courtAnnotation.coordinate.longitude) {
                [self.mapView viewForAnnotation:courtAnnotation].hidden = YES;
                annotation.title = courtAnnotation.title;
                annotation.subtitle = courtAnnotation.subtitle;
                
            }
        }
        [self.mapView addAnnotation:courtAnnotation];
    }
    
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)courtAnnotation
{
    if ([annotation isEqual:courtAnnotation]) {
        MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:NSStringFromClass([MKPinAnnotationView class])];
        
        annotationView.pinColor = MKPinAnnotationColorRed;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = YES;
        annotationView.draggable = YES;
        UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        imgView.image = [UIImage imageNamed:@"court_callout"];
        annotationView.leftCalloutAccessoryView = imgView;
        return annotationView;
    }
    return [ActivityAnnotation createViewAnnotationForMapView:mapView annotation:courtAnnotation];
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState
{
    if (newState == MKAnnotationViewDragStateDragging) {
        for (int i = 0; i < mapView.annotations.count; i++) {
            if ([[mapView.annotations objectAtIndex:i] isKindOfClass:[ActivityAnnotation class]]) {
                ActivityAnnotation *courtAnnotation = (ActivityAnnotation *)[mapView.annotations objectAtIndex:i];
                if (courtAnnotation.coordinate.latitude == annotation.coordinate.latitude && courtAnnotation.coordinate.longitude == annotation.coordinate.longitude) {
                    [self.mapView viewForAnnotation:courtAnnotation].hidden = NO;
                    break;
                }
            }
        }
    }
    if (![view.annotation isEqual:annotation]) {
        return;
    }
    if (newState == MKAnnotationViewDragStateEnding) {
        ActivityAnnotation *courtAnnotation = (ActivityAnnotation *)view.annotation;
        AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        int i = 0;
        for (i = 0; i < delegate.courts.count; i++) {
            NSDictionary *court = [delegate.courts objectAtIndex:i];
            if ([[court objectForKey:@"latitude"] doubleValue] == courtAnnotation.coordinate.latitude && [[court objectForKey:@"longitude"] doubleValue] == courtAnnotation.coordinate.longitude) {
                courtName = [court objectForKey:@"name"];
            }
        }
        if (i == delegate.courts.count) {
            annotation.title = @"Match Location";
            annotation.subtitle = nil;
            courtName = nil;
        }
        self.mapView.centerCoordinate = courtAnnotation.coordinate;
    }
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if (![view.annotation isKindOfClass:[ActivityAnnotation class]]) {
        return;
    }
    ActivityAnnotation *courtAnnotation = (ActivityAnnotation *)view.annotation;
    courtName = nil;
    for (int i = 0; i < self.mapView.annotations.count; i++) {
        if (![[mapView.annotations objectAtIndex:i] isKindOfClass:[ActivityAnnotation class]]) {
            continue;
        }
        ActivityAnnotation *curAnnotation = (ActivityAnnotation *)[self.mapView.annotations objectAtIndex:i];
        if (curAnnotation.coordinate.latitude == annotation.coordinate.latitude && curAnnotation.coordinate.longitude == annotation.coordinate.longitude) {
            [self.mapView viewForAnnotation:curAnnotation].hidden = NO;
            break;
        }
    }
    self.mapView.centerCoordinate = courtAnnotation.coordinate;
    annotation.coordinate = courtAnnotation.coordinate;
    annotation.title = courtAnnotation.title;
    annotation.subtitle = courtAnnotation.subtitle;
    courtName = courtAnnotation.title;
    [self.mapView selectAnnotation:annotation animated:NO];
    view.hidden = YES;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if ([self.searchDisplayController.searchResultsTableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.searchDisplayController.searchResultsTableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.searchDisplayController.searchResultsTableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.searchDisplayController.searchResultsTableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - table helper

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    MKMapItem *mapItem = [self.objects objectAtIndex:indexPath.row];
   
    /*cell.textLabel.text = [[mapItem placemark] title];
    if(![[mapItem placemark] title]){
        cell.textLabel.text = [mapItem name];
    }*/
    NSString *searchText = mapItem.name;
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:searchText];
    NSRange range = [[searchText lowercaseString] rangeOfString:[self.searchBar.text lowercaseString]];
    if (range.location != NSNotFound) {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Bold" size:15.0f] range:range];
        if (range.location > 0) {
            [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:15.0f] range:NSMakeRange(0, range.location)];
        }
        if (range.location + range.length < [mapItem name].length) {
            [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:15.0f] range:NSMakeRange(range.location + range.length, [mapItem name].length - range.location - range.length)];
        }
    }else {
        [attrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"MyriadPro-Regular" size:15.0f] range:NSMakeRange(0, [mapItem name].length)];
    }
    cell.textLabel.attributedText = attrStr;
    cell.imageView.image = [UIImage imageNamed:@"search"];
    cell.detailTextLabel.text = mapItem.placemark.title;
    cell.detailTextLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12.0f];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.objects.count;
}

#pragma mark - Tableview delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    /*if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }*/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    MKMapItem *mapItem = [self.objects objectAtIndex:indexPath.row];
    [self.mapView setCenterCoordinate:mapItem.placemark.coordinate animated:YES];
    [self.searchDisplayController setActive:NO animated:YES];
    self.searchBar.text = mapItem.name;
    courtName = nil;
    [annotation setCoordinate:mapItem.placemark.coordinate];
}


#pragma mark - UISearch Delegate


// Temp search functions (for testing)

- (void)filterResults:(NSString *)searchTerm {
    if(searchTerm.length < 2) return;
    
    // Create a search request with a string
    MKLocalSearchRequest *searchRequest = [[MKLocalSearchRequest alloc] init];
    [searchRequest setNaturalLanguageQuery:searchTerm];
    
    // Create the local search to perform the search
    MKLocalSearch *localSearch = [[MKLocalSearch alloc] initWithRequest:searchRequest];
    [localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error) {
        if (!error) {
            [self.objects removeAllObjects];
            for (MKMapItem *mapItem in [response mapItems]) {
                NSLog(@"Name: %@, Placemark title: %@", [mapItem name], [[mapItem placemark] title]);
                NSLog(@"ITEM: %@", mapItem);
                if(!mapItem.name || [mapItem.placemark.title isEqualToString:@""] || mapItem.placemark.title == 0)continue;
                //NSLog(@"PLace MAKR: %@", mapItem.placemark.coordinate);
                [self.objects addObject:mapItem];
            }
            [self.searchDisplayController.searchResultsTableView reloadData];
        } else {
            NSLog(@"Search Request Error: %@", [error localizedDescription]);
        }
    }];
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    [self filterResults:searchString];
    return YES;
}




#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Not Enabled" message:@"Please Turn on Location Services\nFor this app to be useful you will need to turn on location services. We use your current location to show you only matches in your area.\nTo enable, go to the settings app on your phone, find the MatchOn app and enable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            //NSLog(@"Location here!");
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        {
            
        }
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"kCLAuthorizationStatusNotDetermined");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
            break;
    }
}

- (IBAction)done:(id)sender {
    CLLocationCoordinate2D location = annotation.coordinate;
    [self.delegate setLocation:[PFGeoPoint geoPointWithLatitude:location.latitude longitude:location.longitude] courtName:courtName];
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)cancel:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];

}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [manager stopUpdatingLocation];
    currentLocation = [locations lastObject];
}
@end
