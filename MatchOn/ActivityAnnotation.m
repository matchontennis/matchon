//
//  ActivityAnnotationView.m
//  AnchorApp
//
//  Created by Kevin Flynn on 3/2/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "ActivityAnnotation.h"

@implementation ActivityAnnotation

//@synthesize title;


/*
- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D theCoordinate;
    theCoordinate.latitude = 37.810000;
    theCoordinate.longitude = -122.477450;
    return theCoordinate;
}


// required if you set the MKPinAnnotationView's "canShowCallout" property to YES
- (NSString *)title
{
    return @"Golden Gate Bridge";
}

// optional
- (NSString *)subtitle
{
    return @"Opened: May 27, 1937";
}
*/
 
+ (MKAnnotationView *)createViewAnnotationForMapView:(MKMapView *)mapView annotation:(id <MKAnnotation>)annotation
{
    // try to dequeue an existing pin view first
    //MKAnnotationView *returnedAnnotationView =
    //[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([ActivityAnnotation class])];
    MKAnnotationView *returnedAnnotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:NSStringFromClass([ActivityAnnotation class])];
    /*if (returnedAnnotationView == nil)
    {
        returnedAnnotationView =
        [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:NSStringFromClass([ActivityAnnotation class])];
        
        ((MKPinAnnotationView *)returnedAnnotationView).pinColor = MKPinAnnotationColorRed;
        ((MKPinAnnotationView *)returnedAnnotationView).animatesDrop = YES;
        ((MKPinAnnotationView *)returnedAnnotationView).canShowCallout = YES;
    }*/
    returnedAnnotationView.image = [UIImage imageNamed:@"court"];
    returnedAnnotationView.canShowCallout = YES;
    
    return returnedAnnotationView;
}

@end
