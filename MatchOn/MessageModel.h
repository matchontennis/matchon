//
//  MessageModel.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/3/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>
#import "ActivityModel.h"

@interface MessageModel : PFObject

@property (nonatomic, strong) PFUser *from;

// Array of users
@property (nonatomic, strong) NSMutableArray *recipients;
@property (nonatomic, strong) NSMutableArray *deleted;

// Count of all the recipients
//@property (nonatomic, assign) int totalRecipients;

// Array of readers
@property (nonatomic, strong) NSMutableArray *read;

// If this is related to a match...
@property (nonatomic, strong) ActivityModel *activity;
@property (nonatomic, strong) NSString *text;


@end
