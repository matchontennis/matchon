//
//  HitPopupViewController.h
//  MatchOn
//
//  Created by Sol on 6/14/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HitPopupViewControllerDelegate

-(void)justHit;

@end

@interface HitPopupViewController : UIViewController

@property (nonatomic, weak) id<HitPopupViewControllerDelegate> delegate;
@property (nonatomic, strong) UIViewController *parent;
@property (weak, nonatomic) IBOutlet UIButton *btnHit;

- (IBAction)onClose:(id)sender;
- (IBAction)onHit:(id)sender;
@end
