//
//  InboxViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/3/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "SWTableViewCell.h"
#import "MessageModel.h"

@interface InboxViewController : UITableViewController <SWTableViewCellDelegate>


@property (nonatomic,assign) BOOL pullToRefreshEnabled;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) NSMutableArray *objects;
@property (nonatomic, assign) BOOL endOfResults;

// Query config
@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;
@property (nonatomic, assign) BOOL noResults;

- (IBAction)back:(id)sender;

@property (nonatomic, strong) MessageModel *message;

@end
