//
//  HelpViewController.m
//  MatchOn
//
//  Created by Sol on 5/8/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController () 

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    CGRect frame = self.scrMain.frame;
    frame.origin.x = 0;
    frame.origin.y = 0;
    frame.size.width = [UIScreen mainScreen].bounds.size.width;
    frame.size.height = [UIScreen mainScreen].bounds.size.height - 48;
    self.scrMain.translatesAutoresizingMaskIntoConstraints = YES;
    self.scrMain.frame = frame;
    for (int i = 0; i < 4; i++) {
        UIImageView *imgHelp = [[UIImageView alloc] initWithFrame:CGRectMake(i * self.scrMain.frame.size.width, 0, self.scrMain.frame.size.width, self.scrMain.frame.size.height)];
        imgHelp.image = [UIImage imageNamed:[NSString stringWithFormat:@"page%d",i + 1]];
        [self.scrMain addSubview:imgHelp];
    }
    
    self.btnStart.layer.cornerRadius = 2.0f;
    self.btnStart.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnStart.layer.borderWidth = 1.0f;
}

-(void)viewDidLayoutSubviews
{
    self.scrMain.contentSize = CGSizeMake(self.scrMain.frame.size.width * 4, self.scrMain.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onSkip:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"1" forKey:@"existing"];
    [defaults synchronize];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)onPage:(id)sender {
    self.scrMain.contentOffset = CGPointMake(self.pgControl.currentPage * self.scrMain.frame.size.width, 0);
    [self setPageControl];
}

- (IBAction)onStart:(id)sender {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"1" forKey:@"existing"];
    [defaults synchronize];
    [self.navigationController popToRootViewControllerAnimated:NO];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView.contentOffset.x < scrollView.frame.size.width) {
        self.pgControl.currentPage = 0;
    }else if (scrollView.contentOffset.x < 2 * scrollView.frame.size.width) {
        self.pgControl.currentPage = 1;
    }else if (scrollView.contentOffset.x < 3 * scrollView.frame.size.width) {
        self.pgControl.currentPage = 2;
    }else {
        self.pgControl.currentPage = 3;
    }
    [self setPageControl];
}

-(void)setPageControl
{
    if (self.pgControl.currentPage < 3) {
        self.pgControl.hidden = NO;
        self.btnSkip.hidden = NO;
        self.btnStart.hidden = YES;
    }else {
        self.pgControl.hidden = YES;
        self.btnSkip.hidden = YES;
        self.btnStart.hidden = NO;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x < scrollView.frame.size.width) {
        self.pgControl.currentPage = 0;
    }else if (scrollView.contentOffset.x < 2 * scrollView.frame.size.width) {
        self.pgControl.currentPage = 1;
    }else if (scrollView.contentOffset.x < 3 * scrollView.frame.size.width) {
        self.pgControl.currentPage = 2;
    }else {
        self.pgControl.currentPage = 3;
    }
    [self setPageControl];
}
@end
