//
//  ChallengeModel.h
//  MatchOn
//
//  Created by Sol on 6/4/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>

@interface ChallengeModel : PFObject <PFSubclassing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *short_description;
@property (nonatomic, strong) NSString *long_description;
@property (nonatomic, strong) NSDate *starttime;
@property (nonatomic, strong) NSDate *endtime;
@property (nonatomic, strong) PFFile *logo;
@property (nonatomic, assign) BOOL ongoing;
@property (nonatomic, strong) NSString *compare_field;
@property (nonatomic, strong) NSMutableArray *players;
@end
