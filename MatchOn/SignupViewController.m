//
//  SignupViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "SignupViewController.h"
#import "WizardViewController.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import <AddressBook/AddressBook.h>
#import "Localytics.h"

@implementation UITextField (custom)

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wobjc-protocol-method-implementation"

/*- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 10, bounds.origin.y + 8,
                      bounds.size.width - 20, bounds.size.height - 16);
}
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}*/

#pragma clang diagnostic pop

@end

@interface SignupViewController () <UITextFieldDelegate>

@property (nonatomic, assign) ABAddressBookRef addressBook;

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
    self.passwordField.delegate = self;
    
    [self.femaleLabel setTitleColor:THEME_COLOR forState:UIControlStateSelected];
    [self.maleLabel setTitleColor:THEME_COLOR forState:UIControlStateSelected];
    self.maleButton.layer.cornerRadius = self.maleButton.frame.size.height/2.0f;
    self.maleButton.clipsToBounds = YES;
    self.femaleButton.layer.cornerRadius = self.femaleButton.frame.size.height/2.0f;
    self.femaleButton.clipsToBounds = YES;
    self.maleButton.layer.borderWidth = 5.0f;
    self.maleButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.femaleButton.layer.borderWidth = 5.0f;
    self.femaleButton.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.viewFirstname.layer.cornerRadius = 3.0f;
    self.viewFirstname.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewFirstname.layer.borderWidth = 1.0f;
    
    self.viewLastname.layer.cornerRadius = 3.0f;
    self.viewLastname.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewLastname.layer.borderWidth = 1.0f;
    
    self.viewEmail.layer.cornerRadius = 3.0f;
    self.viewEmail.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewEmail.layer.borderWidth = 1.0f;
    
    self.viewPassword.layer.cornerRadius = 3.0f;
    self.viewPassword.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewPassword.layer.borderWidth = 1.0f;
    
    self.btnNext.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnNext.layer.borderWidth = 1.0f;
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


#pragma mark - Text field delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - IBActions

- (IBAction)male:(id)sender {
    self.maleButton.selected = YES;
    self.femaleButton.selected = NO;
    self.maleLabel.selected = YES;
    self.femaleLabel.selected = NO;
    self.maleButton.backgroundColor = THEME_COLOR;
    self.femaleButton.backgroundColor = [UIColor whiteColor];
}

- (IBAction)female:(id)sender {
    self.maleButton.selected = NO;
    self.femaleButton.selected = YES;
    self.maleLabel.selected = NO;
    self.femaleLabel.selected = YES;
    self.femaleButton.backgroundColor = THEME_COLOR;
    self.maleButton.backgroundColor = [UIColor whiteColor];
}

- (IBAction)next:(id)sender {
    
    if([self.firstField.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please enter your first name!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if([self.lastField.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please enter your last name!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if([self.emailField.text length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please enter an email!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    else if([self.passwordField.text length] < 5 ) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Your password must be atleast 6 characters!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    if(!self.maleButton.selected && !self.femaleButton.selected) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Please select your gender." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    PFUser *user = [PFUser user];
    if([PFUser currentUser]) {
        user = [PFUser currentUser];
    }
    
    user.password = self.passwordField.text;
    user.email = self.emailField.text;
    user.username = [self.emailField.text lowercaseString];
    user[@"first"] = self.firstField.text;
    user[@"last"] = self.lastField.text;
    user[@"name"] = [NSString stringWithFormat:@"%@ %@", self.firstField.text,  self.lastField.text];
    user[@"female"] = self.femaleButton.selected?@YES:@NO;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    if([PFUser currentUser]) {
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (!error) {
                [self performSegueWithIdentifier:@"wizard" sender:self];
            } else {
                NSString *errorString = [error userInfo][@"error"];
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }];
        return;
    }
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            [Localytics tagEvent:@"Account is created"];
            _addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
            [self loadAddressBook];
            [self performSegueWithIdentifier:@"wizard" sender:self];
        } else {
            NSString *errorString = [error userInfo][@"error"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"wizard"]) {
        WizardViewController *vc = [segue destinationViewController];
        vc.prevViewController = self;
    }
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)loadAddressBook{
    [self checkAddressBookAccess];
}

-(void)checkAddressBookAccess
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case  kABAuthorizationStatusAuthorized:
            [self accessGrantedForAddressBook];
            break;
            // Prompt the user for access to Contacts if there is no definitive answer
        case  kABAuthorizationStatusNotDetermined :
            [self requestAddressBookAccess];
            NSLog(@"Not determined");
            break;
            // Display a message if the user has denied or restricted access to Contacts
        case  kABAuthorizationStatusDenied:
            NSLog(@"Denied");
            break;
        case  kABAuthorizationStatusRestricted:
            NSLog(@"Restricted");
            break;
        default:
            break;
    }
}

// Prompt the user for access to their Address Book data
-(void)requestAddressBookAccess
{
    SignupViewController * __weak weakSelf = self;
    
    ABAddressBookRequestAccessWithCompletion(self.addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [weakSelf accessGrantedForAddressBook];
                                                     });
                                                 }
                                             });
}


- (void)accessGrantedForAddressBook{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, ^{
        
        NSArray *originalArray = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(self.addressBook));
        NSMutableArray *emailContacts = [NSMutableArray array];
        
        for (id object in originalArray) {
            ABRecordRef record = (__bridge ABRecordRef)object; // get address book record
            
            CFTypeRef emailProp = ABRecordCopyValue(record, kABPersonEmailProperty);
            NSMutableArray *emails = [[NSMutableArray alloc] initWithArray:(__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(emailProp)];
            CFRelease(emailProp);
            for(NSString *email in emails){
                [emailContacts addObject:email];
            }
        }
        [self sendNotification:emailContacts];
    });
}

-(void)sendNotification:(NSArray *)emails
{
    PFQuery *query = [PFUser query];
    [query whereKey:@"email" containedIn:emails];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" containedIn:objects];
        NSString *first = [[PFUser currentUser] objectForKey:@"first"];
        NSString *last = [[PFUser currentUser] objectForKey:@"last"];
        if(!first) {
            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        NSString *full = first;
        if(last && last.length > 0) {
            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
        }
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"alert": [NSString stringWithFormat:@"Your contact %@ has joined MatchOn! Invite them to play a match.", full], @"badge" : @"Increment"}];
        
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];
    }];
}
@end
