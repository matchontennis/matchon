//
//  HitPopupViewController.m
//  MatchOn
//
//  Created by Sol on 6/14/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "HitPopupViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface HitPopupViewController ()

@end

@implementation HitPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnHit.layer.cornerRadius = 5.0f;
    self.btnHit.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnHit.layer.borderWidth = 2.0f;
    self.btnHit.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClose:(id)sender {
    if (self.parent) {
        [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
}

- (IBAction)onHit:(id)sender {
    if (self.parent) {
        [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    [self.delegate justHit];
}
@end
