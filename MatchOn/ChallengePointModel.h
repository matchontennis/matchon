//
//  ChallengePointModel.h
//  MatchOn
//
//  Created by Sol on 6/20/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>

@class ChallengeModel;

@interface ChallengePointModel : PFObject <PFSubclassing>

@property (nonatomic, strong) ChallengeModel *challenge;
@property (nonatomic, strong) PFUser *user;
@property (nonatomic, assign) int point;

@end
