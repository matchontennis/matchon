//
//  ChallengesViewController.m
//  MatchOn
//
//  Created by Sol on 5/27/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "ChallengesViewController.h"
#import "MainViewController.h"
#import "ChallengeDetailViewController.h"
#import "ChallengeModel.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface ChallengesViewController () <ChallengeDetailViewControllerDelegate>

@end

@implementation ChallengesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    [self.tblChallenges addSubview:self.refreshControl];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.limit = 100;
    self.skip = 0;
    
    [self refreshInbox];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    //self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    self.navigationItem.title = @"Challenges";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc loadNewMessages];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"challengedetail"]) {
        ChallengeDetailViewController *viewController = (ChallengeDetailViewController *)segue.destinationViewController;
        viewController.delegate = self;
        if (selectedChallenge != nil) {
            viewController.challenge = selectedChallenge;
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return challenges.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 104.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChallengeCell" forIndexPath:indexPath];
    ChallengeModel *challenge = (ChallengeModel *)[challenges objectAtIndex:indexPath.section];
    UIImageView *imgState = (UIImageView *)[cell viewWithTag:10];
    PFImageView *imgLogo = (PFImageView *)[cell viewWithTag:11];
    UILabel *lblName = (UILabel *)[cell viewWithTag:12];
    UILabel *lblDesc = (UILabel *)[cell viewWithTag:13];
    UILabel *lblDate = (UILabel *)[cell viewWithTag:14];
    BOOL isJoined = NO;
    if (challenge.players != nil && ![challenge.players isEqual:[NSNull null]]) {
        for (int i = 0; i < challenge.players.count; i++) {
            PFUser *chUser = (PFUser *)[challenge.players objectAtIndex:i];
            NSLog(@"%@,%@",chUser.objectId, [PFUser currentUser].objectId);
            if (chUser != nil && ![chUser isEqual:[NSNull null]] && [chUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                isJoined = YES;
                break;
            }
        }
    }
    imgState.hidden = !isJoined;
    imgLogo.file = challenge.logo;
    imgLogo.image = [UIImage imageNamed:@"challenge_logo"];
    [imgLogo loadInBackground];
    if (challenge.name != nil && ![challenge.name isEqualToString:@""]) {
        lblName.text = [challenge.name uppercaseString];
    }else {
        lblName.text = @"";
    }
    if (challenge.short_description != nil && ![challenge.short_description isEqualToString:@""]) {
        lblDesc.text = challenge.short_description;
    }else {
        lblDesc.text = @"";
    }
    if (challenge.ongoing) {
        lblDate.text = @"ongoing";
    }else if (challenge.endtime != nil && ![challenge.endtime isEqual:[NSNull null]]) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"M.dd.yy"];
        //lblDate.text = [formatter stringFromDate:challenge.endtime];
        lblDate.text = [formatter stringFromDate:challenge.starttime];
    }else {
        lblDate.text = @"";
    }
    cell.contentView.layer.borderWidth = 1.0f;
    cell.contentView.layer.borderColor = [UIColor colorWithRed:193 / 255.0f green:196 / 255.0f blue:214 / 255.0f alpha:1.0f].CGColor;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedChallenge = (ChallengeModel *)[challenges objectAtIndex:indexPath.section];
    NSLog(@"%@",selectedChallenge);
    [self performSegueWithIdentifier:@"challengedetail" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)loadObjects{
    PFQuery *query = [self queryForTable];
    if(self.pullToRefreshEnabled) {
        self.skip = 0;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        NSLog(@"%@",objects);
        if(objects.count == 0) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [challenges removeAllObjects];
        }
        if(whichPass > 1) {
            [challenges removeAllObjects];
        }
        if (challenges == nil && objects != nil) {
            challenges = [NSMutableArray array];
        }
        [challenges addObjectsFromArray:objects];
        
        if(challenges.count == 0) {
            if(!self.noResults) {
                self.noResults = TRUE;
            }
        } else {
            self.noResults = FALSE;
        }
        [self.tblChallenges reloadData];
        
        self.pullToRefreshEnabled = FALSE;
        whichPass = whichPass + 1;
    }];
}


#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query = [PFQuery queryWithClassName:@"Challenge"];
    [query orderByDescending:@"ongoing"];
    [query addAscendingOrder:@"createdAt"];
    return query;
}

- (void)refreshInbox {
    if(![PFUser currentUser]) {
        [challenges removeAllObjects];
        [self.tblChallenges reloadData];
        return;
    }
    self.pullToRefreshEnabled = TRUE;
    self.skip = 0;
    [self loadObjects];
}

-(void)setJoin:(ChallengeModel *)challenge
{
    for (int i = 0; i < challenges.count; i++) {
        ChallengeModel *curChallenge = (ChallengeModel *)[challenges objectAtIndex:i];
        if ([curChallenge.objectId isEqualToString:challenge.objectId]) {
            [challenges setObject:challenge atIndexedSubscript:i];
            break;
        }
    }
    [self.tblChallenges reloadData];
}

@end
