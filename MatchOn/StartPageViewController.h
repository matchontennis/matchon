//
//  StartPageViewController.h
//  MatchOn
//
//  Created by Dandeljane Maraat on 4/18/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "ActivityModel.h"
#import "HomeViewController.h"

@protocol StartPageViewControllerDelegate<NSObject>
- (void)matchRequested:(BOOL)requested;
- (void)hideStartPage:(BOOL)show;
@end
@interface StartPageViewController : UIViewController<StartPageViewControllerDelegate>
@property (nonatomic, strong) ActivityModel *activity;

- (IBAction)play:(id)sender;
- (IBAction)home:(id)sender;

@property (nonatomic, assign) int total;
@property (nonatomic, assign) int week;
@property (nonatomic, assign) int month;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentMainLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentSubLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailsImageView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *notNowButton;

@property (weak) id <HomeViewControllerDelegate> homeDelegate;

@end
