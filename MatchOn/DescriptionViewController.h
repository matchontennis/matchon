//
//  DescriptionViewController.h
//  MatchOn
//
//  Created by Sol on 5/8/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionViewController : UIViewController

@property (nonatomic, strong) UIViewController *prevViewController;
@property (weak, nonatomic) IBOutlet UIImageView *imgBack;
@property (nonatomic, strong) UIImage *imgBg;
- (IBAction)onBack:(id)sender;
@end
