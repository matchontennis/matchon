//
//  ProfileViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/4/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "ProfileViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "MBProgressHUD.h"
#import "RatingModel.h"
#import "ScoreModel.h"
#import "MLKMenuPopover.h"
#import "LocationViewController.h"
#import "DescriptionViewController.h"

#define MENU_POPOVER_FRAME CGRectMake(62,72,246,175)

@interface ProfileViewController () <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITableViewDelegate, UITableViewDataSource,MLKMenuPopoverDelegate,LocationViewControllerDelegate,UITextViewDelegate>

@property(nonatomic,strong) MLKMenuPopover *menuPopover;
@property(nonatomic,strong) NSArray *menuItems;

@property (nonatomic, strong) UIImagePickerController *imagePicker;


@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.first.delegate = self;
    self.last.delegate = self;
    self.first.returnKeyType = UIReturnKeyDone;
    self.last.returnKeyType = UIReturnKeyDone;
    
    self.email.delegate = self;
    self.email.returnKeyType = UIReturnKeyDone;
    self.email.keyboardType = UIKeyboardTypeEmailAddress;
    //self.view.backgroundColor = [UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
    
    self.scrMain.translatesAutoresizingMaskIntoConstraints = YES;
    
    CGRect frame = self.scrMain.frame;
    frame.origin.y = 0;
    frame.size = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 64 - 44);
    
    self.scrMain.frame = frame;
    self.viewAbout.hidden = NO;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(photo:)];
    [tap1 setCancelsTouchesInView:YES];
    [self.viewPhoto addGestureRecognizer:tap1];
    
    tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [tap2 setCancelsTouchesInView:YES];
    [self.view addGestureRecognizer:tap2];
    
    frame = self.viewCommon.frame;
    frame.size.height = 0;
    self.viewCommon.translatesAutoresizingMaskIntoConstraints = YES;
    self.viewCommon.frame = frame;
    self.segType.selectedSegmentIndex = 0;
    [self.segType setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0f]} forState:UIControlStateNormal];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    //self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    self.navigationItem.title = @"My Profile";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.email.text = [PFUser currentUser].email;
    self.first.text = [PFUser currentUser][@"first"];
    self.last.text = [PFUser currentUser][@"last"];
    
    
    NSString *first = [[PFUser currentUser] objectForKey:@"first"];
    NSString *last = [[PFUser currentUser] objectForKey:@"last"];
    
    if(!first) {
        NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
        first = [name firstObject];
        if(!last) {
            if(name.count > 1) {
                last = [name lastObject];
            }
        }
    }
    self.lblName.text = first;
    if(last && last.length > 0) {
        self.lblName.text = [NSString stringWithFormat:@"%@ %@.",self.lblName.text, [last substringToIndex:1]];
    }
    
    self.imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
    PFFile *photoImage = [PFUser currentUser][@"image"];
    //if (photoImage != nil && ![photoImage isEqual:[NSNull null]]) {
        self.imgPhoto.file = [PFUser currentUser][@"image"];
        /*if([[PFUser currentUser][@"female"] boolValue]) {
            self.imgPhoto.image = [UIImage imageNamed:@"female"];
            //self.imgGender.image = [UIImage imageNamed:@"male_gender"];
        } else {
            self.imgPhoto.image = [UIImage imageNamed:@"male"];
            //self.imgGender.image = [UIImage imageNamed:@"female_gender"];
        }
    }else {*/
        //self.imgPhoto.file = nil;
        self.imgPhoto.image = [UIImage imageNamed:@"default_profile"];
    //}
    
    

    self.imgPhoto.clipsToBounds = YES;
    [self.imgPhoto loadInBackground:^(UIImage *image, NSError *error) {
        NSLog(@"Loaded");
    }];
    if ([PFUser currentUser][@"geopoint"] != nil && ![[PFUser currentUser][@"geopoint"] isEqual:[NSNull null]]) {
        self.geopt = [PFUser currentUser][@"geopoint"];
    }
    
    self.court = [PFUser currentUser][@"homecourt"];
    
    if ([PFUser currentUser][@"homecourt"] && ![[PFUser currentUser][@"homecourt"] isEqual:[NSNull null]]) {
        self.txtHomeCourt.text = [PFUser currentUser][@"homecourt"];
    }else {
        self.txtHomeCourt.text = @"";
    }
    if ([PFUser currentUser][@"aboutme"] && ![[PFUser currentUser][@"aboutme"] isEqual:[NSNull null]]) {
        self.txtAboutMe.text = [PFUser currentUser][@"aboutme"];
    }else {
        self.txtAboutMe.text = @"";
    }
    
    
    self.imgPhoto.layer.cornerRadius = self.imgPhoto.frame.size.width/2.0f;
    self.imgPhoto.layer.borderWidth = 2.0f;
    self.imgPhoto.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.imgPhoto.clipsToBounds = YES;
    
    RatingModel *rating = (RatingModel *)[PFUser currentUser][@"rating"];
    [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (error) {
            return;
        }
        self.lblAllPoints.text = [NSString stringWithFormat:@"%d", [[object objectForKey:@"score"] intValue]];
    }];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Score"];
    [query whereKey:@"players" equalTo:[PFUser currentUser]];
    //[query whereKey:@"approved" equalTo:@YES];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        self.lblLastPlayed.text = @"";
        if (error) {
            return;
        }
        self.lblPlayedMatches.text = [NSString stringWithFormat:@"%ld",(long)objects.count];
        int ball = 0;
        if (objects.count >= 1) {
            ball = 1;
        }
        NSMutableArray *opponents = [[NSMutableArray alloc] init];
        int matchesCompleted = 0;
        doubleMatches = [[NSMutableArray alloc] init];
        doubleExpanded = [[NSMutableArray alloc] init];
        singleExpanded = [[NSMutableArray alloc] init];
        singleMatches = [[NSMutableArray alloc] init];
        for (int i = 0; i < objects.count; i++) {
            ScoreModel *score = (ScoreModel *)[objects objectAtIndex:i];
            NSLog(@"%d",score.approved);
            //if (score.approved) {
            if (score.players.count == 4) {
                [doubleMatches addObject:score];
                [doubleExpanded addObject:@NO];
            }else {
                [singleMatches addObject:score];
                [singleExpanded addObject:@NO];
            }
            if (score.approved) {
                matchesCompleted++;
            }
            
            if (i == 0) {
                NSTimeInterval lastSeconds = [[NSDate date] timeIntervalSinceDate:score.createdAt];
                if (lastSeconds < 60) {
                    if (lastSeconds == 1.0) {
                        self.lblLastPlayed.text = @"Last played 1 second ago";
                    }else {
                        self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d seconds ago",(int)lastSeconds];
                    }
                }else if (lastSeconds < 3600) {
                    if ((int)(lastSeconds / 60) == 1) {
                        self.lblLastPlayed.text = @"Last played 1 minute ago";
                    }else {
                        self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d minutes ago",(int)(lastSeconds / 60)];
                    }
                }else if (lastSeconds < 3600 * 24) {
                    if ((int)(lastSeconds / 3600) == 1) {
                        self.lblLastPlayed.text = @"Last played 1 hour ago";
                    }else {
                        self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d hours ago",(int)(lastSeconds / 3600)];
                    }
                }else {
                    if ((int)(lastSeconds / (3600 * 24)) == 1) {
                        self.lblLastPlayed.text = @"Last played 1 day ago";
                    }else {
                        self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d days ago",(int)(lastSeconds / (3600 * 24))];
                    }
                }
            }
            for (int j = 0; j < score.players.count; j++) {
                PFUser *player = (PFUser *)[score.players objectAtIndex:j];
                if ([opponents containsObject:player.objectId]) {
                    continue;
                }
                if ([player.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    continue;
                }
                [opponents addObject:player.objectId];
            }
        }
        if (objects.count >= 4 && opponents.count >= 3) {
            ball = 2;
        }
        if (objects.count >= 9 && opponents.count >= 5) {
            ball = 3;
        }
        if (objects.count >= 12) {
            ball = 4;
        }
        if (ball < 4) {
            self.viewBalls.hidden = NO;
            self.imgVerified.hidden = YES;
            for (int i = 0; i < 3; i++) {
                UIImageView *imgSubBall = (UIImageView *)[self.viewBalls.subviews objectAtIndex:i];
                if (i < ball) {
                    imgSubBall.image = [UIImage imageNamed:@"ball_on"];
                }else {
                    imgSubBall.image = [UIImage imageNamed:@"ball_off"];
                }
            }
        }else {
            self.viewBalls.hidden = YES;
            self.imgVerified.hidden = NO;
        }
        //self.lblCompleted.text = [NSString stringWithFormat:@"%d",matchesCompleted];
        [self.tblDoubles reloadData];
        [self.tblSingles reloadData];
        [self expandTableView];
    }];
    
    [self.segType setTintColor:[UIColor colorWithRed:1.0f green:156 / 255.0f blue:0.01 alpha:1.0f]];
    for (int i = 0; i < 3; i++) {
        if (i == 0) {
            [self.segType setWidth:self.segType.frame.size.width / 4 forSegmentAtIndex:0];
        }else {
            [self.segType setWidth:(self.segType.frame.size.width * 3) / 8 forSegmentAtIndex:i];
        }
    }
    //[self.segType setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir Next Condensed Medium" size:13.0f]} forState:UIControlStateNormal];
    
    self.menuItems = @[@{@"title":@"EDIT NAME/EMAIL/PASSWORD",
                         @"image":@"small_edit",
                         @"color":[UIColor whiteColor],
                         @"bg_color":[UIColor clearColor]},
                       @{@"title":@"REPORT ABUSE",
                         @"image":@"small_not",
                         @"color":[UIColor whiteColor],
                         @"bg_color":[UIColor clearColor]},
                       @{@"title":@"LOGOUT",
                         @"image":@"small_logout",
                         @"color":[UIColor colorWithRed:152 / 255.0f green:157 / 255.0f blue:187 / 255.0f alpha:1.0f],
                         @"bg_color":[UIColor colorWithRed:42 / 255.0f green:42 / 255.0f blue:50 / 255.0f alpha:1.0f]}];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView)];
    [tap4 setCancelsTouchesInView:YES];
    [self.viewBalls addGestureRecognizer:tap4];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView)];
    [tap3 setCancelsTouchesInView:YES];
    [self.imgVerified addGestureRecognizer:tap3];
}

-(UIBarButtonItem *)addDoneBtn
{
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 54, 44);
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(actionDone) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *doneBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 54, 44)];
    
    doneBtnView.bounds = CGRectOffset(doneBtnView.bounds, -10, 0);
    [doneBtnView addSubview:doneBtn];
    
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithCustomView:doneBtnView];
    return cancelItem;
}

-(void)actionDone
{
    [self.txtAboutMe endEditing:YES];
}

-(void)showDescView
{
    [self performSegueWithIdentifier:@"description" sender:nil];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSString *email = [PFUser currentUser].email;
    NSString *username = [PFUser currentUser].username;
    
    PFUser *user = [PFUser currentUser];

    if(textField == self.first) {
        user[@"first"] = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else if(textField == self.last) {
        user[@"last"] = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        // Email
        user.email = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        user.username = [[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    }
    if(textField == self.first || textField == self.last) {
        user[@"name"] = [NSString stringWithFormat:@"%@ %@",user[@"first"],user[@"last"]];
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (error) {
            NSString *errorString = [error userInfo][@"error"];
            // Show the errorString somewhere and let the user try again.
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Whoops" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
            if (textField == self.email) {
                user.username = username;
                user.email = email;
                self.email.text = user.email;
            }
        } else {
            [[PFUser currentUser] fetchInBackground];
        }
        NSString *first = self.first.text;
        NSString *last = self.last.text;
        
        if(!first) {
            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        self.lblName.text = first;
        if(last && last.length > 0) {
            self.lblName.text = [NSString stringWithFormat:@"%@ %@.",self.lblName.text, [last substringToIndex:1]];
        }
    }];
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    curOffset = self.scrMain.contentOffset;
    if ([textView.text isEqualToString:@"Introduce yourself to other local players..."]) {
        textView.text = @"";
    }
    self.scrMain.contentOffset = CGPointMake(0, self.txtAboutMe.frame.origin.y + self.viewCommon.frame.origin.y - 20);
    curBarItem = self.navigationItem.rightBarButtonItem;
    self.navigationItem.rightBarButtonItem = [self addDoneBtn];
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    NSString *curNote = @"";
    if (textView.text.length == 0) {
        textView.text = @"Introduce yourself to other local players...";
    }else {
        curNote = textView.text;
    }
    [[PFUser currentUser] setObject:curNote forKey:@"aboutme"];
    [[PFUser currentUser] saveInBackground];
    self.txtAboutMe.selectable = NO;
    self.txtAboutMe.editable = NO;
    self.txtAboutMe.backgroundColor = [UIColor clearColor];
    self.txtAboutMe.layer.borderWidth = 0.0f;
    self.scrMain.contentOffset = curOffset;
    self.navigationItem.rightBarButtonItem = curBarItem;
    return YES;
}



#pragma mark - IBActions

- (IBAction)onType:(id)sender {
    if (self.segType.selectedSegmentIndex == 0) {
        self.viewAbout.hidden = NO;
        self.tblDoubles.hidden = YES;
        self.tblSingles.hidden = YES;
        [self expandTableView];
    }else if (self.segType.selectedSegmentIndex == 1) {
        self.viewAbout.hidden = YES;
        self.tblDoubles.hidden = YES;
        self.tblSingles.hidden = NO;
        [self expandTableView];
    }else if (self.segType.selectedSegmentIndex == 2) {
        self.viewAbout.hidden = YES;
        self.tblDoubles.hidden = NO;
        self.tblSingles.hidden = YES;
        [self expandTableView];
    }
}

- (IBAction)onSettings:(id)sender {
    [self.menuPopover dismissMenuPopover];
    self.menuPopover = [[MLKMenuPopover alloc] initWithFrame:MENU_POPOVER_FRAME menuItems:self.menuItems];
    
    self.menuPopover.menuPopoverDelegate = self;
    [self.menuPopover showInView:self.scrMain];
    
    [self.view removeGestureRecognizer:tap2];
}

- (IBAction)onCollapse:(id)sender {
    [UIView beginAnimations:@"Common" context:nil];
    [UIView setAnimationDuration:0.5f];
    CGRect frame = self.viewCommon.frame;
    frame.size.height = 0;
    self.viewCommon.translatesAutoresizingMaskIntoConstraints = YES;
    self.viewCommon.frame = frame;
    [UIView commitAnimations];
}

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex
{
    [self.menuPopover dismissMenuPopover];
    
    if (selectedIndex == 0) {
        [UIView beginAnimations:@"Common" context:nil];
        [UIView setAnimationDuration:0.5f];
        CGRect frame = self.viewCommon.frame;
        frame.size.height = 395;
        self.viewCommon.translatesAutoresizingMaskIntoConstraints = YES;
        self.viewCommon.frame = frame;
        [UIView commitAnimations];
    }else if (selectedIndex == 1) {
        [self performSegueWithIdentifier:@"support" sender:self];
    }else if (selectedIndex == 2) {
        [self logout:nil];
    }
    [self.view addGestureRecognizer:tap2];
}

- (IBAction)password:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset Password"
                                                    message:@"Instructions will be sent to your email address."
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Send",nil];
    [alert show];
}

- (IBAction)photo:(id)sender {
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] < 8) {
        // iOS 7
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Take Photo",
                                @"Choose Existing Photo",
                                nil];
        
        for(UIButton *button in [popup subviews]) {
            NSLog(@"Button title: %@",button.titleLabel.text);
            if([button.titleLabel.text isEqualToString:@"Take Photo"]) {
                [button addTarget:self action:@selector(snap:) forControlEvents:UIControlEventTouchUpInside];
            }
            else if ([button.titleLabel.text isEqualToString:@"Choose Existing Photo"]) {
                [button addTarget:self action:@selector(album:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else {
        NSLog(@"iOS 8");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
                NSLog(@"Dismissed");
            }];
        }];
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self snap:self];
        }];
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"Choose Existing Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self album:self];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:cameraAction];
        [alertController addAction:albumAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)logout:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Logout" object:self];
    [self.tabBarController setSelectedIndex:0];
}

- (IBAction)onEditCourt:(id)sender {
    [self performSegueWithIdentifier:@"location" sender:self];
}

- (IBAction)onEditAboutMe:(id)sender {
    self.txtAboutMe.backgroundColor = [UIColor whiteColor];
    self.txtAboutMe.editable = YES;
    self.txtAboutMe.selectable = YES;
    self.txtAboutMe.layer.cornerRadius = 5.0f;
    self.txtAboutMe.layer.borderColor = [[UIColor colorWithRed:151 / 255.0f green:152 / 255.0f blue:163 / 255.0f alpha:1.0f] CGColor];
    self.txtAboutMe.layer.borderWidth = 2.0f;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"location"]) {
        LocationViewController *locationViewController = (LocationViewController *)segue.destinationViewController;
        locationViewController.delegate = self;
        locationViewController.courtName = self.court;
        locationViewController.geopt = self.geopt;
    }else if ([segue.identifier isEqualToString:@"description"]) {
        DescriptionViewController *descriptionViewController = (DescriptionViewController *)segue.destinationViewController;
        descriptionViewController.imgBg = [UIImage imageNamed:@"badge_desc"];
    }
}

-(void)setLocation:(PFGeoPoint *)geopoint courtName:(NSString *)courtName
{
    if (geopoint != nil) {
        self.geopt = geopoint;
    }
    self.court = courtName;
    [self setLocationFromGeoPoint];
}

- (void)setLocationFromGeoPoint
{
    if (self.court != nil && ![self.court isEqualToString:@""]) {
        self.txtHomeCourt.text = self.court;
        [[PFUser currentUser] setObject:self.court forKey:@"homecourt"];
        [[PFUser currentUser] setObject:self.geopt forKey:@"geopoint"];
        [[PFUser currentUser] saveInBackground];
        return;
    }
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *l = [[CLLocation alloc] initWithLatitude:self.geopt.latitude longitude:self.geopt.longitude];
    [geocoder reverseGeocodeLocation:l completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"%@",error);
        CLPlacemark *place = [placemarks objectAtIndex:0];
        self.location = place.addressDictionary;
        
        [self setLocationForLabel];
    }];
}

- (void)setLocationForLabel {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *string = @"";
        if([self.location objectForKey:@"Street"]){
            string = [NSString stringWithFormat:@"%@, ",self.location[@"Street"]];
        }
        if([self.location objectForKey:@"City"]){
            string = [NSString stringWithFormat:@"%@%@",string, self.location[@"City"]];
        }
        if([self.location objectForKey:@"State"]){
            if([self.location objectForKey:@"City"]){
                string = [NSString stringWithFormat:@"%@, ",string];
            }
            string = [NSString stringWithFormat:@"%@%@",string, self.location[@"State"]];
        }
        if (self.court != nil && ![self.court isEqualToString:@""]) {
            self.txtHomeCourt.text = self.court;
        }else if(![self.location objectForKey:@"City"]) {
            self.txtHomeCourt.text = @"";
        } else if ([self.location objectForKey:@"Name"] != nil && ![[self.location objectForKey:@"Name"] isEqualToString:@""]) {
            self.txtHomeCourt.text = self.location[@"Name"];
        }else {
            self.txtHomeCourt.text = string;
        }
        [[PFUser currentUser] setObject:self.txtHomeCourt.text forKey:@"homecourt"];
        [[PFUser currentUser] setObject:self.geopt forKey:@"geopoint"];
        [[PFUser currentUser] saveInBackground];
    });
}


- (IBAction)snap:(id)sender {
    NSLog(@"Photo selected");
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = NO;
    // If image is available
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)album:(id)sender {
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = NO;
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

# pragma mark - Alert delegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSLog(@"OK Tapped. Hello World!");
        [PFUser requestPasswordResetForEmailInBackground:[PFUser currentUser].email];
    }
}

#pragma mark - Image picker controller delegate

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        // A Photo was taken/selected!
        // Resize image
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];;
        [self dismissViewControllerAnimated:YES completion:nil];
        UIGraphicsBeginImageContext(CGSizeMake(image.size.width/2.0f,image.size.height/2.0f));
        [image drawInRect: CGRectMake(0, 0, image.size.width/2.0f,image.size.height/2.0f)];
        UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        // Upload image
        //NSData *imageData = UIImageJPEGRepresentation(smallImage, 0.05f);
        self.imgPhoto.image = smallImage;
        PFUser *user = [PFUser currentUser];
        NSData *imageData = UIImagePNGRepresentation(self.imgPhoto.image);
        PFFile *file = [PFFile fileWithName:@"image.png" data:imageData];
        [user setObject:file forKey:@"image"];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.margin = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [[PFUser currentUser] fetchInBackground];
        }];
    }
}

-(void)viewDidLayoutSubviews
{
    if (self.viewCommon.frame.size.height > 0) {
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.viewCommon.frame.origin.y + self.viewCommon.frame.size.height);
    }else if (self.segType.selectedSegmentIndex == 0) {
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.viewAbout.frame.origin.y + self.viewAbout.frame.size.height);
    }else if (self.segType.selectedSegmentIndex == 1) {
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.tblSingles.frame.origin.y + self.tblSingles.frame.size.height);
    }else if (self.segType.selectedSegmentIndex == 2) {
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.tblDoubles.frame.origin.y +  + self.tblDoubles.frame.size.height);
    }
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([tableView isEqual:self.tblDoubles]) {
        return doubleMatches.count;
    }
    return singleMatches.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0f;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tblDoubles]) {
        if (section == doubleMatches.count - 1) {
            return 0.01f;
        }
        if ([[doubleExpanded objectAtIndex:section] boolValue]) {
            return 10.0f;
        }
        return 0.01f;
    }
    if (section == singleMatches.count - 1) {
        return 0.01f;
    }
    if ([[singleExpanded objectAtIndex:section] boolValue]) {
        return 10.0f;
    }
    return 0.01f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ScoreModel *match = nil;
    BOOL expanded = NO;
    if ([tableView isEqual:self.tblDoubles]) {
        match = (ScoreModel *)[doubleMatches objectAtIndex:section];
        expanded = [[doubleExpanded objectAtIndex:section] boolValue];
    }else {
        match = (ScoreModel *)[singleMatches objectAtIndex:section];
        expanded = [[singleExpanded objectAtIndex:section] boolValue];
    }
    if (expanded) {
        if (match.approved) {
            return 3;
        }
        return 2;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScoreModel *match = nil;
    BOOL expanded = NO;
    if ([tableView isEqual:self.tblDoubles]) {
        match = (ScoreModel *)[doubleMatches objectAtIndex:indexPath.section];
        expanded = [[doubleExpanded objectAtIndex:indexPath.section] boolValue];
    }else {
        match = (ScoreModel *)[singleMatches objectAtIndex:indexPath.section];
        expanded = [[singleExpanded objectAtIndex:indexPath.section] boolValue];
    }
    if ([tableView isEqual:self.tblDoubles]) {
        if (indexPath.row == 0) {
            return 30.0f;
        }
        if (match.approved) {
            return 39.0f;
        }
        return 78.0f;
    }
    if (match.approved || indexPath.row == 0) {
        return 30.0f;
    }
    return 60.0f;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ScoreModel *match = nil;
    BOOL expanded = NO;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM.dd.yy"];
    if ([tableView isEqual:self.tblDoubles]) {
        match = (ScoreModel *)[doubleMatches objectAtIndex:indexPath.section];
        expanded = [[doubleExpanded objectAtIndex:indexPath.section] boolValue];
    }else {
        match = (ScoreModel *)[singleMatches objectAtIndex:indexPath.section];
        expanded = [[singleExpanded objectAtIndex:indexPath.section] boolValue];
    }
    if (indexPath.row == 0) {
        UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
        UIImageView *imgCup = (UIImageView *)[cell viewWithTag:10];
        BOOL bWin = NO;
        for (int i = 0; i < match.winner.count; i++) {
            PFUser *winner = (PFUser *)[match.winner objectAtIndex:i];
            if ([winner.objectId isEqualToString:[PFUser currentUser].objectId]) {
                bWin = YES;
                break;
            }
        }
        if (match.approved) {
            if (bWin) {
                imgCup.hidden = NO;
                imgCup.image = [UIImage imageNamed:@"cup_off"];
            }else {
                imgCup.hidden = NO;
                imgCup.image = [UIImage imageNamed:@"cup_mid"];
            }
        }else {
            imgCup.hidden = NO;
            if (expanded) {
                imgCup.image = [UIImage imageNamed:@"note_icon"];
            }else {
                imgCup.image = [UIImage imageNamed:@"note_icon_off"];
            }
        }
        
        UILabel *lblTeamMate = (UILabel *)[cell viewWithTag:11];
        lblTeamMate.text = @"";
        PFUser *mate = nil;
        if ([tableView isEqual:self.tblDoubles]) {
            for (int i = 0; i < match.winner.count; i++) {
                PFUser *winner = (PFUser *)[match.winner objectAtIndex:i];
                if ([winner.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    if (i == 0) {
                        mate = (PFUser *)[match.winner objectAtIndex:1];
                    }else {
                        mate = (PFUser *)[match.winner objectAtIndex:0];
                    }
                }
            }
            for (int i = 0; i < match.loser.count; i++) {
                PFUser *winner = (PFUser *)[match.loser objectAtIndex:i];
                if ([winner.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    if (i == 0) {
                        mate = (PFUser *)[match.loser objectAtIndex:1];
                    }else {
                        mate = (PFUser *)[match.loser objectAtIndex:0];
                    }
                }
            }
        }else {
            for (int i = 0; i < match.players.count; i++) {
                PFUser *winner = (PFUser *)[match.players objectAtIndex:i];
                if ([winner.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    if (i == 0) {
                        mate = (PFUser *)[match.players objectAtIndex:1];
                    }else {
                        mate = (PFUser *)[match.players objectAtIndex:0];
                    }
                }
            }
        }
        if (mate != nil) {
            [mate fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                NSString *first = [mate objectForKey:@"first"];
                NSString *last = [mate objectForKey:@"last"];
                
                if(!first) {
                    NSArray *name = [mate[@"name"] componentsSeparatedByString:@" "];
                    first = [name firstObject];
                    if(!last) {
                        if(name.count > 1) {
                            last = [name lastObject];
                        }
                    }
                }
                NSString *mateName = first;
                if(last && last.length > 0) {
                    mateName = [NSString stringWithFormat:@"%@ %@.",mateName, [last substringToIndex:1]];
                }
                NSString *mateStr = nil;
                if ([tableView isEqual:self.tblDoubles]) {
                    mateStr = [NSString stringWithFormat:@"WITH  %@",mateName];
                }else {
                    mateStr = [NSString stringWithFormat:@"VS.  %@",mateName];
                }
                NSMutableAttributedString *attrName = [[NSMutableAttributedString alloc] initWithString:mateStr];
                [attrName addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:119 / 255.0f green:121 / 255.0f blue:143 / 255.0f alpha:1.0f],NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-BoldCond" size:15.0f]} range:NSMakeRange(0, 4)];
                [attrName addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:52 / 255.0f green:53 / 255.0f blue:63 / 255.0f alpha:1.0f],NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:15.0f]} range:NSMakeRange(4, mateStr.length - 4)];
                lblTeamMate.attributedText = attrName;
            }];
            
        }
        
        UILabel *lblDate = (UILabel *)[cell viewWithTag:12];
        if (match.createdAt != nil) {
            lblDate.text = [formatter stringFromDate:match.createdAt];
        }
        UIImageView *imgArrow = (UIImageView *)[cell viewWithTag:13];
        if (expanded) {
            lblTeamMate.hidden = YES;
            imgArrow.image = [UIImage imageNamed:@"arrow_up"];
            cell.backgroundColor = [UIColor colorWithRed:119 / 255.0f green:121 / 255.0f blue:143 / 255.0f alpha:1.0f];
        }else {
            lblTeamMate.hidden = NO;
            imgArrow.image = [UIImage imageNamed:@"arrow_down"];
            cell.backgroundColor = [UIColor whiteColor];
        }
        cell.layer.borderColor = [[UIColor colorWithRed:200 / 255.0f green:202 / 255.0f blue:218 / 255.0f alpha:1.0f] CGColor];
        cell.layer.borderWidth = 1.0f;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onMatch:)];
        [tap setCancelsTouchesInView:YES];
        [cell addGestureRecognizer:tap];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else {
        if (match.approved) {
            UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];
            UIImageView *imgCup = (UIImageView *)[cell viewWithTag:11];
            if (indexPath.row == 1) {
                imgCup.hidden = NO;
            }else {
                imgCup.hidden = YES;
            }
            UILabel *lblName = (UILabel *)[cell viewWithTag:12];
            lblName.text = @"";
            NSMutableArray *players = nil;
            if (indexPath.row == 1) {
                players = match.winner;
            }else {
                players = match.loser;
            }
            [PFUser fetchAllIfNeededInBackground:players block:^(NSArray *objects, NSError *error) {
                if (error) {
                    return;
                }
                NSString *mateName = @"";
                for (int i = 0; i < objects.count; i++) {
                    PFUser *player = (PFUser *)[objects objectAtIndex:i];
                    [player fetchIfNeeded];
                    if (![mateName isEqualToString:@""]) {
                        mateName = [NSString stringWithFormat:@"%@\n",mateName];
                    }
                    NSString *tempMateName = @"";
                    NSString *first = [player objectForKey:@"first"];
                    NSString *last = [player objectForKey:@"last"];
                    
                    if(!first) {
                        NSArray *name = [player[@"name"] componentsSeparatedByString:@" "];
                        first = [name firstObject];
                        if(!last) {
                            if(name.count > 1) {
                                last = [name lastObject];
                            }
                        }
                    }
                    tempMateName = first;
                    if(last && last.length > 0) {
                        tempMateName = [NSString stringWithFormat:@"%@ %@.",tempMateName, [last substringToIndex:1]];
                    }
                    mateName = [NSString stringWithFormat:@"%@%@",mateName,tempMateName];
                }
                lblName.text = mateName;
            }];
            
            if ([tableView isEqual:self.tblDoubles]) {
                lblName.numberOfLines = 2;
            }else {
                lblName.numberOfLines = 1;
            }
            
            NSMutableDictionary *dicScore = nil;
            NSMutableDictionary *dicOtherScore = nil;
            
            NSMutableArray *firstTeamIDs = [[NSMutableArray alloc] init];
            for (int i = 0; i < match.firstTeam.count; i++) {
                PFUser *firstPlayer = (PFUser *)[match.firstTeam objectAtIndex:i];
                if (firstPlayer.objectId != nil) {
                    [firstTeamIDs addObject:firstPlayer.objectId];
                }
            }
            int i = 0;
            for (i = 0; i < match.winner.count; i++) {
                PFUser *winner = (PFUser *)[match.winner objectAtIndex:i];
                if ([firstTeamIDs containsObject:winner.objectId]) {
                    break;
                }
            }
            if (i < match.winner.count) {
                if (indexPath.row == 1) {
                    dicScore = match.firstScore;
                    dicOtherScore = match.secondScore;
                }else {
                    dicScore = match.secondScore;
                    dicOtherScore = match.firstScore;
                }
            }else {
                if (indexPath.row == 1) {
                    dicScore = match.secondScore;
                    dicOtherScore = match.firstScore;
                }else {
                    dicScore = match.firstScore;
                    dicOtherScore = match.secondScore;
                }
            }
            for (int i = 0; i < 5; i++) {
                UILabel *lblScore = (UILabel *)[cell viewWithTag:i + 1];
                if ([dicScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] == nil) {
                    lblScore.text = @"";
                    continue;
                }
                int thisScore = [[dicScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                int otherScore = -1;
                if ([dicOtherScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] != nil) {
                    otherScore = [[dicOtherScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                }
                lblScore.text = [NSString stringWithFormat:@"%d",thisScore];
                if (thisScore > otherScore) {
                    lblScore.textColor = [UIColor colorWithRed:52 / 255.0f green:53 / 255.0f blue:63 / 255.0f alpha:1.0f];
                }else {
                    lblScore.textColor = [UIColor colorWithRed:152 / 255.0f green:157 / 255.0f blue:187 / 255.0f alpha:1.0f];
                }
            }
            
            cell.backgroundColor = [UIColor whiteColor];
            cell.layer.borderColor = [[UIColor colorWithRed:200 / 255.0f green:202 / 255.0f blue:218 / 255.0f alpha:1.0f] CGColor];
            cell.layer.borderWidth = 1.0f;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else {
            UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AwaitingCell" forIndexPath:indexPath];
            UILabel *lblName1 = (UILabel *)[cell viewWithTag:12];
            lblName1.text = @"";
            [PFUser fetchAllIfNeededInBackground:match.firstTeam block:^(NSArray *objects, NSError *error) {
                if (error) {
                    return;
                }
                NSString *mateName = @"";
                for (int i = 0; i < objects.count; i++) {
                    PFUser *player = (PFUser *)[objects objectAtIndex:i];
                    [player fetchIfNeeded];
                    if (![mateName isEqualToString:@""]) {
                        mateName = [NSString stringWithFormat:@"%@\n",mateName];
                    }
                    NSString *tempMateName = @"";
                    NSString *first = [player objectForKey:@"first"];
                    NSString *last = [player objectForKey:@"last"];
                    
                    if(!first) {
                        NSArray *name = [player[@"name"] componentsSeparatedByString:@" "];
                        first = [name firstObject];
                        if(!last) {
                            if(name.count > 1) {
                                last = [name lastObject];
                            }
                        }
                    }
                    tempMateName = first;
                    if(last && last.length > 0) {
                        tempMateName = [NSString stringWithFormat:@"%@ %@.",tempMateName, [last substringToIndex:1]];
                    }
                    mateName = [NSString stringWithFormat:@"%@%@",mateName,tempMateName];
                }
                lblName1.text = mateName;
            }];
            UILabel *lblName2 = (UILabel *)[cell viewWithTag:13];
            lblName2.text = @"";
            [PFUser fetchAllIfNeededInBackground:match.secondTeam block:^(NSArray *objects, NSError *error) {
                if (error) {
                    return;
                }
                NSString *mateName = @"";
                for (int i = 0; i < objects.count; i++) {
                    PFUser *player = (PFUser *)[objects objectAtIndex:i];
                    [player fetchIfNeeded];
                    if (![mateName isEqualToString:@""]) {
                        mateName = [NSString stringWithFormat:@"%@\n",mateName];
                    }
                    NSString *tempMateName = @"";
                    NSString *first = [player objectForKey:@"first"];
                    NSString *last = [player objectForKey:@"last"];
                    
                    if(!first) {
                        NSArray *name = [player[@"name"] componentsSeparatedByString:@" "];
                        first = [name firstObject];
                        if(!last) {
                            if(name.count > 1) {
                                last = [name lastObject];
                            }
                        }
                    }
                    tempMateName = first;
                    if(last && last.length > 0) {
                        tempMateName = [NSString stringWithFormat:@"%@ %@.",tempMateName, [last substringToIndex:1]];
                    }
                    mateName = [NSString stringWithFormat:@"%@%@",mateName,tempMateName];
                }
                lblName2.text = mateName;
            }];
            
            if ([tableView isEqual:self.tblDoubles]) {
                lblName1.numberOfLines = 2;
                lblName2.numberOfLines = 2;
            }else {
                lblName1.numberOfLines = 1;
                lblName2.numberOfLines = 1;
            }
            
            NSMutableDictionary *dicScore = nil;
            NSMutableDictionary *dicOtherScore = nil;
            NSMutableArray *firstTeamIDs = [[NSMutableArray alloc] init];
            for (int i = 0; i < match.firstTeam.count; i++) {
                PFUser *firstPlayer = (PFUser *)[match.firstTeam objectAtIndex:i];
                if (firstPlayer.objectId != nil) {
                    [firstTeamIDs addObject:firstPlayer.objectId];
                }
            }
            int i = 0;
            for (i = 0; i < match.winner.count; i++) {
                PFUser *winner = (PFUser *)[match.winner objectAtIndex:i];
                if ([firstTeamIDs containsObject:winner.objectId]) {
                    break;
                }
            }
            if (i < match.winner.count) {
                if (indexPath.row == 1) {
                    dicScore = match.firstScore;
                    dicOtherScore = match.secondScore;
                }else {
                    dicScore = match.secondScore;
                    dicOtherScore = match.firstScore;
                }
            }else {
                if (indexPath.row == 1) {
                    dicScore = match.secondScore;
                    dicOtherScore = match.firstScore;
                }else {
                    dicScore = match.firstScore;
                    dicOtherScore = match.secondScore;
                }
            }
            for (int i = 0; i < 10; i++) {
                UILabel *lblScore = (UILabel *)[cell viewWithTag:i + 1];
                if (i < 5) {
                    if ([dicScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] == nil) {
                        lblScore.text = @"";
                        continue;
                    }
                    int thisScore = [[dicScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                    int otherScore = -1;
                    if ([dicOtherScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] != nil) {
                        otherScore = [[dicOtherScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                    }
                    lblScore.text = [NSString stringWithFormat:@"%d",thisScore];
                    if (thisScore > otherScore) {
                        lblScore.textColor = [UIColor colorWithRed:52 / 255.0f green:53 / 255.0f blue:63 / 255.0f alpha:1.0f];
                    }else {
                        lblScore.textColor = [UIColor colorWithRed:152 / 255.0f green:157 / 255.0f blue:187 / 255.0f alpha:1.0f];
                    }
                }else {
                    if ([dicOtherScore objectForKey:[NSString stringWithFormat:@"%d",i - 4]] == nil) {
                        lblScore.text = @"";
                        continue;
                    }
                    int thisScore = [[dicOtherScore objectForKey:[NSString stringWithFormat:@"%d",i - 4]] intValue];
                    int otherScore = -1;
                    if ([dicScore objectForKey:[NSString stringWithFormat:@"%d",i - 4]] != nil) {
                        otherScore = [[dicScore objectForKey:[NSString stringWithFormat:@"%d",i - 4]] intValue];
                    }
                    lblScore.text = [NSString stringWithFormat:@"%d",thisScore];
                    if (thisScore > otherScore) {
                        lblScore.textColor = [UIColor colorWithRed:52 / 255.0f green:53 / 255.0f blue:63 / 255.0f alpha:1.0f];
                    }else {
                        lblScore.textColor = [UIColor colorWithRed:152 / 255.0f green:157 / 255.0f blue:187 / 255.0f alpha:1.0f];
                    }
                }
            }
            
            cell.backgroundColor = [UIColor whiteColor];
            cell.layer.borderColor = [[UIColor colorWithRed:200 / 255.0f green:202 / 255.0f blue:218 / 255.0f alpha:1.0f] CGColor];
            cell.layer.borderWidth = 1.0f;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }
}

-(void)onMatch:(UITapGestureRecognizer *)gesture
{
    UITableViewCell *cell = (UITableViewCell *)gesture.view;
    NSIndexPath *indexPath = [self.tblDoubles indexPathForCell:cell];
    if (indexPath == nil) {
        indexPath = [self.tblSingles indexPathForCell:cell];
        [singleExpanded setObject:@(![[singleExpanded objectAtIndex:indexPath.section] boolValue]) atIndexedSubscript:indexPath.section];
        [self expandTableView];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
        [self.tblSingles reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }else {
        if (doubleExpanded != nil) {
            [doubleExpanded setObject:@(![[doubleExpanded objectAtIndex:indexPath.section] boolValue]) atIndexedSubscript:indexPath.section];
        }
        [self expandTableView];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
        [self.tblDoubles reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10.0f)];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)expandTableView
{
    if (self.segType.selectedSegmentIndex == 0) {
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.viewAbout.frame.origin.y + self.viewAbout.frame.size.height);
    }else if (self.segType.selectedSegmentIndex == 1) {
        CGFloat singleHeight = 10;
        for (int i = 0; i < singleMatches.count; i++) {
            singleHeight += 10.0f;
            if ([[singleExpanded objectAtIndex:i] boolValue]) {
                singleHeight += 10.0f;
                singleHeight += 30.0f * 3;
            }else {
                singleHeight += 30.0f;
            }
        }
        CGRect frame = self.tblSingles.frame;
        frame.size.height = singleHeight;
        self.tblSingles.translatesAutoresizingMaskIntoConstraints = YES;
        self.tblSingles.frame = frame;
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.tblSingles.frame.origin.y + self.tblSingles.frame.size.height);
        [self.view layoutIfNeeded];
    }else if (self.segType.selectedSegmentIndex == 2) {
        CGFloat doubleHeight = 10;
        for (int i = 0; i < doubleMatches.count; i++) {
            doubleHeight += 10.0f;
            if ([[doubleExpanded objectAtIndex:i] boolValue]) {
                doubleHeight += 10.0f;
                doubleHeight += 30.0f + 39.0f * 2;
            }else {
                doubleHeight += 30.0f;
            }
        }
        CGRect frame = self.tblDoubles.frame;
        frame.size.height = doubleHeight;
        self.tblDoubles.translatesAutoresizingMaskIntoConstraints = YES;
        self.tblDoubles.frame = frame;
        self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, self.tblDoubles.frame.origin.y +  + self.tblDoubles.frame.size.height);
        [self.view layoutIfNeeded];
    }
}

@end
