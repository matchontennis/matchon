//
//  FeedModel.h
//  MatchOn
//
//  Created by Kevin Flynn on 4/26/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>
#import <Foundation/Foundation.h>

@interface FeedModel : PFObject <PFSubclassing>


@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *button;
@property (nonatomic, strong) NSString *segue;
@property (nonatomic, assign) int number;
@property (nonatomic, assign) BOOL active;
@property (nonatomic, strong) PFFile *image;


@end
