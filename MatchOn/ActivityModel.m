//
//  ActivityModel.m
//  Page
//
//  Created by Kevin Flynn on 11/16/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "ActivityModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation ActivityModel

@dynamic type;
@dynamic note;
@dynamic user;
@dynamic startTime;
@dynamic geopoint;
@dynamic location;
@dynamic doubles;
@dynamic sport;
@dynamic recipients;
//@dynamic totalRecipients;
@dynamic messageRecipients;

@dynamic spots;

/*@dynamic singlesRatings;
@dynamic doublesRatings;
@dynamic singlesMap;
@dynamic doublesMap;*/

@dynamic ratings;
@dynamic ratingMap;

/*@dynamic singlesAverage;
@dynamic doublesAverage;
@dynamic singlesHigh;
@dynamic singlesLow;
@dynamic singlesMin;
@dynamic singlesMax;
@dynamic doublesHigh;
@dynamic doublesLow;
@dynamic doublesMin;
@dynamic doublesMax;*/


//@dynamic ratingsAverage;
@dynamic ratingsMin;
@dynamic ratingsMax;
//@dynamic ratingsHigh;
//@dynamic ratingsLow;
@dynamic declines;
@dynamic court;


@dynamic full;
@dynamic active;
@dynamic private;

//@dynamic played;

@dynamic scorer;
//@dynamic scorers;
//@dynamic reviewer;
//@dynamic reviewers;

@dynamic winner;
@dynamic loser;
@dynamic firstTeam;
@dynamic secondTeam;


//@dynamic matchOn;
@dynamic confirmed;
@dynamic score;
@dynamic approved;
//@dynamic parent;

// Invite properties
@dynamic invitations;

+ (void)load {
    NSLog(@"Load");
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Activity";
}

- (void)setRatingValues {
    //[self setSinglesRatings];
    //[self setDoublesRatings];
    self.ratings = [[NSMutableArray alloc] initWithArray:[self.ratingMap allValues]];
    double sum = 0;
    int max = -10000;
    int min = 10000;
    for (NSNumber * n in self.ratings) {
        sum += [n doubleValue];
        if([n intValue]>max) {
            max = [n intValue];
        }
        if([n intValue]< min) {
            min = [n intValue];
        }
    }
    self.ratingsMin = min;
    self.ratingsMax = max;
    
    //self.ratingsAverage = sum/(double)self.ratings.count;
    //self.ratingsHigh = self.ratingsAverage;
    //self.ratingsLow = self.ratingsAverage;
}

- (void)setSinglesRatings{
    /*self.singlesRatings = [[NSMutableArray alloc] initWithArray:[self.singlesMap allValues]];
    double sum = 0;
    int max = -10000;
    int min = 10000;
    for (NSNumber * n in self.singlesRatings) {
        sum += [n doubleValue];
        if([n intValue]>max) {
            max = [n intValue];
        }
        if([n intValue]< min) {
            min = [n intValue];
        }
    }
    self.singlesMin = min;
    self.singlesMax = max;
    
    self.singlesAverage = sum/(double)self.singlesRatings.count;
    self.singlesHigh = self.singlesAverage;
    self.singlesLow = self.singlesAverage;*/
}

- (void)setDoublesRatings{
    /*self.doublesRatings = [[NSMutableArray alloc] initWithArray:[self.doublesMap allValues]];
    double sum = 0;
    int max = -10000;
    int min = 10000;
    for (NSNumber * n in self.doublesRatings) {
        sum += [n doubleValue];
        if([n intValue]>max) {
            max = [n intValue];
        }
        if([n intValue]< min) {
            min = [n intValue];
        }
    }
    self.doublesMin = min;
    self.doublesMax = max;
    
    self.doublesAverage = sum/(double)self.doublesRatings.count;
    self.doublesHigh = self.doublesAverage;
    self.doublesLow = self.doublesAverage;*/
}




@end
