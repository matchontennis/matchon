//
//  OtherProfileViewController.m
//  MatchOn
//
//  Created by Sol on 5/1/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "OtherProfileViewController.h"
#import "RatingModel.h"
#import "ScoreModel.h"
#import "DescriptionViewController.h"

@interface OtherProfileViewController ()

@end

@implementation OtherProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"Profile";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    self.scrMain.translatesAutoresizingMaskIntoConstraints = YES;
    
    CGRect frame = self.scrMain.frame;
    frame.origin.y = 0;
    frame.size = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 64);
    
    self.scrMain.frame = frame;
    
    [self.curUser fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        PFUser *currentUser = (PFUser *)object;
        NSString *first = [currentUser objectForKey:@"first"];
        NSString *last = [currentUser objectForKey:@"last"];
        
        if(!first) {
            NSArray *name = [currentUser[@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        self.lblName.text = first;
        if(last && last.length > 0) {
            self.lblName.text = [NSString stringWithFormat:@"%@ %@.",self.lblName.text, [last substringToIndex:1]];
        }
        
        if([currentUser[@"female"] boolValue]) {
            self.imgGender.image = [UIImage imageNamed:@"female_gender"];
            //self.imgPhoto.image = [UIImage imageNamed:@"female"];
        } else {
            self.imgGender.image = [UIImage imageNamed:@"male_gender"];
            //self.imgPhoto.image = [UIImage imageNamed:@"male"];
        }
        
        PFFile *file = [currentUser objectForKey:@"image"];
        self.imgPhoto.file = file;
        self.imgPhoto.image = [UIImage imageNamed:@"default_profile"];
        
        [self.imgPhoto loadInBackground];
        self.imgPhoto.layer.cornerRadius = self.imgPhoto.frame.size.height/2.0f;
        self.imgPhoto.clipsToBounds = YES;
        self.imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
        
        if (currentUser[@"homecourt"] && ![currentUser[@"homecourt"] isEqual:[NSNull null]]) {
            self.lblHomeCourt.text = currentUser[@"homecourt"];
        }else {
            self.lblHomeCourt.text = @"";
        }
        if (currentUser[@"aboutme"] && ![currentUser[@"aboutme"] isEqual:[NSNull null]]) {
            self.txtAboutMe.text = currentUser[@"aboutme"];
        }else {
            self.txtAboutMe.text = @"";
        }
        
        NSMutableArray *ratings = [[NSMutableArray alloc] init];
        [ratings addObject:[PFUser currentUser][@"rating"]];
        [ratings addObject:currentUser[@"rating"]];
        [RatingModel fetchAllInBackground:ratings block:^(NSArray *objects, NSError *error) {
            if (error) {
                NSLog(@"%@",error);
                return;
            }
            if (objects.count < 2) {
                return;
            }
            RatingModel *rate1 = (RatingModel *)objects[0];
            RatingModel *rate2 = (RatingModel *)objects[1];
            if (abs(rate1.rating - rate2.rating) <= 7) {
                self.imgProfileBg.image = [UIImage imageNamed:@"compatible"];
            }else if (abs(rate1.rating - rate2.rating) <= 10) {
                self.imgProfileBg.image = [UIImage imageNamed:@"borderline"];
            }else {
                self.imgProfileBg.image = [UIImage imageNamed:@"incompatible"];
            }
            self.lblAllPoints.text = [NSString stringWithFormat:@"%d", rate2.score];
        }];
        
        PFQuery *query = [PFQuery queryWithClassName:@"Score"];
        [query whereKey:@"players" equalTo:currentUser];
        [query whereKey:@"approved" equalTo:@YES];
        [query orderByDescending:@"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            self.lblLastPlayed.text = @"";
            if (error) {
                self.viewBalls.hidden = YES;
                self.imgVerified.hidden = YES;
                return;
            }
            self.lblPlayedMatches.text = [NSString stringWithFormat:@"%ld",(long)objects.count];
            int ball = 0;
            if (objects.count <= 3 && objects.count >= 1) {
                ball = 1;
            }
            NSMutableArray *opponents = [[NSMutableArray alloc] init];
            int matchesCompleted = 0;
            for (int i = 0; i < objects.count; i++) {
                ScoreModel *score = (ScoreModel *)[objects objectAtIndex:i];
                if (score.approved) {
                    matchesCompleted++;
                }
                if (i == 0) {
                    NSTimeInterval lastSeconds = [[NSDate date] timeIntervalSinceDate:score.createdAt];
                    if (lastSeconds < 60) {
                        if (lastSeconds == 1.0) {
                            self.lblLastPlayed.text = @"Last played 1 second ago";
                        }else {
                            self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d seconds ago",(int)lastSeconds];
                        }
                    }else if (lastSeconds < 3600) {
                        if ((int)(lastSeconds / 60) == 1) {
                            self.lblLastPlayed.text = @"Last played 1 minute ago";
                        }else {
                            self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d minutes ago",(int)(lastSeconds / 60)];
                        }
                    }else if (lastSeconds < 3600 * 24) {
                        if ((int)(lastSeconds / 3600) == 1) {
                            self.lblLastPlayed.text = @"Last played 1 hour ago";
                        }else {
                            self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d hours ago",(int)(lastSeconds / 3600)];
                        }
                    }else {
                        if ((int)(lastSeconds / (3600 * 24)) == 1) {
                            self.lblLastPlayed.text = @"Last played 1 day ago";
                        }else {
                            self.lblLastPlayed.text = [NSString stringWithFormat:@"Last played %d days ago",(int)(lastSeconds / (3600 * 24))];
                        }
                    }
                }
                for (int j = 0; j < score.players.count; j++) {
                    PFUser *player = (PFUser *)[score.players objectAtIndex:j];
                    if ([opponents containsObject:player.objectId]) {
                        continue;
                    }
                    if ([player.objectId isEqualToString:currentUser.objectId]) {
                        continue;
                    }
                    [opponents addObject:player.objectId];
                }
            }
            if (objects.count >= 4 && objects.count <= 8 && opponents.count >= 3) {
                ball = 2;
            }
            if (objects.count >= 9 && objects.count <= 12 && opponents.count >= 4) {
                ball = 3;
            }
            if (objects.count >= 12 && opponents.count >= 5) {
                ball = 4;
            }
            if (ball < 4) {
                self.viewBalls.hidden = NO;
                self.imgVerified.hidden = YES;
                for (int i = 0; i < 3; i++) {
                    UIImageView *imgSubBall = (UIImageView *)[self.viewBalls.subviews objectAtIndex:i];
                    if (i < ball) {
                        imgSubBall.image = [UIImage imageNamed:@"ball_on"];
                    }else {
                        imgSubBall.image = [UIImage imageNamed:@"ball_off"];
                    }
                }
            }else {
                self.viewBalls.hidden = YES;
                self.imgVerified.hidden = NO;
            }
            //self.lblCompleted.text = [NSString stringWithFormat:@"%d",matchesCompleted];
        }];
    }];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView)];
    [tap4 setCancelsTouchesInView:YES];
    [self.viewBalls addGestureRecognizer:tap4];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView)];
    [tap3 setCancelsTouchesInView:YES];
    [self.imgVerified addGestureRecognizer:tap3];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"description"]) {
        DescriptionViewController *descriptionViewController = (DescriptionViewController *)segue.destinationViewController;
        descriptionViewController.imgBg = [UIImage imageNamed:@"badge_desc"];
    }
}

-(void)viewDidLayoutSubviews
{
    self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 504);
}

- (IBAction)back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showDescView
{
    [self performSegueWithIdentifier:@"description" sender:nil];
}

@end
