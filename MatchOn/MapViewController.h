//
//  MapViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 3/9/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "ActivityModel.h"

@class ActivityAnnotation;

@interface MapViewController : UIViewController <MKMapViewDelegate> {
    ActivityAnnotation *annotation;
}

@property (nonatomic,strong) ActivityModel *activity;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)back:(id)sender;


@end
