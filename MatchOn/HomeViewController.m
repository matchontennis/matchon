//
//  HomeViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 10/23/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeViewCell.h"
#import "DateTools.h"
#import "MainViewController.h"
#import "MessageViewController.h"
#import "PlayViewController.h"
#import "InviteViewController.h"
#import "Constants.h"
#import "RatingModel.h"
#import "PointsModel.h"
#import "MBProgressHUD.h"
#import "MapViewController.h"
#import "StartPageViewController.h"
#import "WizardViewController.h"
#import "OtherProfileViewController.h"
#import "FeedModel.h"
#import "ScoreModel.h"
#import "DescriptionViewController.h"
#import "TeammateTableViewController.h"
#import "ScoreViewController.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface HomeViewController ()<CLLocationManagerDelegate> {
    BOOL showStart;
}

@property (nonatomic, strong) CLLocationManager *locationManager;


@end

@implementation HomeViewController


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutClear:) name:@"Logout" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshInbox) name:@"Match" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshFromPush:) name:MatchOnAppDelegateApplicationDidReceiveRemoteNotification object:nil];
    
    //self.tableView.rowHeight = 210.0f;
    
    // Config
    self.limit = 25;
    self.skip = 0;
    
    // Refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    //self.objects = [NSMutableArray array];
    //self.objects1 = [NSMutableArray array];
    self.defaults = [NSMutableArray array];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 1)];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    is_selected_score = NO;
    if(![PFUser currentUser]) {
        [self performSegueWithIdentifier:@"landing" sender:self];
        return;
    }
    
    PFUser *user = [PFUser currentUser];
    MainViewController *mainViewController = (MainViewController *)self.tabBarController;
    mainViewController.badge.hidden = NO;
    if(user) {
        if(![user objectForKey:@"first"] || ![user objectForKey:@"last"]) {
            NSArray *name = [user[@"name"] componentsSeparatedByString:@" "];
            user[@"first"] = [name firstObject];
            if(name.count > 1) {
                user[@"last"] = [name lastObject];
            }
            [user saveInBackground];
        }
        if (![user objectForKey:@"rating"]) {
            [self performSegueWithIdentifier:@"rate" sender:self];
            return;
        }
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"existing"] == nil) {
        [self performSegueWithIdentifier:@"help" sender:self];
    }
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = [UIColor whiteColor];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.view.backgroundColor = [UIColor whiteColor];
    if ([PFUser currentUser]) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"existing"] == nil) {
            return;
        }
        if (([PFUser currentUser].isNew && [[NSUserDefaults standardUserDefaults] objectForKey:[PFUser currentUser].username] == nil) || [[NSUserDefaults standardUserDefaults] boolForKey:[PFUser currentUser].username]) {
            return;
        }
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
            
            
            if (!error) {
                
                self.geopoint = geoPoint;
                if (self.prevViewController == nil) {
                    [self refreshInbox];
                }
            } else {
                if (self.prevViewController == nil) {
                    [self refreshInbox];
                }
            }
        }];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (![PFUser currentUser]) {
        return;
    }
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"existing"] == nil) {
        return;
    }
    
    
    if ([PFUser currentUser].isNew) {
        if ([[NSUserDefaults standardUserDefaults] objectForKey:[PFUser currentUser].username] == nil) {
            [self storeShowStart:YES];
        }
        [self validateShowStart];
    }
    else {
        PFInstallation *installation = [PFInstallation currentInstallation];
        installation[@"user"] = [PFUser currentUser];
        if(installation.badge>0) {
            installation.badge = 0;
        }
        if(installation.deviceToken) {
            [installation saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if(error){
                    NSLog(@"Error saving device installation: %@",error);
                }
            }];
        }
        [self validateShowStart];
    }
    self.prevViewController = nil;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)refreshFromPush:(NSNotification *)notification {
    
    //NSString *activityId = [notification.userInfo objectForKey:@"a"];
    //if(activityId) {
    //    self.selectedActivity = [ActivityModel objectWithoutDataWithObjectId:activityId];
    //    [self performSegueWithIdentifier:@"message" sender:self];
    //}
    
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc loadNewMessages];
    [self refreshInbox];
}


#pragma mark - Disappear methods

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}


#pragma mark - Data for table

- (void)refreshInbox{
    if(![PFUser currentUser]) {
        [self.objects1 removeAllObjects];
        [self.objects2 removeAllObjects];
        [self.objects3 removeAllObjects];
        [self.objects4 removeAllObjects];
        [self.tableView reloadData];
        return;
    }
    self.pullToRefreshEnabled = TRUE;
    self.skip = 0;
    [self loadObjects];
}

- (void)loadNextPage{
    self.skip += self.limit;
    if(!self.endOfResults) {
        [self loadObjects];
    }
}


- (void)loadObjects{
    PFQuery *queryHome = [self queryForTable];
    PFQuery *queryScore = [self queryForScoreTable];
    PFQuery *queryReview = [self queryForReviewTable];
    PFQuery *query =  [PFQuery orQueryWithSubqueries:@[queryHome,queryScore, queryReview]];
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }else if (self.objects1.count == 0 && self.objects2.count == 0 && self.objects3.count == 0 && self.objects4.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query orderByAscending:@"full"];
    [query orderByAscending:@"startTime"];
    
    [query includeKey:@"recipients"];
    [query includeKey:@"user"];
    [query includeKey:@"scorer"];
    [query includeKey:@"firstTeam"];
    [query includeKey:@"secondTeam"];
    [query includeKey:@"winner"];
    [query includeKey:@"loser"];
    //[query includeKey:@"reviewer"];
    
    if(self.pullToRefreshEnabled) {
        self.skip = 0;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        NSLog(@"Resuults");
        if(objects.count == 0) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [self.objects1 removeAllObjects];
            [self.objects2 removeAllObjects];
            [self.objects3 removeAllObjects];
            [self.objects4 removeAllObjects];
        }
        if(whichPass > 1) {
            [self.objects1 removeAllObjects];
            [self.objects2 removeAllObjects];
            [self.objects3 removeAllObjects];
            [self.objects4 removeAllObjects];
        }
        if (self.objects1 == nil && objects != nil) {
            self.objects1 = [NSMutableArray array];
        }
        if (self.objects2 == nil && objects != nil) {
            self.objects2 = [NSMutableArray array];
        }
        if (self.objects3 == nil && objects != nil) {
            self.objects3 = [NSMutableArray array];
        }
        if (self.objects4 == nil && objects != nil) {
            self.objects4 = [NSMutableArray array];
        }
        for (int i = 0; i < objects.count; i++) {
            ActivityModel *activity = (ActivityModel *)[objects objectAtIndex:i];
            if (activity.full && [activity.startTime compare:[NSDate date]] == NSOrderedAscending) {
                if (activity.score != nil && ![activity.score isEqual:[NSNull null]] && activity.scorer != nil && ![activity.scorer isEqual:[NSNull null]]) {
                    [self.objects3 addObject:[objects objectAtIndex:i]];
                }else {
                    [self.objects2 addObject:[objects objectAtIndex:i]];
                }
            }else {
                BOOL invitation = FALSE;
                if (activity.invitations) {
                    for(PFUser *user in activity.invitations) {
                        if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                            invitation = TRUE;
                        }
                    }
                }
                // Already joined the match!!!
                for(PFUser *user in activity.recipients) {
                    if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        invitation = FALSE;
                    }
                }
                if (invitation) {
                    [self.objects1 addObject:[objects objectAtIndex:i]];
                }else {
                    [self.objects4 addObject:[objects objectAtIndex:i]];
                }
            }
        }
        /*PFUser *currentUser = [PFUser currentUser];
        NSMutableArray *players = [currentUser objectForKey:@"players"];
        for (int i = 0; i < self.objects.count; i++) {
            ActivityModel *activity = (ActivityModel *)[self.objects objectAtIndex:i];
            if (!activity.matchOn) {
                continue;
            }
            if (![players containsObject:activity.user]) {
                [players addObject:activity.user];
            }
            if ([activity.startTime compare:[NSDate date]] == NSOrderedDescending) {
                for (int j = 0; j < activity.recipients.count; j++) {
                    PFUser *user = (PFUser *)[activity.recipients objectAtIndex:j];
                    if (![players containsObject:user]) {
                        [players addObject:user];
                    }
                }
            }
        }
        if (players != nil) {
            [currentUser setObject:players forKey:@"players"];
            [currentUser saveInBackground];
        }*/
        
        NSLog(@"%@",objects);
        [self queryDefaults];
        if(self.objects.count == 0 && self.objects1.count == 0 && self.objects2.count == 0 && self.objects3.count == 0) {
            if(!self.noResults) {
                self.noResults = TRUE;
            }
            
        } else {
            self.noResults = FALSE;
        }
        
        [self.tableView reloadData];
        
        self.pullToRefreshEnabled = FALSE;
        whichPass = whichPass + 1;
    }];
}

- (void)queryDefaults {
    if(![PFUser currentUser])return;
    PFQuery *query = [PFQuery queryWithClassName:@"Feed"];
    [query orderByAscending:@"order"];
    [query whereKey:@"active" equalTo:@YES];
    query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [self.defaults removeAllObjects];
        [self.defaults addObjectsFromArray:objects];
        [self.tableView reloadData];
    }];
}

#pragma mark - QueryTableViewController


- (PFQuery *)queryForTable {
    if(![PFUser currentUser])return nil;
    
    //PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    
    PFQuery *playQuery = [PFQuery queryWithClassName:@"Activity"];
    [playQuery whereKey:@"recipients" equalTo:[PFUser currentUser]];
    
    PFQuery *inviteQuery = [PFQuery queryWithClassName:@"Activity"];
    [inviteQuery whereKey:@"invitations" equalTo:[PFUser currentUser]];
    
    PFQuery *query = [PFQuery orQueryWithSubqueries:@[inviteQuery,playQuery]];
    
    /*if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }else if (self.objects.count == 0 && self.objects1.count == 0) {
        NSLog(@"Cached first");
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }*/
    
    //[query orderByAscending:@"matchOn"];
    //[query orderByAscending:@"startTime"];
    //[query addAscendingOrder:@"startTime"];
    
    //[query includeKey:@"recipients"];
    //[query includeKey:@"user"];
    [query whereKey:@"sport" equalTo:@1];
    [query whereKey:@"type" equalTo:@"play"];
    [query whereKey:@"startTime" greaterThanOrEqualTo:[NSDate date]];
    [query whereKey:@"active" equalTo:@YES];
    return query;
}

- (PFQuery *)queryForScoreTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    
    //[query orderByDescending:@"startTime"];
    [query whereKey:@"startTime" lessThan:[NSDate date]];
    //[query includeKey:@"recipients"];
    //[query includeKey:@"user"];
    //[query includeKey:@"score"];
    //[query includeKey:@"scorer"];
    //[query includeKey:@"firstTeam"];
    //[query includeKey:@"secondTeam"];
    //[query includeKey:@"winner"];
    //[query includeKey:@"loser"];
    //[query includeKey:@"reviewer"];
    [query whereKey:@"approved" notEqualTo:@YES];
    [query whereKeyDoesNotExist:@"scorer"];
    [query whereKey:@"recipients" equalTo:[PFUser currentUser]];
    [query whereKey:@"full" equalTo:@YES];
    [query whereKey:@"active" equalTo:@YES];
    
    return query;
}

- (PFQuery *)queryForReviewTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    /*if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }else if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }*/
    //[query orderByDescending:@"startTime"];
    [query whereKey:@"startTime" lessThan:[NSDate date]];
    //[query includeKey:@"recipients"];
    //[query includeKey:@"user"];
    //[query includeKey:@"score"];
    //[query includeKey:@"scorer"];
    //[query includeKey:@"firstTeam"];
    //[query includeKey:@"secondTeam"];
    //[query includeKey:@"winner"];
    //[query includeKey:@"loser"];
    //[query includeKey:@"reviewer"];
    [query whereKey:@"approved" notEqualTo:@YES];
    [query whereKeyExists:@"scorer"];
    [query whereKey:@"scorer" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"recipients" equalTo:[PFUser currentUser]];
    [query whereKey:@"full" equalTo:@YES];
    [query whereKey:@"active" equalTo:@YES];
    
    return query;
}

#pragma mark - Object at index


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        tableView.bounces = NO;
        return 1;
    }
    tableView.bounces = YES;
    int numSections = 1;
    if (self.objects1.count > 0) {
        numSections++;
    }
    if (self.objects2.count > 0) {
        numSections++;
    }
    if (self.objects3.count > 0) {
        numSections++;
    }
    if (self.objects4.count > 0) {
        numSections++;
    }
    if (self.defaults.count > 0) {
        numSections++;
    }
    return numSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        return 1;
    }
    if (section == 0) {
        return 0;
    }
    int numSections = 1;
    if (self.objects1.count > 0) {
        numSections++;
    }
    if (self.objects2.count > 0) {
        numSections++;
    }
    if (self.objects3.count > 0) {
        numSections++;
    }
    if (self.objects4.count > 0) {
        numSections++;
    }
    if (section == numSections) {
        return self.defaults.count;
    }else if (section == 1 && self.objects1.count > 0) {
        return self.objects1.count;
    }else if (((section == 1 && self.objects1.count == 0) || (section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) {
        return self.objects2.count;
    }else if (((section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0) {
        return self.objects3.count;
    }
    return self.objects4.count;
}

- (ActivityModel *)objectForRowAtIndexPath:(NSIndexPath *)indexPath invite:(BOOL)invite
{
    ActivityModel *object;
    if (invite) {
        object = [self.objects1 objectAtIndex:indexPath.row];
    }else {
        object = [self.objects objectAtIndex:indexPath.row];
    }
    return object;
}


#pragma mark - UITableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        return 0.01f;
    }
    if (section == 0) {
        return 85.0f;
    }
    int numSections = 1;
    if (self.objects1.count > 0) {
        numSections++;
    }
    if (self.objects2.count > 0) {
        numSections++;
    }
    if (self.objects3.count > 0) {
        numSections++;
    }
    if (self.objects4.count > 0) {
        numSections++;
    }
    if (section == numSections) {
        if (self.objects1.count > 0 || self.objects4.count > 0) {
            return 0.01f;
        }else {
            return 50.0f;
        }
    }else if (section == 1 && self.objects1.count > 0) {
        return 40.0f;
    }else if (((section == 1 && self.objects1.count == 0) || (section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) {
        return 0.01f;
    }else if (((section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0) {
        return 0.01f;
    }
    return 0.01f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        return tableView.frame.size.height;
    }
    if (indexPath.section == 0) {
        return 0.01f;
    }
    int numSections = 1;
    if (self.objects1.count > 0) {
        numSections++;
    }
    if (self.objects2.count > 0) {
        numSections++;
    }
    if (self.objects3.count > 0) {
        numSections++;
    }
    if (self.objects4.count > 0) {
        numSections++;
    }
    if (indexPath.section == numSections) {
        return 100.0f;
    }else if (indexPath.section == 1 && self.objects1.count > 0) {
        return 200.0f;
    }else if (((indexPath.section == 1 && self.objects1.count == 0) || (indexPath.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) {
        return 42.0f;
    }else if (((indexPath.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (indexPath.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (indexPath.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0) {
        return 42.0f;
    }
    return 200.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if(![PFUser currentUser]) return nil;
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        return nil;
    }
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 40.0f)];
    if(section == 0) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 85.0f)];
        view.backgroundColor = [UIColor colorWithRed:100.0f/255.0f green:101.0f/255.0f blue:125.0f/255.0f alpha:1.0f];
        PFImageView *imageView = [[PFImageView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 60.0f, 60.0f)];
        PFFile *file = [PFUser currentUser][@"image"];
        [imageView setFile:file];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        /*if([PFUser currentUser] && [[PFUser currentUser][@"female"] boolValue]) {
            imageView.image = [UIImage imageNamed:@"female"];
        } else {
            imageView.image = [UIImage imageNamed:@"male"];
        }*/
        imageView.image = [UIImage imageNamed:@"default_profile"];
        [imageView loadInBackground];
        imageView.layer.cornerRadius = 30.0f;
        imageView.clipsToBounds = YES;
        imageView.layer.borderWidth = 2.0f;
        imageView.layer.borderColor = THEME_COLOR.CGColor;
        [view addSubview:imageView];
        
        CGRect frame = view.frame;
        frame.size = CGSizeMake(110.0f, 28.0f);
        frame.origin= CGPointMake(85.0f, 10.0f);
        
        UILabel *label = [[UILabel alloc] initWithFrame:frame];
        label.text = @"My Scoreboard";//[NSString stringWithFormat:@"%@'s \rScoreboard",[PFUser currentUser][@"first"]];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"MyriadPro-Cond" size:24.0f];
        label.numberOfLines = 1;
        
        // Add a rightBorder.
        CALayer *scoreBorder = [CALayer layer];
        scoreBorder.frame = CGRectMake(0, label.frame.size.height - 1.0f, label.frame.size.width + 23.0f, 1.0f);
        scoreBorder.backgroundColor = [UIColor colorWithRed:145 / 255.0f green:147 / 255.0f blue:165 / 255.0f alpha:1.0f].CGColor;
        [label.layer addSublayer:scoreBorder];
        [view addSubview:label];
        
        CGRect bf = view.frame;
        bf.size = CGSizeMake(141.0f, 72.0f);
        bf.origin= CGPointMake(75.0f, 10.0f);
        UIButton *btnHelp = [[UIButton alloc] initWithFrame:bf];
        [btnHelp addTarget:self action:@selector(showHelp:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:btnHelp];
        
        /*CGRect f = view.frame;
        f.size = CGSizeMake(138.0f, 30.0f);
        f.origin= CGPointMake(182.0f, 3.0f);
        UILabel *l = [[UILabel alloc] initWithFrame:f];
        l.textColor = THEME_COLOR;
        l.text = @"POINTS EARNED";
        l.textAlignment = NSTextAlignmentCenter;
        l.font = [UIFont boldSystemFontOfSize:13.0f];
        [view addSubview:l];*/
        
        if(!self.total)self.total = 0;
        CGRect fTotal = view.frame;
        fTotal.size = CGSizeMake(15.0f, 30.0f);
        fTotal.origin= CGPointMake(85.0f, 40.0f);
        UILabel *lTotal = [[UILabel alloc] initWithFrame:fTotal];
        lTotal.textColor = [UIColor whiteColor];
        lTotal.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:30.0f];
        lTotal.numberOfLines = 1;
        lTotal.textAlignment = NSTextAlignmentLeft;
        //lTotal.text = [@(self.total) stringValue];
        [view addSubview:lTotal];
        CGRect fSTotal = view.frame;
        fSTotal.size = CGSizeMake(40.0f, 35.0f);
        fSTotal.origin= CGPointMake(100.0f, 37.0f);
        UILabel *lSTotal = [[UILabel alloc] initWithFrame:fSTotal];
        lSTotal.textColor = THEME_COLOR;
        NSMutableAttributedString *attrTotalPoints = [[NSMutableAttributedString alloc] initWithString:@"TOTAL\nPOINTS"];
        [attrTotalPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Cond" size:15.0f]} range:NSMakeRange(0, 5)];
        [attrTotalPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-BoldCond" size:15.0f]} range:NSMakeRange(6, 6)];
        lSTotal.attributedText = attrTotalPoints;
        lSTotal.textAlignment = NSTextAlignmentLeft;
        lSTotal.numberOfLines = 2;
        [view addSubview:lSTotal];
        
        
        //NSDate *month = [NSDate dateWithTimeIntervalSinceNow:(-60*60*24*7*30)];
        
        if(!self.week)self.week = 0;
        if(!self.month)self.month = 0;

        
        CGRect fm = view.frame;
        fm.size = CGSizeMake(15.0f, 30.0f);
        fm.origin= CGPointMake(140.0f, 40.0f);
        UILabel *lm = [[UILabel alloc] initWithFrame:fm];
        lm.textColor = [UIColor whiteColor];
        lm.text = @"0";//[@(self.month) stringValue];
        lm.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:30.0f];
        lm.numberOfLines = 1;
        lm.textAlignment = NSTextAlignmentLeft;
        [view addSubview:lm];
        
        NSMutableAttributedString *attrCurPoints = [[NSMutableAttributedString alloc] initWithString:@"PRIZES\nWON"];
        [attrCurPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Cond" size:15.0f]} range:NSMakeRange(0, 6)];
        [attrCurPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-BoldCond" size:15.0f]} range:NSMakeRange(7, 3)];
        
        CGRect ftm = view.frame;
        ftm.size = CGSizeMake(45.0f, 35.0f);
        ftm.origin= CGPointMake(155.0f, 37.0f);
        UILabel *ltm = [[UILabel alloc] initWithFrame:ftm];
        ltm.textColor = THEME_COLOR; //[UIColor colorWithWhite:0.8f alpha:1.0f];
        ltm.attributedText = attrCurPoints;
        ltm.textAlignment = NSTextAlignmentLeft;
        //ltm.font = [UIFont boldSystemFontOfSize:12.0f];
        ltm.numberOfLines = 2;
        [view addSubview:ltm];
        
        RatingModel *rating = [PFUser currentUser][@"rating"];
        [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            RatingModel *refreshed = (RatingModel *)object;
            dispatch_async(dispatch_get_main_queue(), ^{
                self.total = refreshed.score;
                lTotal.text = [@(self.total) stringValue];
                CGRect fTotal = lTotal.frame;
                CGRect fSTotal = lSTotal.frame;
                CGRect fm = lm.frame;
                CGRect ftm = ltm.frame;
                if (self.total == 0) {
                    fTotal.size = CGSizeMake(15.0f, 30.0f);
                }else {
                    fTotal.size = CGSizeMake(15.0f * ((int)log10(self.total) + 1), 30.0f);
                }
                /*if (refreshed.curpoint == 0) {
                    fm.size = CGSizeMake(15.0f, 30.0f);
                }else {
                    fm.size = CGSizeMake(15.0f * ((int)log10(refreshed.curpoint) + 1), 30.0f);
                }*/
                fSTotal.origin.x = fTotal.origin.x + fTotal.size.width;
                if (self.total < 10) {
                    fm.origin.x = fSTotal.origin.x + fSTotal.size.width + 15;
                }else {
                    fm.origin.x = fSTotal.origin.x + fSTotal.size.width + 5;
                }
                ftm.origin.x = fm.origin.x + fm.size.width;
                lTotal.frame = fTotal;
                lSTotal.frame = fSTotal;
                lm.frame = fm;
                ltm.frame = ftm;
                //lm.text = [@(refreshed.curpoint) stringValue];
                NSLog(@"Total: %i",self.total);
                //NSLog(@"Total: @",[@(refreshed.score) stringValue]);
            });
        }];
        
        
        CGRect fball1 = view.frame;
        fball1.size = CGSizeMake(20.0f, 15.0f);
        fball1.origin= CGPointMake(240.0f, 30.0f);
        UIImageView *iball1 = [[UIImageView alloc] initWithFrame:fball1];
        iball1.image = [UIImage imageNamed:@"ball_off"];
        iball1.hidden = YES;
        [view addSubview:iball1];
        
        CGRect fball2 = view.frame;
        fball2.size = CGSizeMake(20.0f, 15.0f);
        fball2.origin= CGPointMake(265.0f, 30.0f);
        UIImageView *iball2 = [[UIImageView alloc] initWithFrame:fball2];
        iball2.image = [UIImage imageNamed:@"ball_off"];
        iball2.hidden = YES;
        [view addSubview:iball2];
        
        CGRect fball3 = view.frame;
        fball3.size = CGSizeMake(20.0f, 15.0f);
        fball3.origin= CGPointMake(290.0f, 30.0f);
        UIImageView *iball3 = [[UIImageView alloc] initWithFrame:fball3];
        iball3.image = [UIImage imageNamed:@"ball_off"];
        iball3.hidden = YES;
        [view addSubview:iball3];
        
        CGRect fv = view.frame;
        fv.size = CGSizeMake(34.0f, 42.0f);
        fv.origin= CGPointMake(270.0f, 15.0f);
        UIImageView *iv = [[UIImageView alloc] initWithFrame:fv];
        iv.image = [UIImage imageNamed:@"v_badge"];
        iv.hidden = YES;
        [view addSubview:iv];
        
        UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
        [tap1 setCancelsTouchesInView:YES];
        iball1.userInteractionEnabled = YES;
        [iball1 addGestureRecognizer:tap1];
        UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
        [tap2 setCancelsTouchesInView:YES];
        iball2.userInteractionEnabled = YES;
        [iball2 addGestureRecognizer:tap2];
        
        UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
        [tap3 setCancelsTouchesInView:YES];
        iball3.userInteractionEnabled = YES;
        [iball3 addGestureRecognizer:tap3];
        
        UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
        [tap4 setCancelsTouchesInView:YES];
        iv.userInteractionEnabled = YES;
        [iv addGestureRecognizer:tap4];
        
        
        PFQuery *query = [PFQuery queryWithClassName:@"Score"];
        [query whereKey:@"players" equalTo:[PFUser currentUser]];
        [query whereKey:@"approved" equalTo:@YES];
        [query orderByDescending:@"createdAt"];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                return;
            }
            int ball = 0;
            if (objects.count >= 1) {
                ball = 1;
            }
            NSMutableArray *opponents = [[NSMutableArray alloc] init];
            for (int i = 0; i < objects.count; i++) {
                ScoreModel *score = (ScoreModel *)[objects objectAtIndex:i];
                for (int j = 0; j < score.players.count; j++) {
                    PFUser *player = (PFUser *)[score.players objectAtIndex:j];
                    if ([opponents containsObject:player.objectId]) {
                        continue;
                    }
                    if ([player.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        continue;
                    }
                    [opponents addObject:player.objectId];
                }
            }
            if (objects.count >= 4 && opponents.count >= 3) {
                ball = 2;
            }
            if (objects.count >= 9 && opponents.count >= 5) {
                ball = 3;
            }
            if (objects.count >= 12) {
                ball = 4;
            }
            if (ball < 4) {
                iball1.hidden = NO;
                iball2.hidden = NO;
                iball3.hidden = NO;
                iv.hidden = YES;
                if (ball == 0) {
                    iball1.image = [UIImage imageNamed:@"ball_off"];
                    iball2.image = [UIImage imageNamed:@"ball_off"];
                    iball3.image = [UIImage imageNamed:@"ball_off"];
                }else if (ball == 1) {
                    iball1.image = [UIImage imageNamed:@"ball_on"];
                    iball2.image = [UIImage imageNamed:@"ball_off"];
                    iball3.image = [UIImage imageNamed:@"ball_off"];
                }else if (ball == 2) {
                    iball1.image = [UIImage imageNamed:@"ball_on"];
                    iball2.image = [UIImage imageNamed:@"ball_on"];
                    iball3.image = [UIImage imageNamed:@"ball_off"];
                }else {
                    iball1.image = [UIImage imageNamed:@"ball_on"];
                    iball2.image = [UIImage imageNamed:@"ball_on"];
                    iball3.image = [UIImage imageNamed:@"ball_on"];
                }
            }else {
                iball1.hidden = YES;
                iball2.hidden = YES;
                iball3.hidden = YES;
                iv.hidden = NO;
            }
        }];
                
        self.tableView.separatorColor = [UIColor whiteColor];
        // Add a bottomBorder.
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height-2.0f, view.frame.size.width, 2.0f);
        bottomBorder.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f].CGColor;
        [view.layer addSublayer:bottomBorder];
    }else {
        int numSections = 1;
        if (self.objects1.count > 0) {
            numSections++;
        }
        if (self.objects2.count > 0) {
            numSections++;
        }
        if (self.objects3.count > 0) {
            numSections++;
        }
        if (self.objects4.count > 0) {
            numSections++;
        }
        if (section == numSections) {
            if (self.objects1.count > 0 || self.objects4.count > 0) {
                return nil;
            }
            UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
            label.text = @"You have no upcoming matches scheduled";
            label.textColor = [UIColor grayColor];
            label.font = [UIFont fontWithName:@"MyriadPro-It" size:15.0f];
            label.textAlignment = NSTextAlignmentCenter;
            [view addSubview:label];
            // Add a bottomBorder.
            CALayer *bottomBorder = [CALayer layer];
            bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height-1.0f, view.frame.size.width, 1.0f);
            bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
            view.backgroundColor = [UIColor whiteColor];
            [view.layer addSublayer:bottomBorder];
        }else if (section == 1 && self.objects1.count > 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
            label.textColor = [UIColor whiteColor];
            label.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:16.0f];
            label.textAlignment = NSTextAlignmentCenter;
            label.text = @"YOU'VE BEEN INVITED TO PLAY";
            label.backgroundColor = [UIColor redColor];
            [view addSubview:label];
        }else if (((section == 1 && self.objects1.count == 0) || (section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
            label.textColor = [UIColor whiteColor];
            label.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:16.0f];
            label.textAlignment = NSTextAlignmentCenter;
            label.text = @"NEEDS SCORES!";
            label.backgroundColor = [UIColor colorWithRed:232 / 255.0f green:42 / 255.0f blue:27 / 255.0f alpha:1.0f];
            [view addSubview:label];
            return nil;
        }else if (((section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0) {
            UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
            label.textColor = [UIColor whiteColor];
            label.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:16.0f];
            label.textAlignment = NSTextAlignmentCenter;
            label.text = @"REVIEW SCORES";
            label.backgroundColor = [UIColor colorWithRed:255 / 255.0f green:156 / 255.0f blue:0.0f alpha:1.0f];
            [view addSubview:label];
            return nil;
        }
    }
    return view;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LogoCell" forIndexPath:indexPath];
        return (HomeViewCell *)cell;
    }
    HomeViewCell *cell = nil;
    ActivityModel *activity;
    int numSections = 1;
    if (self.objects1.count > 0) {
        numSections++;
    }
    if (self.objects2.count > 0) {
        numSections++;
    }
    if (self.objects3.count > 0) {
        numSections++;
    }
    if (self.objects4.count > 0) {
        numSections++;
    }
    if (indexPath.section == numSections) {
        cell = (HomeViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Feed"];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        NSLog(@"Section section");
        FeedModel *feed = [self.defaults objectAtIndex:indexPath.row];
        PFImageView *imageView = (PFImageView *)[cell.contentView viewWithTag:1];
        imageView.image = [UIImage alloc];
        imageView.file = feed.image;
        [imageView loadInBackground];
        UILabel *label = (UILabel *)[cell.contentView viewWithTag:2];
        if(!label) {
            label = [[UILabel alloc] initWithFrame:CGRectMake(71.0f, 20.0f, 246.0f, 80.0f)];
            label.tag = 1;
            label.lineBreakMode = NSLineBreakByWordWrapping;
            label.numberOfLines = 0;
            label.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14.0f];
            label.textColor = [UIColor darkGrayColor];
            [cell.contentView addSubview:label];
        }
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:3];
        if(!button) {
            button = [[UIButton alloc] initWithFrame:CGRectMake(71.0f, 64.0f, 246.0f, 24.0f)];
            [button setTitleColor:THEME_COLOR forState:UIControlStateNormal];
            button.titleLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:14.0f];
            [cell.contentView addSubview:button];
        }
        if(indexPath.row == 0) {
            label.text = feed.text;
            [button setTitle:feed.button forState:UIControlStateNormal];
            //imageView.image = [UIImage imageNamed:@"Award-Icon"];
            //[button addTarget:self action:@selector(play:) forControlEvents:UIControlEventTouchUpInside];
        } else if (indexPath.row == 1) {
            label.text = feed.text;
            [button setTitle:feed.button forState:UIControlStateNormal];
            //imageView.image = [UIImage imageNamed:@"Racket-Icon"];
            //[button addTarget:self action:@selector(browse:) forControlEvents:UIControlEventTouchUpInside];
        } else if(indexPath.row == 2) {
            label.text = feed.text;
            [button setTitle:feed.button forState:UIControlStateNormal];
            //imageView.image = [UIImage imageNamed:@"Invite-Icon"];
            //[button addTarget:self action:@selector(inviteAll:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        button.translatesAutoresizingMaskIntoConstraints = YES;
        [button sizeToFit];
        label.translatesAutoresizingMaskIntoConstraints = YES;
        [label sizeToFit];
        return cell;
    }else {
        if ((((indexPath.section == 1 && self.objects1.count == 0) || (indexPath.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((indexPath.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (indexPath.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (indexPath.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScoreCell"];
            UILabel *lblTitle = (UILabel *)[cell viewWithTag:10];
            UILabel *lblDesc = (UILabel *)[cell viewWithTag:11];
            if ((((indexPath.section == 1 && self.objects1.count == 0) || (indexPath.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
                activity = [self.objects2 objectAtIndex:indexPath.row];
                lblTitle.text = @"NEEDS SCORES!";
                cell.contentView.backgroundColor = [UIColor colorWithRed:232 / 255.0f green:42 / 255.0f blue:27 / 255.0f alpha:1.0f];
                
            }else {
                activity = [self.objects3 objectAtIndex:indexPath.row];
                NSLog(@"%@",activity);
                lblTitle.text = @"REVIEW SCORES!";
                cell.contentView.backgroundColor = [UIColor colorWithRed:255 / 255.0f green:156 / 255.0f blue:0.0f alpha:1.0f];
            }
            
            NSString *type = @"Singles match";
            if(activity.doubles) {
                type = @"Doubles match";
            }
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"M.dd.yy"];
            NSString *startTime = [formatter stringFromDate:activity.startTime];
            NSString *withPerson = @"";
            PFUser *otherUser = nil;
            if (activity.doubles) {
                NSMutableArray *ourTeam = nil;
                for (int i = 0; i < activity.firstTeam.count; i++) {
                    PFUser *curUser = (PFUser *)[activity.firstTeam objectAtIndex:i];
                    if (curUser != nil && ![curUser isEqual:[NSNull null]]) {
                        if (![curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                            ourTeam = activity.firstTeam;
                        }
                    }
                }
                if (ourTeam == nil) {
                    for (int i = 0; i < activity.secondTeam.count; i++) {
                        PFUser *curUser = (PFUser *)[activity.secondTeam objectAtIndex:i];
                        if (curUser != nil && ![curUser isEqual:[NSNull null]]) {
                            if (![curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                                ourTeam = activity.secondTeam;
                            }
                        }
                    }
                }
                for (int i = 0; i < ourTeam.count; i++) {
                    PFUser *curUser = (PFUser *)[ourTeam objectAtIndex:i];
                    if (curUser != nil && ![curUser isEqual:[NSNull null]]) {
                        if (![curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                            otherUser = curUser;
                        }
                    }
                }
            }else {
                for (int i = 0; i < activity.recipients.count; i++) {
                    PFUser *curUser = (PFUser *)[activity.recipients objectAtIndex:i];
                    if (curUser != nil && ![curUser isEqual:[NSNull null]]) {
                        if (![curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                            otherUser = curUser;
                        }
                    }
                    
                }
            }
            
            if (otherUser != nil) {
                NSString *first = [otherUser objectForKey:@"first"];
                NSString *last = [otherUser objectForKey:@"last"];
                
                if(!first) {
                    NSArray *name = [otherUser[@"name"] componentsSeparatedByString:@" "];
                    first = [name firstObject];
                    if(!last) {
                        if(name.count > 1) {
                            last = [name lastObject];
                        }
                    }
                }
                withPerson = first;
                if(last && last.length > 0) {
                    withPerson = [NSString stringWithFormat:@"%@ %@.",withPerson, [last substringToIndex:1]];
                }
            }
            
            lblDesc.text = [NSString stringWithFormat:@"%@ played %@ with %@",type,startTime,withPerson];
            
            /*UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(score:)];
            [tap setCancelsTouchesInView:YES];
            [cell.contentView addGestureRecognizer:tap];*/
            
            //HomeViewCell *cell = (HomeViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ScoreCell"];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            //PFUser *user = activity.user;
            
            /*NSString *type = @"Singles Match";
            if(activity.doubles) {
                type = @"Doubles Match";
            }
            cell.sportLabel.text = [NSString stringWithFormat:@"%@",type];
            
            NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
            
            
            if(activity.startTime.year != [NSDate date].year) {
                dateTimeFormatter.dateFormat = @"MMM d, YYYY @h:mm a";
            } else {
                dateTimeFormatter.dateFormat = @"MMM d @h:mm a";
            }
            
            NSString *datetime = [dateTimeFormatter stringFromDate: activity.startTime];
            
            cell.fromNowLabel.text = [NSString stringWithFormat:@"%@ ago",activity.startTime.shortTimeAgoSinceNow];
            cell.timeLabel.text = datetime;
            
            cell.locationLabel.text = [NSString stringWithFormat:@"NEAR %@ %@",
                                       activity.location[@"Street"],
                                       activity.location[@"City"]];
            
            
            
            
            // Set the players labels
            for(int i=0;i<activity.recipients.count;i++) {
                PFUser *recipient = [activity.recipients objectAtIndex:i];
                if(!recipient || [recipient isEqual:[NSNull null]] || !recipient.isDataAvailable)continue;
                int tag = i+1;
                UIView *container = [cell.contentView viewWithTag:tag];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onProfile:)];
                [tap setCancelsTouchesInView:YES];
                [container addGestureRecognizer:tap];
                if(container) {
                    PFImageView *imageView = (PFImageView *)[container viewWithTag:tag*10];
                    if(imageView) {
                        PFFile *file = [recipient objectForKey:@"image"];
                        imageView.file = file;
                        if(recipient[@"female"]) {
                            imageView.image = [UIImage imageNamed:@"male"];
                        } else {
                            imageView.image = [UIImage imageNamed:@"female"];
                        }
                        [imageView loadInBackground];
                        imageView.layer.cornerRadius = imageView.frame.size.height/2.0f;
                        imageView.clipsToBounds = YES;
                        imageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
                        imageView.layer.borderWidth = 2.0f;
                        imageView.contentMode = UIViewContentModeScaleAspectFill;
                    }
                    UILabel *name = (UILabel *)[container viewWithTag:tag*11];
                    if(name) {
                        NSString *first = [recipient objectForKey:@"first"];
                        NSString *last = [recipient objectForKey:@"last"];
                        
                        if(!first) {
                            NSArray *name = [recipient[@"name"] componentsSeparatedByString:@" "];
                            first = [name firstObject];
                            if(!last) {
                                if(name.count > 1) {
                                    last = [name lastObject];
                                }
                            }
                        }
                        name.text = first;
                        if(last && last.length > 0) {
                            name.text = [NSString stringWithFormat:@"%@ %@.",name.text, [last substringToIndex:1]];
                        }
                    }
                    UILabel *host = (UILabel *)[container viewWithTag:tag*12];
                    if(activity.scorer && [activity.scorer.objectId isEqualToString:recipient.objectId]) {
                        host.text = @"SCORER";
                        if(imageView) {
                            imageView.layer.borderColor = THEME_COLOR.CGColor;
                        } else {
                            host.text = @"";
                        }
                    } else {
                        host.text = @"";
                    }
                }
            }
            
            // If not scored - title should be score
            // If scored by you or teammate - title should be Pending (verification or opponent)
            // If scored by opponent - title should be Review
            
            
            BOOL scored = FALSE;
            BOOL opponent = FALSE;
            
            
            if(activity.score) {
                // The match has been scored
                scored = TRUE;
                if(!activity.approved) {
                    // Either pending or review
                    for(PFUser *user in activity.reviewers) {
                        // Reviewers is the team of reviewers
                        NSLog(@"User: %@",user);
                        if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                            NSLog(@"User: %@",user.objectId);
                            NSLog(@"User: %@",[PFUser currentUser].objectId);
                            // Our team needs to review
                            opponent = TRUE;
                            break;
                        }
                    }
                }
            }
            cell.actionButton.enabled = YES;
            [cell.actionButton setBackgroundColor:THEME_COLOR];
            if(activity.approved) {
                [cell.actionButton setTitle:@"View" forState:UIControlStateNormal];
            } else {
                if(scored) {
                    // Match is scored
                    if(opponent) {
                        // Opponent scored
                        [cell.actionButton setTitle:@"Review" forState:UIControlStateNormal];
                        
                    } else {
                        // Team scored
                        [cell.actionButton setTitle:@"Pending" forState:UIControlStateDisabled];
                        [cell.actionButton setBackgroundColor:OPAQUE_THEME];
                        cell.actionButton.enabled = NO;
                    }
                } else {
                    // Not scored
                    [cell.actionButton setTitle:@"Score" forState:UIControlStateNormal];
                }
            }
            
            
            cell.actionButton.layer.cornerRadius = 4.0f;
            cell.actionButton.clipsToBounds = YES;
            cell.actionButton.layer.borderColor = [UIColor whiteColor].CGColor;
            cell.actionButton.layer.borderWidth = 2.0f;
            if (indexPath.row % 2 == 1) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:235 / 255.0f green:235 / 255.0f blue:241 / 255.0f alpha:1.0f];
            }else {
                cell.contentView.backgroundColor = [UIColor whiteColor];
            }*/
            return cell;
        }else {
            if (indexPath.section == 1 && self.objects1.count > 0) {
                activity = [self.objects1 objectAtIndex:indexPath.row];
            }else {
                activity = [self.objects4 objectAtIndex:indexPath.row];
            }
            HomeViewCell *cell = (HomeViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
            BOOL invitation = FALSE;
            if(activity.invitations) {
                for(PFUser *user in activity.invitations) {
                    if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        invitation = TRUE;
                    }
                }
            }
            // Already joined the match!!!
            for(PFUser *user in activity.recipients) {
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    invitation = FALSE;
                }
            }
            
            
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            
            PFUser *user = activity.user;
            
            cell.messageButton.layer.cornerRadius = 4.0f;
            cell.actionButton.layer.cornerRadius = 4.0f;
            cell.messageButton.clipsToBounds = YES;
            cell.actionButton.clipsToBounds = YES;
            if (activity.note == nil || [activity.note isEqualToString:@""]) {
                cell.notesButton.hidden = YES;
            }else {
                cell.notesButton.hidden = NO;
            }
            UIButton *invite = (UIButton *)[cell.contentView viewWithTag:22];
            UIView *viewFull = [cell viewWithTag:120];
            viewFull.hidden = YES;
            if(invitation) {
                if (activity.recipients.count >= activity.spots) {
                    viewFull.hidden = NO;
                }
                [invite setTitle:@"INVITED" forState:UIControlStateNormal];
                cell.messageButton.backgroundColor = THEME_COLOR;
                
                [cell.messageButton setTitle:@"JOIN" forState:UIControlStateNormal];
                [cell.messageButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                cell.messageButton.layer.borderColor = [[UIColor whiteColor] CGColor];
                
                cell.actionButton.backgroundColor = [UIColor whiteColor];
                
                [cell.actionButton setTitle:@"DECLINE" forState:UIControlStateNormal];
                [cell.actionButton setTitleColor:THEME_COLOR forState:UIControlStateNormal];
                cell.actionButton.layer.borderColor = [THEME_COLOR CGColor];
                cell.actionButton.layer.borderWidth = 2.0f;
                //invite.hidden = TRUE;
                /*invite.highlighted = TRUE;
                 [invite setTitle:@"DECLINE" forState:UIControlStateHighlighted];
                 [invite setBackgroundColor:[UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:1.0f]];*/
            } else {
                cell.messageButton.backgroundColor = [UIColor whiteColor];
                [cell.messageButton setTitle:@"MESSAGE" forState:UIControlStateNormal];
                [cell.messageButton setTitleColor:THEME_COLOR forState:UIControlStateNormal];
                cell.messageButton.layer.borderColor = [THEME_COLOR CGColor];
                cell.messageButton.layer.borderWidth = 2.0f;
                
                cell.actionButton.backgroundColor = THEME_COLOR;
                [cell.actionButton setTitle:@"EDIT" forState:UIControlStateNormal];
                [cell.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                cell.actionButton.layer.borderColor = [[UIColor whiteColor] CGColor];
                cell.actionButton.layer.borderWidth = 2.0f;
                [invite setTitle:@"OPEN" forState:UIControlStateNormal];
            }
            
            
            NSString *type = @"SINGLES MATCH";
            if(activity.doubles) {
                type = @"DOUBLES MATCH";
            }
            //cell.sportLabel.text = [NSString stringWithFormat:@"%@ %@",sport,type];
            cell.sportLabel.text = [NSString stringWithFormat:@"%@",type];
            
            NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
            
            
            if(activity.startTime.year != [NSDate date].year) {
                dateTimeFormatter.dateFormat = @"EEE, MMM d, YYYY @ h:mm a";
            } else {
                dateTimeFormatter.dateFormat = @"EEE, MMM d @ h:mm a";
            }
            
            NSString *datetime = [dateTimeFormatter stringFromDate: activity.startTime];
            
            cell.fromNowLabel.text =  [NSString stringWithFormat:@"In %@",activity.startTime.shortTimeAgoSinceNow];
            //if([activity.startTime isEarlierThan:[NSDate date]])
            cell.timeLabel.text = datetime;
            if (activity.court != nil && ![activity.court isEqual:[NSNull null]] && ![activity.court isEqualToString:@""]) {
                cell.locationLabel.text = activity.court;
            }else {
                cell.locationLabel.text = [[NSString stringWithFormat:@"NEAR %@ %@",
                                            activity.location[@"Thoroughfare"],
                                            activity.location[@"City"]] uppercaseString];
                if(!activity.location[@"Thoroughfare"]) {
                    cell.locationLabel.text = [[NSString stringWithFormat:@"NEAR %@",
                                                activity.location[@"City"]] uppercaseString];
                }
            }
            
            if([activity.startTime isEarlierThan:[NSDate date]]){
                // Date is in the past
                cell.openingsLabel.hidden = YES;
                cell.openingsLabel.text = @"";
                //[cell.actionButton setTitle:@"Score" forState:UIControlStateNormal];
                invite.hidden = NO;
            } else {
                // Date is in the future
                //[cell.actionButton setTitle:@"Edit" forState:UIControlStateNormal];
                UIView *view = [cell.contentView viewWithTag:213];
                UIImageView *viewTick = (UIImageView *)[cell.contentView viewWithTag:1213];
                if(activity.recipients.count >= activity.spots){
                    //NSLog(@"Recipients count: %li",(long)activity.recipients.count);
                    //NSLog(@"Spots: %li",(long)activity.spots);
                    // FULL
                    //cell.contentView.backgroundColor = [UIColor whiteColor];
                    cell.openingsLabel.hidden = NO;
                    cell.openingsLabel.text = [NSString stringWithFormat:@"MATCH ON"];
                    cell.openingsLabel.textColor = [UIColor whiteColor];
                    view.backgroundColor = [UIColor colorWithRed:138.0f / 255.0f green:174.0f / 255.0f blue:0.0f alpha:1.0f];
                    viewTick.image = [UIImage imageNamed:@"tick_green"];
                    invite.hidden = TRUE;
                    
                } else {
                    // OPENINGS
                    cell.openingsLabel.hidden = YES;
                    cell.openingsLabel.text = [NSString stringWithFormat:@"%i OPENING",(int)(activity.spots-activity.recipients.count)];
                    if((activity.spots-activity.recipients.count) != 1){
                        cell.openingsLabel.text = [NSString stringWithFormat:@"%@s",cell.openingsLabel.text];
                    }
                    cell.openingsLabel.textColor = [UIColor redColor];
                    view.backgroundColor = [UIColor colorWithRed:180.0f/255.0f green:183.0f/255.0f blue:205.0f/255.0f alpha:1.0f];
                    viewTick.image = [UIImage imageNamed:@"tick_gray"];
                    
                    if(invitation) {
                        view.backgroundColor = [UIColor colorWithRed:255.0f/255.0f green:194.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
                        viewTick.image = [UIImage imageNamed:@"tick_yellow"];
                    }
                    invite.hidden = FALSE;
                }
                
            }
            
            for(int i=0;i<5;i++) {
                int tag = i+1;
                UIView *container = [cell.contentView viewWithTag:tag];
                if (tag == 5) {
                    container.hidden = YES;
                    break;
                }
                if(container) {
                    UILabel *label = (UILabel *)[container viewWithTag:tag*13];
                    label.backgroundColor = [UIColor clearColor];
                    label.text = @"";
                    PFImageView *imageView = (PFImageView *)[container viewWithTag:tag*10];
                    imageView.image = nil;
                    imageView.layer.borderColor = [UIColor clearColor].CGColor;
                    UILabel *name = (UILabel *)[container viewWithTag:tag*11];
                    name.text = @"";
                    UILabel *host = (UILabel *)[container viewWithTag:tag*12];
                    host.text = @"";
                }
            }
            if (activity.recipients.count < activity.spots) {
                BOOL isAccepted = NO;
                for (int i = 0; i < activity.recipients.count; i++) {
                    PFUser *recipient = (PFUser *)[activity.recipients objectAtIndex:i];
                    if ([recipient.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        isAccepted = YES;
                        break;
                    }
                }
                int tag1 = (int)activity.recipients.count + 1;
                UIView *container1 = [cell.contentView viewWithTag:tag1];
                UIView *container = [cell.contentView viewWithTag:5];
                if (isAccepted) {
                    PFImageView *imageView = (PFImageView *)[container viewWithTag:50];
                    imageView.layer.cornerRadius = imageView.frame.size.height/2.0f;
                    imageView.clipsToBounds = YES;
                    imageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
                    imageView.layer.borderWidth = 2.0f;
                    imageView.contentMode = UIViewContentModeScaleAspectFill;
                    CGRect frame = container.frame;
                    frame.origin = container1.frame.origin;
                    container.translatesAutoresizingMaskIntoConstraints = YES;
                    container.frame = frame;
                    container.hidden = NO;
                    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(invite:)];
                    [tap setCancelsTouchesInView:YES];
                    [container addGestureRecognizer:tap];
                }else {
                    container.hidden = YES;
                }
            }
            
            // Set the players labels
            for(int i=0;i<activity.recipients.count;i++) {
                PFUser *recipient = [activity.recipients objectAtIndex:i];
                if(!recipient || [recipient isEqual:[NSNull null]] || !recipient.isDataAvailable)continue;
                int tag = i+1;
                UIView *container = [cell.contentView viewWithTag:tag];
                UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onProfile:)];
                [tap setCancelsTouchesInView:YES];
                [container addGestureRecognizer:tap];
                if(container) {
                    PFImageView *imageView = (PFImageView *)[container viewWithTag:tag*10];
                    if(imageView) {
                        PFFile *file = [recipient objectForKey:@"image"];
                        imageView.file = file;
                        /*if([recipient[@"female"] boolValue]) {
                            imageView.image = [UIImage imageNamed:@"female"];
                        } else {
                            imageView.image = [UIImage imageNamed:@"male"];
                        }*/
                        imageView.image = [UIImage imageNamed:@"default_profile"];
                        [imageView loadInBackground];
                        imageView.layer.cornerRadius = imageView.frame.size.height/2.0f;
                        imageView.clipsToBounds = YES;
                        imageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
                        imageView.layer.borderWidth = 2.0f;
                        imageView.contentMode = UIViewContentModeScaleAspectFill;
                    }
                    UILabel *name = (UILabel *)[container viewWithTag:tag*11];
                    if(name) {
                        
                        NSString *first = [recipient objectForKey:@"first"];
                        NSString *last = [recipient objectForKey:@"last"];
                        
                        if(!first) {
                            NSArray *name = [recipient[@"name"] componentsSeparatedByString:@" "];
                            first = [name firstObject];
                            if(!last) {
                                if(name.count > 1) {
                                    last = [name lastObject];
                                }
                            }
                        }
                        name.text = first;
                        if(last && last.length > 0) {
                            name.text = [NSString stringWithFormat:@"%@ %@.",name.text, [last substringToIndex:1]];
                        }
                        
                    }
                    UILabel *host = (UILabel *)[container viewWithTag:tag*12];
                    if([recipient.objectId isEqualToString:user.objectId]) {
                        host.text = @"HOST";
                        if(imageView) {
                            imageView.layer.borderColor = THEME_COLOR.CGColor;
                        }
                    } else {
                        host.text = @"";
                    }
                }
            }
            
            
            if(self.geopoint && activity.geopoint) {
                CLLocation *location1 = [[CLLocation alloc] initWithLatitude:self.geopoint.latitude longitude:self.geopoint.longitude];
                CLLocation *location2 = [[CLLocation alloc] initWithLatitude:activity.geopoint.latitude longitude:activity.geopoint.longitude];
                CLLocationDistance distance = [location1 distanceFromLocation:location2];
                float miles = roundf(0.000621371*distance);
                cell.distanceLabel.text = [NSString stringWithFormat:@"%.1f mi",miles];
                
            } else {
                cell.distanceLabel.text = @"";
            }
            if (indexPath.row % 2 == 1) {
                cell.contentView.backgroundColor = [UIColor colorWithRed:235 / 255.0f green:235 / 255.0f blue:241 / 255.0f alpha:1.0f];
            }else {
                cell.contentView.backgroundColor = [UIColor whiteColor];
            }
            return cell;
        }
    }
}

-(void)onProfile:(UIGestureRecognizer *)gesture
{
    UIView *sender = gesture.view;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }
    PFUser *curUser = (PFUser *)[self.selectedActivity.recipients objectAtIndex:sender.tag - 1];
    if ([curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
        return;
    }
    [self performSegueWithIdentifier:@"profile" sender:sender];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.objects1 == nil || self.objects2 == nil || self.objects3 == nil || self.objects4 == nil) {
        return;
    }
    if (indexPath.section == 0) {
        return;
    }
    int numSections = 1;
    if (self.objects1.count > 0) {
        numSections++;
    }
    if (self.objects2.count > 0) {
        numSections++;
    }
    if (self.objects3.count > 0) {
        numSections++;
    }
    if (self.objects4.count > 0) {
        numSections++;
    }
    self.selectedIndex = indexPath;
    if (indexPath.section == numSections) {
        FeedModel *feed = [self.defaults objectAtIndex:indexPath.row];
        if ([feed.segue isEqualToString:@"play"]) {
            [self play:self];
        }else if ([feed.segue isEqualToString:@"browse"]) {
            [self browse:self];
        }else if ([feed.segue isEqualToString:@"invite"]) {
            [self inviteAll:self];
        }else if ([feed.segue isEqualToString:@"rate"]) {
            
        }else if ([feed.segue isEqualToString:@"message"]) {
            
        }else if ([feed.segue isEqualToString:@"map"]) {
            
        }else if ([feed.segue isEqualToString:@"start"]) {
            
        }else if ([feed.segue isEqualToString:@"inbox"]) {
            
        }else if ([feed.segue isEqualToString:@"landing"]) {
            
        }else if ([feed.segue isEqualToString:@"edit"]) {
            
        }else if ([feed.segue isEqualToString:@"score"]) {
            
        }else if ([feed.segue isEqualToString:@"profile"]) {
            
        }
        return;
    }else {
        if ((((indexPath.section == 1 && self.objects1.count == 0) || (indexPath.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((indexPath.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (indexPath.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (indexPath.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
            if (((indexPath.section == 1 && self.objects1.count == 0) || (indexPath.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) {
                self.selectedActivity = [self.objects2 objectAtIndex:self.selectedIndex.row];
                is_selected_score = YES;
            }else {
                self.selectedActivity = [self.objects3 objectAtIndex:self.selectedIndex.row];
            }
        }else {
            return;
        }
    }
    if(!is_selected_score) {
        [self performSegueWithIdentifier:@"score" sender:self];
    } else {
        // Score
        if(self.selectedActivity.spots > 2) {
            [self performSegueWithIdentifier:@"teammate" sender:self];
        } else {
            [self performSegueWithIdentifier:@"score" sender:self];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(HomeViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;{
    if(!self.pullToRefreshEnabled &&  self.objects.count > 0 && !self.endOfResults){
        NSInteger total = (self.objects.count - 2);
        if (indexPath.row == total)
        {
            if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
            {
                [self loadNextPage];
            }
        }
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}




#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Not Enabled" message:@"Please Turn on Location Services\nFor this app to be useful you will need to turn on location services. We use your current location to show you only matches in your area.\nTo enable, go to the settings app on your phone, find the MatchOn app and enable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            //NSLog(@"Location here!");
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        {
            
        }
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"kCLAuthorizationStatusNotDetermined");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
            break;
    }
}



#pragma mark - IBActions

- (IBAction)message:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
        return;
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }
    
    UIButton *button = (UIButton *)sender;
    if(![button.titleLabel.text isEqualToString:@"JOIN"]) {
        [self performSegueWithIdentifier:@"message" sender:self];
        return;
    }
    NSLog(@"INVITE~!!");
    HomeViewCell *cell = (HomeViewCell *)[self.tableView cellForRowAtIndexPath:selectedIndex];
    [self joinWithActivity:self.selectedActivity andCell:cell];
}

- (IBAction)action:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }

    BOOL invitation = FALSE;
    UIButton *button = (UIButton *)sender;
    if ([button.titleLabel.text isEqualToString:@"DECLINE"]) {
        NSLog(@"Confirm cancellation");
        [self declineWithActivity:self.selectedActivity];
        return;
    }
    invitation = YES;
    
    // Already joined the match!!!
    for(PFUser *user in self.selectedActivity.recipients) {
        if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
            invitation = FALSE;
        }
    }
    
    
    
    
    if([self.selectedActivity.startTime isEarlierThan:[NSDate date]]){
        // Date is in the past
        
        // If score is submitted - edit if on the same team, approve if on the other team
        [self performSegueWithIdentifier:@"score" sender:self];
        
    } else {
        [self performSegueWithIdentifier:@"edit" sender:self];
    }
}

- (IBAction)invite:(UITapGestureRecognizer *)gesture {
    UIView *sender = gesture.view;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }
    [self performSegueWithIdentifier:@"invite" sender:self];
}

- (IBAction)map:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }
    [self performSegueWithIdentifier:@"map" sender:self];
}

- (IBAction)notes:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }
    [self showPopup:self.selectedActivity.note];
}

- (IBAction)onClose:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((selectedIndex.section == 1 && self.objects1.count == 0) || (selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:selectedIndex.row];
        }
    }else {
        if (selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:selectedIndex.row];
            NSMutableArray *invites = [NSMutableArray array];
            for(PFUser *user in self.selectedActivity.invitations) {
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    continue;
                }
                [invites addObject:user];
            }
            self.selectedActivity.invitations = invites;
            
            [self.selectedActivity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                NSLog(@"%@",error);
                [self.objects1 removeObjectAtIndex:selectedIndex.row];
                [self.tableView reloadData];
            }];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:selectedIndex.row];
        }
    }
    
}

-(void)showPopup:(NSString *)note
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotePopup" object:nil userInfo:@{@"note":note}];
}


#pragma mark - IBAction feed cells

- (IBAction)play:(id)sender {
    [self.tabBarController setSelectedIndex:2];
}

- (IBAction)browse:(id)sender {
    [self.tabBarController setSelectedIndex:1];
}

- (IBAction)inviteAll:(id)sender {
    self.selectedActivity = nil;
    [self performSegueWithIdentifier:@"invite" sender:self];
}


#pragma mark Helper

- (void)joinWithActivity:(ActivityModel *)activity andCell:(HomeViewCell *)cell {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [activity fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if(error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"An error occurred. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        } else {
            
            if(activity.recipients.count == activity.spots) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                /*UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Looks like this match is full!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];*/
                UIView *viewFull = [cell viewWithTag:120];
                viewFull.hidden = NO;
                
            } else {
                for(PFUser *user in activity.recipients) {
                    if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        // Error Return
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Looks like you've already joined this match!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alertView show];
                        return;
                    }
                }
                [activity.recipients addObject:[PFUser currentUser]];
                //activity.totalRecipients = (int)activity.recipients.count;
                activity.full = (activity.recipients.count>=activity.spots);
                //activity.matchOn = (activity.totalRecipients >= activity.spots);
                
                /*if (activity.matchOn && [activity.startTime compare:[NSDate date]] == NSOrderedDescending) {
                    PFUser *currentUser = [PFUser currentUser];
                    NSMutableArray *players = [currentUser objectForKey:@"players"];
                    if (![players containsObject:activity.user]) {
                        [players addObject:activity.user];
                    }
                    for (int i = 0; i < activity.recipients.count; i++) {
                        PFUser *user = (PFUser *)[activity.recipients objectAtIndex:i];
                        if (![players containsObject:user]) {
                            [players addObject:user];
                        }
                    }
                    if (players != nil) {
                        [currentUser setObject:players forKey:@"players"];
                        [currentUser saveInBackground];
                    }
                }*/
                
                RatingModel *rating = [PFUser currentUser][@"rating"];
                [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    RatingModel *refreshed = (RatingModel *)object;
                    /*if(!activity.singlesMap) {
                        activity.singlesMap = [NSMutableDictionary dictionary];
                    }
                    if(!activity.doublesMap) {
                        activity.doublesMap = [NSMutableDictionary dictionary];
                    }*/
                    
                    if(!activity.ratingMap) {
                        activity.ratingMap = [NSMutableDictionary dictionary];
                    }
                    //NSNumber *s = [[NSNumber alloc] initWithInt:refreshed.singles];
                    //NSNumber *d = [[NSNumber alloc] initWithInt:refreshed.singles];
                    
                    //[activity.singlesMap setObject:s forKey:[PFUser currentUser].objectId];
                    //[activity.doublesMap setObject:d forKey:[PFUser currentUser].objectId];
                    
                    NSNumber *r = [[NSNumber alloc] initWithInt:refreshed.rating];
                    [activity.ratingMap setObject:r forKey:[PFUser currentUser].objectId];
                    
                    [activity setRatingValues];
                    [self.objects1 removeObject:activity];
                    int i = 0;
                    for (i = 0; i < self.objects.count; i++) {
                        if ([[[self.objects objectAtIndex:i] startTime] isLaterThan:activity.startTime]) {
                            break;
                        }
                    }
                    if (i < self.objects.count) {
                         [self.objects insertObject:activity atIndex:i];
                    }else {
                        [self.objects addObject:activity];
                    }
                    [activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        PFQuery *pushQuery = [PFInstallation query];
                        NSMutableArray *recipients = [NSMutableArray array];
                        for(PFUser *user in activity.recipients){
                            if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
                            [recipients addObject:user];
                        }
                        [pushQuery whereKey:@"user" containedIn:recipients];
                        
                        NSString *first = [[PFUser currentUser] objectForKey:@"first"];
                        NSString *last = [[PFUser currentUser] objectForKey:@"last"];
                        if(!first) {
                            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
                            first = [name firstObject];
                            if(!last) {
                                if(name.count > 1) {
                                    last = [name lastObject];
                                }
                            }
                        }
                        NSString *full = first;
                        if(last && last.length > 0) {
                            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
                        }
                        
                        NSLog(@"Sending join push to recipients: %@",recipients);
                        NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ joined your match!", full], @"badge" : @"Increment", @"a": activity.objectId};
                        PFPush *push = [[PFPush alloc] init];
                        [push setQuery:pushQuery];
                        [push setData:data];
                        [push sendPushInBackground];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
                        
                        //cell.actionButton.hidden = TRUE;
                        //cell.actionButton.enabled = FALSE;
                        [self.objects1 removeObject:activity];
                        if (activity.full && [activity.startTime compare:[NSDate date]] == NSOrderedAscending) {
                            [self.objects3 addObject:activity];
                        }else {
                            [self.objects4 addObject:activity];
                        }
                        [self.tableView reloadData];
                    }];
                    
                }];
                
            }
        }
        
    }];
}


- (void)declineWithActivity:(ActivityModel *)activity {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [activity fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if(error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"An error occurred. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        } else {
          
                
            NSMutableArray *recipients = [NSMutableArray array];
            for(PFUser *user in activity.recipients) {
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    continue;
                }
                [recipients addObject:user];
            }
            activity.recipients = recipients;
            
            NSMutableArray *invites = [NSMutableArray array];
            for(PFUser *user in activity.invitations) {
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    continue;
                }
                [invites addObject:user];
            }
            activity.invitations = invites;
            
            NSMutableArray *declines = [NSMutableArray array];
            BOOL isRegistered = NO;
            for (PFUser *user in activity.declines) {
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    isRegistered = YES;
                }
                [declines addObject:user];
            }
            if (!isRegistered) {
                [declines addObject:[PFUser currentUser]];
            }
            activity.declines = declines;
            
                //activity.totalRecipients = (int)activity.recipients.count;
                activity.full = (activity.recipients.count>=activity.spots);
                //activity.matchOn = (activity.totalRecipients >= activity.spots);
            
                RatingModel *rating = [PFUser currentUser][@"rating"];
                [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    /*if(!activity.singlesMap) {
                        activity.singlesMap = [NSMutableDictionary dictionary];
                    }
                    if(!activity.doublesMap) {
                        activity.doublesMap = [NSMutableDictionary dictionary];
                    }*/
                    
                    if(!activity.ratingMap) {
                        activity.ratingMap = [NSMutableDictionary dictionary];
                    }
                    
                    //[activity.singlesMap removeObjectForKey:[PFUser currentUser].objectId];
                    //[activity.doublesMap removeObjectForKey:[PFUser currentUser].objectId];
                    [activity.ratingMap removeObjectForKey:[PFUser currentUser].objectId];
                    [activity setRatingValues];
                    
                    [activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        PFQuery *pushQuery = [PFInstallation query];
                        NSMutableArray *recipients = [NSMutableArray array];
                        for(PFUser *user in activity.recipients){
                            if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
                            [recipients addObject:user];
                        }
                        [pushQuery whereKey:@"user" containedIn:recipients];
                        
                        
                        
                        NSString *first = [[PFUser currentUser] objectForKey:@"first"];
                        NSString *last = [[PFUser currentUser] objectForKey:@"last"];
                        if(!first) {
                            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
                            first = [name firstObject];
                            if(!last) {
                                if(name.count > 1) {
                                    last = [name lastObject];
                                }
                            }
                        }
                        NSString *full = first;
                        if(last && last.length > 0) {
                            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
                        }
                        
                        NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ declined your match invitation!", full], @"badge" : @"Increment", @"a": activity.objectId,@"d":@"decline"};
                        PFPush *push = [[PFPush alloc] init];
                        [push setQuery:pushQuery];
                        [push setData:data];
                        [push sendPushInBackground];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
                        [self refreshInbox];
                    }];
                    
                }];
            
        }
        
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"message"]) {
        MessageViewController *message = [segue destinationViewController];
        message.activity = self.selectedActivity;
        if(self.selectedActivity != nil && ![self.selectedActivity isEqual:[NSNull null]] && self.selectedActivity.isDataAvailable) {
            message.recipients = self.selectedActivity.recipients;
        }
        
    } else if([segue.identifier isEqualToString:@"edit"]) {
        UINavigationController *nav = [segue destinationViewController];
        PlayViewController *play = [nav.viewControllers firstObject];
        play.activity = self.selectedActivity;
        play.editing = TRUE;
    } else if([segue.identifier isEqualToString:@"invite"]) {
        UINavigationController *nav = [segue destinationViewController];
        InviteViewController *vc = [nav.viewControllers firstObject];
        vc.activity = self.selectedActivity;
    } else if([segue.identifier isEqualToString:@"map"]) {
        UINavigationController *nav = [segue destinationViewController];
        MapViewController *vc = [nav.viewControllers firstObject];
        vc.activity = self.selectedActivity;
    } else if([segue.identifier isEqualToString:@"start"]) {
        UINavigationController *nav = [segue destinationViewController];
        StartPageViewController *vc = [nav.viewControllers firstObject];
        vc.activity = self.selectedActivity;
        vc.homeDelegate = self;
        [self.view setHidden:YES];

    } else if ([segue.identifier isEqualToString:@"rate"]) {
        WizardViewController *vc = [segue destinationViewController];
        vc.prevViewController = self;
    } else if ([segue.identifier isEqualToString:@"profile"]) {
        OtherProfileViewController *vc = [segue destinationViewController];
        UIView *viewS = (UIView *)sender;
        vc.curUser = [self.selectedActivity.recipients objectAtIndex:viewS.tag - 1];
    }else if ([segue.identifier isEqualToString:@"description"]) {
        DescriptionViewController *descriptionViewController = (DescriptionViewController *)segue.destinationViewController;
        if ([sender isKindOfClass:[UIButton class]]) {
            descriptionViewController.imgBg = [UIImage imageNamed:@"score_desc"];
        }else {
            descriptionViewController.imgBg = [UIImage imageNamed:@"badge_desc"];
        }
        descriptionViewController.prevViewController = self;
    }else if([segue.identifier isEqualToString:@"teammate"]) {
        TeammateTableViewController *team = [segue destinationViewController];
        NSMutableArray *recipients = [NSMutableArray array];
        for(PFUser *user in self.selectedActivity.recipients){
            if([user.objectId isEqualToString:[PFUser currentUser].objectId]){
                continue;
            }
            [recipients addObject:user];
        }
        team.activity = self.selectedActivity;
        team.recipients = [[NSMutableArray alloc] initWithArray:recipients];
    }else if ([segue.identifier isEqualToString:@"score"]) {
        MainViewController *mainViewController = (MainViewController *)self.tabBarController;
        mainViewController.badge.hidden = YES;
        self.navigationItem.title = @"";
        ScoreViewController *score = [segue destinationViewController];
        score.activity = self.selectedActivity;
        score.action = @"score";
        //HomeViewCell *cell = (HomeViewCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndex];
        if(!is_selected_score) {
            score.action = @"review";
        } else {
            score.action = @"score";
        }/* else {
            
            score.scoreSaved = ^(ScoreModel *score) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // Submit view for saving score
                    
                });
            };
        }*/
    
    }
}

#pragma mark - Logout helper

- (void) logoutClear:(NSNotification *) notification
{
    // Unsubscribe from push notifications by removing the user association from the current installation.
    [[PFInstallation currentInstallation] removeObjectForKey:@"user"];
    [[PFInstallation currentInstallation] saveInBackground];
    [PFQuery clearAllCachedResults];
    [PFUser logOut];
    [[PFFacebookUtils session] close];
    //[self loadObjects];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults removeObjectForKey:@"existing"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"landing" sender:self];
}

- (void)storeShowStart:(BOOL)theShowStart {
    // set the start temp switch
    [[NSUserDefaults standardUserDefaults] setBool:theShowStart forKey:[PFUser currentUser].username];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)validateShowStart{
    // retrieve the show start value
    showStart = [[NSUserDefaults standardUserDefaults] boolForKey:[PFUser currentUser].username];
    [[[NSUserDefaults standardUserDefaults] stringForKey:[PFUser currentUser].username] boolValue];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:[PFUser currentUser].username]) [self performSegueWithIdentifier:@"start" sender:self];
}

- (void)showView {
    [self.view setHidden:NO];
}

-(void)showDescView:(UIGestureRecognizer *)gesture
{
    [self performSegueWithIdentifier:@"description" sender:gesture.view];
}

-(void)showHelp:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"description" sender:sender];
}

- (void)score:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    self.selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"%d",(int)self.selectedIndex.row);
    is_selected_score = NO;
    if ((((self.selectedIndex.section == 1 && self.objects1.count == 0) || (self.selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0) || (((self.selectedIndex.section == 1 && self.objects1.count == 0 && self.objects2.count == 0) || (self.selectedIndex.section == 2 && ((self.objects1.count == 0 && self.objects2.count > 0) || (self.objects1.count > 0 && self.objects2.count == 0))) || (self.selectedIndex.section == 3 && self.objects1.count > 0 && self.objects2.count > 0)) && self.objects3.count > 0)) {
        if ((((self.selectedIndex.section == 1 && self.objects1.count == 0) || (self.selectedIndex.section == 2 && self.objects1.count > 0)) && self.objects2.count > 0)) {
            self.selectedActivity = [self.objects2 objectAtIndex:self.selectedIndex.row];
            is_selected_score = YES;
        }else {
            self.selectedActivity = [self.objects3 objectAtIndex:self.selectedIndex.row];
        }
    }else {
        if (self.selectedIndex.section == 1 && self.objects1.count > 0) {
            self.selectedActivity = [self.objects1 objectAtIndex:self.selectedIndex.row];
        }else {
            self.selectedActivity = [self.objects4 objectAtIndex:self.selectedIndex.row];
        }
        return;
    }
    //HomeViewCell *cell = (HomeViewCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndex];
    
    if(!is_selected_score) {
        [self performSegueWithIdentifier:@"score" sender:self];
    } else {
        // Score
        if(self.selectedActivity.spots > 2) {
            [self performSegueWithIdentifier:@"teammate" sender:self];
        } else {
            [self performSegueWithIdentifier:@"score" sender:self];
        }
    }
}

@end
