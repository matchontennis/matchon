//
//  MessageViewController.m
//  wave
//
//  Created by Kevin Flynn on 8/25/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//


//
//  ViewController.m
//  DAKeyboardControlExample
//
//  Created by Daniel Amitay on 7/16/12.
//  Copyright (c) 2012 Daniel Amitay. All rights reserved.
//

#import "MessageViewController.h"
#import "MainViewController.h"
#import "DAKeyboardControl.h"
#import "MessageViewCell.h"
#import "DateTools.h"
#import "Constants.h"

@interface MessageViewController () <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate>

@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;

@end

@implementation MessageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.80f alpha:1.0f];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f,
                                                                           0.0f,
                                                                           self.view.bounds.size.width, self.view.bounds.size.height - 52.702f)];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:tableView];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0.0f,
                                                                     self.view.bounds.size.height - 52.702f,
                                                                     self.view.bounds.size.width,
                                                                     52.702f)];
    toolBar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:toolBar];
    
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(10.0f,
                                                                        10.0f,
                                                                        toolBar.bounds.size.width - 20.0f - 68.0f,
                                                                        32.702)];
    textView.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14.0f];
    textView.layer.borderColor = [UIColor colorWithWhite:0.7f alpha:1.0f].CGColor;
    textView.layer.borderWidth = 1.0f;
    textView.layer.cornerRadius = 2.0f;
    textView.clipsToBounds = YES;
    textView.text = @"Message";
    textView.textColor = [UIColor lightGrayColor]; //optional
    
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [toolBar addSubview:textView];
    
    UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    sendButton.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
    [sendButton setTitle:@"Send" forState:UIControlStateNormal];
    sendButton.layer.cornerRadius = 2.0f;
    sendButton.clipsToBounds = YES;
    sendButton.frame = CGRectMake(toolBar.bounds.size.width - 68.0f,
                                  10.0f,
                                  58.0f,
                                  30.0f);
    sendButton.backgroundColor  = THEME_COLOR;
    [sendButton setTitleColor:[UIColor colorWithWhite:1.0f alpha:1.0f] forState:UIControlStateNormal];
    
    [toolBar addSubview:sendButton];
    
    
    self.view.keyboardTriggerOffset = toolBar.bounds.size.height;
    
    __weak typeof(self) weakSelf = self;
    [self.view addKeyboardPanningWithFrameBasedActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        /*
         Try not to call "self" inside this block (retain cycle).
         But if you do, make sure to remove DAKeyboardControl
         when you are done with the view controller by calling:
         [self.view removeKeyboardControl];
         */
        CGRect toolBarFrame = toolBar.frame;
        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
        toolBar.frame = toolBarFrame;
        
        CGRect tableViewFrame = tableView.frame;
        tableViewFrame.size.height = toolBarFrame.origin.y;
        tableView.frame = tableViewFrame;
        int total = (int)weakSelf.messages.count;
        if(total > 0) {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(total - 1) inSection:0];
            [weakSelf.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    } constraintBasedActionHandler:nil];
    
    [textView addKeyboardPanningWithActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
        //NSLog(@"Pannding");
    }];
    
    self.toolBar = toolBar;
    
    self.tableView = tableView;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.separatorColor = [UIColor clearColor];
    [self.tableView registerClass:[MessageViewCell class] forCellReuseIdentifier:@"Cell"];
    [self.tableView registerClass:[MessageViewCell class] forCellReuseIdentifier:@"Me"];
    
    
    self.textView = textView;
    self.textView.delegate = self;
    self.sendButton = sendButton;
    [self.sendButton addTarget:self action:@selector(send:) forControlEvents:UIControlEventTouchUpInside];
    
    // Data resources
    self.messages = [NSMutableArray array];
    self.limit = 500;
    self.skip = 0;
    
    //NSDictionary *dimensions = @{ @"type": @"viewed"};
    // Send the dimensions to Parse along with the 'search' event
   // [PFAnalytics trackEvent:@"comments" dimensions:dimensions];
    
    if(self.activity != nil && ![self.activity isEqual:[NSNull null]] && self.activity.isDataAvailable) {
        [self loadObjects];
    } else {
        NSLog(@"Need to get it!!!");
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.scrolled = FALSE;
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc.badge removeFromSuperview];
    tc.badge = nil;
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.view hideKeyboard];
    [self.view removeKeyboardControl];
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc loadNewMessages];
    //[tc loadNewScores];
}

- (void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    NSLog(@"Will layout subviews");
    if(!self.scrolled) {
        NSLog(@"Will Scroll to botton with messages: %@",self.messages);

    }
    if((!self.scrolled && self.messages.count > 0)) {
        NSLog(@"Will Scroll to botton");
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.messages.count - 1) inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        self.scrolled = TRUE;
    }
}

- (void)viewDidLayoutSubviews {
    if((!self.scrolled && self.messages.count > 0)) {
        NSLog(@"Scroll to botton");
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.messages.count - 1) inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        self.scrolled = TRUE;
    }
}


#pragma mark - Helper 

- (void)activityFromPushWithId:(NSString *)objectId {
    self.activity = [ActivityModel objectWithoutDataWithObjectId:objectId];
}

#pragma mark - Query helpers

- (void)loadObjects{
    PFQuery *query = [self queryForTable];
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(whichPass > 1){
            [self.messages removeAllObjects];
        }
        if(self.messages.count == 0) {
            self.scrolled = FALSE;
            NSLog(@"Scroll to bottom");
        }
        [self.messages addObjectsFromArray:objects];
        [self sortMessages];
        [self setCellHeights];
        
        [self.tableView reloadData];
        whichPass += 1;
    }];
}


- (PFQuery *) queryForTable{
    PFQuery *query = [PFQuery queryWithClassName:@"Message"];
    query.cachePolicy = kPFCachePolicyNetworkOnly;
    if ([self.messages count] == 0) {
        NSLog(@"Cache then network");
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    
    if(self.activity) {
        [query whereKey:@"activity" equalTo:self.activity];
    } else {
        [query whereKey:@"recipients" containsAllObjectsInArray:self.recipients];
        //[query whereKey:@"totalRecipients" equalTo:[NSNumber numberWithInt:(int)self.recipients.count]];
    }
    [query includeKey:@"from"];
    [query includeKey:@"recipients"];
    
    [query orderByDescending:@"createdAt"];
    return query;
}


#pragma mark - UITableViewDelegate/UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // For reverse view
    if(cellHeights.count > indexPath.row){
        return [[cellHeights objectAtIndex:indexPath.row] floatValue];
    }
    return 60.0f;
}

- (MessageViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MessageViewCell *cell = (MessageViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    MessageModel *message = [self.messages objectAtIndex:indexPath.row];
    if([message.from.objectId isEqualToString:[PFUser currentUser].objectId]){
        
    }
    BOOL read = FALSE;
    for(PFUser *user in message.read){
        if([user.objectId isEqualToString:[PFUser currentUser].objectId]){
            read = TRUE;
            break;
        }
    }
    if(!read) {
        [message fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            MessageModel *m = (MessageModel *)object;
            if(!m.read){
                m.read = [NSMutableArray array];
            }
            [m.read addObject:[PFUser currentUser]];
            [m saveInBackground];
        }];
    }
    
    CGSize size = [MessageViewController sizeString:message.text withFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0f] andMax:CGSizeMake(252.0f,300.0f)];
    [cell.commentLabel setFrame:CGRectMake(cell.commentLabel.frame.origin.x, cell.commentLabel.frame.origin.y, size.width, size.height)];
    PFUser *sender = message.from;
    NSString *name = sender[@"name"];
    cell.senderLabel.text = name;
    cell.commentLabel.text = message.text;
    cell.timeLabel.text = message.createdAt.shortTimeAgoSinceNow;
    PFFile *file = sender[@"image"];
    cell.avatarView.file = file;
    /*if ([sender[@"female"] boolValue]) {
        cell.avatarView.image = [UIImage imageNamed:@"female"];
    }else {
        cell.avatarView.image = [UIImage imageNamed:@"male"];
    }*/
    cell.avatarView.image = [UIImage imageNamed:@"default_profile"];
    [cell.avatarView loadInBackground];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.messages.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if(!self.activity) {
        return 0.0f;
    }
    return 82.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 82.0f)];
    if(!self.activity || [self.activity isEqual:[NSNull null]] || !self.activity.isDataAvailable) {
        return nil;
    }
    
    view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 82.0f)];
    view.backgroundColor = [UIColor colorWithRed:119 / 255.0f green:121 / 255.0f blue:143 / 255.0f alpha:1.0f];
    
    UIImageView *imgTimeLogo = [[UIImageView alloc] initWithFrame:CGRectMake(15, 13, 16, 16)];
    imgTimeLogo.image = [UIImage imageNamed:@"dateCheckIcon_on"];
    [view addSubview:imgTimeLogo];
    
    UIImageView *imgLocLogo = [[UIImageView alloc] initWithFrame:CGRectMake(17, 33, 12, 16)];
    imgLocLogo.image = [UIImage imageNamed:@"locationSmallIcon_on"];
    [view addSubview:imgLocLogo];
    
    UIImageView *imgUsersLogo = [[UIImageView alloc] initWithFrame:CGRectMake(14, 54, 20, 15)];
    imgUsersLogo.image = [UIImage imageNamed:@"users_on"];
    [view addSubview:imgUsersLogo];
    
    NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
    
    if(self.activity.startTime.year != [NSDate date].year) {
        dateTimeFormatter.dateFormat = @"MMM d, YYYY @h:mm a";
    } else {
        dateTimeFormatter.dateFormat = @"MMM d @h:mm a";
    }
    
    NSString *datetime = [dateTimeFormatter stringFromDate: self.activity.startTime];
    
    UILabel *lblTime = [[UILabel alloc] initWithFrame:CGRectMake(36, 13, 160, 16)];
    lblTime.text = datetime;
    lblTime.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:15.0f];
    lblTime.textColor = [UIColor colorWithRed:224 / 255.0f green:225 / 255.0f blue:234 / 255.0f alpha:1.0f];
    [view addSubview:lblTime];
    
    UILabel *lblState = [[UILabel alloc] initWithFrame:CGRectMake(240, 12, 60, 20)];
    if (self.activity.full) {
        lblState.text = @"MATCH ON";
        lblState.textColor = [UIColor colorWithRed:153 / 255.0f green:185 / 255.0f blue:4 / 255.0f alpha:1.0f];
    }else {
        lblState.text = @"OPEN";
        lblState.textColor = [UIColor colorWithRed:232 / 255.0f green:42 / 255.0f blue:27 / 255.0f alpha:1.0f];
    }
    lblState.textAlignment = NSTextAlignmentRight;
    lblState.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:18.0f];
    
    [view addSubview:lblState];
    
    NSString *location = [NSString stringWithFormat:@"%@ %@, %@",
                          self.activity.location[@"Street"],
                          self.activity.location[@"City"],
                          self.activity.location[@"State"]];
    if (self.activity.court != nil && ![self.activity.court isEqualToString:@""]) {
        location = self.activity.court;
    }else if(!self.activity.location[@"Street"]) {
        location = [NSString stringWithFormat:@"%@, %@",
                    self.activity.location[@"City"],
                    self.activity.location[@"State"]];
    }else if(!self.activity.location[@"City"]) {
        location = @"";
    }
    
    UILabel *lblLocation = [[UILabel alloc] initWithFrame:CGRectMake(36, 34, 240, 16)];
    lblLocation.text = location;
    lblLocation.font = [UIFont fontWithName:@"MyriadPro-Cond" size:15.0f];
    lblLocation.textColor = [UIColor colorWithRed:224 / 255.0f green:225 / 255.0f blue:234 / 255.0f alpha:1.0f];
    [view addSubview:lblLocation];
    
    UILabel *lblPlayers = [[UILabel alloc] initWithFrame:CGRectMake(36, 54, 270, 16)];
    MessageModel *message = [self.messages lastObject];
    PFUser *first = [self.activity.recipients firstObject];
    if(message) {
        NSLog(@"Message!!!!");
        [PFObject fetchAllIfNeededInBackground:message.recipients block:^(NSArray *objects, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *names = @"";
                for (PFUser *recipient in objects) {
                    NSString *curName = [recipient objectForKey:@"name"];
                    if ([recipient.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        curName = @"Me";
                    }
                    if (![names isEqualToString:@""]) {
                        names = [names stringByAppendingString:@", "];
                    }
                    names = [names stringByAppendingString:curName];
                }
                NSMutableAttributedString *attrNames = [[NSMutableAttributedString alloc] initWithString:names];
                int curPos = 0;
                [self.activity.user fetchIfNeeded];
                for (PFUser *recipient in objects) {
                    NSString *curName = [recipient objectForKey:@"name"];
                    if ([recipient.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        curName = @"Me";
                    }
                    if (curPos > 0) {
                        curPos += 2;
                    }
                    if ([recipient.objectId isEqualToString:self.activity.user.objectId]) {
                        [attrNames addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:1.0f green:156 / 255.0f blue:0.0f alpha:1.0f]} range:NSMakeRange(curPos,curName.length)];
                        break;
                    }
                    curPos += curName.length;
                }
                lblPlayers.attributedText = attrNames;
            });
        }];
    } else if (first && ![first isEqual:[NSNull null]] && first.isDataAvailable) {
        NSLog(@"No message");
        [PFObject fetchAllIfNeededInBackground:self.activity.recipients block:^(NSArray *objects, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *names = @"";
                for (PFUser *recipient in objects) {
                    NSString *curName = [recipient objectForKey:@"name"];
                    if ([recipient.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        curName = @"Me";
                    }
                    if (![names isEqualToString:@""]) {
                        names = [names stringByAppendingString:@", "];
                    }
                    names = [names stringByAppendingString:curName];
                }
                NSMutableAttributedString *attrNames = [[NSMutableAttributedString alloc] initWithString:names];
                int curPos = 0;
                [self.activity.user fetchIfNeeded];
                for (PFUser *recipient in objects) {
                    NSString *curName = [recipient objectForKey:@"name"];
                    if ([recipient.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        curName = @"Me";
                    }
                    if (curPos > 0) {
                        curPos += 2;
                    }
                    if ([recipient.objectId isEqualToString:self.activity.user.objectId]) {
                        
                        [attrNames addAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:1.0f green:156 / 255.0f blue:0.0f alpha:1.0f]} range:NSMakeRange(curPos,curName.length)];
                        break;
                    }
                    curPos += curName.length;
                }
                lblPlayers.attributedText = attrNames;
            });
        }];
        //players.text = [NSString stringWithFormat:@"%@",[[self.activity.recipients valueForKey:@"name"] componentsJoinedByString:@", "]];
    }
    lblPlayers.font = [UIFont fontWithName:@"MyriadPro-Cond" size:15.0f];
    lblPlayers.textColor = [UIColor colorWithRed:224 / 255.0f green:225 / 255.0f blue:234 / 255.0f alpha:1.0f];
    [view addSubview:lblPlayers];
    
    /*UILabel *players = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 0.0f, self.view.frame.size.width - 20.0f, 40.0f)];
    MessageModel *message = [self.messages lastObject];
    PFUser *first = [self.activity.recipients firstObject];
    if(message) {
        NSLog(@"Message!!!!");
        [PFObject fetchAllIfNeededInBackground:message.recipients block:^(NSArray *objects, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                players.text = [NSString stringWithFormat:@"%@",[[objects valueForKey:@"name"] componentsJoinedByString:@", "]];
            });
        }];
    } else if (first && ![first isEqual:[NSNull null]] && first.isDataAvailable) {
        NSLog(@"No message");
        [PFObject fetchAllIfNeededInBackground:self.activity.recipients block:^(NSArray *objects, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                players.text = [NSString stringWithFormat:@"%@",[[objects valueForKey:@"name"] componentsJoinedByString:@", "]];
            });
        }];
        //players.text = [NSString stringWithFormat:@"%@",[[self.activity.recipients valueForKey:@"name"] componentsJoinedByString:@", "]];
    }
    players.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14.0f];
    players.lineBreakMode = NSLineBreakByWordWrapping;
    players.numberOfLines = 0;
    [view addSubview:players];
    players.textColor = [UIColor darkGrayColor];
    players.backgroundColor = [UIColor clearColor];
    UILabel *details = [[UILabel alloc] initWithFrame:CGRectMake(10.0f, 40.0f, self.view.frame.size.width - 20.0f, 40.0f)];
    
    
    
    NSString *location = [NSString stringWithFormat:@"%@ %@, %@",
                        self.activity.location[@"Street"],
                        self.activity.location[@"City"],
                        self.activity.location[@"State"]];
    if(!self.activity.location[@"Street"]) {
        location = [NSString stringWithFormat:@"%@, %@",
                    self.activity.location[@"City"],
                    self.activity.location[@"State"]];
    }
    if(!self.activity.location[@"City"]) {
        location = @"";
    }
    
    
    if(self.activity.startTime.year != [NSDate date].year) {
        dateTimeFormatter.dateFormat = @"MMM d, YYYY @h:mm a";
    } else {
        dateTimeFormatter.dateFormat = @"MMM d @h:mm a";
    }
    
    //NSString *datetime = [dateTimeFormatter stringFromDate: self.activity.startTime];
    details.text = [NSString stringWithFormat:@"Time: %@\rLocation: %@",datetime, location];
    details.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14.0f];
    details.lineBreakMode = NSLineBreakByWordWrapping;
    details.numberOfLines = 2;
    details.textColor = [UIColor grayColor];
    details.backgroundColor = [UIColor clearColor];
    
    [view addSubview:details];*/
    
    return view;
}

#pragma mark - Helper methods

+ (CGSize)sizeString: (NSString *)caption withFont:(UIFont *)font andMax:(CGSize)max{
    NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey: NSFontAttributeName];
    // Expected size of txt
    return [caption boundingRectWithSize:max options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
}



#pragma mark - UITextView Delegate


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Message"]) {
        textView.text = @"";
        self.edited = TRUE;
        textView.textColor = [UIColor darkGrayColor];
    }
    [textView becomeFirstResponder];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@""]) {
        textView.text = @"Message";
        self.edited = FALSE;
        textView.textColor = [UIColor lightGrayColor]; //optional
    }
    [textView resignFirstResponder];
}

- (void)textViewDidChange:(UITextView *)textView {
    
    [self _updateInputViewFrameWithKeyboardFrame];
}

- (void)_updateInputViewFrameWithKeyboardFrame
{
    // Calculate the height the input view ideally
    // has based on its textview's content
    UITextView *textView = self.textView;
    
    CGFloat newInputViewHeight;
    if ([NSURLSession class])
    {
        newInputViewHeight = textViewHeight(textView);
    } else {
        newInputViewHeight = self.textView.contentSize.height;
    }
    
    //10 is the border of the uitoolbar top and bottom
    //newInputViewHeight += 10;
    NSLog(@"New input height: %f",newInputViewHeight);
    
    newInputViewHeight = ceilf(newInputViewHeight);
    //newInputViewHeight = MIN(maxInputViewHeight, newInputViewHeight);
    
    // If the new input view height equals the current,
    // nothing has to be changed
    if (self.textView.bounds.size.height == newInputViewHeight) {
        return;
    }
    // If the new input view height is bigger than the view available, do nothing
    if ((self.view.bounds.size.height - self.view.keyboardFrameInView.size.height < newInputViewHeight)) {
        return;
    }
    
    CGRect inputViewFrame = self.textView.frame;
    inputViewFrame.size.height = newInputViewHeight;
    self.textView.frame = inputViewFrame;
    
    CGRect toolBarFrame = self.toolBar.frame;
    toolBarFrame.size.height = newInputViewHeight + 20;
    toolBarFrame.origin.y = self.view.keyboardFrameInView.origin.y - toolBarFrame.size.height;
    self.toolBar.frame = toolBarFrame;
    self.view.keyboardTriggerOffset = self.toolBar.bounds.size.height;
}

static inline CGFloat textViewHeight(UITextView *textView) {
    NSTextContainer *textContainer = textView.textContainer;
    CGRect textRect =
    [textView.layoutManager usedRectForTextContainer:textContainer];
    
    CGFloat textViewHeight = textRect.size.height +
    textView.textContainerInset.top + textView.textContainerInset.bottom;
    
    return textViewHeight;
}


#pragma mark - IBActions

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)send:(id)sender {
    NSString *text = [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(text.length == 0)return;
    if(!self.edited)return;
    
    self.messageBackgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.messageBackgroundTask];
    }];
    
    if(!self.activity.messageRecipients) {
        self.activity.messageRecipients = [[NSMutableArray alloc] initWithArray:self.recipients];
    }
    BOOL add = TRUE;
    for(PFUser *user in self.activity.messageRecipients) {
        if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
            add = FALSE;
        }
    }
    if(add) {
        [self.activity.messageRecipients addObject:[PFUser currentUser]];
    }
    
    
    MessageModel *message = [MessageModel object];
    message.from = [PFUser currentUser];
    message.recipients = self.activity.messageRecipients;
    //message.totalRecipients = (int)self.activity.messageRecipients.count;
    message.activity = self.activity?self.activity:NULL;
    message.text = text;
    message.read = [[NSMutableArray alloc] initWithArray:@[[PFUser currentUser]]];
    [message saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        PFQuery *pushQuery = [PFInstallation query];
        NSMutableArray *recipients = [NSMutableArray array];
        for(PFUser *user in message.recipients){
            if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
            [recipients addObject:user];
        }
        NSLog(@"Test");
        [pushQuery whereKey:@"user" containedIn:recipients];
        NSLog(@"Test2");
        
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"alert": [NSString stringWithFormat:@"New message from %@", [PFUser currentUser][@"name"]], @"badge" : @"Increment", @"m": message.objectId}];
        if(self.activity) {
            data[@"a"] = self.activity.objectId;
        }
        
        NSLog(@"Send push");
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];
    }];
    
    [self.messages addObject:message];
    [self setCellHeights];
    [self.tableView reloadData];
    
    //[self addMessage];
    self.textView.text = @"";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.messages.count - 1) inSection:0];
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    [self _updateInputViewFrameWithKeyboardFrame];
}


- (void)sortMessages {
    self.messages = [[NSMutableArray alloc] initWithArray:[self.messages sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        PFObject *firstObject = (PFObject *)a;
        PFObject *secondObject = (PFObject *)b;
        NSDate *first = firstObject.createdAt;
        NSDate *second = secondObject.createdAt;
        return [first compare:second];
    }]];
}

- (void)setCellHeights{
    cellHeights = [NSMutableArray array];
    for(PFObject *comment in self.messages){
        CGSize size = [MessageViewController sizeString:comment[@"text"] withFont:[UIFont fontWithName:@"MyriadPro-Regular" size:14.0f] andMax:CGSizeMake(252.0f,300.0f)];
        [cellHeights addObject:[NSNumber numberWithFloat:(size.height + 54.0f)]];
    }
}


@end

