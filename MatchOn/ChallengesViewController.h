//
//  ChallengesViewController.h
//  MatchOn
//
//  Created by Sol on 5/27/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChallengeModel;
@interface ChallengesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *challenges;
    ChallengeModel *selectedChallenge;
}

@property (weak, nonatomic) IBOutlet UITableView *tblChallenges;

@property (nonatomic,assign) BOOL pullToRefreshEnabled;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, assign) BOOL endOfResults;

// Query config
@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;

@property (nonatomic, assign) BOOL noResults;

@end
