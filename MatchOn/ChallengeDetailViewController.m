//
//  ChallengeDetailViewController.m
//  MatchOn
//
//  Created by Sol on 6/4/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "ChallengeDetailViewController.h"
#import "MainViewController.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "RatingModel.h"
#import "Constants.h"
#import "ChallengeModel.h"
#import "ChallengePointModel.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "OtherProfileViewController.h"
#import <AddressBook/ABPerson.h>
#import <AddressBook/AddressBook.h>

@interface ChallengeDetailViewController ()

@property (nonatomic, assign) ABAddressBookRef addressBook;

@end

@implementation ChallengeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    descExpanded = NO;
    self.btnJoin.layer.cornerRadius = 2.0f;
    self.btnJoin.titleEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
    self.btnJoin.layer.borderColor = THEME_COLOR.CGColor;
    self.btnJoin.layer.borderWidth = 1.0f;
    [self.segType setTitleTextAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Regular" size:12.0f]} forState:UIControlStateNormal];
    if (self.challenge != nil && ![self.challenge isEqual:[NSNull null]] && self.challenge.isDataAvailable) {
        self.imgLogo.file = self.challenge.logo;
        self.imgLogo.image = [UIImage imageNamed:@"challenge_logo"];
        [self.imgLogo loadInBackground];
        self.navigationItem.title = self.challenge.name;
        [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
        self.lblChName.text = [self.challenge.name uppercaseString];
        if (self.challenge.long_description != nil && ![self.challenge.long_description isEqualToString:@""]) {
            self.lblDesc.text = [NSString stringWithFormat:@"DESCRIPTION: %@",self.challenge.long_description];
        }
        if (self.challenge.ongoing) {
            self.lblDate.text = @"ongoing";
        }else if (self.challenge.starttime != nil && self.challenge.endtime != nil) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"M.dd.yy"];
            self.lblDate.text = [NSString stringWithFormat:@"Starts: %@    Ends: %@",[formatter stringFromDate:self.challenge.starttime],[formatter stringFromDate:self.challenge.endtime]];
        }else {
            self.lblDate.text = @"";
        }
    }
    
    if (self.challenge.players != nil && ![self.challenge.players isEqual:[NSNull null]]) {
        NSMutableArray *players = self.challenge.players;
        int i = 0;
        for (i = 0; i < players.count; i++) {
            PFUser *player = (PFUser *)[players objectAtIndex:i];
            if ([player.objectId isEqualToString:[PFUser currentUser].objectId]) {
                self.btnJoin.hidden = YES;
                break;
            }
        }
        if (i == players.count) {
            self.btnJoin.hidden = NO;
        }
    }
    //self.refreshControl = [[UIRefreshControl alloc] init];
    //[self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    //[self.tblPlayers addSubview:self.refreshControl];
    
    [self.segType setSelectedSegmentIndex:0];
    self.limit = 100;
    self.skip = 0;
    self.viewDetail.layer.borderColor = [UIColor colorWithRed:193 / 255.0f green:196 / 255.0f blue:214 / 255.0f alpha:1.0f].CGColor;
    self.viewDetail.layer.borderWidth = 1.0f;
    
    [self refreshInbox];
    
}

- (void)refreshInbox {
    if(![PFUser currentUser]) {
        [ratings removeAllObjects];
        [chpoints removeAllObjects];
        [self.tblPlayers reloadData];
        return;
    }
    self.endOfResults = NO;
    self.pullToRefreshEnabled = TRUE;
    self.skip = 0;
    [self loadObjects];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)back:(id)sender {
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc loadNewMessages];
    //[tc loadNewScores];
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)onTypeChanged:(id)sender {
    if (self.segType.selectedSegmentIndex == 0) {
        [self refreshInbox];
    }else {
        if (self.emails != nil) {
            [self refreshInbox];
        }else {
            [self loadAddressBook];
            /*if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
                [PFFacebookUtils logInWithPermissions:@[@"public_profile",@"email",@"user_friends"] block:^(PFUser *user, NSError *error) {
                    if (error) {
                        return;
                    }
                    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        if (error) {
                            [self refreshInbox];
                            return;
                        }
                        NSArray *friendObjects = [result objectForKey:@"data"];
                        friendIds = [NSMutableArray array];
                        for (NSDictionary *friendObject in friendObjects) {
                            [friendIds addObject:[friendObject objectForKey:@"id"]];
                        }
                        [self refreshInbox];
                    }];
                }];
            }*/
        }
    }
}

- (IBAction)onJoin:(id)sender {
    if (self.challenge) {
        NSMutableArray *players = [NSMutableArray array];
        if (self.challenge.players != nil && ![self.challenge.players isEqual:[NSNull null]]) {
            players = [NSMutableArray arrayWithArray:self.challenge.players];
        }
        NSLog(@"%@",players);
        int i = 0;
        for (i = 0; i < players.count; i++) {
            PFUser *player = (PFUser *)[players objectAtIndex:i];
            if ([player.objectId isEqualToString:[PFUser currentUser].objectId]) {
                break;
            }
        }
        if (i == players.count) {
            [players addObject:[PFUser currentUser]];
            self.challenge.players = players;
            NSLog(@"%@",self.challenge);
            [self.challenge saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error) {
                    NSLog(@"%@",error);
                    return;
                }
                if (self.challenge.ongoing) {
                    [self refreshInbox];
                    self.btnJoin.hidden = YES;
                    return;
                }
                [PFCloud callFunctionInBackground:@"Join" withParameters:@{@"chId":self.challenge.objectId} block:^(id object, NSError *error) {
                    if (error) {
                        NSLog(@"%@",error);
                        return;
                    }
                    [self refreshInbox];
                    self.btnJoin.hidden = YES;
                }];
            }];
        }
    }
}

- (IBAction)onMore:(id)sender {
    descExpanded = !descExpanded;
    CGSize allSize = CGSizeMake(self.viewDesc.frame.size.width, 65);
    CGFloat nHeight = 40;
    
    if (descExpanded) {
        //allSize = [self.lblDesc.text boundingRectWithSize:CGSizeMake(self.lblDesc.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:self.lblDesc.font} context:nil].size;
        nHeight = [self getLabelHeight:self.lblDesc];
        if (nHeight < 40)
        {
            nHeight = 40.0f;
        }
        [self.btnMore setBackgroundImage:[UIImage imageNamed:@"arrow_up"] forState:UIControlStateNormal];
    }else {
        [self.btnMore setBackgroundImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
    }
    NSLog(@"%f",nHeight);
    CGRect frame = self.viewDesc.frame;
    //frame.size.height = allSize.height;
    frame.size.height = nHeight+25;
    self.viewDesc.translatesAutoresizingMaskIntoConstraints = YES;
    self.viewDesc.frame = frame;
    //frame = self.lblDesc.frame;
    //frame.size.height = allSize.height;
    //self.lblDesc.translatesAutoresizingMaskIntoConstraints = YES;
    //self.lblDesc.frame = frame;
    self.lblDesc.numberOfLines = 0;
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    //CGSize constraint = CGSizeMake(label.frame.size.width, 20000.0f);
    CGSize constraint = CGSizeMake([UIScreen mainScreen].bounds.size.width-18, 20000.0f);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"profile"]) {
        OtherProfileViewController *vc = [segue destinationViewController];
        vc.curUser = selectedUser;
    }
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 15.0f)];
    viewHeader.backgroundColor = [UIColor whiteColor];
    UILabel *lblRank = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 42, 15.0f)];
    lblRank.text = @"RANK";
    lblRank.textAlignment = NSTextAlignmentCenter;
    lblRank.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12.0f];
    lblRank.textColor = [UIColor colorWithRed:157 / 255.0f green:159 / 255.0f blue:176 / 255.0f alpha:1.0f];
    
    UILabel *lblPlayer = [[UILabel alloc] initWithFrame:CGRectMake(84, 0, 75, 15.0f)];
    lblPlayer.text = @"PLAYER";
    lblPlayer.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12.0f];
    lblPlayer.textAlignment = NSTextAlignmentCenter;
    lblPlayer.textColor = [UIColor colorWithRed:157 / 255.0f green:159 / 255.0f blue:176 / 255.0f alpha:1.0f];
    
    UILabel *lblPts = [[UILabel alloc] initWithFrame:CGRectMake(244, 0, 56, 15.0f)];
    lblPts.text = @"PTS";
    lblPts.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12.0f];
    lblPts.textAlignment = NSTextAlignmentCenter;
    lblPts.textColor = [UIColor colorWithRed:157 / 255.0f green:159 / 255.0f blue:176 / 255.0f alpha:1.0f];
    
    [viewHeader addSubview:lblRank];
    [viewHeader addSubview:lblPlayer];
    [viewHeader addSubview:lblPts];
    
    CALayer *headerBorder = [[CALayer alloc] init];
    headerBorder.frame = CGRectMake(0, viewHeader.frame.size.height - 1.0f, viewHeader.frame.size.width, 1.0f);
    headerBorder.backgroundColor = [UIColor colorWithRed:224 / 255.0f green:225 / 255.0f blue:234 / 255.0f alpha:1.0f].CGColor;
    [viewHeader.layer addSublayer:headerBorder];
    return viewHeader;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.segType.selectedSegmentIndex == 1) {
        if (self.challenge.ongoing) {
            return ratings.count;
        }else {
            return chpoints.count;
        }
    }else {
        if (lastIndex > 7) {
            if (self.challenge.ongoing) {
                return filteredRatings.count + 1;
            }else {
                return filteredChPoints.count + 1;
            }
        }else {
            if (self.challenge.ongoing) {
                return filteredRatings.count;
            }else {
                return filteredChPoints.count;
            }
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    RatingModel *rating;
    ChallengePointModel *chpoint;
    if (self.segType.selectedSegmentIndex == 1) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];
        if (self.challenge.ongoing) {
            rating = (RatingModel *)[ratings objectAtIndex:indexPath.row];
        }else {
            chpoint = (ChallengePointModel *)[chpoints objectAtIndex:indexPath.row];
        }
        
    }else {
        if (lastIndex > 7) {
            if (indexPath.row == 5) {
                cell = [[UITableViewCell alloc] init];
                cell.textLabel.text = @"⋮";
                return cell;
            }else if (indexPath.row < 5) {
                cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];
                if (self.challenge.ongoing) {
                    rating = (RatingModel *)[filteredRatings objectAtIndex:indexPath.row];
                }else {
                    chpoint = (ChallengePointModel *)[filteredChPoints objectAtIndex:indexPath.row];
                }
                
            }else {
                cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];
                if (self.challenge.ongoing) {
                    rating = (RatingModel *)[filteredRatings objectAtIndex:indexPath.row - 1];
                }else {
                    chpoint = (ChallengePointModel *)[filteredChPoints objectAtIndex:indexPath.row - 1];
                }
                
            }
        }else {
            cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];
            if (self.challenge.ongoing) {
                rating = (RatingModel *)[filteredRatings objectAtIndex:indexPath.row];
            }else {
                chpoint = (ChallengePointModel *)[filteredChPoints objectAtIndex:indexPath.row];
            }
            
        }
    }
    PFUser *player;
    if (self.challenge.ongoing) {
        player = (PFUser *)rating.user;
    }else {
        player = (PFUser *)chpoint.user;
    }
    UILabel *lblRank = (UILabel *)[cell viewWithTag:10];
    if (self.segType.selectedSegmentIndex == 1) {
        lblRank.text = [NSString stringWithFormat:@"%d",(int)indexPath.row + 1];
    }else {
        if (lastIndex > 7) {
            if (indexPath.row < 5) {
                lblRank.text = [NSString stringWithFormat:@"%d",(int)indexPath.row + 1];
            }else {
                lblRank.text = [NSString stringWithFormat:@"%d",lastIndex + (int)indexPath.row - 7];
            }
        }else {
            lblRank.text = [NSString stringWithFormat:@"%d",(int)indexPath.row + 1];
        }
    }
    
    PFImageView *imgPhoto = (PFImageView *)[cell viewWithTag:11];
    /*if([player[@"female"] boolValue]) {
        imgPhoto.image = [UIImage imageNamed:@"female"];
    } else {
        imgPhoto.image = [UIImage imageNamed:@"male"];
    }*/
    PFFile *file = [player objectForKey:@"image"];
    imgPhoto.file = file;
    imgPhoto.image = [UIImage imageNamed:@"default_profile"];
    [imgPhoto loadInBackground];
    
    imgPhoto.clipsToBounds = YES;
    imgPhoto.layer.cornerRadius = imgPhoto.frame.size.width / 2;
    imgPhoto.clipsToBounds = YES;
    imgPhoto.layer.borderWidth = 2.0f;
    imgPhoto.layer.borderColor = [UIColor colorWithRed:39 / 255.0f green:42 / 255.0f blue:56 / 255.0f alpha:1.0f].CGColor;
    
    UILabel *lblName = (UILabel *)[cell viewWithTag:12];
    
    if ([player.objectId isEqualToString:[PFUser currentUser].objectId]) {
        lblRank.textColor = THEME_COLOR;
        lblName.textColor = THEME_COLOR;
        lblName.text = @"Me";
    }else {
        NSString *first = [player objectForKey:@"first"];
        NSString *last = [player objectForKey:@"last"];
        
        if(!first) {
            NSArray *name = [player[@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        lblName.text = first;
        if(last && last.length > 0) {
            lblName.text = [NSString stringWithFormat:@"%@ %@",lblName.text, last];
        }
        lblRank.textColor = [UIColor colorWithRed:52 / 255.0f green:53 / 255.0f blue:63 / 255.0f alpha:1.0f];
        lblName.textColor = [UIColor colorWithRed:52 / 255.0f green:53 / 255.0f blue:63 / 255.0f alpha:1.0f];
    }
    UILabel *lblPts = (UILabel *)[cell viewWithTag:13];
    lblPts.text = @"";
    if ([self.challenge.compare_field isEqualToString:@"point"]) {
        if (self.challenge.ongoing) {
            lblPts.text = [NSString stringWithFormat:@"%d",rating.score];
        }else {
            lblPts.text = [NSString stringWithFormat:@"%d",chpoint.point];
        }
        
    }
    
    return cell;
}

- (void)loadObjects{
    PFQuery *query = [self queryForTable];
    if(self.pullToRefreshEnabled) {
        self.skip = 0;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(objects.count < self.limit) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [ratings removeAllObjects];
            [chpoints removeAllObjects];
        }
        if(whichPass > 1) {
            [ratings removeAllObjects];
            [chpoints removeAllObjects];
        }
        if (ratings == nil && objects != nil) {
            ratings = [NSMutableArray array];
        }
        if (chpoints == nil && objects != nil) {
            chpoints = [NSMutableArray array];
        }
        NSLog(@"%@",objects);
        int i = 0;
        if (self.challenge.ongoing) {
            [ratings addObjectsFromArray:objects];
            
            if(ratings.count == 0) {
                if(!self.noResults) {
                    self.noResults = TRUE;
                }
            } else {
                self.noResults = FALSE;
            }
            for (i = 0; i < objects.count; i++) {
                RatingModel *rating = [objects objectAtIndex:i];
                PFUser *curUser = rating.user;
                if ([curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    break;
                }
            }
        }else {
            [chpoints addObjectsFromArray:objects];
            
            if(chpoints.count == 0) {
                if(!self.noResults) {
                    self.noResults = TRUE;
                }
            } else {
                self.noResults = FALSE;
            }
            
            for (i = 0; i < objects.count; i++) {
                ChallengePointModel *chpoint = [objects objectAtIndex:i];
                PFUser *curUser = chpoint.user;
                if ([curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    break;
                }
            }
        }
        
        self.pullToRefreshEnabled = FALSE;
        whichPass = whichPass + 1;
        
        if (self.segType.selectedSegmentIndex == 1) {
            [self.tblPlayers reloadData];
        }else {
            if (i >= objects.count - 1) {
                if (self.endOfResults) {
                    [self filterRatings];
                    
                }else {
                    [self loadNextPage];
                }
            }else {
                self.endOfResults = YES;
                [self filterRatings];
                //[self.tblPlayers reloadData];
            }
        }
        
    }];
}


#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    if(![PFUser currentUser])return nil;
    
    if (self.challenge.ongoing) {
        if ([self.challenge.compare_field isEqualToString:@"point"]) {
            PFQuery *query = [PFUser query];
            //[query includeKey:@"rating"];
            if (self.segType.selectedSegmentIndex == 1) {
                PFQuery *query1 = [self queryForAppFriends];
                PFQuery *query2 = [self queryForFBFriends];
                PFQuery *query3 = [self queryForCurUser];
                NSMutableArray *allQueries = [NSMutableArray array];
                if (query1 != nil) {
                    [allQueries addObject:query1];
                }
                if (query2 != nil) {
                    [allQueries addObject:query2];
                }
                if (query3 != nil) {
                    [allQueries addObject:query3];
                }
                if (allQueries.count == 0) {
                    return nil;
                }
                query = [PFQuery orQueryWithSubqueries:allQueries];
            }
            PFQuery *innerQuery = [PFQuery queryWithClassName:@"Rating"];
            [innerQuery orderByDescending:@"score"];
            [innerQuery includeKey:@"user"];
            [innerQuery whereKey:@"user" matchesQuery:query];
            if (self.challenge.players != nil && ![self.challenge.players isEqual:[NSNull null]]) {
                [innerQuery whereKey:@"user" containedIn:self.challenge.players];
            }else {
                return nil;
            }
            //[query orderByAscending:@"last"];
            return innerQuery;
        }
        
    }else {
        if ([self.challenge.compare_field isEqualToString:@"point"]) {
            PFQuery *query = [PFUser query];
            if (self.segType.selectedSegmentIndex == 1) {
                PFQuery *query1 = [self queryForAppFriends];
                PFQuery *query2 = [self queryForFBFriends];
                PFQuery *query3 = [self queryForCurUser];
                NSMutableArray *allQueries = [NSMutableArray array];
                if (query1 != nil) {
                    [allQueries addObject:query1];
                }
                if (query2 != nil) {
                    [allQueries addObject:query2];
                }
                if (query3 != nil) {
                    [allQueries addObject:query3];
                }
                if (allQueries.count == 0) {
                    return nil;
                }
                query = [PFQuery orQueryWithSubqueries:allQueries];
            }
            PFQuery *innerQuery = [PFQuery queryWithClassName:@"ChallengePoint"];
            [innerQuery orderByDescending:@"point"];
            [innerQuery includeKey:@"user"];
            [innerQuery whereKey:@"user" matchesQuery:query];
            [innerQuery whereKey:@"challenge" equalTo:self.challenge];
            if (self.challenge.players != nil && ![self.challenge.players isEqual:[NSNull null]]) {
                [innerQuery whereKey:@"user" containedIn:self.challenge.players];
            }else {
                return nil;
            }
            //[query orderByAscending:@"last"];
            return innerQuery;
        }
    }
    return nil;
}

- (PFQuery *)queryForAppFriends {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query1 = [PFUser query];
    PFUser *currentUser = [PFUser currentUser];
    NSMutableArray *playerIds = [[NSMutableArray alloc] init];
    NSArray *players = [currentUser objectForKey:@"players"];
    if (players != nil) {
        for (int i = 0; i < players.count; i++) {
            PFUser *player = (PFUser *)[players objectAtIndex:i];
            [playerIds addObject:player.objectId];
        }
    }
    [query1 whereKey:@"objectId" containedIn:playerIds];
    PFQuery *query2 = [PFUser query];
    [query2 whereKey:@"players" equalTo:[PFUser currentUser]];
    PFQuery *query;
    if (self.emails != nil) {
        PFQuery *query3 = [PFUser query];
        [query3 whereKey:@"email" containedIn:self.emails];
        query = [PFQuery orQueryWithSubqueries:@[query1,query2,query3]];
    }else {
        query = [PFQuery orQueryWithSubqueries:@[query1,query2]];
    }
    return query;
}

- (PFQuery *)queryForCurUser {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"objectId" equalTo:[PFUser currentUser].objectId];
    return query;
}

- (PFQuery *)queryForFBFriends {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query = [PFUser query];
    if (friendIds != nil && friendIds.count > 0) {
        [query whereKey:@"fbId" containedIn:friendIds];
    }else {
        return nil;
    }
    return query;
}

/*- (PFQuery *)queryForFBFriends {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query = [PFUser query];
    if (friendIds != nil && friendIds.count > 0) {
        [query whereKey:@"fbId" containedIn:friendIds];
    }else {
        return nil;
    }
    return query;
}*/

- (void)loadNextPage{
    self.skip += self.limit;
    if(!self.endOfResults) {
        [self loadObjects];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;{
    if (self.challenge.ongoing) {
        if (!self.pullToRefreshEnabled &&  ratings.count > 0 && !self.endOfResults) {
            NSInteger total = (ratings.count - 2);
            if (indexPath.row == total)
            {
                if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
                {
                    [self loadNextPage];
                }
            }
        }
    }else {
        if (!self.pullToRefreshEnabled && chpoints.count > 0 && !self.endOfResults) {
            NSInteger total = (chpoints.count - 2);
            if (indexPath.row == total)
            {
                if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
                {
                    [self loadNextPage];
                }
            }
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RatingModel *rating;
    ChallengePointModel *chpoint;
    if (self.segType.selectedSegmentIndex == 0) {
        if (lastIndex > 7) {
            if (indexPath.row == 5) {
                rating = nil;
            }else if (indexPath.row < 5) {
                if (self.challenge.ongoing) {
                    rating = (RatingModel *)[filteredRatings objectAtIndex:indexPath.row];
                }else {
                    chpoint = (ChallengePointModel *)[filteredChPoints objectAtIndex:indexPath.row];
                }
                
            }else {
                if (self.challenge.ongoing) {
                    rating = (RatingModel *)[filteredRatings objectAtIndex:indexPath.row - 1];
                }else {
                    chpoint = (ChallengePointModel *)[filteredChPoints objectAtIndex:indexPath.row - 1];
                }
                
            }
        }else {
            if (self.challenge.ongoing) {
                rating = (RatingModel *)[filteredRatings objectAtIndex:indexPath.row];
            }else {
                chpoint = (ChallengePointModel *)[filteredChPoints objectAtIndex:indexPath.row];
            }
            
        }
    }else {
        if (self.challenge.ongoing) {
            rating = (RatingModel *)[ratings objectAtIndex:indexPath.row];
        }else {
            chpoint = (ChallengePointModel *)[chpoints objectAtIndex:indexPath.row];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (rating == nil && chpoint == nil) {
        return;
    }
    if (self.challenge.ongoing) {
        selectedUser = (PFUser *)rating.user;
    }else {
        selectedUser = (PFUser *)chpoint.user;
    }
    
    if ([selectedUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
        return;
    }
    [self performSegueWithIdentifier:@"profile" sender:nil];
}

-(void)filterRatings
{
    if (self.challenge.ongoing) {
        filteredRatings = [[NSMutableArray alloc] init];
        lastIndex = 0;
        for (int i = 0; i < ratings.count; i++) {
            RatingModel *rating = [ratings objectAtIndex:i];
            PFUser *curUser = rating.user;
            if (i < 5) {
                [filteredRatings addObject:rating];
                lastIndex = i;
            }else {
                RatingModel *prevRating = [ratings objectAtIndex:i - 1];
                PFUser *prevUser = prevRating.user;
                if ([prevUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    [filteredRatings addObject:rating];
                    lastIndex = i;
                }
                if (i < ratings.count - 1) {
                    RatingModel *nextRating = [ratings objectAtIndex:i + 1];
                    PFUser *nextUser = nextRating.user;
                    if ([nextUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        [filteredRatings addObject:rating];
                    }
                }
                if ([curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    [filteredRatings addObject:rating];
                }
            }
        }
    }else {
        filteredChPoints = [[NSMutableArray alloc] init];
        lastIndex = 0;
        for (int i = 0; i < chpoints.count; i++) {
            ChallengePointModel *chpoint = [chpoints objectAtIndex:i];
            PFUser *curUser = chpoint.user;
            if (i < 5) {
                [filteredChPoints addObject:chpoint];
                lastIndex = i;
            }else {
                ChallengePointModel *prevChPoint = [chpoints objectAtIndex:i - 1];
                PFUser *prevUser = prevChPoint.user;
                if ([prevUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    [filteredChPoints addObject:chpoint];
                    lastIndex = i;
                }
                if (i < chpoints.count - 1) {
                    ChallengePointModel *nextChPoint = [chpoints objectAtIndex:i + 1];
                    PFUser *nextUser = nextChPoint.user;
                    if ([nextUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        [filteredChPoints addObject:chpoint];
                    }
                }
                if ([curUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    [filteredChPoints addObject:chpoint];
                }
            }
        }
    }
    
    [self.tblPlayers reloadData];
}

#pragma mark Address Book Delegate

- (void)loadAddressBook{
    [self checkAddressBookAccess];
}

-(void)checkAddressBookAccess
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case  kABAuthorizationStatusAuthorized:
            [self accessGrantedForAddressBook];
            break;
            // Prompt the user for access to Contacts if there is no definitive answer
        case  kABAuthorizationStatusNotDetermined :
            [self requestAddressBookAccess];
            NSLog(@"Not determined");
            break;
            // Display a message if the user has denied or restricted access to Contacts
        case  kABAuthorizationStatusDenied:
            NSLog(@"Denied");
            break;
        case  kABAuthorizationStatusRestricted:
            NSLog(@"Restricted");
            break;
        default:
            break;
    }
}

// Prompt the user for access to their Address Book data
-(void)requestAddressBookAccess
{
    ChallengeDetailViewController * __weak weakSelf = self;
    
    ABAddressBookRequestAccessWithCompletion(self.addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [weakSelf accessGrantedForAddressBook];
                                                     });
                                                 }
                                             });
}


- (void)accessGrantedForAddressBook{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, ^{
        
        NSArray *originalArray = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(self.addressBook));
        self.emails = [NSMutableArray array];
        
        NSArray *abContactArray = [originalArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            ABRecordRef record1 = (__bridge ABRecordRef)obj1; // get address book record
            NSString *firstName1 = CFBridgingRelease(ABRecordCopyValue(record1, kABPersonFirstNameProperty));
            NSString *lastName1 = CFBridgingRelease(ABRecordCopyValue(record1, kABPersonLastNameProperty));
            
            ABRecordRef record2 = (__bridge ABRecordRef)obj2; // get address book record
            NSString *firstName2 = CFBridgingRelease(ABRecordCopyValue(record2, kABPersonFirstNameProperty));
            NSString *lastName2 = CFBridgingRelease(ABRecordCopyValue(record2, kABPersonLastNameProperty));
            
            NSString *compare1;
            if(firstName1) {
                compare1 = [firstName1 lowercaseString];
            }
            else {
                compare1 = [lastName1 lowercaseString];
            }
            NSString *compare2;
            if(firstName2) {
                compare2 = [firstName2 lowercaseString];
            }
            else {
                compare2 = [lastName2 lowercaseString];
            }
            
            NSComparisonResult result = [compare1  compare:compare2];
            if (result != NSOrderedSame) {
                return result;
            }
            else {
                return [lastName1 compare:lastName2];
            }
            
        }];
        NSMutableArray *contacts = [NSMutableArray array];
        for (id object in abContactArray) {
            ABRecordRef record = (__bridge ABRecordRef)object; // get address book record
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonLastNameProperty));
            
            
            ABRecordID recordID = ABRecordGetRecordID(record);
            NSNumber *recordInt = [NSNumber numberWithInt:recordID];
            
            NSMutableDictionary *data = [NSMutableDictionary dictionary];
            
            NSString *name = @"";
            if(firstName) {
                [data setObject:firstName forKey:@"first"];
                name = firstName;
            }
            if(lastName) {
                [data setObject:lastName forKey:@"last"];
                if (name.length > 0) {
                    name = [NSString stringWithFormat:@"%@ %@", name, lastName];
                }else{
                    name = lastName;
                }
            }
            if(name.length > 0) {
                [data setObject:name forKey:@"name"];
            }
            
            if(record) {
                [data setObject:recordInt forKey:@"recordId"];
            }
            
            if(!firstName && !lastName) {
                continue;
            }
            
            CFTypeRef emailProp = ABRecordCopyValue(record, kABPersonEmailProperty);
            NSMutableArray *emails = [[NSMutableArray alloc] initWithArray:(__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(emailProp)];
            CFRelease(emailProp);
            for(NSString *email in emails){
                [self.emails addObject:email];
            }
            
            [data setObject:emails forKey:@"emails"];
            
            
            CFTypeRef phoneProperty = ABRecordCopyValue(record, kABPersonPhoneProperty);
            NSMutableArray *phoneNumbers = (__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(phoneProperty);
            CFRelease(phoneProperty);
            
            
            for(int i = 0; i < phoneNumbers.count; i++) {
                NSString *phoneNumber = [phoneNumbers objectAtIndex:i];
                
                NSString *phoneStripped = [phoneNumber stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [phoneNumber length])];
                if([phoneStripped length] > 10 && [phoneStripped hasPrefix:@"1"]) {
                    // Subtring the one
                    phoneStripped = [phoneStripped substringFromIndex:1];
                }
                data[@"phone"] = phoneStripped;
                
            }
            [contacts addObject:data];
        }
        if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
            [PFFacebookUtils logInWithPermissions:@[@"public_profile",@"email",@"user_friends"] block:^(PFUser *user, NSError *error) {
                if (user) {
                    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        if (error) {
                            [self refreshInbox];
                            return;
                        }
                        NSArray *friendObjects = [result objectForKey:@"data"];
                        friendIds = [NSMutableArray array];
                        for (NSDictionary *friendObject in friendObjects) {
                            [friendIds addObject:[friendObject objectForKey:@"id"]];
                        }
                        [self refreshInbox];
                    }];
                }
            }];
            
        }else {
            [self refreshInbox];
        }
        //[self loadObjects];
        //[self queryContacts:contacts];
    });
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.delegate setJoin:self.challenge];
}

@end
