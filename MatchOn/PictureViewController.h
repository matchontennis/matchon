//
//  PictureViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "RatingModel.h"

@interface PictureViewController : UIViewController {
    NSString *curNote;
    CGPoint curOffset;
    UIImage *curImage;
    BOOL bAsked;
    UIBarButtonItem *curBarItem;
}

@property (nonatomic, strong) RatingModel *rating;

@property (strong, nonatomic) NSString *filePath;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIButton *photoButton;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *lblExclaim;
@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (weak, nonatomic) IBOutlet UIView *viewPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIView *viewHomeCourt;
@property (weak, nonatomic) IBOutlet UILabel *lblHomeCourt;
@property (weak, nonatomic) IBOutlet UITextView *txtAboutMe;
@property (nonatomic, strong) PFGeoPoint *geopt;
@property (nonatomic, strong) NSString *court;
@property (nonatomic, strong) NSDictionary *location;
@property (weak, nonatomic) IBOutlet UILabel *lblCourt;
@property (weak, nonatomic) IBOutlet UIView *viewPhotoBar;


- (IBAction)addImage:(id)sender;

- (IBAction)done:(id)sender;

- (IBAction)onback:(id)sender;
@end
