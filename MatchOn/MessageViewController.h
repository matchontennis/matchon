//
//  MessageViewController.h
//  wave
//
//  Created by Kevin Flynn on 8/25/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "MessageModel.h"
#import "ActivityModel.h"

@interface MessageViewController : UIViewController {
    NSMutableArray *cellHeights;
}

@property (nonatomic, strong) NSMutableArray *messages;
@property (nonatomic, strong) NSMutableArray *recipients;

@property (nonatomic, strong) MessageModel *message;
@property (nonatomic, strong) ActivityModel *activity;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UITableView *previousTable;

@property (nonatomic, assign) UIBackgroundTaskIdentifier messageBackgroundTask;

- (IBAction)back:(id)sender;

@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic, strong) IBOutlet UIButton *sendButton;

- (void)activityFromPushWithId:(NSString *)objectId;

@property (nonatomic, assign) BOOL edited;
@property (nonatomic, assign) BOOL scrolled;


@end
