//
//  MessageViewCell.m
//  wave
//
//  Created by Kevin Flynn on 8/26/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//

#import "MessageViewCell.h"

@implementation MessageViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        CGSize size = self.contentView.frame.size;
        CGRect frame = CGRectMake(10.0f, 10.0f, 40.0f, 40.0f);
        if([reuseIdentifier isEqualToString:@"Me"]) {
            //frame = CGRectMake(270.0f, 10.0f, 40.0f, 40.0f);
        }
        self.avatarView = [[PFImageView alloc] initWithFrame:frame];
        self.avatarView.clipsToBounds = YES;
        self.avatarView.layer.cornerRadius = self.avatarView.frame.size.width/2.0f;
        [self.contentView addSubview:self.avatarView];
        
        // Configure detail container
        self.senderLabel = [[UILabel alloc] initWithFrame:CGRectMake(58.0f, 10.0f, 252.0f-58.0f, 20.0f)];
        // Configure sender label
        self.senderLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:14.0f];
        self.senderLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.senderLabel];
        
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.senderLabel.frame.size.width+58.0f, 10.0f, 40.0f, 20.0f)];
        self.timeLabel.textColor = [UIColor lightGrayColor];
        self.timeLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:12.0f];
        self.timeLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.timeLabel];
        
        // Configure detail container
        self.commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(58.0f, 30.0f, size.width - 88.0f, 16.0f)];
        
        // Configure sender label
        self.commentLabel.textColor = [UIColor darkGrayColor];
        self.commentLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14.0f];
        self.commentLabel.textAlignment = NSTextAlignmentLeft;
        self.commentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.commentLabel.numberOfLines = 0;
        
        [self.contentView addSubview:self.commentLabel];
    }
    return self;
}



@end
