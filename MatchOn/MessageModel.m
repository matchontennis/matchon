//
//  MessageModel.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/3/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "MessageModel.h"

@implementation MessageModel

@dynamic from;
@dynamic recipients;
//@dynamic totalRecipients;
@dynamic read;
@dynamic activity;
@dynamic text;
@dynamic deleted;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Message";
}


@end
