//
//  InvitePopupViewController.h
//  MatchOn
//
//  Created by Sol on 4/18/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InvitePopupViewControllerDelegate

-(void)answerForInvite:(BOOL)result;

@end


@interface InvitePopupViewController : UIViewController

@property (nonatomic, strong) UIViewController *parent;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;

@property (weak, nonatomic) id<InvitePopupViewControllerDelegate> delegate;

- (IBAction)onYes:(id)sender;
- (IBAction)onNo:(id)sender;
@end
