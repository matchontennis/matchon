//
//  ChallengeDetailViewController.h
//  MatchOn
//
//  Created by Sol on 6/4/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChallengeModel, PFUser, PFImageView;

@protocol ChallengeDetailViewControllerDelegate <NSObject>

-(void)setJoin:(ChallengeModel *)challenge;

@end

@interface ChallengeDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *ratings;
    NSMutableArray *chpoints;
    NSMutableArray *friendIds;
    PFUser *selectedUser;
    NSMutableArray *filteredRatings;
    NSMutableArray *filteredChPoints;
    int lastIndex;
    BOOL descExpanded;
}

@property (nonatomic, weak) id<ChallengeDetailViewControllerDelegate> delegate;

@property (nonatomic, strong) NSMutableArray *emails;
@property (weak, nonatomic) IBOutlet UIButton *btnJoin;

@property (weak, nonatomic) IBOutlet UILabel *lblLeaderBoard;
@property (nonatomic, strong) ChallengeModel *challenge;
@property (weak, nonatomic) IBOutlet PFImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblChName;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIView *viewDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIView *viewDetail;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segType;
@property (weak, nonatomic) IBOutlet UITableView *tblPlayers;

@property (nonatomic,assign) BOOL pullToRefreshEnabled;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, assign) BOOL endOfResults;

// Query config
@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;

@property (nonatomic, assign) BOOL noResults;

-(IBAction)back:(id)sender;
- (IBAction)onTypeChanged:(id)sender;
- (IBAction)onJoin:(id)sender;
- (IBAction)onMore:(id)sender;
@end
