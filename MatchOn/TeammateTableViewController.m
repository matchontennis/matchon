//
//  TeammateTableViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/4/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "TeammateTableViewController.h"
#import "ScoreViewController.h"

@interface TeammateTableViewController ()

@end

@implementation TeammateTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView reloadData];
}


#pragma mark - Table view data source


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Select your teammate";
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.recipients.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    HomeViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    //cell.imageView.image = [UIImage imageNamed:@"profile"];
    PFUser *user = [self.recipients objectAtIndex:indexPath.row];
    PFFile *file = [user objectForKey:@"image"];
    cell.profileImage.file = file;
    cell.profileImage.image = [UIImage imageNamed:@"appIcon"];
    [cell.profileImage loadInBackground];
    cell.userLabel.text = user[@"name"];
    return cell;
}



#pragma mark - Table view delegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.teammate = [self.recipients objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"score" sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ScoreViewController *score = [segue destinationViewController];
    score.teammate = self.teammate;
    score.activity = self.activity;
}

@end
