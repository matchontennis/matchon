//
//  PasswordViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 1/22/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()

@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

- (IBAction)submit:(id)sender {
    if(self.emailField.text.length > 0) {
        UILabel *message = [[UILabel alloc] initWithFrame:self.emailField.frame];
        message.text = @"Password reset instructions will be sent to your email address.";
        message.textAlignment = NSTextAlignmentCenter;
        message.textColor = [UIColor whiteColor];
        message.numberOfLines = 0;
        message.lineBreakMode = NSLineBreakByWordWrapping;
        message.font = [UIFont fontWithName:@"MyriadPro-Regular" size:14.0];
        
        [PFUser requestPasswordResetForEmailInBackground:self.emailField.text];
        [self.emailField removeFromSuperview];
        [self.resetButton removeFromSuperview];
        [self.view addSubview:message];
    }
}

@end
