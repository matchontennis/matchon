//
//  LoginViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController () <UITextFieldDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.email.keyboardType = UIKeyboardTypeEmailAddress;
    self.email.delegate = self;
    self.password.delegate = self;
    
    self.viewEmail.layer.cornerRadius = 3.0f;
    self.viewEmail.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewEmail.layer.borderWidth = 1.0f;
    
    self.viewPassword.layer.cornerRadius = 3.0f;
    self.viewPassword.layer.borderColor = [UIColor whiteColor].CGColor;
    self.viewPassword.layer.borderWidth = 1.0f;
    
    self.btnLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnLogin.layer.borderWidth = 1.0f;
    self.btnLogin.layer.cornerRadius = 3.0f;
    self.btnLogin.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];    
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
}

- (IBAction)login:(id)sender {
    NSString *email = [[self.email.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    NSString *password = self.password.text;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([email length] == 0 || [password length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Make sure you enter your email and password" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alertView show];
    }
    else {
        [PFUser logInWithUsernameInBackground:email password:password block:^(PFUser *user, NSError *error) {
            if (!error) {
                if ([defaults objectForKey:@"existing"] == nil) {
                    [self performSegueWithIdentifier:@"help" sender:nil];
                }else {
                    [self.navigationController popToRootViewControllerAnimated:NO];
                }
                
            } else {
                NSString *errorString = [error userInfo][@"error"];
                // Show the errorString somewhere and let the user try again.
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            }
        }];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == self.email){
        [self.password becomeFirstResponder];
        return NO;
    }
    [self login:self];
    return YES;
}

- (IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

@end
