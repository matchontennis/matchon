//
//  PointsModel.h
//  MatchOn
//
//  Created by Kevin Flynn on 3/28/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>
#import "ActivityModel.h"

@interface PointsModel : PFObject <PFSubclassing>


@property (nonatomic, strong) ActivityModel *activity;
@property (nonatomic, strong) PFUser *user;
@property (nonatomic, assign) int total;

@end
