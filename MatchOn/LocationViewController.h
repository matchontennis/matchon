//
//  LocationViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/1/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>


@protocol LocationViewControllerDelegate <NSObject>

-(void)setLocation:(PFGeoPoint *)geopoint courtName:(NSString *)courtName;

@end


@interface LocationViewController : UIViewController <MKMapViewDelegate> {
    MKPointAnnotation *annotation;
    CLLocation *currentLocation;
}

@property (nonatomic, strong) NSString *courtName;
@property (nonatomic, strong) NSMutableArray *objects;

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIView *centerLocation;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) id<LocationViewControllerDelegate> delegate;

- (IBAction)done:(id)sender;
- (IBAction)cancel:(id)sender;


@property (nonatomic, strong) PFGeoPoint *geopt;


@end
