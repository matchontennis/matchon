//
//  InviteViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 2/12/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ActivityModel.h"

@protocol InviteViewControllerDelegate

-(void)dismissInviteViewController;

@end


@interface InviteViewController : UIViewController<UISearchBarDelegate, UISearchDisplayDelegate> {
    NSMutableArray *friendIds;
}


@property (weak, nonatomic) IBOutlet UITableView *contentView;
@property (weak, nonatomic) id<InviteViewControllerDelegate> delegate;

- (IBAction)cancel:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *inviteButton;

- (IBAction)invite:(id)sender;

@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic, strong) NSMutableArray *searchResults;
@property (nonatomic, strong) NSMutableArray *userSearchResults;
@property (nonatomic, strong) NSMutableArray *playerSearchResults;
@property (nonatomic, strong) NSMutableArray *contactSearchResults;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, strong) NSMutableArray *users;
@property (nonatomic, strong) NSMutableArray *players;
@property (nonatomic, strong) NSMutableSet *playersMap;
@property (nonatomic, strong) NSMutableDictionary *userInvites;
@property (nonatomic, strong) NSMutableDictionary *contactInvites;

@property (nonatomic,assign) BOOL pullToRefreshEnabled;
@property (nonatomic, assign) BOOL endOfResults;

// Query config
@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;


@property (nonatomic, strong) ActivityModel *activity;


// address
@property (nonatomic, strong) MFMessageComposeViewController *messageViewController;

@property (nonatomic, assign) BOOL signup;

@property (nonatomic, strong) NSMutableArray *emails;


@property (nonatomic, strong) NSString *link;

@property (weak, nonatomic) IBOutlet UIButton *btnFBLink;

@property (weak, nonatomic) IBOutlet UIButton *btnInviteContact;
@property (weak, nonatomic) IBOutlet UIButton *btnAllDone;
- (IBAction)onInviteContact:(id)sender;
- (IBAction)onAllDone:(id)sender;
- (IBAction)remind:(id)sender;

-(void)refreshInbox;
- (IBAction)onFacebookLink:(id)sender;

@end
