//
//  AppDelegate.h
//  MatchOn
//
//  Created by Kevin Flynn on 11/22/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "InviteViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong) InviteViewController *inviteViewController;
@property (nonatomic, strong) NSMutableArray *courts;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end

/*
- (void)handlePush:(NSDictionary *)launchOptions {
    // Extract the notification payload dictionary
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    // Check if the app was open from a notification and a user is logged in
    if (remoteNotificationPayload && [PFUser currentUser]) {
        
        // Push the referenced photo into view
        NSString *objectId = [remoteNotificationPayload objectForKey:@"a"];
        if (objectId && objectId.length != 0) {
            PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
            [query includeKey:@"recipients"];
            [query getObjectInBackgroundWithId:objectId block:^(PFObject *object, NSError *error) {
                if (!error) {
                    MessageViewController *m = [[MessageViewController alloc] init];
                    
                    UINavigationController *homeNavigationController = [[self.tabBarController viewControllers] objectAtIndex:0];
                    [self.tabBarController setSelectedViewController:homeNavigationController];
                    [homeNavigationController pushViewController:detailViewController animated:YES];
                }
            }];
        }
    }
}

*/
