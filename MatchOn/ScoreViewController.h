//
//  ScoreViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/4/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <MessageUI/MessageUI.h>
#import "ActivityModel.h"
#import "RatingModel.h"
#import "ScoreModel.h"
#import "PointsModel.h"

@interface ScoreViewController : UIViewController

@property (nonatomic, strong) ActivityModel *activity;
@property (nonatomic, strong) PFUser *teammate;
@property (weak, nonatomic) IBOutlet UIButton *btnJustHit;

@property (weak, nonatomic) IBOutlet UIView *firstTeamContainer;
@property (weak, nonatomic) IBOutlet UIView *secondTeamContainer;
@property (weak, nonatomic) IBOutlet UILabel *firstTeamLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondTeamLabel;
@property (weak, nonatomic) IBOutlet UIButton *rejectButton;
@property (weak, nonatomic) IBOutlet UIButton *approveButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

@property (nonatomic, copy) void (^scoreSaved)(ScoreModel *score);

@property (weak, nonatomic) IBOutlet UIView *feedbackView;
@property (weak, nonatomic) IBOutlet UIView *feedbackCard;
@property (weak, nonatomic) IBOutlet UIButton *xButton;

@property (nonatomic, strong) NSMutableArray *firstTeam;
@property (nonatomic, strong) NSMutableArray *secondTeam;
- (IBAction)save:(id)sender;
- (IBAction)reject:(id)sender;
- (IBAction)approve:(id)sender;
- (IBAction)back:(id)sender;
- (IBAction)feedback:(id)sender;
- (IBAction)justhit:(id)sender;


@property (nonatomic, strong) MFMailComposeViewController *mailViewController;


@property (nonatomic, strong) NSString *action;

@end
