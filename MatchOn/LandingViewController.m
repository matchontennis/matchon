//
//  LandingViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "LandingViewController.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import "MBProgressHUD.h"

@interface LandingViewController ()

@property (nonatomic, assign) BOOL loading;
@end

@implementation LandingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.signupButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.signupButton.layer.borderWidth = 1.0f;
    self.signupButton.layer.cornerRadius = 3.0f;
    self.signupButton.clipsToBounds = YES;
    
    self.loginButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.loginButton.layer.borderWidth = 1.0f;
    self.loginButton.layer.cornerRadius = 3.0f;
    self.loginButton.clipsToBounds = YES;
    
    self.btnFBLogin.layer.cornerRadius = 3.0f;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:TRUE];
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}

- (IBAction)onFacebookLogin:(id)sender {
    if (self.loading) {
        return;
    }
    self.loading = YES;
    __weak typeof (self) wself = self;
    PFUserResultBlock resultBlock = ^(PFUser *user, NSError *error) {
        __strong typeof(wself) sself = wself;
        sself.loading = NO;
        if (user) {
            [sself _loginDidSucceedWithUser:user];
        }else if (error) {
            [sself _loginDidFailWithError:error];
        }else {
            
        }
    };
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [PFFacebookUtils logInWithPermissions:@[@"public_profile",@"email",@"user_friends"] block:resultBlock];
}

- (void)_loginDidSucceedWithUser:(PFUser *)user {
    [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (error) {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return;
        }
        NSDictionary *userData = (NSDictionary *)result;
        NSString *fbId = userData[@"id"];
        NSString *first = userData[@"first_name"];
        NSString *last = userData[@"last_name"];
        NSString *gender = userData[@"gender"];
        NSString *name = [NSString stringWithFormat:@"%@ %@",first,last];
        NSString *email = userData[@"email"];
        [[PFUser currentUser] setObject:first forKey:@"first"];
        [[PFUser currentUser] setObject:last forKey:@"last"];
        [[PFUser currentUser] setObject:name forKey:@"name"];
        if ([gender isEqualToString:@"male"]) {
            [[PFUser currentUser] setObject:@(NO) forKey:@"female"];
        }else {
            [[PFUser currentUser] setObject:@(YES) forKey:@"female"];
        }
        
        [[PFUser currentUser] setObject:email forKey:@"email"];
        
        [[PFUser currentUser] setObject:fbId forKey:@"fbId"];
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",fbId]]];
            if (data == nil) {
                return;
            }
            [[PFUser currentUser] setObject:[PFFile fileWithData:data] forKey:@"image"];
            if ([PFUser currentUser].isNew) {
                [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    [self.navigationController popToRootViewControllerAnimated:NO];
                }];
            }else {
                [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    if ([defaults objectForKey:@"existing"] == nil) {
                        [self performSegueWithIdentifier:@"help" sender:nil];
                    }else {
                        [self.navigationController popToRootViewControllerAnimated:NO];
                    }
                }];
            }
        });
        
    }];
}

- (void)_loginDidFailWithError:(NSError *)error {
    if (error) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

@end
