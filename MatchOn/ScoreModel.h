//
//  ScoreModel.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/10/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>
#import "ActivityModel.h"

@interface ScoreModel : PFObject <PFSubclassing>


// Activity pointer
@property (nonatomic, strong) ActivityModel *activity;

// Scorer of match
@property (nonatomic, strong) PFUser *scorer;

// Reviewer of match score
//@property (nonatomic, strong) PFUser *reviewer;


// Dictionary - set - score
@property (nonatomic, strong) NSMutableDictionary *firstScore;
@property (nonatomic, strong) NSMutableDictionary *secondScore;

// Array of pointers
@property (nonatomic, strong) NSMutableArray *firstTeam;
@property (nonatomic, strong) NSMutableArray *secondTeam;

// Array of pointers
@property (nonatomic, strong) NSMutableArray *winner; // On users team
@property (nonatomic, strong) NSMutableArray *loser; // On opponents team

// Potential viewers - array of pointers
//@property (nonatomic, strong) NSMutableArray *reviewers;

// Team of scorers - array of pointers
//@property (nonatomic, strong) NSMutableArray *scorers;


// All players - array of pointers
@property (nonatomic, strong) NSMutableArray *players;

// Score is approved
@property (nonatomic, assign) BOOL approved;
//@property (nonatomic, assign) BOOL rejected;

- (void)createScoreForFirstScore:(NSMutableDictionary *)firstScore andSecondScore:(NSMutableDictionary *)secondScore withFirstTeam:(NSMutableArray *)firstTeam andSecondTeam:(NSMutableArray *)secondTeam andWinner:(NSMutableArray *)winner andLoser:(NSMutableArray *)loser withActivity:(ActivityModel *)activity;


@end
