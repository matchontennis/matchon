//
//  WaveConstants.h
//  wave
//
//  Created by Kevin Flynn on 4/27/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString *const MatchOnAppDelegateApplicationDidReceiveRemoteNotification;


#pragma mark - Colors

#define THEME_COLOR     [UIColor colorWithRed:246.0f/225.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:1.0]
#define BASE_COLOR      [UIColor colorWithRed:56.0f/225.0f green:56.0f/255.0f blue:58.0f/255.0f alpha:1.0]
#define OPAQUE_THEME     [UIColor colorWithRed:246.0f/225.0f green:139.0f/255.0f blue:31.0f/255.0f alpha:0.5]

#define LOCALYTICS_APP_KEY @"58691e9fdcdd4d71a2e7ce9-a58b278c-f287-11e4-b2d9-009c5fda0a25"
