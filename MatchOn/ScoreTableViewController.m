//
//  ScoreTableViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 10/23/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "ScoreTableViewController.h"
#import "MainViewController.h"
#import "HomeViewCell.h"
#import "DateTools.h"
#import "MessageViewController.h"
#import "TeammateTableViewController.h"
#import "ScoreViewController.h"
#import "Constants.h"


@interface ScoreTableViewController ()


@end

@implementation ScoreTableViewController


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshInbox) name:@"Scored" object:nil];
    
    // Config
    self.limit = 25;
    self.skip = 0;
    
    //self.objects = [NSMutableArray array];


    // Refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = [UIColor colorWithRed:0.8f green:0.8f blue:0.0f alpha:0.6f];
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    

    self.tableView.backgroundColor = [UIColor whiteColor];
    
    if(![PFUser currentUser]){
        [self performSegueWithIdentifier:@"landing" sender:self];
    }
    if(self.push) {
        [self performSegueWithIdentifier:@"score" sender:self];
    }
    
    [self refreshInbox];
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc loadNewMessages];
    //[tc loadNewScores];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.push = FALSE;
}


#pragma mark - Data for table

- (void)refreshInbox {
    if(![PFUser currentUser]) {
        [self.objects removeAllObjects];
        [self.tableView reloadData];
        return;
    }
    self.pullToRefreshEnabled = TRUE;
    self.skip = 0;
    [self loadObjects];
}

- (void)loadNextPage{
    self.skip += self.limit;
    if(!self.endOfResults) {
        [self loadObjects];
    }
}

- (void)loadObjects{
    PFQuery *query = [self queryForTable];
    if(self.pullToRefreshEnabled) {
        self.skip = 0;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(objects.count == 0) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [self.objects removeAllObjects];
        }
        if(whichPass > 1) {
            [self.objects removeAllObjects];
        }
        if (self.objects == nil && objects != nil) {
            self.objects = [NSMutableArray array];
        }
        [self.objects addObjectsFromArray:objects];
        
        /*PFUser *currentUser = [PFUser currentUser];
        NSMutableArray *players = [currentUser objectForKey:@"players"];
        for (int i = 0; i < self.objects.count; i++) {
            ActivityModel *activity = (ActivityModel *)[self.objects objectAtIndex:i];
            if (!activity.matchOn) {
                continue;
            }
            if (![players containsObject:activity.user]) {
                [players addObject:activity.user];
            }
            if ([activity.startTime compare:[NSDate date]] == NSOrderedDescending) {
                for (int j = 0; j < activity.recipients.count; j++) {
                    PFUser *user = (PFUser *)[activity.recipients objectAtIndex:j];
                    if (![players containsObject:user]) {
                        [players addObject:user];
                    }
                }
            }
        }
        if (players != nil) {
            [currentUser setObject:players forKey:@"players"];
            [currentUser saveInBackground];
        }*/
        
        if(self.objects.count == 0) {
            if(!self.noResults) {
                self.noResults = TRUE;
            }
        } else {
            self.noResults = FALSE;
        }
        [self.tableView reloadData];
        
        self.pullToRefreshEnabled = FALSE;
        whichPass = whichPass + 1;
    }];
}


#pragma mark - PFQueryTableViewController

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }else if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query orderByDescending:@"startTime"];
    [query whereKey:@"startTime" lessThan:[NSDate date]];
    [query includeKey:@"recipients"];
    [query includeKey:@"user"];
    [query includeKey:@"score"];
    [query includeKey:@"scorer"];
    [query includeKey:@"firstTeam"];
    [query includeKey:@"secondTeam"];
    [query includeKey:@"winner"];
    [query includeKey:@"loser"];
    //[query includeKey:@"reviewer"];
    [query whereKey:@"approved" notEqualTo:@YES];
    [query whereKey:@"scorer" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"recipients" equalTo:[PFUser currentUser]];
    [query whereKey:@"full" equalTo:@YES];
    [query whereKey:@"active" equalTo:@YES];
    
    return query;
}


#pragma mark - Object at index

- (ActivityModel *)objectForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityModel *object = [self.objects objectAtIndex:indexPath.row];
    return object;
}


#pragma mark - UITableView Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    float height = 40.0f;
    if(self.objects.count == 0) {
        if (self.objects == nil) {
            return 0.01;
        }
        height = self.tableView.frame.size.height;
    }
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view;
    CGRect f = CGRectMake(0.0f, 0.0f, self.view.frame.size.width,self.tableView.frame.size.height);
    if(self.objects.count != 0) {
        f.size = CGSizeMake(f.size.width, 40.0f);
    }
    view = [[UIView alloc] initWithFrame:f];
    CGRect frame = view.frame;
    frame.size = CGSizeMake(frame.size.width-20.0f, frame.size.height);
    frame.origin= CGPointMake(10.0f, 0.0f);
    
    if(self.objects.count == 0) {
        if (self.objects == nil) {
            return nil;
        }
        view.backgroundColor = [UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
        UILabel *label = [[UILabel alloc] initWithFrame:frame];
        label.text = @"YOU DON'T HAVE ANY SCORES TO SUBMIT";
        label.textColor = [UIColor lightGrayColor];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.numberOfLines = 0;
        [label setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0f]];
        label.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label];
        self.tableView.separatorColor = [UIColor clearColor];
    } else {
        view.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:245.0f/255.0f blue:250.0f/255.0f alpha:1.0f];
        UILabel *label = [[UILabel alloc] initWithFrame:frame];
        label.text = @"";
        label.textColor = [UIColor darkGrayColor];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.numberOfLines = 0;
        [label setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0f]];
        label.textAlignment = NSTextAlignmentCenter;
        [view addSubview:label];
        self.tableView.separatorColor = [UIColor colorWithRed:0.8f green:0.8f blue:0.0f alpha:0.6f];
    }
    
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.objects == nil) {
        tableView.bounces = NO;
        return 1;
    }
    tableView.bounces = YES;
    return self.objects.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.objects == nil) {
        return tableView.frame.size.height;
    }
    return 180.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.objects == nil) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FirstCell" forIndexPath:indexPath];
        return cell;
    }
    static NSString *CellIdentifier = @"Cell";
    
    ActivityModel *activity = [self objectForRowAtIndexPath:indexPath];
    
    HomeViewCell *cell = (HomeViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    //PFUser *user = activity.user;
    
    NSString *type = @"Singles Match";
    if(activity.doubles) {
        type = @"Doubles Match";
    }
    cell.sportLabel.text = [NSString stringWithFormat:@"%@",type];
    
    NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
    
    
    if(activity.startTime.year != [NSDate date].year) {
        dateTimeFormatter.dateFormat = @"MMM d, YYYY @h:mm a";
    } else {
        dateTimeFormatter.dateFormat = @"MMM d @h:mm a";
    }
    
    NSString *datetime = [dateTimeFormatter stringFromDate: activity.startTime];
    
    cell.fromNowLabel.text = [NSString stringWithFormat:@"%@ ago",activity.startTime.shortTimeAgoSinceNow];
    cell.timeLabel.text = datetime;
    
    cell.locationLabel.text = [NSString stringWithFormat:@"NEAR %@ %@",
                               activity.location[@"Street"],
                               activity.location[@"City"]];
    
    
    
    
    // Set the players labels
    for(int i=0;i<activity.recipients.count;i++) {
        PFUser *recipient = [activity.recipients objectAtIndex:i];
        if(!recipient || [recipient isEqual:[NSNull null]] || !recipient.isDataAvailable)continue;
        int tag = i+1;
        UIView *container = [cell.contentView viewWithTag:tag];
        if(container) {
            PFImageView *imageView = (PFImageView *)[container viewWithTag:tag*10];
            if(imageView) {
                PFFile *file = [recipient objectForKey:@"image"];
                imageView.file = file;
                /*if([recipient[@"female"] boolValue]) {
                    imageView.image = [UIImage imageNamed:@"female"];
                } else {
                    imageView.image = [UIImage imageNamed:@"male"];
                }*/
                imageView.image = [UIImage imageNamed:@"default_profile"];
                [imageView loadInBackground];
                imageView.layer.cornerRadius = imageView.frame.size.height/2.0f;
                imageView.clipsToBounds = YES;
                imageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
                imageView.layer.borderWidth = 2.0f;
                imageView.contentMode = UIViewContentModeScaleAspectFill;
            }
            UILabel *name = (UILabel *)[container viewWithTag:tag*11];
            if(name) {
                NSString *first = [recipient objectForKey:@"first"];
                NSString *last = [recipient objectForKey:@"last"];
                
                if(!first) {
                    NSArray *name = [recipient[@"name"] componentsSeparatedByString:@" "];
                    first = [name firstObject];
                    if(!last) {
                        if(name.count > 1) {
                            last = [name lastObject];
                        }
                    }
                }
                name.text = first;
                if(last && last.length > 0) {
                    name.text = [NSString stringWithFormat:@"%@ %@.",name.text, [last substringToIndex:1]];
                }
            }
            UILabel *host = (UILabel *)[container viewWithTag:tag*12];
            if(activity.scorer && [activity.scorer.objectId isEqualToString:recipient.objectId]) {
                host.text = @"SCORER";
                if(imageView) {
                    imageView.layer.borderColor = THEME_COLOR.CGColor;
                } else {
                    host.text = @"";
                }
            } else {
                host.text = @"";
            }
        }
    }
    
    // If not scored - title should be score
    // If scored by you or teammate - title should be Pending (verification or opponent)
    // If scored by opponent - title should be Review
    
    
    BOOL scored = FALSE;
    BOOL opponent = FALSE;
    
    
    if(activity.score) {
        // The match has been scored
        scored = TRUE;
        if(!activity.approved) {
            // Either pending or review
            /*for(PFUser *user in activity.reviewers) {
                // Reviewers is the team of reviewers
                NSLog(@"User: %@",user);
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    NSLog(@"User: %@",user.objectId);
                    NSLog(@"User: %@",[PFUser currentUser].objectId);
                    // Our team needs to review
                    opponent = TRUE;
                    break;
                }
            }*/
        }
    }
    cell.actionButton.enabled = YES;
    [cell.actionButton setBackgroundColor:THEME_COLOR];
    if(activity.approved) {
        [cell.actionButton setTitle:@"View" forState:UIControlStateNormal];
    } else {
        if(scored) {
            // Match is scored
            if(opponent) {
                // Opponent scored
                [cell.actionButton setTitle:@"Review" forState:UIControlStateNormal];
                
            } else {
                // Team scored
                [cell.actionButton setTitle:@"Pending" forState:UIControlStateDisabled];
                [cell.actionButton setBackgroundColor:OPAQUE_THEME];
                cell.actionButton.enabled = NO;
            }
        } else {
            // Not scored
            [cell.actionButton setTitle:@"Score" forState:UIControlStateNormal];
        }
    }
    
    
    cell.actionButton.layer.cornerRadius = 3.0f;
    cell.actionButton.clipsToBounds = YES;
    cell.actionButton.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.actionButton.layer.borderWidth = 1.0f;
    
    return cell;
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(HomeViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;{
    
    if(!self.pullToRefreshEnabled &&  self.objects.count > 0 && !self.endOfResults){
        NSInteger total = (self.objects.count - 2);
        if (indexPath.row == total)
        {
            if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
            {
                [self loadNextPage];
            }
        }
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        NSLog(@"Set it");
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Not Enabled" message:@"Please Turn on Location Services\nFor this app to be useful you will need to turn on location services. We use your current location to show you only matches in your area.\nTo enable, go to the settings app on your phone, find the MatchOn app and enable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            //NSLog(@"Location here!");
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        {
            
        }
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"kCLAuthorizationStatusNotDetermined");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
            break;
    }
}



#pragma mark - IBActions

- (IBAction)message:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    self.selectedActivity = (ActivityModel *)[self.objects objectAtIndex:selectedIndex.row];
    //[self performSegueWithIdentifier:@"message" sender:self];
}

- (IBAction)score:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    self.selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    self.selectedActivity = (ActivityModel *)[self.objects objectAtIndex:self.selectedIndex.row];

    HomeViewCell *cell = (HomeViewCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndex];
    
    if(![cell.actionButton.titleLabel.text isEqualToString:@"Score"]) {
        [self performSegueWithIdentifier:@"score" sender:self];
    } else {
        // Score
        if(self.selectedActivity.spots > 2) {
            [self performSegueWithIdentifier:@"teammate" sender:self];
        } else {
            [self performSegueWithIdentifier:@"score" sender:self];
        }
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc.badge removeFromSuperview];
    tc.badge = nil;
    if([segue.identifier isEqualToString:@"message"]) {
        MessageViewController *message = [segue destinationViewController];
        message.recipients = self.selectedActivity.recipients;
        message.activity = self.selectedActivity;
        
    }
    if([segue.identifier isEqualToString:@"teammate"]) {
        TeammateTableViewController *team = [segue destinationViewController];
        NSMutableArray *recipients = [NSMutableArray array];
        for(PFUser *user in self.selectedActivity.recipients){
            if([user.objectId isEqualToString:[PFUser currentUser].objectId]){
                continue;
            }
            [recipients addObject:user];
        }
        team.activity = self.selectedActivity;
        team.recipients = [[NSMutableArray alloc] initWithArray:recipients];
    }
    if([segue.identifier isEqualToString:@"score"]) {
        ScoreViewController *score = [segue destinationViewController];
        score.activity = self.selectedActivity;
        score.action = @"score";
        
        if(self.push) {
            score.action = @"review";
        } else {
            HomeViewCell *cell = (HomeViewCell *)[self.tableView cellForRowAtIndexPath:self.selectedIndex];
            if([cell.actionButton.titleLabel.text isEqualToString:@"Pending"]) {
                score.action = @"pending";
            } else if([cell.actionButton.titleLabel.text isEqualToString:@"Review"]) {
                score.action = @"review";
            } else if([cell.actionButton.titleLabel.text isEqualToString:@"View"]) {
                score.action = @"view";
            } else {
                
                score.scoreSaved = ^(ScoreModel *score) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // Submit view for saving score
                        
                    });
                };
            }
        }
        
    }
}


@end
