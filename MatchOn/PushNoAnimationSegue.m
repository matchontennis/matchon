//
//  PushNoAnimationSegue.m
//  wave
//
//  Created by Kevin Flynn on 4/16/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue

-(void) perform{
    [[[self sourceViewController] navigationController] pushViewController:[self   destinationViewController] animated:NO];
}

@end
