//
//  PointsModel.m
//  MatchOn
//
//  Created by Kevin Flynn on 3/28/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "PointsModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation PointsModel

@dynamic user;
@dynamic activity;
@dynamic total;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Points";
}


@end
