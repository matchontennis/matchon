//
//  ChallengeModel.m
//  MatchOn
//
//  Created by Sol on 6/4/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "ChallengeModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation ChallengeModel

@dynamic name;
@dynamic short_description;
@dynamic long_description;
@dynamic starttime;
@dynamic endtime;
@dynamic logo;
@dynamic ongoing;
@dynamic compare_field;
@dynamic players;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Challenge";
}

@end
