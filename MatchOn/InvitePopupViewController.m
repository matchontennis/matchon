//
//  InvitePopupViewController.m
//  MatchOn
//
//  Created by Sol on 4/18/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "InvitePopupViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface InvitePopupViewController ()

@end

@implementation InvitePopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnYes.layer.cornerRadius = 5.0f;
    self.btnYes.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnYes.layer.borderWidth = 2.0f;
    
    self.btnNo.layer.cornerRadius = 5.0f;
    self.btnNo.layer.borderColor = [[UIColor colorWithRed:246 / 255.0f green:139 / 255.0f blue:31 / 255.0f alpha:1.0f] CGColor];
    self.btnNo.layer.borderWidth = 2.0f;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onYes:(id)sender {
    if (self.parent) {
        [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    [self.delegate answerForInvite:YES];
}

- (IBAction)onNo:(id)sender {
    if (self.parent) {
        [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    [self.delegate answerForInvite:NO];
}
@end
