//
//  ProfileViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/4/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface ProfileViewController : UIViewController {
    NSMutableArray *singleMatches;
    NSMutableArray *doubleMatches;
    NSMutableArray *singleExpanded;
    NSMutableArray *doubleExpanded;
    CGPoint curOffset;
    UITapGestureRecognizer *tap2;
    UIBarButtonItem *curBarItem;
}

@property (nonatomic, strong) PFGeoPoint *geopt;
@property (nonatomic, strong) NSString *court;
@property (nonatomic, strong) NSDictionary *location;

@property (weak, nonatomic) IBOutlet UITableView *tblSingles;
@property (weak, nonatomic) IBOutlet UITableView *tblDoubles;

@property (weak, nonatomic) IBOutlet UIView *viewAbout;
@property (weak, nonatomic) IBOutlet UIImageView *imgGender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segType;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPlayed;
@property (weak, nonatomic) IBOutlet UIView *viewBalls;
@property (weak, nonatomic) IBOutlet UIImageView *imgVerified;
@property (weak, nonatomic) IBOutlet UITextField *first;
@property (weak, nonatomic) IBOutlet UITextField *last;

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet PFImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (weak, nonatomic) IBOutlet PFImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblAllPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayedMatches;
@property (weak, nonatomic) IBOutlet UILabel *lblCompleted;
@property (weak, nonatomic) IBOutlet UITextField *txtHomeCourt;
@property (weak, nonatomic) IBOutlet UITextView *txtAboutMe;
@property (weak, nonatomic) IBOutlet UIView *viewPhoto;
@property (weak, nonatomic) IBOutlet UIView *viewCommon;
- (IBAction)onType:(id)sender;
- (IBAction)onSettings:(id)sender;
- (IBAction)onCollapse:(id)sender;

- (IBAction)password:(id)sender;
- (IBAction)photo:(id)sender;

- (IBAction)logout:(id)sender;
- (IBAction)onEditCourt:(id)sender;
- (IBAction)onEditAboutMe:(id)sender;

@end
