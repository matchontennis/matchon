//
//  ActivityAnnotationView.h
//  AnchorApp
//
//  Created by Kevin Flynn on 3/2/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "ActivityModel.h"

@interface ActivityAnnotation : NSObject <MKAnnotation>


@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@property (nonatomic, strong) ActivityModel *activity;


+ (MKAnnotationView *)createViewAnnotationForMapView:(MKMapView *)mapView annotation:(id <MKAnnotation>)annotation;


@end
