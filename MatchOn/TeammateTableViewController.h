//
//  TeammateTableViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/4/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "HomeViewCell.h"
#import "ActivityModel.h"

@interface TeammateTableViewController : UITableViewController

@property (nonatomic, strong) ActivityModel *activity;
@property (nonatomic, strong) PFUser *teammate;

@property (nonatomic, strong) NSMutableArray *recipients;

@end
