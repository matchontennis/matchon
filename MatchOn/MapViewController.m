//
//  MapViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 3/9/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "MapViewController.h"
#import "ActivityAnnotation.h"
#import "AppDelegate.h"

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(self.activity.geopoint.latitude, self.activity.geopoint.longitude);
    [self.mapView setCenterCoordinate:location animated:NO];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(location, 6000, 6000);
    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
    [self.mapView setRegion:adjustedRegion animated:NO];
    
    annotation = [[ActivityAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake(self.activity.geopoint.latitude, self.activity.geopoint.longitude);
    //annotation.title = self.activity.name;
    annotation.activity = self.activity;
    [self.mapView addAnnotation:annotation];
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    for (int i = 0; i < delegate.courts.count; i++) {
        NSDictionary *court = [delegate.courts objectAtIndex:i];
        ActivityAnnotation *courtAnnotation = [[ActivityAnnotation alloc] init];
        courtAnnotation.coordinate = CLLocationCoordinate2DMake([[court objectForKey:@"latitude"] doubleValue], [[court objectForKey:@"longitude"] doubleValue]);
        courtAnnotation.title = [court objectForKey:@"name"];
        courtAnnotation.subtitle = [court objectForKey:@"full_address"];
        courtAnnotation.activity = self.activity;
        [self.mapView addAnnotation:courtAnnotation];
    }
}

-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)courtAnnotation
{
    // If the annotation is the user location, just return nil.
    if ([annotation isEqual:courtAnnotation]) {
        MKPinAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:NSStringFromClass([MKPinAnnotationView class])];
        
        annotationView.pinColor = MKPinAnnotationColorRed;
        annotationView.animatesDrop = YES;
        annotationView.canShowCallout = YES;
        return annotationView;
    }
    return [ActivityAnnotation createViewAnnotationForMapView:mapView annotation:annotation];
}

- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

@end
