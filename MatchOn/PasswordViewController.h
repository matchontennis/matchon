//
//  PasswordViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 1/22/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface PasswordViewController : UIViewController

- (IBAction)cancel:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
- (IBAction)submit:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@end
