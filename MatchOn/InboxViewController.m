//
//  InboxViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 10/23/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "InboxViewController.h"
#import "ActivityModel.h"
#import "MessageViewController.h"
#import "DateTools.h"
#import "InboxViewCell.h"
#import "MainViewController.h"

@interface InboxViewController ()

@end

@implementation InboxViewController




#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Config
    self.limit = 25;
    self.skip = 0;
    
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
    
    // Refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.objects = [NSMutableArray array];
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc.badge removeFromSuperview];
    tc.badge = nil;
    
    
    [self refreshInbox];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark - Data for table

- (void)refreshInbox{
    if(![PFUser currentUser]) {
        [self.objects removeAllObjects];
        [self.tableView reloadData];
        return;
    }
    self.pullToRefreshEnabled = TRUE;
    self.skip = 0;
    if(!self.refreshControl.isRefreshing) {
        [self.objects removeAllObjects];
        [self.tableView reloadData];
    }
    
    [self loadObjects];
}

- (void)loadNextPage{
    self.skip += self.limit;
    if(!self.endOfResults) {
        [self loadObjects];
    }
}


- (void)loadObjects{
    PFQuery *query = [self queryForTable];
    if(self.pullToRefreshEnabled) {
        self.skip = 0;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(objects.count == 0) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [self.objects removeAllObjects];
        }
        if(whichPass > 1) {
            [self.objects removeAllObjects];
        }
        //NSLog(@"Loading: %@",objects);
        NSMutableArray *newObjects = [[NSMutableArray alloc] init];
        NSString *activityID = @"";
        for (int i = 0; i < objects.count; i++) {
            MessageModel *message = [objects objectAtIndex:i];
            ActivityModel *activity = message.activity;
            if (![activityID isEqualToString:activity.objectId]) {
                [newObjects addObject:message];
            }
            activityID = activity.objectId;
        }
        [self.objects addObjectsFromArray:newObjects];
        
        if(self.objects.count == 0) {
            if(!self.noResults) {
                self.noResults = TRUE;
            }
        } else {
            self.noResults = FALSE;
        }
        [self.tableView reloadData];
        
        
        self.pullToRefreshEnabled = FALSE;
        whichPass = whichPass + 1;
    }];
}


#pragma mark - PFQueryTableViewController


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:@"Message"];
    [query includeKey:@"recipients"];
    [query includeKey:@"from"];
    [query includeKey:@"activity"];
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }
    if ([self.objects count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    [query whereKey:@"recipients" equalTo:[PFUser currentUser]];
    [query whereKey:@"active" notEqualTo:@NO];
    [query whereKey:@"deleted" notEqualTo:[PFUser currentUser]];
    [query orderByDescending:@"createdAt"];
    return query;
}


#pragma mark - Object at index

- (MessageModel *)objectForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageModel *object = [self.objects objectAtIndex:indexPath.row];
    return object;
}

#pragma mark - UITableView Data Source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MessageModel *message = [self objectForRowAtIndexPath:indexPath];
    ActivityModel *activity = message.activity;
    InboxViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.detailsLabel.text = [NSString stringWithFormat:@"%@",message.text];
    
    PFUser *sender = message.from;
    if (![sender.objectId isEqualToString:[PFUser currentUser].objectId]) {
        [sender fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            PFUser *user = (PFUser *)object;
            NSString *first = [user objectForKey:@"first"];
            NSString *last = [user objectForKey:@"last"];
            
            if(!first) {
                NSArray *name = [user[@"name"] componentsSeparatedByString:@" "];
                first = [name firstObject];
                if(!last) {
                    if(name.count > 1) {
                        last = [name lastObject];
                    }
                }
            }
            NSString *username = first;
            /*if(last && last.length > 0) {
                username = [NSString stringWithFormat:@"%@ %@.",username, [last substringToIndex:1]];
            }*/
            cell.detailsLabel.text = [NSString stringWithFormat:@"%@ from %@",message.text, username];
        }];
    }
    NSMutableArray *allPersons = [[NSMutableArray alloc] init];
    if (![activity.user.objectId isEqualToString:[PFUser currentUser].objectId]) {
        [allPersons addObject:activity.user];
    }
    for (int i = 0; i < activity.recipients.count; i++) {
        PFUser *user = (PFUser *)[activity.recipients objectAtIndex:i];
        if ([user.objectId isEqualToString:[PFUser currentUser].objectId] || [user.objectId isEqualToString:activity.user.objectId]) {
            continue;
        }
        [allPersons addObject:user];
    }
    
    if (allPersons.count > 0) {
        cell.profileImage1.hidden = NO;
    }else {
        cell.profileImage1.hidden = YES;
    }
    
    if (allPersons.count > 1) {
        cell.profileImage2.hidden = NO;
    }else {
        cell.profileImage2.hidden = YES;
    }
    
    if (allPersons.count > 2) {
        cell.profileImage3.hidden = NO;
    }else {
        cell.profileImage3.hidden = YES;
    }
    
    if (allPersons.count > 3) {
        cell.profileImage4.hidden = NO;
    }else {
        cell.profileImage4.hidden = YES;
    }
    
    cell.profileImage1.layer.cornerRadius = cell.profileImage2.frame.size.height/2.0f;
    cell.profileImage1.clipsToBounds = YES;
    cell.profileImage2.layer.cornerRadius = cell.profileImage2.frame.size.height/2.0f;
    cell.profileImage2.clipsToBounds = YES;
    cell.profileImage3.layer.cornerRadius = cell.profileImage2.frame.size.height/2.0f;
    cell.profileImage3.clipsToBounds = YES;
    cell.profileImage4.layer.cornerRadius = cell.profileImage2.frame.size.height/2.0f;
    cell.profileImage4.clipsToBounds = YES;
    
    [PFUser fetchAllIfNeededInBackground:allPersons block:^(NSArray *objects, NSError *error) {
        NSString *usernames = @"";
        for (int i = 0; i < objects.count; i++) {
            PFUser *user = (PFUser *)[objects objectAtIndex:i];
            if (![usernames isEqualToString:@""]) {
                if (i % 2 == 1) {
                    usernames = [usernames stringByAppendingString:@" , "];
                }else {
                    usernames = [usernames stringByAppendingString:@"\n"];
                }
            }
            PFFile *file = user[@"image"];
            if (i == 0) {
                if (file != nil) {
                    cell.profileImage1.file = file;
                }
                /*if ([user[@"female"] boolValue]) {
                    cell.profileImage1.image = [UIImage imageNamed:@"female"];
                }else {
                    cell.profileImage1.image = [UIImage imageNamed:@"male"];
                }*/
                cell.profileImage1.image = [UIImage imageNamed:@"default_profile"];
                [cell.profileImage1 loadInBackground];
            }else if (i == 1) {
                if (file != nil) {
                    cell.profileImage2.file = file;
                }
                /*if ([user[@"female"] boolValue]) {
                    cell.profileImage2.image = [UIImage imageNamed:@"female"];
                }else {
                    cell.profileImage2.image = [UIImage imageNamed:@"male"];
                }*/
                cell.profileImage2.image = [UIImage imageNamed:@"default_profile"];
                [cell.profileImage2 loadInBackground];
            }else if (i == 2) {
                if (file != nil) {
                    cell.profileImage3.file = file;
                }
                /*if ([user[@"female"] boolValue]) {
                    cell.profileImage3.image = [UIImage imageNamed:@"female"];
                }else {
                    cell.profileImage3.image = [UIImage imageNamed:@"male"];
                }*/
                cell.profileImage3.image = [UIImage imageNamed:@"default_profile"];
                [cell.profileImage3 loadInBackground];
            }else {
                if (file != nil) {
                    cell.profileImage4.file = file;
                }
                /*if ([user[@"female"] boolValue]) {
                    cell.profileImage4.image = [UIImage imageNamed:@"female"];
                }else {
                    cell.profileImage4.image = [UIImage imageNamed:@"male"];
                }*/
                cell.profileImage4.image = [UIImage imageNamed:@"default_profile"];
                [cell.profileImage4 loadInBackground];
            }
            NSString *first = [user objectForKey:@"first"];
            NSString *last = [user objectForKey:@"last"];
            
            if(!first) {
                NSArray *name = [user[@"name"] componentsSeparatedByString:@" "];
                first = [name firstObject];
                if(!last) {
                    if(name.count > 1) {
                        last = [name lastObject];
                    }
                }
            }
            NSString *username = first;
            /*if(last && last.length > 0) {
                username = [NSString stringWithFormat:@"%@ %@.",username, [last substringToIndex:1]];
            }*/
            usernames = [usernames stringByAppendingString:username];
        }
        cell.userLabel.text = usernames;
        cell.userLabel.numberOfLines = 0;
    }];
    
    
    NSLog(@"Cell");
    
    
    BOOL read = FALSE;
    for(PFUser *user in message.read) {
        if([[PFUser currentUser].objectId isEqualToString:user.objectId]){
            read = TRUE;
        }
    }
    if(!read) {
        NSLog(@"Not read");
        cell.userLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:15.0f];
        cell.backgroundColor = [UIColor colorWithWhite:0.8f alpha:0.8f];
    } else {
        cell.userLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:15.0f];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    cell.fromNowLabel.text = message.createdAt.shortTimeAgoSinceNow;
    cell.delegate = self;
    cell.rightUtilityButtons = [self rightButtons];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (NSArray *)rightButtons {
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                title:@"X"];
    return rightUtilityButtons;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    MessageModel *message = [self objectForRowAtIndexPath:indexPath];
    if(!message.deleted) {
        
    }
    [message fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        // Delete message for this user
        MessageModel *m = (MessageModel *)object;
        if(!m.deleted) {
            m.deleted = [NSMutableArray array];
        }
        [m.deleted addObject:[PFUser currentUser]];
        [m saveInBackground];
    }];

    [self.objects removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return YES;
}

- (BOOL)swipeableTableViewCell:(SWTableViewCell *)cell canSwipeToState:(SWCellState)state
{
    switch (state) {
        case 1:
            // set to NO to disable all left utility buttons appearing
            return NO;
            break;
        case 2:
            // set to NO to disable all right utility buttons appearing
            return YES;
            break;
        default:
            break;
    }
    return YES;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.message = [self.objects objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"message" sender:self];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (indexPath.row == self.objects.count)
    {
        if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
        {
            [self loadNextPage];
        }
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        NSLog(@"Set it");
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"message"]) {
        MessageViewController *message = [segue destinationViewController];
        message.recipients = self.message.recipients;
        if(self.message.activity) {
            message.activity = self.message.activity;
        }
        message.message = self.message;
    }
}



- (IBAction)back:(id)sender {
    MainViewController *tc = (MainViewController *)self.tabBarController;
    [tc loadNewMessages];
    //[tc loadNewScores];
    [self.navigationController popViewControllerAnimated:NO];
    
}

@end
