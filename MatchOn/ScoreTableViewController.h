//
//  ScoreTableViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 11/22/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <CoreLocation/CoreLocation.h>
#import "ActivityModel.h"


@interface ScoreTableViewController : UITableViewController

@property (nonatomic,assign) BOOL pullToRefreshEnabled;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) NSMutableArray *objects;
@property (nonatomic, assign) BOOL endOfResults;
@property (nonatomic, assign) BOOL push;

// Query config
@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;

@property (nonatomic, strong) PFGeoPoint *geopoint;
@property (nonatomic, assign) BOOL noResults;

@property (nonatomic, strong) ActivityModel *selectedActivity;
@property (nonatomic, strong) NSIndexPath *selectedIndex;

- (IBAction)message:(id)sender;
- (IBAction)score:(id)sender;


@end
