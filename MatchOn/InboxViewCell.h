//
//  InboxViewCell.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/30/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ParseUI/ParseUI.h>
#import "SWTableViewCell.h"


@interface InboxViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet PFImageView *profileImage1;
@property (weak, nonatomic) IBOutlet PFImageView *profileImage2;
@property (weak, nonatomic) IBOutlet PFImageView *profileImage3;
@property (weak, nonatomic) IBOutlet PFImageView *profileImage4;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;

@property (weak, nonatomic) IBOutlet UILabel *fromNowLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

@end
