//
//  InviteViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 2/12/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "InviteViewController.h"
#import <AddressBook/ABPerson.h>
#import <AddressBook/AddressBook.h>
#import "MSCellAccessory.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "ECPhoneNumberFormatter.h"
#import "InviteContactViewController.h"
#import "AppDelegate.h"
#import "Localytics.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface InviteViewController () <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, assign) ABAddressBookRef addressBook;


@end

@implementation InviteViewController


- (void)dealloc
{
    if(_addressBook)
    {
        CFRelease(_addressBook);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.contentView setRowHeight:84.0f];
    self.contentView.delegate = self;
    self.contentView.dataSource = self;
    //self.contactInvites       = [NSMutableDictionary dictionary];
    //self.userInvites          = [NSMutableDictionary dictionary];
    //self.contacts           = [NSMutableArray array];
    //self.searchResults      = [NSMutableArray array];
    //self.userSearchResults = [NSMutableArray array];
    //self.playerSearchResults = [NSMutableArray array];
    //self.contactSearchResults = [NSMutableArray array];
    //self.users              = [NSMutableArray array];
    //self.players            = [NSMutableArray array];
    //self.playersMap         = [NSMutableSet set];
    self.limit = 100;
    self.skip = 0;
    
    //New Code By Yong
    self.title = @"Invite Friends";
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    
    self.contentView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
    self.btnAllDone.layer.cornerRadius = 3.0f;
    self.btnAllDone.layer.borderColor = [[UIColor colorWithRed:246 / 255.0f green:139 / 255.0f blue:31 / 255.0f alpha:1.0f] CGColor];
    self.btnAllDone.layer.borderWidth = 2.0f;
    self.btnAllDone.clipsToBounds = YES;
    
    self.btnInviteContact.layer.cornerRadius = 3.0f;
    self.btnInviteContact.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnInviteContact.layer.borderWidth = 2.0f;
    self.btnInviteContact.clipsToBounds = YES;
    
    
    //End
    
    
    _addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    if(self.activity) {
        [self refreshInbox];
    }
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    [self.contentView addSubview:self.refreshControl];
    //[self loadAddressBook];
    [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
        self.link = [config objectForKey:@"invite"];
        //NSLog(@"Config: %@",config);
        if(!self.link) self.link = @"http://www.matchon.co";
    }];
    
    if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        self.btnFBLink.hidden = YES;
    }else {
        self.btnFBLink.hidden = NO;
    }
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.contentView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.contentView.separatorColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.inviteViewController = self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    AppDelegate *delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    delegate.inviteViewController = nil;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if ([self.contentView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.contentView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.contentView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.contentView setLayoutMargins:UIEdgeInsetsZero];
    }
}



#pragma mark - Query


- (void)refreshInbox {
    if(![PFUser currentUser]) {
        [self.players removeAllObjects];
        //[self.playersMap removeAllObjects];
        [self.contentView reloadData];
        return;
    }
    [self.activity fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        self.activity = (ActivityModel *)object;
        NSLog(@"%@",self.activity.declines);
        self.pullToRefreshEnabled = TRUE;
        self.skip = 0;
        [self loadAddressBook];
    }];
    
    //[self loadObjects];
}

- (IBAction)onFacebookLink:(id)sender {
    [PFFacebookUtils linkUser:[PFUser currentUser] permissions:@[@"public_profile",@"email",@"user_friends"] block:^(BOOL succeeded, NSError *error) {
        NSLog(@"%@",error);
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook Connection Failed" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alertView show];
            return;
        }
        self.btnFBLink.hidden = YES;
        [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
            if (error) {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook Connection Failed" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
                return;
            }
            NSDictionary *userData = (NSDictionary *)result;
            NSString *fbId = userData[@"id"];
            [[PFUser currentUser] setObject:fbId forKey:@"fbId"];
            [[PFUser currentUser] saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (error) {
                    //[self loadObjects];
                    return;
                }
                [self refreshInbox];
            }];
        }];
        
    }];
}

- (void)loadNextPage{
    self.skip += self.limit;
    if(!self.endOfResults) {
        [self loadAddressBook];
    }
}


- (void)loadObjects {
    PFQuery *query1 = [self queryForTable];
    PFQuery *query2 = [self queryForFBFriends];
    NSMutableArray *allQueries = [NSMutableArray array];
    if (query1 != nil) {
        [allQueries addObject:query1];
    }
    if (query2 != nil) {
        [allQueries addObject:query2];
    }
    if (allQueries.count == 0) {
        return;
    }
    PFQuery *query = [PFQuery orQueryWithSubqueries:allQueries];
    //[query includeKey:@"recipients"];
    [query orderByAscending:@"last"];
    
    if (self.pullToRefreshEnabled) {
        self.skip = 0;
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }else if (self.players.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    NSLog(@"Loading objects");
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if(objects.count == 0) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [self.players removeAllObjects];
        }
        if (whichPass > 1) {
            //[self.playersMap removeAllObjects];
            [self.players removeAllObjects];
        }
        if (self.players == nil && objects != nil) {
            self.players = [NSMutableArray array];
        }
        /*for(ActivityModel *activity in objects) {
            for(PFUser *user in activity.recipients) {
                // User cannot be current user
                if([user.objectId isEqualToString:[PFUser currentUser].objectId]) continue;
                
                if(![self.playersMap containsObject:user.objectId]) {
                    NSLog(@"Adding user");
                    [self.playersMap addObject:user.objectId];
                    [self.players addObject:user];
                }
            }
        }*/
        for (PFUser *user in objects) {
            if ([user.objectId isEqualToString:[PFUser currentUser].objectId]) continue;
            //[self.playersMap addObject:user.objectId];
            [self.players addObject:user];
        }
        
        //[self combinePlayers];
        [self.contentView reloadData];
        self.pullToRefreshEnabled = FALSE;
        whichPass = whichPass + 1;
    }];
}

/*-(void)combinePlayers
{
    combinedPlayers = [[NSMutableArray alloc] init];
    for (int i = 0; i < self.contacts.count; i++) {
        PFUser *user = (PFUser *)[self.contacts objectAtIndex:i];
        if (![combinedPlayers containsObject:user]) {
            [combinedPlayers addObject:user];
        }
    }
    for (int i = 0; i < self.players.count; i++) {
        PFUser *user = (PFUser *)[self.players objectAtIndex:i];
        if (![combinedPlayers containsObject:user]) {
            [combinedPlayers addObject:user];
        }
    }
}*/

#pragma mark - QueryTableViewController


- (PFQuery *)queryForTable {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query1 = [PFUser query];
    PFUser *currentUser = [PFUser currentUser];
    NSMutableArray *playerIds = [[NSMutableArray alloc] init];
    NSArray *players = [currentUser objectForKey:@"players"];
    if (players != nil) {
        for (int i = 0; i < players.count; i++) {
            PFUser *player = (PFUser *)[players objectAtIndex:i];
            [playerIds addObject:player.objectId];
        }
    }
    [query1 whereKey:@"objectId" containedIn:playerIds];
    PFQuery *query2 = [PFUser query];
    [query2 whereKey:@"players" equalTo:[PFUser currentUser]];
    PFQuery *query;
    if (self.emails != nil) {
        PFQuery *query3 = [PFUser query];
        [query3 whereKey:@"email" containedIn:self.emails];
        query = [PFQuery orQueryWithSubqueries:@[query1,query2,query3]];
    }else {
        query = [PFQuery orQueryWithSubqueries:@[query1,query2]];
    }
    return query;
}

- (PFQuery *)queryForFBFriends {
    if(![PFUser currentUser])return nil;
    
    PFQuery *query = [PFUser query];
    if (friendIds != nil && friendIds.count > 0) {
        [query whereKey:@"fbId" containedIn:friendIds];
    }else {
        return nil;
    }
    return query;
}



#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    //return 30.0f;
    return 0.01f;
}


/*- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 44.0f, self.view.frame.size.width, 30.0f)];
    view.backgroundColor = [UIColor colorWithWhite:0.92f alpha:1.0f];
    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
    CGRect frame = label.frame;
    frame.origin = CGPointMake(10.0f, 0.0f);
    frame.size = CGSizeMake(self.view.frame.size.width-20.0f, 30.0f);
    label.frame = frame;
    if(!self.activity) {
        label.text = @" Invite Contacts";
    } else {
        if(section == 0) {
            label.text = @" Contacts on MatchOn";
        } else if(section == 1) {
            label.text = @" Played with on MatchOn";
        } else {
            label.text = @" Invite Contacts";
        }
    }
    
    [view addSubview:label];
    return view;
}*/


/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(!self.activity) {
        return 1;
    }
    return 3;
}*/

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    /*if (tableView == self.searchDisplayController.searchResultsTableView) {
        if(section == 0) {
            return self.userSearchResults.count;
        } else if (section == 1) {
            return self.playerSearchResults.count;
        } else{
            return self.contactSearchResults.count;
        }
        
    } else {
        if(!self.activity) {
            return self.contacts.count;
        }
        if(section == 0) {
            return self.users.count;
        } else if (section == 1) {
            return self.players.count;
        }
    }*/
    if (self.players == nil) {
        tableView.bounces = NO;
    }else {
        tableView.bounces = YES;
    }
    
    if (self.players.count == 0) {
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 1;
    }
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    return self.players.count;
    
    //return self.contacts.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.players.count == 0) {
        if (self.players == nil) {
            return tableView.frame.size.height;
        }
        return 150.0f;
    }
    if (!self.activity) {
        return 44.0f;
    }
    PFUser *player = (PFUser *)[self.players objectAtIndex:indexPath.row];
    BOOL isDeclined = NO;
    for (PFUser *user in self.activity.declines) {
        if ([user.objectId isEqualToString:player.objectId]) {
            isDeclined = YES;
            break;
        }
    }
    if (isDeclined) {
        return 64.0f;
    }
    BOOL isInvited = NO;
    for (PFUser *user in self.activity.invitations) {
        if ([user.objectId isEqualToString:player.objectId]) {
            isInvited = YES;
            break;
        }
    }
    if (!isInvited) {
        return 44.0f;
    }
    PFUser *user = [self.players objectAtIndex:indexPath.row];
    int i = 0;
    for (i = 0; i < self.activity.recipients.count; i++) {
        PFUser *curUser = [self.activity.recipients objectAtIndex:i];
        if (curUser != nil && ![curUser isEqual:[NSNull null]] && [user.objectId isEqualToString:curUser.objectId]) {
            break;
        }
    }
    if (i == self.activity.recipients.count) {
        return 72.0f;
    }else {
        return 48.0f;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    if (self.players == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LogoCell" forIndexPath:indexPath];
        return cell;
    }
    if (self.players.count == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        return cell;
    }
    if (!self.activity) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NonInvitedCell" forIndexPath:indexPath];
        UILabel *lblName = (UILabel *)[cell viewWithTag:10];
        PFUser *user = [self.players objectAtIndex:indexPath.row];
        lblName.text = [NSString stringWithFormat:@"%@", user[@"name"]];
        UIButton *btnInvite = (UIButton *)[cell viewWithTag:11];
        btnInvite.layer.cornerRadius = 2.0f;
        btnInvite.layer.borderColor = [THEME_COLOR CGColor];
        btnInvite.layer.borderWidth = 1.0f;
        return cell;
    }
    PFUser *player = (PFUser *)[self.players objectAtIndex:indexPath.row];
    BOOL isDeclined = NO;
    for (PFUser *user in self.activity.declines) {
        if ([user.objectId isEqualToString:player.objectId]) {
            isDeclined = YES;
            break;
        }
    }
    if (isDeclined) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"DeclinedCell" forIndexPath:indexPath];
        UILabel *lblName = (UILabel *)[cell viewWithTag:10];
        PFUser *user = [self.players objectAtIndex:indexPath.row];
        lblName.text = [NSString stringWithFormat:@"%@", user[@"name"]];
        return cell;
    }
    BOOL isInvited = NO;
    for (PFUser *user in self.activity.invitations) {
        if ([user.objectId isEqualToString:player.objectId]) {
            isInvited = YES;
            break;
        }
    }
    if (!isInvited) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"NonInvitedCell" forIndexPath:indexPath];
        UILabel *lblName = (UILabel *)[cell viewWithTag:10];
        PFUser *user = [self.players objectAtIndex:indexPath.row];
        lblName.text = [NSString stringWithFormat:@"%@", user[@"name"]];
        UIButton *btnInvite = (UIButton *)[cell viewWithTag:11];
        btnInvite.layer.cornerRadius = 3.0f;
        btnInvite.layer.borderColor = [THEME_COLOR CGColor];
        btnInvite.layer.borderWidth = 1.0f;
        btnInvite.titleEdgeInsets = UIEdgeInsetsMake(4, 0, 0, 0);
        return cell;
    }
    cell = [tableView dequeueReusableCellWithIdentifier:@"AwaitingCell" forIndexPath:indexPath];
    UILabel *lblName = (UILabel *)[cell viewWithTag:10];
    UILabel *lblAwaiting = (UILabel *)[cell viewWithTag:11];
    UIButton *btnSend = (UIButton *)[cell viewWithTag:12];
    PFUser *user = [self.players objectAtIndex:indexPath.row];
    int i = 0;
    for (i = 0; i < self.activity.recipients.count; i++) {
        PFUser *curUser = [self.activity.recipients objectAtIndex:i];
        if (curUser != nil && ![curUser isEqual:[NSNull null]] && [user.objectId isEqualToString:curUser.objectId]) {
            break;
        }
    }
    if (i == self.activity.recipients.count) {
        lblAwaiting.text = @"AWAITING RESPONSE...";
        btnSend.hidden = NO;
    }else {
        lblAwaiting.text = @"ACCEPTED";
        btnSend.hidden = YES;
    }
    lblName.text = [NSString stringWithFormat:@"%@", user[@"name"]];
    return cell;
    
    /*if(indexPath.section == 0 && self.activity) {
        //cell = [tableView dequeueReusableCellWithIdentifier:@"User" forIndexPath:indexPath];
        cell = [tableView dequeueReusableCellWithIdentifier:@"User"];
        
        if (cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"User" forIndexPath:indexPath];
        }
        PFUser *user = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            user = [self.userSearchResults objectAtIndex:indexPath.row];
        }else{
            user = [self.users objectAtIndex:indexPath.row];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@", user[@"name"]];
        
        if([self.userInvites objectForKey:user.objectId]){
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_CHECKMARK color:THEME_COLOR];
        }
        else {
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_OPEN color:[UIColor colorWithWhite:0.90f alpha:1.0f]];
        }
        cell.detailTextLabel.text = @"";
        
        
        
    } if(indexPath.section == 1 && self.activity) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"User"];
        
        if (cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"User" forIndexPath:indexPath];
        }
        PFUser *user = nil;
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            user = [self.playerSearchResults objectAtIndex:indexPath.row];
        }else{
            user = [self.players objectAtIndex:indexPath.row];
        }
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@", user[@"name"]];
        
        if([self.userInvites objectForKey:user.objectId]){
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_CHECKMARK color:THEME_COLOR];
        }
        else {
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_OPEN color:[UIColor colorWithWhite:0.90f alpha:1.0f]];
        }
        cell.detailTextLabel.text = @"";
        
    } else if(indexPath.section == 2 || !self.activity) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if (cell == nil)
        {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        }
        NSDictionary *record = nil;
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            record = [self.contactSearchResults objectAtIndex:indexPath.row];
            //record = [[self.searchResultDictionary objectForKey:@"contacts"] objectAtIndex:indexPath.row];
        }else{
            record = [self.contacts objectAtIndex:indexPath.row];
        }
        
        if([record objectForKey:@"name"]) {
            cell.textLabel.text = [record objectForKey:@"name"];
        }
        
        cell.detailTextLabel.text = [record objectForKey:@"name"];
        
         // Refortmat the text for display
         //ECPhoneNumberFormatter *formatter = [[ECPhoneNumberFormatter alloc] init];
         //NSString *formattedNumber = [formatter stringForObjectValue:record[@"phone"]];
         //cell.detailTextLabel.text = formattedNumber;
         //NSLog(@"Setting phone %@",formattedNumber);
        
        
        if([self.contactInvites objectForKey:record
            [@"recordId"]]){
            cell.accessoryView = [MSCellAccessory accessoryWithType:SMS_SEND color:THEME_COLOR highlightedColor:THEME_COLOR];
        }
        else {
            cell.accessoryView = [MSCellAccessory accessoryWithType:SMS_OPEN color:[UIColor lightGrayColor]];
        }
        
        //NSLog(@"Set it for record: %@",record);
    }*/
}

#pragma mark - Table view delegate

- (void)tableView: (UITableView *) tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*[self.contentView deselectRowAtIndexPath:indexPath animated:NO];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if(indexPath.section == 0 && self.activity) {
        // Users
        PFUser *user = nil;
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            user = [self.userSearchResults objectAtIndex:indexPath.row];
        }else{
            user = [self.users objectAtIndex:indexPath.row];
        }
        
        if([self.userInvites objectForKey:user.objectId]) {
            [self.userInvites removeObjectForKey:user.objectId];
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_OPEN color:[UIColor colorWithWhite:0.90f alpha:1.0f]];
        } else {
            [self.userInvites setObject:user forKey:user.objectId];
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_CHECKMARK color:THEME_COLOR];
        }
    } else if(indexPath.section == 1 && self.activity) {
        // Users
        PFUser *user = nil;
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            user = [self.playerSearchResults objectAtIndex:indexPath.row];
        }else{
            user = [self.players objectAtIndex:indexPath.row];
        }
        if([self.userInvites objectForKey:user.objectId]) {
            [self.userInvites removeObjectForKey:user.objectId];
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_OPEN color:[UIColor colorWithWhite:0.90f alpha:1.0f]];
        } else {
            [self.userInvites setObject:user forKey:user.objectId];
            cell.accessoryView = [MSCellAccessory accessoryWithType:CIRCLE_CHECKMARK color:THEME_COLOR];
        }
    } else if(indexPath.section == 2 || !self.activity) {
        // Contacts
        NSDictionary *record = nil;
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            record = [self.contactSearchResults objectAtIndex:indexPath.row];
            //record = [[self.searchResultDictionary objectForKey:@"contacts"] objectAtIndex:indexPath.row];
        }else{
            record = [self.contacts objectAtIndex:indexPath.row];
        }
        if([self.contactInvites objectForKey:record[@"recordId"]]) {
            [self.contactInvites removeObjectForKey:record[@"recordId"]];
            cell.accessoryView = [MSCellAccessory accessoryWithType:SMS_OPEN color:[UIColor lightGrayColor]];
        } else {
            [self.contactInvites setObject:record forKey:record[@"recordId"]];
            cell.accessoryView = [MSCellAccessory accessoryWithType:SMS_SEND color:THEME_COLOR highlightedColor:THEME_COLOR];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell setNeedsDisplay];
    });
    if(self.userInvites.count > 0 || self.contactInvites.count > 0) {
        self.inviteButton.enabled = TRUE;
    } else {
        self.inviteButton.enabled = FALSE;
    }*/
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;{
    if(!self.pullToRefreshEnabled &&  self.players.count > 0 && !self.endOfResults && self.activity){
        NSInteger total = (self.players.count - 2);
        if (indexPath.row == total)
        {
            if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
            {
                [self loadNextPage];
            }
        }
    }
    if (self.players.count == 0) {
        if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
    }
}

#pragma mark - Search Filter
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    
    //self.userSearchResults = [NSMutableArray arrayWithArray:[self.users filteredArrayUsingPredicate:resultPredicate]];
    //self.playerSearchResults = [NSMutableArray arrayWithArray:[self.players filteredArrayUsingPredicate:resultPredicate]];
    //self.contactSearchResults = [NSMutableArray arrayWithArray:[self.contacts filteredArrayUsingPredicate:resultPredicate]];
    
    //[self.searchResults removeAllObjects];
    //[self.searchResults addObjectsFromArray:self.userSearchResults ];
    //[self.searchResults addObjectsFromArray:self.playerSearchResults ];
    //[self.searchResults addObjectsFromArray:self.contactSearchResults];
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}
- (void)searchDisplayController:(UISearchDisplayController *)controller didLoadSearchResultsTableView:(UITableView *)tableView
{
    //tableView.rowHeight = 64.0f; // or some other height
    [tableView setRowHeight:84.0f];
    tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    tableView.separatorColor = [UIColor colorWithWhite:0.85f alpha:1.0f];
    
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    [tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"User"];
}

#pragma mark - IBActions

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate dismissInviteViewController];
    }];
}

#pragma mark Address Book Delegate

- (void)loadAddressBook{
    [self checkAddressBookAccess];
}

-(void)checkAddressBookAccess
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case  kABAuthorizationStatusAuthorized:
            [self accessGrantedForAddressBook];
            break;
            // Prompt the user for access to Contacts if there is no definitive answer
        case  kABAuthorizationStatusNotDetermined :
            [self requestAddressBookAccess];
            NSLog(@"Not determined");
            break;
            // Display a message if the user has denied or restricted access to Contacts
        case  kABAuthorizationStatusDenied:
            NSLog(@"Denied");
            break;
        case  kABAuthorizationStatusRestricted:
            NSLog(@"Restricted");
            break;
        default:
            break;
    }
}

// Prompt the user for access to their Address Book data
-(void)requestAddressBookAccess
{
    InviteViewController * __weak weakSelf = self;
    
    ABAddressBookRequestAccessWithCompletion(self.addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [weakSelf accessGrantedForAddressBook];
                                                     });
                                                 }
                                             });
}


- (void)accessGrantedForAddressBook{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, ^{
        
        NSArray *originalArray = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(self.addressBook));
        self.emails = [NSMutableArray array];
        
        NSArray *abContactArray = [originalArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            ABRecordRef record1 = (__bridge ABRecordRef)obj1; // get address book record
            NSString *firstName1 = CFBridgingRelease(ABRecordCopyValue(record1, kABPersonFirstNameProperty));
            NSString *lastName1 = CFBridgingRelease(ABRecordCopyValue(record1, kABPersonLastNameProperty));
            
            ABRecordRef record2 = (__bridge ABRecordRef)obj2; // get address book record
            NSString *firstName2 = CFBridgingRelease(ABRecordCopyValue(record2, kABPersonFirstNameProperty));
            NSString *lastName2 = CFBridgingRelease(ABRecordCopyValue(record2, kABPersonLastNameProperty));
            
            NSString *compare1;
            if(firstName1) {
                compare1 = [firstName1 lowercaseString];
            }
            else {
                compare1 = [lastName1 lowercaseString];
            }
            NSString *compare2;
            if(firstName2) {
                compare2 = [firstName2 lowercaseString];
            }
            else {
                compare2 = [lastName2 lowercaseString];
            }
            
            NSComparisonResult result = [compare1  compare:compare2];
            if (result != NSOrderedSame) {
                return result;
            }
            else {
                return [lastName1 compare:lastName2];
            }
            
        }];
        NSMutableArray *contacts = [NSMutableArray array];
        for (id object in abContactArray) {
            ABRecordRef record = (__bridge ABRecordRef)object; // get address book record
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonLastNameProperty));
            
            
            ABRecordID recordID = ABRecordGetRecordID(record);
            NSNumber *recordInt = [NSNumber numberWithInt:recordID];
            
            NSMutableDictionary *data = [NSMutableDictionary dictionary];
            
            NSString *name = @"";
            if(firstName) {
                [data setObject:firstName forKey:@"first"];
                name = firstName;
            }
            if(lastName) {
                [data setObject:lastName forKey:@"last"];
                if (name.length > 0) {
                    name = [NSString stringWithFormat:@"%@ %@", name, lastName];
                }else{
                    name = lastName;
                }
            }
            if(name.length > 0) {
                [data setObject:name forKey:@"name"];
            }
            
            if(record) {
                [data setObject:recordInt forKey:@"recordId"];
            }
            
            if(!firstName && !lastName) {
                continue;
            }
            
            CFTypeRef emailProp = ABRecordCopyValue(record, kABPersonEmailProperty);
            NSMutableArray *emails = [[NSMutableArray alloc] initWithArray:(__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(emailProp)];
            CFRelease(emailProp);
            for(NSString *email in emails){
                [self.emails addObject:email];
            }
            
            [data setObject:emails forKey:@"emails"];
            
            
            CFTypeRef phoneProperty = ABRecordCopyValue(record, kABPersonPhoneProperty);
            NSMutableArray *phoneNumbers = (__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(phoneProperty);
            CFRelease(phoneProperty);
            
            
            for(int i = 0; i < phoneNumbers.count; i++) {
                NSString *phoneNumber = [phoneNumbers objectAtIndex:i];
                
                NSString *phoneStripped = [phoneNumber stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [phoneNumber length])];
                if([phoneStripped length] > 10 && [phoneStripped hasPrefix:@"1"]) {
                    // Subtring the one
                    phoneStripped = [phoneStripped substringFromIndex:1];
                }
                data[@"phone"] = phoneStripped;
                
            }
            [contacts addObject:data];
        }
        if ([PFUser currentUser] && [PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
            [PFFacebookUtils logInWithPermissions:@[@"public_profile",@"email",@"user_friends"] block:^(PFUser *user, NSError *error) {
                if (user) {
                    [FBRequestConnection startForMyFriendsWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                        if (error) {
                            [self loadObjects];
                            return;
                        }
                        NSArray *friendObjects = [result objectForKey:@"data"];
                        friendIds = [NSMutableArray array];
                        for (NSDictionary *friendObject in friendObjects) {
                            [friendIds addObject:[friendObject objectForKey:@"id"]];
                        }
                        [self loadObjects];
                    }];
                }
            }];
            
        }else {
            [self loadObjects];
        }
        //[self loadObjects];
        //[self queryContacts:contacts];
    });
}





#pragma mark Table Load helpers


- (void)queryContacts:(NSMutableArray *)contacts{
    if(self.emails.count == 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self loadOrderedContacts:contacts andEmails:(NSMutableArray *)@[]];
            [self.contentView reloadData];
        });
        return;
    }
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" containedIn:self.emails];
    //NSLog(@"Emails: %@",self.emails);
    query.limit = 1000;
    query.skip = 0;
    query.cachePolicy = kPFCachePolicyCacheElseNetwork;
    if ([self.users count] == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query orderByAscending:@"name"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.users = [NSMutableArray arrayWithArray:objects];
            NSMutableArray *emails = [NSMutableArray array];
            for(PFUser *user in self.users){
                [emails addObject:user.email.lowercaseString];
            }
            [self loadOrderedContacts:contacts andEmails:emails];
            [self.contentView reloadData];
        });
        
    }];
}

- (void)loadOrderedContacts:(NSMutableArray *)contacts andEmails:(NSMutableArray *)emails{
    NSMutableArray *filteredContacts = [NSMutableArray array];
    for(NSDictionary *record in contacts) {
        BOOL add = TRUE;
        NSMutableArray *contactEmails = [record objectForKey:@"email"];
        for(NSString *email in contactEmails) {
            if([emails containsObject:email.lowercaseString]){
                add = FALSE;
                break;
            }
        }
        if(add){
            [filteredContacts addObject:record];
        }
    }
    self.contacts = [[NSMutableArray alloc] initWithArray:filteredContacts];
}

#pragma mark - IBActions

- (IBAction)remind:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.contentView];
    NSIndexPath *selectedIndex = [self.contentView indexPathForRowAtPoint:buttonPosition];
    
    if([self.players objectAtIndex:selectedIndex.row] != nil) {
            
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
        timeFormatter.dateFormat = @"h:mma";
        NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
        dateTimeFormatter.dateFormat = @"EEE, MMM d, YYYY";
        
        NSString *first = [[PFUser currentUser] objectForKey:@"first"];
        NSString *last = [[PFUser currentUser] objectForKey:@"last"];
        if(!first) {
            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        NSString *full = first;
        if(last && last.length > 0) {
            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
        }
        
        
        PFQuery *pushQuery = [PFInstallation query];
        [pushQuery whereKey:@"user" equalTo:[self.players objectAtIndex:selectedIndex.row]];
        NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"alert": [NSString stringWithFormat:@"%@ invited you to play on %@ at %@", full,[dateTimeFormatter stringFromDate: self.activity.startTime],[timeFormatter stringFromDate: self.activity.startTime]], @"badge" : @"Increment", @"i": self.activity.objectId}];
        if(self.activity) {
            data[@"a"] = self.activity.objectId;
        }
        
        //NSLog(@"Recipients: %@",self.userInvites);
        
        PFPush *push = [[PFPush alloc] init];
        [push setQuery:pushQuery];
        [push setData:data];
        [push sendPushInBackground];
    }



    /*
     NSString *string = [NSString stringWithFormat:@"MatchOn! Let's play tennis!"];
     NSMutableArray *sharingItems = [[NSMutableArray alloc] initWithArray:@[string]];
     UIActivityViewController *activityController =
     [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
     [self presentViewController:activityController animated:YES completion:nil];
     [self.contacts removeAllObjects];
     */
    
}

- (IBAction)invite:(id)sender {
    [self.emails removeAllObjects];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.contentView];
    NSIndexPath *selectedIndex = [self.contentView indexPathForRowAtPoint:buttonPosition];
    
    if([self.players objectAtIndex:selectedIndex.row] != nil) {
        // Invite users first
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Sending invites";
        hud.margin = 20.f;
        hud.removeFromSuperViewOnHide = YES;
        
        ActivityModel *invite = [ActivityModel object];
        invite.type = @"invite";
        //invite.parent = self.activity;
        invite.user = [PFUser currentUser];
        [invite.recipients addObject:[self.players objectAtIndex:selectedIndex.row]];
        //invite.totalRecipients = (int)invite.recipients.count;
        [invite saveInBackground];
        
        if(!self.activity.invitations) {
            self.activity.invitations = [NSMutableArray array];
        }
        [self.activity.invitations addObject:[self.players objectAtIndex:selectedIndex.row]];
        
        [self.activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
            timeFormatter.dateFormat = @"h:mma";
            NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
            dateTimeFormatter.dateFormat = @"EEE, MMM d, YYYY";
            
            NSString *first = [[PFUser currentUser] objectForKey:@"first"];
            NSString *last = [[PFUser currentUser] objectForKey:@"last"];
            if(!first) {
                NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
                first = [name firstObject];
                if(!last) {
                    if(name.count > 1) {
                        last = [name lastObject];
                    }
                }
            }
            NSString *full = first;
            if(last && last.length > 0) {
                full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
            }
            
            
            PFQuery *pushQuery = [PFInstallation query];
            NSLog(@"%@",[self.players objectAtIndex:selectedIndex.row]);
            [pushQuery whereKey:@"user" equalTo:[self.players objectAtIndex:selectedIndex.row]];
            NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithDictionary:@{@"alert": [NSString stringWithFormat:@"%@ invited you to play on %@ at %@", full,[dateTimeFormatter stringFromDate: self.activity.startTime],[timeFormatter stringFromDate: self.activity.startTime]], @"badge" : @"Increment", @"i": self.activity.objectId}];
            if(self.activity) {
                data[@"a"] = self.activity.objectId;
            }
            
            //NSLog(@"Recipients: %@",self.userInvites);
            
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.contentView reloadData];
            [Localytics tagEvent:@"Invitation Sent"];
            /*if(self.contactInvites.count > 0) {
             [self showSMS];
             [self.contactInvites removeAllObjects];
             [self.userInvites removeAllObjects];
             [self.contentView reloadData];
             } else {
             dispatch_async(dispatch_get_main_queue(), ^{
             [self dismissViewControllerAnimated:YES completion:^{
             [self.delegate dismissInviteViewController];
             
             }];
             });
             }*/
            
        }];
    }
    
    
    
    /*
     NSString *string = [NSString stringWithFormat:@"MatchOn! Let's play tennis!"];
     NSMutableArray *sharingItems = [[NSMutableArray alloc] initWithArray:@[string]];
     UIActivityViewController *activityController =
     [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
     [self presentViewController:activityController animated:YES completion:nil];
     [self.contacts removeAllObjects];
     */
    
}

#pragma mark - Send SMS

- (void)showSMS {
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor blueColor]]; // this will change the back button tint
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    self.messageViewController = [[MFMessageComposeViewController alloc] init];
    
    self.messageViewController.messageComposeDelegate = self;
    
    
    if(!self.activity) {
        self.messageViewController.body =  [NSString stringWithFormat:@"Let's play tennis. To join, download MatchOn from the app store, %@",self.link];
    } else {
        self.messageViewController.body =  [NSString stringWithFormat:@"Let's play tennis. To join my match, download MatchOn from the app store, %@",self.link];
    }
    NSMutableArray *phones = [NSMutableArray array];
    //NSLog(@"Contacts: %@",self.contactInvites);
    for(NSDictionary *contact in [self.contactInvites allValues]) {
        [phones addObject:contact[@"phone"]];
    }
    
    [self.messageViewController setRecipients:phones];
    
    [self presentViewController:self.messageViewController animated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultSent:
            break;
        case MessageComposeResultFailed:
            break;
        default:
            break;
    }
    [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
        //[self contactsNav:self];
        [self.delegate dismissInviteViewController];
    }];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:THEME_COLOR,NSForegroundColorAttributeName, nil]];
    [[UINavigationBar appearance] setTintColor:THEME_COLOR];
}


- (IBAction)onInviteContact:(id)sender {
    [self performSegueWithIdentifier:@"InviteContact" sender:nil];
}

- (IBAction)onAllDone:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate dismissInviteViewController];
    }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"InviteContact"]) {
        InviteContactViewController *contactController = (InviteContactViewController *)segue.destinationViewController;
        contactController.parent = self;
        contactController.activity = self.activity;
    }
}

@end
