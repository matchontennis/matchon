//
//  FeedModel.m
//  MatchOn
//
//  Created by Kevin Flynn on 4/26/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "FeedModel.h"

@implementation FeedModel

@dynamic segue;
@dynamic button;
@dynamic image;
@dynamic number;
@dynamic active;
@dynamic text;


+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Feed";
}

@end
