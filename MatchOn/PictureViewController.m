//
//  PictureViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "PictureViewController.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import "LocationViewController.h"
#import "SignupViewController.h"
#import "MBProgressHUD.h"
#import "HelpViewController.h"

@interface PictureViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate,LocationViewControllerDelegate, UITextFieldDelegate>

@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) UIImage *image;

@end

@implementation PictureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    bAsked = NO;
    
    self.viewPhoto.layer.cornerRadius = self.viewPhoto.frame.size.width / 2;
    self.viewPhoto.layer.borderColor = [[UIColor blackColor] CGColor];
    self.viewPhoto.layer.borderWidth = 2.0f;
    
    self.txtAboutMe.layer.cornerRadius = 5.0f;
    self.txtAboutMe.layer.borderColor = [[UIColor colorWithRed:222 / 255.0f green:222 / 255.0f blue:232 / 255.0f alpha:1.0f] CGColor];
    self.txtAboutMe.layer.borderWidth = 1.0f;
    
    self.viewPhotoBar.layer.borderColor = [[UIColor colorWithRed:222 / 255.0f green:222 / 255.0f blue:232 / 255.0f alpha:1.0f] CGColor];
    self.viewPhotoBar.layer.borderWidth = 1.0f;
    
    self.viewHomeCourt.layer.borderColor = [[UIColor colorWithRed:222 / 255.0f green:222 / 255.0f blue:232 / 255.0f alpha:1.0f] CGColor];
    self.viewHomeCourt.layer.borderWidth = 1.0f;
    
    self.doneButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.doneButton.layer.borderWidth = 1.0f;
    self.doneButton.layer.cornerRadius = 3.0f;
    self.doneButton.clipsToBounds = YES;
    
    self.scrMain.translatesAutoresizingMaskIntoConstraints = YES;
    
    CGRect frame = self.scrMain.frame;
    frame.origin.y = 0;
    frame.size = CGSizeMake([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height - 64);
    
    self.scrMain.frame = frame;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addImage:)];
    [tap setCancelsTouchesInView:YES];
    [self.viewPhoto addGestureRecognizer:tap];
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onLocation:)];
    [tap1 setCancelsTouchesInView:YES];
    [self.viewHomeCourt addGestureRecognizer:tap1];
    
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [tap2 setCancelsTouchesInView:YES];
    [self.view addGestureRecognizer:tap2];
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

-(void)onLocation:(UITapGestureRecognizer *)gesture
{
    [self performSegueWithIdentifier:@"location" sender:nil];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.photoButton.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.photoButton.layer.borderWidth = 2.0f;
    self.photoButton.layer.cornerRadius = self.photoButton.frame.size.height/2.0f;
    self.photoButton.clipsToBounds = YES;
    self.doneButton.layer.cornerRadius = 3.0f;
    self.doneButton.clipsToBounds = YES;
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    
    if ([PFUser currentUser]) {
        PFFile *image = [PFUser currentUser][@"image"];
        if (image != nil && ![image isEqual:[NSNull null]] && image.isDataAvailable) {
            [image getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
                if (error) {
                    return;
                }
                self.imgPhoto.image = [UIImage imageWithData:data];
                curImage = self.imgPhoto.image;
            }];
        }
    }
    
}

- (IBAction)addImage:(id)sender {
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] < 8) {
        // iOS 7
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                @"Take Photo",
                                @"Choose Existing Photo",
                                nil];
        
        for(UIButton *button in [popup subviews]) {
            NSLog(@"Button title: %@",button.titleLabel.text);
            if([button.titleLabel.text isEqualToString:@"Take Photo"]) {
                [button addTarget:self action:@selector(photo:) forControlEvents:UIControlEventTouchUpInside];
            }
            else if([button.titleLabel.text isEqualToString:@"Choose Existing Photo"]) {
                [button addTarget:self action:@selector(album:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else {
        NSLog(@"iOS 8");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:YES completion:^{
                NSLog(@"Dismissed");
            }];
        }];
        UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self photo:self];
        }];
        UIAlertAction *albumAction = [UIAlertAction actionWithTitle:@"Choose Existing Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self album:self];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:cameraAction];
        [alertController addAction:albumAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    
}

- (IBAction)done:(id)sender {
    // Signup the user
    [self dismissKeyboard];
    PFUser *user = [PFUser currentUser];
    
    NSLog(@"%@,%@",self.txtAboutMe.text, self.lblHomeCourt.text);
    if(!curImage || !self.txtAboutMe.text || [self.txtAboutMe.text isEqualToString:@""] || [self.lblHomeCourt.text isEqualToString:@""]) {
        if (!bAsked) {
            bAsked = YES;
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Profile Error" message:@"Players are more likely to play with players who fill out their profile" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return;
        }
    }
    if (!curImage && (!self.txtAboutMe.text || [self.txtAboutMe.text isEqualToString:@""]) && [self.lblHomeCourt.text isEqualToString:@""]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults objectForKey:@"existing"] == nil) {
            [self performSegueWithIdentifier:@"help" sender:nil];
        }else {
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
        return;
    }
    if (curImage != nil) {
        NSData *imageData = UIImagePNGRepresentation(curImage);
        if(imageData) {
            PFFile *file = [PFFile fileWithName:@"image.png" data:imageData];
            
            if(file){
                [user setObject:file forKey:@"image"];
                
            }
        }
    }
    
    if (![self.lblHomeCourt.text isEqualToString:@""]) {
        [user setObject:self.lblHomeCourt.text forKey:@"homecourt"];
    }
    
    if (self.geopt) {
        [user setObject:self.geopt forKey:@"geopoint"];
    }
    
    if (![self.txtAboutMe.text isEqualToString:@""]) {
        [user setObject:self.txtAboutMe.text forKey:@"aboutme"];
    }
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;


    [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (!error) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            if ([defaults objectForKey:@"existing"] == nil) {
                [self performSegueWithIdentifier:@"help" sender:nil];
            }else {
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        } else {
            NSString *errorString = [error userInfo][@"error"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry" message:errorString delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }];
}

- (IBAction)onback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)photo:(id)sender {
    NSLog(@"Photo selected");
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = NO;
    // If image is available
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    }
    else {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

- (IBAction)album:(id)sender {
    
    self.imagePicker = [[UIImagePickerController alloc] init];
    self.imagePicker.delegate = self;
    self.imagePicker.allowsEditing = NO;
    
    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    self.imagePicker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:self.imagePicker.sourceType];
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

#pragma mark - Image picker controller delegate

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {

        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        UIGraphicsBeginImageContext(CGSizeMake(image.size.width/2.0f,image.size.height/2.0f));
        [image drawInRect: CGRectMake(0, 0, image.size.width/2.0f,image.size.height/2.0f)];
        UIImage *smallImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgPhoto.image = smallImage;
            curImage = smallImage;
            self.imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
        });
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"location"]){
        LocationViewController *location = (LocationViewController *)[segue destinationViewController];
        location.delegate = self;
        location.geopt = self.geopt;
        location.courtName = self.court;
    }
}

-(void)setLocation:(PFGeoPoint *)geopoint courtName:(NSString *)courtName
{
    self.geopt = geopoint;
    self.court = courtName;
    [self setLocationFromGeoPoint];
}

- (void)setLocationFromGeoPoint
{
    if (self.court != nil && [self.court isEqualToString:@""]) {
        self.lblHomeCourt.text = self.court;
        return;
    }
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *l = [[CLLocation alloc] initWithLatitude:self.geopt.latitude longitude:self.geopt.longitude];
    [geocoder reverseGeocodeLocation:l completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"%@",error);
        CLPlacemark *place = [placemarks objectAtIndex:0];
        self.location = place.addressDictionary;
        
        [self setLocationForLabel];
    }];
}

- (void)setLocationForLabel {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *string = @"";
        if([self.location objectForKey:@"Street"]){
            string = [NSString stringWithFormat:@"%@, ",self.location[@"Street"]];
        }
        if([self.location objectForKey:@"City"]){
            string = [NSString stringWithFormat:@"%@%@",string, self.location[@"City"]];
        }
        if([self.location objectForKey:@"State"]){
            if([self.location objectForKey:@"City"]){
                string = [NSString stringWithFormat:@"%@, ",string];
            }
            string = [NSString stringWithFormat:@"%@%@",string, self.location[@"State"]];
        }
        if (self.court != nil && ![self.court isEqualToString:@""]) {
            CGRect frame = self.lblCourt.frame;
            frame.size.height = 33;
            self.lblCourt.translatesAutoresizingMaskIntoConstraints = YES;
            self.lblCourt.frame = frame;
            self.lblHomeCourt.text = self.court;
        }else if(![self.location objectForKey:@"City"]) {
            CGRect frame = self.lblCourt.frame;
            frame.size.height = 44;
            self.lblCourt.translatesAutoresizingMaskIntoConstraints = YES;
            self.lblCourt.frame = frame;
            self.lblHomeCourt.text = @"";
        } else if ([self.location objectForKey:@"Name"] != nil && ![[self.location objectForKey:@"Name"] isEqualToString:@""]) {
            CGRect frame = self.lblCourt.frame;
            frame.size.height = 33;
            self.lblCourt.translatesAutoresizingMaskIntoConstraints = YES;
            self.lblCourt.frame = frame;
            self.lblHomeCourt.text = self.location[@"Name"];
        }else {
            CGRect frame = self.lblCourt.frame;
            frame.size.height = 33;
            self.lblCourt.translatesAutoresizingMaskIntoConstraints = YES;
            self.lblCourt.frame = frame;
            self.lblHomeCourt.text = string;
        }
    });
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    curOffset = self.scrMain.contentOffset;
    if ([textView.text isEqualToString:@"Introduce yourself to other local players..."]) {
        textView.text = @"";
    }
    self.scrMain.contentOffset = CGPointMake(0, self.txtAboutMe.frame.origin.y - 20);
    curBarItem = self.navigationItem.rightBarButtonItem;
    self.navigationItem.rightBarButtonItem = [self addDoneBtn];
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    curNote = textView.text;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    curNote = textView.text;
    if (textView.text.length == 0) {
        textView.text = @"Introduce yourself to other local players...";
    }
    self.scrMain.contentOffset = curOffset;
    self.navigationItem.rightBarButtonItem = curBarItem;
    return YES;
}

-(void)viewDidLayoutSubviews
{
    self.scrMain.contentSize = CGSizeMake([UIScreen mainScreen].bounds.size.width, 568);
}

-(void)actionDone
{
    [self.txtAboutMe endEditing:YES];
}

-(UIBarButtonItem *)addDoneBtn
{
    UIButton *doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    doneBtn.frame = CGRectMake(0, 0, 54, 44);
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(actionDone) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *doneBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 54, 44)];
    
    doneBtnView.bounds = CGRectOffset(doneBtnView.bounds, -10, 0);
    [doneBtnView addSubview:doneBtn];
    
    UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithCustomView:doneBtnView];
    return cancelItem;
}

@end
