//
//  NotsurePopupViewController.m
//  MatchOn
//
//  Created by Sol on 4/24/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "NotsurePopupViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface NotsurePopupViewController ()

@end

@implementation NotsurePopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onClose:(id)sender {
    if (self.parent) {
        [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
