//
//  InviteContactViewController.m
//  MatchOn
//
//  Created by Sol on 4/24/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "InviteContactViewController.h"
#import <AddressBook/ABPerson.h>
#import <AddressBook/AddressBook.h>
#import "Constants.h"
#import "Localytics.h"

@interface InviteContactViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, assign) ABAddressBookRef addressBook;

@end

@implementation InviteContactViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.btnInvite.layer.cornerRadius = 3.0f;
    self.btnInvite.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.btnInvite.layer.borderWidth = 2.0f;
    
    //self.contacts = [NSMutableArray array];
    //self.contactSearchResults = [NSMutableArray array];
    self.emails = [NSMutableArray array];
    //self.contactInvites = [NSMutableArray array];
    
    self.searchBar.delegate = self;
    
    self.title = @"Invite Contacts";
    
    self.navigationController.navigationBar.translucent = NO;
    
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor whiteColor]};
    
    self.tblContacts.contentInset = UIEdgeInsetsMake(50, 0, 0, 0);
    
    if (self.contacts.count == 0) {
        self.viewEmpty.hidden = NO;
        UIImageView *imgLogo = (UIImageView *)[self.viewEmpty viewWithTag:101];
        UILabel *lblEmpty = (UILabel *)[self.viewEmpty viewWithTag:100];
        if (self.contacts == nil) {
            imgLogo.hidden = NO;
            lblEmpty.hidden = YES;
        }else {
            imgLogo.hidden = YES;
            lblEmpty.hidden = NO;
        }
    }else {
        self.viewEmpty.hidden = YES;
    }
    
    _addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
    
    [self loadAddressBook];
    [PFConfig getConfigInBackgroundWithBlock:^(PFConfig *config, NSError *error) {
        self.link = [config objectForKey:@"invite"];
        //NSLog(@"Config: %@",config);
        if(!self.link) self.link = @"http://www.matchon.co";
    }];
    
    self.searchBar.tintColor = [UIColor grayColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    /*if (self.contacts == nil) {
        tableView.bounces = NO;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        tableView.scrollEnabled = NO;
        return 1;
    }else {
        tableView.bounces = YES;
        tableView.scrollEnabled = YES;
        if (self.contacts.count == 0) {
            tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
            return 1;
        }else {*/
            tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
            if (self.searchBar.text == nil || [self.searchBar.text isEqualToString:@""]) {
                return self.contacts.count;
            }else {
                return self.contactSearchResults.count;
            }
        //}
    //}
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*if (self.contacts == nil) {
        return tableView.frame.size.height;
    }else {
        tableView.bounces = YES;
        if (self.contacts.count == 0) {
            return 150.0f;
        }else {*/
            return 64.0f;
        //}
    //}
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSDictionary *contact;
    /*if (self.contacts == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"LogoCell" forIndexPath:indexPath];
    }else {
        if (self.contacts.count == 0) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        }else {*/
    cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    if ((self.searchBar.text == nil || [self.searchBar.text isEqualToString:@""])) {
        contact = [self.contacts objectAtIndex:indexPath.row];
    }else {
        contact = [self.contactSearchResults objectAtIndex:indexPath.row];
    }
    UILabel *lblName = (UILabel *)[cell viewWithTag:10];
    lblName.text = [contact objectForKey:@"name"];
    
    UILabel *lblPhone = (UILabel *)[cell viewWithTag:12];
    NSString *phone = [contact objectForKey:@"phone"];
    if (phone != nil) {
        lblPhone.text = [self convert_to_phone:phone];
    }else {
        lblPhone.text = @"";
    }
    return cell;
        //}
    //}
    //return cell;
}

-(NSString *)convert_to_phone:(NSString *)unformatted
{
    if (unformatted.length <= 6) {
        return @"";
    }
    NSArray *stringComponents = [NSArray arrayWithObjects:[unformatted substringWithRange:NSMakeRange(0, 3)], [unformatted substringWithRange:NSMakeRange(3, 3)], [unformatted substringWithRange:NSMakeRange(6, unformatted.length - 6)], nil];
    NSString *formattedString = @"";
    for (NSString *stringComponent in stringComponents) {
        if (![formattedString isEqualToString:@""]) {
            formattedString = [formattedString stringByAppendingString:@"-"];
        }
        formattedString = [formattedString stringByAppendingString:stringComponent];
    }
    return formattedString;
}

-(void)check:(UIButton *)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tblContacts];
    NSIndexPath *selectedIndex = [self.tblContacts indexPathForRowAtPoint:buttonPosition];
    
    if ([self.searchBar.text isEqualToString:@""]) {
        if (self.contacts.count == 0) {
            return;
        }
    }
    
    if (sender.selected) {
        [sender setSelected:NO];
    }else {
        [sender setSelected:YES];
    }
     
    NSDictionary *contact;
    if ([self.searchBar.text isEqualToString:@""]) {
        contact = [self.contacts objectAtIndex:selectedIndex.row];
    }else {
        contact = [self.contactSearchResults objectAtIndex:selectedIndex.row];
    }
    if (self.contactInvites == nil) {
        self.contactInvites = [NSMutableArray array];
    }
    if (sender.selected) {
        [self.contactInvites addObject:contact];
    }else {
        [self.contactInvites removeObject:contact];
    }
}

#pragma mark Address Book Delegate

- (void)loadAddressBook{
    [self checkAddressBookAccess];
}

-(void)checkAddressBookAccess
{
    switch (ABAddressBookGetAuthorizationStatus())
    {
            // Update our UI if the user has granted access to their Contacts
        case  kABAuthorizationStatusAuthorized:
            [self accessGrantedForAddressBook];
            break;
            // Prompt the user for access to Contacts if there is no definitive answer
        case  kABAuthorizationStatusNotDetermined :
            [self requestAddressBookAccess];
            NSLog(@"Not determined");
            break;
            // Display a message if the user has denied or restricted access to Contacts
        case  kABAuthorizationStatusDenied:
            NSLog(@"Denied");
            break;
        case  kABAuthorizationStatusRestricted:
            NSLog(@"Restricted");
            break;
        default:
            break;
    }
}

// Prompt the user for access to their Address Book data
-(void)requestAddressBookAccess
{
    InviteContactViewController * __weak weakSelf = self;
    
    ABAddressBookRequestAccessWithCompletion(self.addressBook, ^(bool granted, CFErrorRef error)
                                             {
                                                 if (granted)
                                                 {
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         [weakSelf accessGrantedForAddressBook];
                                                     });
                                                 }
                                             });
}


- (void)accessGrantedForAddressBook{
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    dispatch_async(queue, ^{
        
        NSArray *originalArray = CFBridgingRelease(ABAddressBookCopyArrayOfAllPeople(self.addressBook));
        self.emails = [NSMutableArray array];
        
        NSArray *abContactArray = [originalArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            ABRecordRef record1 = (__bridge ABRecordRef)obj1; // get address book record
            NSString *firstName1 = CFBridgingRelease(ABRecordCopyValue(record1, kABPersonFirstNameProperty));
            NSString *lastName1 = CFBridgingRelease(ABRecordCopyValue(record1, kABPersonLastNameProperty));
            
            ABRecordRef record2 = (__bridge ABRecordRef)obj2; // get address book record
            NSString *firstName2 = CFBridgingRelease(ABRecordCopyValue(record2, kABPersonFirstNameProperty));
            NSString *lastName2 = CFBridgingRelease(ABRecordCopyValue(record2, kABPersonLastNameProperty));
            
            NSString *compare1;
            if(firstName1) {
                compare1 = [firstName1 lowercaseString];
            }
            else {
                compare1 = [lastName1 lowercaseString];
            }
            NSString *compare2;
            if(firstName2) {
                compare2 = [firstName2 lowercaseString];
            }
            else {
                compare2 = [lastName2 lowercaseString];
            }
            
            NSComparisonResult result = [compare1  compare:compare2];
            if (result != NSOrderedSame) {
                return result;
            }
            else {
                return [lastName1 compare:lastName2];
            }
            
        }];
        
        NSMutableArray *tempContacts = [[NSMutableArray alloc] init];
        for (id object in abContactArray) {
            ABRecordRef record = (__bridge ABRecordRef)object; // get address book record
            NSString *firstName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonFirstNameProperty));
            NSString *lastName = CFBridgingRelease(ABRecordCopyValue(record, kABPersonLastNameProperty));
            
            
            ABRecordID recordID = ABRecordGetRecordID(record);
            NSNumber *recordInt = [NSNumber numberWithInt:recordID];
            
            NSMutableDictionary *data = [NSMutableDictionary dictionary];
            
            NSString *name = @"";
            if(firstName) {
                [data setObject:firstName forKey:@"first"];
                name = firstName;
            }
            if(lastName) {
                [data setObject:lastName forKey:@"last"];
                if (name.length > 0) {
                    name = [NSString stringWithFormat:@"%@ %@", name, lastName];
                }else{
                    name = lastName;
                }
            }
            if(name.length > 0) {
                [data setObject:name forKey:@"name"];
            }
            
            if(record) {
                [data setObject:recordInt forKey:@"recordId"];
            }
            
            if(!firstName && !lastName) {
                continue;
            }
            
            CFTypeRef emailProp = ABRecordCopyValue(record, kABPersonEmailProperty);
            NSMutableArray *emails = [[NSMutableArray alloc] initWithArray:(__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(emailProp)];
            CFRelease(emailProp);
            for(NSString *email in emails){
                [self.emails addObject:email];
            }
            
            [data setObject:emails forKey:@"emails"];
            
            
            CFTypeRef phoneProperty = ABRecordCopyValue(record, kABPersonPhoneProperty);
            NSMutableArray *phoneNumbers = (__bridge_transfer NSMutableArray *)ABMultiValueCopyArrayOfAllValues(phoneProperty);
            CFRelease(phoneProperty);
            
            
            for(int i = 0; i < phoneNumbers.count; i++) {
                NSString *phoneNumber = [phoneNumbers objectAtIndex:i];
                
                NSString *phoneStripped = [phoneNumber stringByReplacingOccurrencesOfString:@"[^0-9]" withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0, [phoneNumber length])];
                if([phoneStripped length] > 10 && [phoneStripped hasPrefix:@"1"]) {
                    // Subtring the one
                    phoneStripped = [phoneStripped substringFromIndex:1];
                }
                data[@"phone"] = phoneStripped;
            }
            if (data[@"phone"] != nil) {
                [tempContacts addObject:data];
            }
        }
        self.contacts = tempContacts;
        if (self.contacts.count == 0) {
            self.viewEmpty.hidden = NO;
            UIImageView *imgLogo = (UIImageView *)[self.viewEmpty viewWithTag:101];
            UILabel *lblEmpty = (UILabel *)[self.viewEmpty viewWithTag:100];
            imgLogo.hidden = YES;
            lblEmpty.hidden = NO;
        }else {
            self.viewEmpty.hidden = YES;
        }
        [self.tblContacts reloadData];
        //[self loadObjects];
        //[self queryContacts:contacts];
    });
}

- (void)showSMS {
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor blueColor]]; // this will change the back button tint
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    self.messageViewController = [[MFMessageComposeViewController alloc] init];
    
    self.messageViewController.messageComposeDelegate = self;
    
    
    if(!self.activity) {
        self.messageViewController.body =  [NSString stringWithFormat:@"Let's play tennis. To join, download MatchOn from the app store, %@",self.link];
    } else {
        self.messageViewController.body =  [NSString stringWithFormat:@"Let's play tennis. To join my match, download MatchOn from the app store, %@",self.link];
    }
    NSMutableArray *phones = [NSMutableArray array];
    //NSLog(@"Contacts: %@",self.contactInvites);
    for(NSDictionary *contact in self.contactInvites) {
        if (contact[@"phone"] != nil) {
            [phones addObject:contact[@"phone"]];
        }
    }
    
    [self.messageViewController setRecipients:phones];
    
    [self presentViewController:self.messageViewController animated:YES completion:NULL];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    // Notifies users about errors associated with the interface
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultSent:
            [Localytics tagEvent:@"Invitation Sent"];
            break;
        case MessageComposeResultFailed:
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:^{
        //[self contactsNav:self];
        //[self.parentViewController.delegate dismissInviteViewController];
    }];
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:THEME_COLOR,NSForegroundColorAttributeName, nil]];
    [[UINavigationBar appearance] setTintColor:THEME_COLOR];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onInvite:(id)sender {
    if (self.contactInvites.count == 0) {
        return;
    }
    [self showSMS];
}

- (IBAction)onback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        [self.parent.delegate dismissInviteViewController];
    }];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (self.searchBar.text == nil || [self.searchBar.text isEqualToString:@""]) {
        self.contactSearchResults = [NSMutableArray arrayWithArray:self.contacts];
    }else {
        [self.contactSearchResults removeAllObjects];
        for (int i = 0; i < self.contacts.count; i++) {
            NSDictionary *contact = [self.contacts objectAtIndex:i];
            NSRange range1 = [[[contact objectForKey:@"name"] lowercaseString] rangeOfString:[self.searchBar.text lowercaseString]];
            //NSRange range2 = [[contact objectForKey:@"phone"] rangeOfString:self.searchBar.text];
            if (range1.location != NSNotFound) {
                if (self.contactSearchResults == nil) {
                    self.contactSearchResults = [[NSMutableArray alloc] init];
                }
                [self.contactSearchResults addObject:contact];
            }
        }
    }
    [self.tblContacts reloadData];
}

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
    return YES;
}

-(BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = NO;
    return YES;
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.view endEditing:YES];
}

@end
