//
//  WizardViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "RatingModel.h"

@interface WizardViewController : UIViewController

@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (nonatomic, strong) RatingModel *rating;
@property (weak, nonatomic) IBOutlet UILabel *explanation;
@property (nonatomic, strong) UIViewController *prevViewController;
- (IBAction)rating:(id)sender;

- (IBAction)next:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *descriptionFrame;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

- (IBAction)onNotSure:(id)sender;
- (IBAction)onback:(id)sender;
@end
