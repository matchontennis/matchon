//
//  PointPopupViewController.m
//  MatchOn
//
//  Created by Sol on 4/22/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "PointPopupViewController.h"
#import "UIViewController+MJPopupViewController.h"

@interface PointPopupViewController () <MFMailComposeViewControllerDelegate>

@end

@implementation PointPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClose:(id)sender {
    if (self.parent) {
        [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    }
    [self.delegate dismissPointPopup];
}

- (IBAction)onFeedback:(id)sender {
    [self showMail];
}

- (void)showMail{
    NSDictionary *dimensions = @{ @"type": @"mail",
                                  @"objectId": [PFUser currentUser].objectId};
    // Send the dimensions to Parse along with the 'search' event
    [PFAnalytics trackEvent:@"share" dimensions:dimensions];
    
    self.mailViewController = [[MFMailComposeViewController alloc] init];
    self.mailViewController.mailComposeDelegate = self;
    [self.mailViewController setToRecipients:@[@"feedback@matchon.co"]];
    [self.mailViewController setSubject:@"MatchOn Feedback"];
    
    NSString *messageBody = @"";
    
    [self.mailViewController setMessageBody:messageBody isHTML:YES];
    
    [self presentViewController:self.mailViewController animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (self.parent) {
            [self.parent dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
        }
        [self.delegate dismissPointPopup];
    }];
}

@end
