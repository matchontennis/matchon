//
//  InviteContactViewController.h
//  MatchOn
//
//  Created by Sol on 4/24/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ActivityModel.h"
#import "InviteViewController.h"

@interface InviteContactViewController : UIViewController <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnInvite;
@property (weak, nonatomic) IBOutlet UITableView *tblContacts;
@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic, strong) NSMutableArray *contactSearchResults;
@property (nonatomic, strong) NSMutableArray *contactInvites;

@property (nonatomic, strong) InviteViewController *parent;

@property (nonatomic, strong) ActivityModel *activity;

@property (nonatomic, strong) NSMutableArray *emails;
@property (nonatomic, strong) NSString *link;

@property (nonatomic, strong) MFMessageComposeViewController *messageViewController;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIView *viewEmpty;

- (IBAction)onInvite:(id)sender;
- (IBAction)onback:(id)sender;
- (IBAction)cancel:(id)sender;
- (IBAction)check:(id)sender;

@end
