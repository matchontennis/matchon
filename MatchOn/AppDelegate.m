//
//  AppDelegate.m
//  MatchOn
//
//  Created by Kevin Flynn on 11/22/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "Constants.h"
#import "MessageViewController.h"
#import "ScoreViewController.h"
#import "HomeViewController.h"
#import "ActivityModel.h"
#import "PlayViewController.h"
//#import <Instabug/Instabug.h>
#import "Localytics.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Parse setApplicationId:@"i91SEe41h2vEOGyInDb2YbCWxEARTpXWniw4zR1S"
                  clientKey:@"JYqtBU70rQSgeErIWeuiWSjaQhajC1xM0ywpooDF"];
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    [PFFacebookUtils initializeFacebook];
    [Fabric with:@[CrashlyticsKit]];
    
    //[PFFacebookUtils unlinkUser:[PFUser currentUser]];
    [Localytics autoIntegrate:LOCALYTICS_APP_KEY launchOptions:launchOptions];
    
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        // use registerUserNotificationSettings
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:notificationSettings];
        [application registerForRemoteNotifications];
        
    } else {
        // use registerForRemoteNotifications
        [application registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
    }
    
    // Tracking
    if (application.applicationState != UIApplicationStateBackground) {
        // Track an app open here if we launch with a push, unless
        // "content_available" was used to trigger a background push (introduced
        // in iOS 7). In that case, we skip tracking here to avoid double
        // counting the app-open.
        BOOL preBackgroundPush = ![application respondsToSelector:@selector(backgroundRefreshStatus)];
        BOOL oldPushHandlerOnly = ![self respondsToSelector:@selector(application:didReceiveRemoteNotification:fetchCompletionHandler:)];
        BOOL noPushPayload = ![launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
        if (preBackgroundPush || oldPushHandlerOnly || noPushPayload) {
            [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
        }
    }
    
    [self customizeUserInterface];
    
    //[Instabug startWithToken:@"a440307cac720de4f75629e5e651acc3"captureSource:IBGCaptureSourceUIKit invocationEvent:IBGInvocationEventShake];
    
    
    // Extract the notification data
    //NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    //NSLog(@"Notification payload: %@",notificationPayload);
    // Create a pointer to the Photo object
    
    
    [self handlePush:launchOptions];
    
    [self loadDefaultCourts];
    
    /*[PFCloud callFunctionInBackground:@"refreshChallenge" withParameters:@{} block:^(NSString *result,NSError *error) {
        NSLog(@"%@",error);
        if (!error) {
            NSLog(@"%@",result);
        }
    }];*/
    return YES;
}

-(void)loadDefaultCourts
{
    NSString *fullPath = [[NSBundle mainBundle] pathForResource:@"tennis" ofType:@"csv"];
    NSError *error;
    NSString *csvData = [NSString stringWithContentsOfFile:fullPath encoding:NSUTF8StringEncoding error:&error];
    NSArray *rawData = [csvData componentsSeparatedByString:@"\n"];
    self.courts = [[NSMutableArray alloc] init];
    for (int i = 1; i < rawData.count; i++)
    {
        NSString *rawString = [NSString stringWithFormat:@"%@", [rawData objectAtIndex:i]];
        if (rawString == nil || [rawString isEqualToString:@""]) {
            continue;
        }
        NSArray *postStr = [rawString componentsSeparatedByString:@","];
        if ([postStr[11] isEqualToString:@""]) {
            continue;
        }
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:postStr[0] forKey:@"name"];
        [dic setObject:[NSString stringWithFormat:@"%@, %@, %@ %@",postStr[3],postStr[4],postStr[5],postStr[6]] forKey:@"full_address"];
        [dic setObject:postStr[11] forKey:@"latitude"];
        [dic setObject:postStr[12] forKey:@"longitude"];
        [self.courts addObject:dic];
    }
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    [FBSession.activeSession setStateChangeHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         
         // Retrieve the app delegate
         //AppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
         // Call the app delegate's sessionStateChanged:state:error method to handle session state changes
         [self sessionStateChanged:session state:state error:error];
     }];
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}

-(void)sessionStateChanged:(FBSession *)session state:(FBSessionState)state error:(NSError *)error
{
    NSLog(@"%@",error);
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    [Localytics setPushToken:deviceToken];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[ @"global" ];
    currentInstallation.channels = @[ @"global",@"admin" ];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    [Localytics handlePushNotificationOpened:userInfo];
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
        [self handlePushWithPayload:userInfo];
    } else {
        [PFPush handlePush:userInfo];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MatchOnAppDelegateApplicationDidReceiveRemoteNotification object:nil userInfo:userInfo];
    completionHandler(UIBackgroundFetchResultNoData);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [Localytics dismissCurrentInAppMessage];
    [Localytics closeSession];
    [Localytics upload];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [Localytics dismissCurrentInAppMessage];
    [Localytics closeSession];
    [Localytics upload];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Localytics openSession];
    [Localytics upload];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //[FBAppCall handleDidBecomeActive];
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
    
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    //[[PFFacebookUtils session] handleDidBecomeActive];
    [Localytics openSession];
    [Localytics upload];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    //[[FBSession activeSession] close];
    [[PFFacebookUtils session] close];
    [self saveContext];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    if ([UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
        [PFAnalytics trackAppOpenedWithRemoteNotificationPayload:userInfo];
        [self handlePushWithPayload:userInfo];
    } else {
        [PFPush handlePush:userInfo];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MatchOnAppDelegateApplicationDidReceiveRemoteNotification object:nil userInfo:userInfo];
}

- (void)handlePush:(NSDictionary *)launchOptions {
    // Extract the notification payload dictionary
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    NSLog(@"Loaded from here %@",remoteNotificationPayload);

    // Check if the app was open from a notification and a user is logged in
    if (remoteNotificationPayload && [PFUser currentUser]) {
        [self handlePushWithPayload:remoteNotificationPayload];
    }
}

- (void)handlePushWithPayload:(NSDictionary *)remoteNotificationPayload {
    NSLog(@"Loaded from here with info: %@",remoteNotificationPayload);
    
    // Push the referenced photo into view
    NSString *objectId = [remoteNotificationPayload objectForKey:@"a"];
    NSString *scoreId = [remoteNotificationPayload objectForKey:@"s"];
    NSString *inviteId = [remoteNotificationPayload objectForKey:@"i"];
    NSString *declined = [remoteNotificationPayload objectForKey:@"d"];
    if(inviteId) {
        objectId = inviteId;
    }
    
    if (objectId && objectId.length != 0) {
        NSLog(@"Object ID: %@",objectId);
        PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
        [query includeKey:@"recipients"];
        [query includeKey:@"user"];
        [query includeKey:@"score"];
        [query includeKey:@"scorer"];
        [query includeKey:@"firstTeam"];
        [query includeKey:@"secondTeam"];
        
        [query getObjectInBackgroundWithId:objectId block:^(PFObject *object, NSError *error) {
            if (!error) {
                if(scoreId) {
                    NSLog(@"IF");
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
                    ScoreViewController *s = (ScoreViewController *)[storyboard instantiateViewControllerWithIdentifier:@"ScoreViewController"];

                    s.activity = (ActivityModel *)object;
                    s.action = @"review";
                    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
                    UINavigationController *homeNavigationController = [[tabBarController viewControllers] objectAtIndex:3];
                    [tabBarController setSelectedViewController:homeNavigationController];
                    s.hidesBottomBarWhenPushed = YES;
                    [homeNavigationController pushViewController:s animated:YES];
                } else if(inviteId) {
                    //HomeViewController *vc = [[HomeViewController alloc] init];
                    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
                    [tabBarController setSelectedIndex:0];
                } else if (declined) {
                    if (self.inviteViewController != nil) {
                        //[self.inviteViewController refreshInbox];
                    }
                }else {
                    NSLog(@"ELSE");
                    MessageViewController *m = [[MessageViewController alloc] init];
                    m.activity = (ActivityModel *)object;
                    m.scrolled = FALSE;
                    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
                    UINavigationController *homeNavigationController = [[tabBarController viewControllers] objectAtIndex:0];
                    [tabBarController setSelectedViewController:homeNavigationController];
                    m.hidesBottomBarWhenPushed = YES;
                    [homeNavigationController pushViewController:m animated:YES];
                    NSLog(@"Go");
                }
                
            }
        }];
    }
}


#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "Flynn.MatchOn" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"MatchOn" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"MatchOn.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}


#pragma mark - Helper methods

-(void)customizeUserInterface
{
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:THEME_COLOR,NSForegroundColorAttributeName, nil]];
    [[UINavigationBar appearance] setTintColor:THEME_COLOR];
    
    //[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil] forState:UIControlStateNormal];
    //[[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:THEME_COLOR, NSForegroundColorAttributeName, nil] forState:UIControlStateSelected];
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,[UIFont fontWithName:@"MyriadPro-Regular" size:10.0f],NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    UITabBarController *tabBarController = (UITabBarController *)self.window.rootViewController;
    UITabBar *tabBar = tabBarController.tabBar;
    
    UITabBarItem *tabHome = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBrowse = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabPlay = [tabBar.items objectAtIndex:2];
    
    UITabBarItem *tabScore = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabProfile = [tabBar.items objectAtIndex:4];
    
    [tabHome setImage:[[UIImage imageNamed:@"tabbar-home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabHome setSelectedImage:[[UIImage imageNamed:@"tabbar-home-on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBrowse setImage:[[UIImage imageNamed:@"tabbar-browse"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBrowse setSelectedImage:[[UIImage imageNamed:@"tabbar-browse-on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabPlay setImage:[[UIImage imageNamed:@"tabbar-matchon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabPlay setSelectedImage:[[UIImage imageNamed:@"tabbar-matchon"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabScore setImage:[[UIImage imageNamed:@"tabbar-challenge"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabScore setSelectedImage:[[UIImage imageNamed:@"tabbar-challenge-on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabProfile setImage:[[UIImage imageNamed:@"tabbar-profile"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabProfile setSelectedImage:[[UIImage imageNamed:@"tabbar-profile-on"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setBackgroundImage:[UIImage imageNamed:@"bg"]];
    UIImageView *imgBack = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tabbar_bg"]];
    CGRect frame = imgBack.frame;
    frame.origin.y = -13;
    imgBack.frame = frame;
    [tabBar insertSubview:imgBack atIndex:0];
    
}


@end
