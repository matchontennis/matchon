//
//  HelpViewController.h
//  MatchOn
//
//  Created by Sol on 5/8/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (weak, nonatomic) IBOutlet UIPageControl *pgControl;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
@property (weak, nonatomic) IBOutlet UIButton *btnStart;
- (IBAction)onSkip:(id)sender;
- (IBAction)onPage:(id)sender;
- (IBAction)onStart:(id)sender;
@end
