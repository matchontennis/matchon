//
//  PlayViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 11/24/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//


#import "PlayViewController.h"
#import "MSCellAccessory.h"
#import "LocationViewController.h"
#import "MBProgressHUD.h"
#import "RatingModel.h"
#import "Constants.h"
#import "UIViewController+MJPopupViewController.h"
#import "PointsModel.h"
#import "Localytics.h"

#define kDatePickerTag              99     // view tag identifiying the date picker view

#define kTitleKey       @"title"   // key for obtaining the data source item's title
#define kDateKey        @"date"    // key for obtaining the data source item's date value

// keep track of which rows have date cells
#define kTimeStartRow   0
#define kDateStartRow   1
#define kDateEndRow     2

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";     // the remaining cells at the end

#pragma mark -

@interface PlayViewController () <CLLocationManagerDelegate, LocationViewControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;


@property (nonatomic, strong) NSDate *startTime;
@property (nonatomic, strong) NSDateFormatter *timeFormatter;

// keep track which indexPath points to the cell with UIDatePicker
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;

@property (assign) NSInteger pickerCellRowHeight;


// this button appears only when the date picker is shown (iOS 6.1.x or earlier)
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneButton;

@end


#pragma mark -

@implementation PlayViewController


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:NSCurrentLocaleDidChangeNotification
                                                  object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    
    self.timeFormatter = [[NSDateFormatter alloc] init];
    //[self.timeFormatter setDateStyle:NSDateFormatterShortStyle];    // show short-style date format
    //[self.timeFormatter setTimeStyle:NSDateFormatterShortStyle];
    [self.timeFormatter setDateFormat:@"EEE MMM d '@' h:mm a"];
    [self.timeFormatter setAMSymbol:@"am"];
    [self.timeFormatter setPMSymbol:@"pm"];
    
    // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
    UITableViewCell *pickerViewCellToCheck = [self.tableView dequeueReusableCellWithIdentifier:kDatePickerID];
    self.pickerCellRowHeight = CGRectGetHeight(pickerViewCellToCheck.frame);
    
    // if the local changes while in the background, we need to be notified so we can update the date
    // format in the table view cells
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(localeChanged:)
                                                 name:NSCurrentLocaleDidChangeNotification
                                               object:nil];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [tap setCancelsTouchesInView:YES];
    curNote = nil;
    if (self.activity) {
        curNote = self.activity.note;
    }
    //[self.view addGestureRecognizer:tap];
    if(self.editing && self.activity){
        // Set start time
        self.startTime = self.activity.startTime;
        self.doubles = self.activity.doubles;
    }
    
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if(!self.locationEdit) {
        self.editing = FALSE;
        self.geopt = nil;
        self.location = nil;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    
    NSDate *startTime = [NSDate date];
    
    
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit
                              fromDate:startTime];
    NSInteger minutes = [time minute];
    float minuteUnit = ceil((float) minutes / 15.0);
    minutes = minuteUnit * 15.0;
    [time setMinute: minutes];
    startTime = [[NSCalendar currentCalendar] dateFromComponents:time];
    
    
    if(self.editing && self.activity){
        // Set start time
        //self.startTime = self.activity.startTime;
        //self.doubles = self.activity.doubles;
        if(!self.locationEdit) {
            self.location = self.activity.location;
            self.court = self.activity.court;
            self.geopt = self.activity.geopoint;
            if (self.geopt == nil) {
                self.geopt = (PFGeoPoint *)[[PFUser currentUser] objectForKey:@"geopoint"];
            }
            if (self.court == nil || [self.court isEqualToString:@""]) {
                if ([self.geopt isEqual:[[PFUser currentUser] objectForKey:@"geopoint"]]) {
                    self.court = [[PFUser currentUser] objectForKey:@"homecourt"];
                }else {
                    self.court = @"";
                }
            }
            
        }
        [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
            if (!error) {
                currentPoint = geoPoint;
                if ([[PFUser currentUser] objectForKey:@"geopoint"]) {
                    currentPoint = (PFGeoPoint *)[[PFUser currentUser] objectForKey:@"geopoint"];
                }
            } else {
                NSLog(@"error for location: %@",error);
            }
        }];
        NSLog(@"Set them!!!");
    } else if(!self.locationEdit){
        NSLog(@"NOT EDITING!!!");
        NSTimeInterval secondsInHours = 2 * 60 * 60;
        self.startTime = [startTime dateByAddingTimeInterval:secondsInHours];
        
        self.activity = nil;
        
        if(!self.geopt) {
            if ([[PFUser currentUser] objectForKey:@"homecourt"]) {
                self.court = [[PFUser currentUser] objectForKey:@"homecourt"];
            }
            [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
                if (!error) {
                    // do something with the new geoPoint
                    NSLog(@"Geopoint: %@",geoPoint);
                    self.geopt = geoPoint;
                    currentPoint = geoPoint;
                    if ([[PFUser currentUser] objectForKey:@"geopoint"]) {
                        currentPoint = (PFGeoPoint *)[[PFUser currentUser] objectForKey:@"geopoint"];
                        self.geopt = currentPoint;
                    }
                    [self.tableView reloadData];
                } else {
                    NSLog(@"error for location: %@",error);
                }
            }];
        }
    }
    self.locationEdit = FALSE;
    
}





#pragma mark - Locale

/*! Responds to region format or locale changes.
 */
- (void)localeChanged:(NSNotification *)notif
{
    // the user changed the locale (region format) in Settings, so we are notified here to
    // update the date format in the table view cells
    //
    [self.tableView reloadData];
}


#pragma mark - Utilities

/*! Returns the major version of iOS, (i.e. for iOS 6.1.3 it returns 6)
 */
NSUInteger DeviceSystemMajorVersion()
{
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _deviceSystemMajorVersion =
        [[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] integerValue];
    });
    
    return _deviceSystemMajorVersion;
}


/*! Determines if the given indexPath has a cell below it with a UIDatePicker.
 
 @param indexPath The indexPath to check if its cell has a UIDatePicker below it.
 */
- (BOOL)hasPickerForIndexPath:(NSIndexPath *)indexPath
{
    BOOL hasDatePicker = NO;
    
    NSInteger targetedRow = indexPath.row;
    targetedRow++;
    
    UITableViewCell *checkDatePickerCell =
    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:targetedRow inSection:indexPath.section]];
    UIDatePicker *checkDatePicker = (UIDatePicker *)[checkDatePickerCell viewWithTag:kDatePickerTag];
    
    hasDatePicker = (checkDatePicker != nil);
    return hasDatePicker;
}

/*! Updates the UIDatePicker's value to match with the date of the cell above it.
 */
- (void)updateDatePicker
{
    if (self.datePickerIndexPath != nil)
    {
        UITableViewCell *associatedDatePickerCell = [self.tableView cellForRowAtIndexPath:self.datePickerIndexPath];
        
        UIDatePicker *targetedDatePicker = (UIDatePicker *)[associatedDatePickerCell viewWithTag:kDatePickerTag];
        if (targetedDatePicker != nil)
        {
            // we found a UIDatePicker in this cell, so update it's date value
            //
            [targetedDatePicker setDate:self.startTime animated:NO];
        }
        NSLog(@"Datepicker indexPath: %li",(long)self.datePickerIndexPath.row);
        
    } else {
        NSLog(@"No datepicker");
    }
}

/*! Determines if the UITableViewController has a UIDatePicker in any of its cells.
 */
- (BOOL)hasInlineDatePicker
{
    return (self.datePickerIndexPath != nil);
}

/*! Determines if the given indexPath points to a cell that contains the UIDatePicker.
 
 @param indexPath The indexPath to check if it represents a cell with the UIDatePicker.
 */
- (BOOL)indexPathHasPicker:(NSIndexPath *)indexPath
{
    if(indexPath.section != 1) return FALSE;

    return ([self hasInlineDatePicker] && self.datePickerIndexPath.row == indexPath.row);
}

/*! Determines if the given indexPath points to a cell that contains the start/end dates.
 
 @param indexPath The indexPath to check if it represents start/end date cell.
 */
- (BOOL)indexPathHasDate:(NSIndexPath *)indexPath
{
    BOOL hasDate = NO;
    
    if ((indexPath.row == kDateStartRow) )
    {
        hasDate = YES;
    }
    
    return hasDate;
}

- (BOOL)indexPathHasTime:(NSIndexPath *)indexPath
{
    BOOL hasTime = NO;
    
    if ((indexPath.row == kTimeStartRow) )
    {
        hasTime = YES;
    }
    
    return hasTime;
}



#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([self indexPathHasPicker:indexPath]) {
        return self.pickerCellRowHeight;
    } else if(indexPath.section == 2) {
        return 44.0f;
    } else if (indexPath.section == 3) {
        return 120.0f;
    } else if(indexPath.section == 4 || indexPath.section == 5) {
        return 50.0f;
    }
    return self.tableView.rowHeight;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 3) {
        return 0.01f;
    }else if (section == 4 || section == 5) {
        if (self.activity) {
            return 0.01f;
        }else {
            return 10.0f;
        }
    }else {
        return 30.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = @"SELECT GAME TYPE";
    if(section == 1) {
        title = @"SELECT DESIRED START TIME AND DATE";
    } else if(section == 2){
        title = @"SELECT LOCATION";
    } else if(section == 3){
        title = @"";
    } else if (section == 4 && self.activity) {
        title = @"";
    } else if (section == 5 || (section == 4 && !self.activity)) {
        title = @"";
    }
    return title;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *viewCap = [[UIView alloc] init];
    UILabel *lblCap = [[UILabel alloc] init];
    if (section == 0) {
        lblCap.text = @"SELECT GAME TYPE";
        viewCap.frame = CGRectMake(0, 0, tableView.frame.size.width, 30.0f);
        lblCap.frame = CGRectMake(10, 0, tableView.frame.size.width - 10, 30.0f);
    }else if(section == 1) {
        lblCap.text = @"SELECT DESIRED START TIME AND DATE";
        lblCap.frame = CGRectMake(10, 0, tableView.frame.size.width - 10, 30.0f);
        viewCap.frame = CGRectMake(0, 0, tableView.frame.size.width, 30.0f);
    } else if(section == 2){
        lblCap.text = @"SELECT LOCATION";
        lblCap.frame = CGRectMake(10, 0, tableView.frame.size.width - 10, 30.0f);
        viewCap.frame = CGRectMake(0, 0, tableView.frame.size.width, 30.0f);
    } else if(section == 3){
        return nil;
    } else if (section == 4) {
        lblCap.text = @"";
        if (self.activity) {
            return nil;
        }else {
            lblCap.frame = CGRectMake(10, 0, tableView.frame.size.width-10, 10.0f);
            viewCap.frame = CGRectMake(0, 0, tableView.frame.size.width, 10.0f);
        }
    } else if (section == 5) {
        lblCap.text = @"";
        lblCap.text = @"";
        if (self.activity) {
            return nil;
        }else {
            lblCap.frame = CGRectMake(10, 0, tableView.frame.size.width - 10, 10.0f);
            viewCap.frame = CGRectMake(0, 0, tableView.frame.size.width, 10.0f);
        }
    }
    lblCap.font = [UIFont fontWithName:@"MyriadPro-Cond" size:15.0f];
    lblCap.textColor = [UIColor darkGrayColor];
    [viewCap addSubview:lblCap];
    return viewCap;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(self.activity && self.editing) {
        return 6;
    }
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(section == 0) {
        return 2;
    } else if( section == 1) {
        if ([self hasInlineDatePicker])
        {
            return 2;
        }
        return 1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    NSString *cellID = kOtherCell;
    
    BOOL playCell = FALSE;
    BOOL cancelCell = FALSE;
    if(indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        
        // Game type
        
        if(indexPath.row == 0) {
            // Singles
            cell.textLabel.text = @"Singles";
            if(!self.doubles) {
                cell.accessoryView = [MSCellAccessory accessoryWithType:CHECKMARK color:[UIColor orangeColor]];
            } else {
                cell.accessoryView = nil;
            }
            
        } else {
            // Doubles
            cell.textLabel.text = @"Doubles";
            if(self.doubles) {
                cell.accessoryView = [MSCellAccessory accessoryWithType:CHECKMARK color:[UIColor orangeColor]];
            } else {
                cell.accessoryView = nil;
            }
        }
        
    }
    else if(indexPath.section == 1) {
        
        if ([self indexPathHasPicker:indexPath])
        {
            // the indexPath is the one containing the inline date picker
            cellID = kDatePickerID;     // the current/opened date picker cell
            
        }
        else if ([self indexPathHasTime:indexPath])
        {
            
            // the indexPath is one that contains the date information
            cellID = kDateCellID;       // the start/end date cells
        }
        else if ([self indexPathHasDate:indexPath])
        {

            // the indexPath is one that contains the date information
            cellID = kDateCellID;       // the start/end date cells
        }
        
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        

        if ([self indexPathHasPicker:indexPath]) {
            UIDatePicker *datePicker = (UIDatePicker *)[cell.contentView viewWithTag:99];
            datePicker.datePickerMode = UIDatePickerModeDateAndTime;
            datePicker.minuteInterval = 5;
        }
        
        // if we have a date picker open whose cell is above the cell we want to update,
        // then we have one more cell than the model allows
        //
        NSInteger modelRow = indexPath.row;
        if (self.datePickerIndexPath != nil && self.datePickerIndexPath.row <= indexPath.row)
        {
            modelRow--;
        }
        
        // proceed to configure our cell
        if ([cellID isEqualToString:kDateCellID])
        {
            // we have either start or end date cells, populate their date field
            if(indexPath.row==0) {
                cell.textLabel.text = @"Start";
                if (self.datePickerIndexPath != nil) {
                    NSLog(@"EOPP");
                    cell.textLabel.text = @"X";
                }
                cell.detailTextLabel.text = [self.timeFormatter stringFromDate:self.startTime];
            }
        }
    } else if(indexPath.section == 2) {
        cellID = @"LocationCell";
        // Location
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        if(self.geopt) {
            [self setLocationFromGeoPoint];
        }
        
    } else if (indexPath.section == 3) {
        cellID = @"NoteCell";
        // Location
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        
        UITextView *txtNode = (UITextView *)[cell viewWithTag:100];
        txtNode.layer.cornerRadius = 5.0f;
        txtNode.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        txtNode.layer.borderWidth = 1.0f;
        if (curNote != nil && ![curNote isEqual:[NSNull null]] && ![curNote isEqualToString:@""]) {
            txtNode.text = curNote;
        }else {
            txtNode.text = @"ADD NOTE";
        }
        //if(self.geopt) {
            //[self setLocationFromGeoPoint];
        //}
    } else if(indexPath.section == 5) {
        cellID = @"CancelCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:11];
        button.layer.cornerRadius = 3.0f;
        button.clipsToBounds = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cancelCell = TRUE;
    } else if(indexPath.section == 4) {
        cellID = @"PlayCell";

        cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        UIButton *button = (UIButton *)[cell.contentView viewWithTag:11];
        if(!button) {
            NSLog(@"NO BUTTON");
        }
        button.layer.cornerRadius = 3.0f;
        button.clipsToBounds = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(self.editing) {
            [button setTitle:@"SAVE CHANGES" forState:UIControlStateNormal];
        }
        playCell = TRUE;
    }
    if (indexPath.section == 3) {
    }else if(cancelCell || playCell) {
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.font = [UIFont fontWithName:@"MyriadPro-Bold" size:18.0f];
        cell.textLabel.textColor = [UIColor whiteColor];
    } else {
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor colorWithWhite:0.1f alpha:1.0f];
    }
    
    if(indexPath.section != 0) {
        cell.accessoryView = nil;
    }
    
    return cell;
}

/*! Adds or removes a UIDatePicker cell below the given indexPath.
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)toggleDatePickerForSelectedIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView beginUpdates];
    
    NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row + 1 inSection:indexPath.section]];
    
    
    // check if 'indexPath' has an attached date picker below it
    if ([self hasPickerForIndexPath:indexPath])
    {
        // found a picker below it, so remove it
        [self.tableView deleteRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    else
    {
        // didn't find a picker below it, so we should insert it
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
    }
    
    [self.tableView endUpdates];
}

/*! Reveals the date picker inline for the given indexPath, called by "didSelectRowAtIndexPath".
 
 @param indexPath The indexPath to reveal the UIDatePicker.
 */
- (void)displayInlineDatePickerForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // display the date picker inline with the table content
    [self.tableView beginUpdates];
    
    
    BOOL before = NO;   // indicates if the date picker is below "indexPath", help us determine which row to reveal
    if ([self hasInlineDatePicker])
    {
        before = self.datePickerIndexPath.row < indexPath.row;
    }
    
    BOOL sameCellClicked = (self.datePickerIndexPath.row - 1 == indexPath.row);
    
    // remove any date picker cell if it exists
    if ([self hasInlineDatePicker])
    {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.datePickerIndexPath.row inSection:indexPath.section]]
                              withRowAnimation:UITableViewRowAnimationFade];
        self.datePickerIndexPath = nil;
    }
    
    if (!sameCellClicked)
    {
        // hide the old date picker and display the new one
        NSInteger rowToReveal = (before ? indexPath.row - 1 : indexPath.row);
        NSIndexPath *indexPathToReveal = [NSIndexPath indexPathForRow:rowToReveal inSection:indexPath.section];
        
        [self toggleDatePickerForSelectedIndexPath:indexPathToReveal];
        self.datePickerIndexPath = [NSIndexPath indexPathForRow:indexPathToReveal.row + 1 inSection:indexPath.section];
    }
    
    
    
    
    // always deselect the row containing the start or end date
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
    [self.tableView endUpdates];
    
    
    // inform our date picker of the current date to match the current cell
    [self updateDatePicker];
}



#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self dismissKeyboard];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0){
        
        if(self.editing) {
            if(indexPath.row == 0) {
                // Want to change to singles
                if(self.activity.doubles && self.activity.recipients.count > 2) {
                    // Error - too many people
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Too many players have signed up for singles." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    [alertView show];
                    return;
                }
            }
        }

        // Game type - singles vs. doubles
        cell.accessoryView = [MSCellAccessory accessoryWithType:CHECKMARK color:[UIColor orangeColor]];
        
        UITableViewCell *otherCell;

        if(indexPath.row == 0) {
            otherCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            self.doubles = FALSE;
        } else {
            otherCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            self.doubles = TRUE;
        }
        otherCell.accessoryView = nil;
    }
    else if(indexPath.section == 1) {
        
        // Date/Time
        
        if (cell.reuseIdentifier == kDateCellID)
        {
            
            if(indexPath.row==0) {
                NSIndexPath *otherIndex = [NSIndexPath indexPathForRow:2 inSection:indexPath.section];
                if(self.datePickerIndexPath) {
                    // Date picker already exists
                    
                    if(self.datePickerIndexPath.row == indexPath.row+1) {
                        // We clicked the cell that's already open - close it
                        cell.textLabel.text = @"Start Time";
                    } else {
                        // The datepicker was open on the other cell - close that one, open this one
                        NSLog(@"Reset the other one");
                        cell.textLabel.text = @"X";
                        otherIndex = [NSIndexPath indexPathForRow:1 inSection:indexPath.section];
                        UITableViewCell *otherCell = [self.tableView cellForRowAtIndexPath:otherIndex];
                        otherCell.textLabel.text = @"Date";
                    }
                } else {
                    cell.textLabel.text = @"X";
                    NSLog(@"Other cell clicked");
                }
            }
            else {
                NSIndexPath *otherIndex = [NSIndexPath indexPathForRow:0 inSection:indexPath.section];

                NSLog(@"Stick it");
                if(self.datePickerIndexPath) {
                    if(self.datePickerIndexPath.row == indexPath.row+1) {
                        // We clicked the cell that's already open - close it
                        cell.textLabel.text = @"Date";
                    } else {
                        // The datepicker was open on the other cell - close that one, open this one
                        cell.textLabel.text = @"X";
                        UITableViewCell *otherCell = [self.tableView cellForRowAtIndexPath:otherIndex];
                        otherCell.textLabel.text = @"Start Time";
                    }
                } else {
                    cell.textLabel.text = @"X";
                }
                
            }
            
            [self displayInlineDatePickerForRowAtIndexPath:indexPath];
        }
        else
        {
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    } else if(indexPath.section == 2) {
        
        // Change location
        
        [self performSegueWithIdentifier:@"location" sender:self];
    } else if(indexPath.section == 4) {
        // Match On! (Save)
        //[self done];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];

    } else if(indexPath.section == 5) {
        // Cancel
        //[self remove];
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
    }
}


#pragma mark - Actions

/*! User chose to change the date by changing the values inside the UIDatePicker.
 
 @param sender The sender for this action: UIDatePicker.
 */
- (IBAction)dateAction:(id)sender
{
    NSIndexPath *targetedCellIndexPath = nil;
    
    if ([self hasInlineDatePicker])
    {
        // inline date picker: update the cell's date "above" the date picker cell
        //
        targetedCellIndexPath = [NSIndexPath indexPathForRow:self.datePickerIndexPath.row - 1 inSection:self.datePickerIndexPath.section];
    }
    else
    {
        // external date picker: update the current "selected" cell's date
        targetedCellIndexPath = [self.tableView indexPathForSelectedRow];
    }
    
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:targetedCellIndexPath];
    UIDatePicker *targetedDatePicker = sender;
    
    // update our data model
    self.startTime = targetedDatePicker.date;

    // update the cell's date string
    cell.detailTextLabel.text = [self.timeFormatter stringFromDate:targetedDatePicker.date];
}




#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Not Enabled" message:@"Please Turn on Location Services\nFor this app to be useful you will need to turn on location services. We use your current location to show you only matches in your area.\nTo enable, go to the settings app on your phone, find the MatchOn app and enable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            //NSLog(@"Location here!");
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        {
            
        }
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"kCLAuthorizationStatusNotDetermined");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
            break;
    }
}


#pragma mark - Modal View Controller delegate

- (void)setLocationFromGeoPoint
{
    /*if (self.court != nil && [self.court isEqualToString:@""]) {
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        UILabel *textLabel = (UILabel *)[cell.contentView viewWithTag:187];
        textLabel.text = self.court;
        if (self.activity) {
            self.activity.geopoint = self.geopt;
            self.activity.court = self.court;
        }
        return;
    }*/
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    CLLocation *l = [[CLLocation alloc] initWithLatitude:self.geopt.latitude longitude:self.geopt.longitude];
    [geocoder reverseGeocodeLocation:l completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"%@",error);
        CLPlacemark *place = [placemarks objectAtIndex:0];
        self.location = place.addressDictionary;
        if(self.activity) {
            self.activity.location = self.location;
            self.activity.geopoint = self.geopt;
            self.activity.court = self.court;
        }
        [self setLocationForLabel];
    }];
}

- (void)setLocationForLabel {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *string = @"";
        if([self.location objectForKey:@"Street"]){
            string = [NSString stringWithFormat:@"%@, ",self.location[@"Street"]];
        }
        if([self.location objectForKey:@"City"]){
            string = [NSString stringWithFormat:@"%@%@",string, self.location[@"City"]];
        }
        if([self.location objectForKey:@"State"]){
            if([self.location objectForKey:@"City"]){
                string = [NSString stringWithFormat:@"%@, ",string];
            }
            string = [NSString stringWithFormat:@"%@%@",string, self.location[@"State"]];
        }
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        UILabel *textLabel = (UILabel *)[cell.contentView viewWithTag:187];
        if (self.court != nil && ![self.court isEqualToString:@""]) {
            textLabel.text = self.court;
        }else if(![self.location objectForKey:@"City"]) {
            textLabel.text = @"Set Location";
            self.location = nil;
        } else if ([self.location objectForKey:@"Name"] != nil && ![[self.location objectForKey:@"Name"] isEqualToString:@""]) {
            textLabel.text = self.location[@"Name"];
            if (currentPoint != nil && self.geopt != nil) {
                if (currentPoint.latitude == self.geopt.latitude && currentPoint.longitude == self.geopt.longitude) {
                    textLabel.text = @"Current Location";
                }
            }
        }else {
            textLabel.text = string;
            if (currentPoint != nil && self.geopt != nil) {
                if (currentPoint.latitude == self.geopt.latitude && currentPoint.longitude == self.geopt.longitude) {
                    textLabel.text = @"Current Location";
                }
            }
        }
        NSLog(@"Setting location for geopt: %@",self.geopt);
        NSLog(@"Setting location for llllll: %@",self.location);
        NSLog(@"Setting location for geopt: %@",self.activity.geopoint);
        NSLog(@"Setting location for llllll: %@",self.activity.location);
        [cell setNeedsDisplay];
    });
}


#pragma mark - IBActions

- (IBAction)done:(id)sender {
    
    // Validate
    NSComparisonResult result = [[NSDate date] compare:self.startTime];
    BOOL passedDate = FALSE;
    switch (result)
    {
        case NSOrderedAscending:
            NSLog(@"Future Date");
            break;
        case NSOrderedDescending:
            NSLog(@"Earlier Date");
            passedDate = TRUE;
            break;
        case NSOrderedSame:
            NSLog(@"Today/Null Date Passed"); //Not sure why This is case when null/wrong date is passed
            break;
        default:
            NSLog(@"Error Comparing Dates");
            break;
    }
    if(passedDate) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Date cannot be in the past!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    
    // Query for similar or save
    
    
    // Query for similar
    
    
    // Save
    [self saveActivity];
}

- (IBAction)cancel:(id)sender {
    NSLog(@"Cancelling from here");
    if(self.editing || self.fromStart) {
        self.activity = nil;
        [self.startPageDelegate hideStartPage:NO];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    } else {
        self.activity = nil;
        [self.tabBarController setSelectedIndex:0];
    }
}



- (IBAction)remove:(id)sender {
    if(!self.activity) {
        return;
    }
    
    NSArray *vComp = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[vComp objectAtIndex:0] intValue] < 8) {
        // iOS 7
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Confirm Cancellation" otherButtonTitles:nil];
        
        for(UIButton *button in [popup subviews]) {
            NSLog(@"Button title: %@",button.titleLabel.text);
            if([button.titleLabel.text isEqualToString:@"Confirm Cancellation"]) {
                [button addTarget:self action:@selector(cancelMe:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
        popup.tag = 1;
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else {
        NSLog(@"iOS 8");
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
            [alertController dismissViewControllerAnimated:YES completion:^{
                NSLog(@"Dismissed");
            }];
        }];
        UIAlertAction *cancelMatch = [UIAlertAction actionWithTitle:@"Confirm Cancellation" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self cancelMe:self];
        }];
        
        [alertController addAction:cancelAction];
        [alertController addAction:cancelMatch];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (IBAction)cancelMe:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Saving";
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    NSMutableArray *recipients = [NSMutableArray array];
    for(PFUser *user in self.activity.recipients) {
        if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
            continue;
        }
        [recipients addObject:user];
    }
    self.activity.recipients = recipients;
    if(recipients.count == 0) {
        NSLog(@"non");
        self.activity.active = FALSE;
        [self.activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSString *first = [[PFUser currentUser] objectForKey:@"first"];
            NSString *last = [[PFUser currentUser] objectForKey:@"last"];
            if(!first) {
                NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
                first = [name firstObject];
                if(!last) {
                    if(name.count > 1) {
                        last = [name lastObject];
                    }
                }
            }
            NSString *full = first;
            if(last && last.length > 0) {
                full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
            }
            [Localytics tagEvent:@"Cancel the match entirely"];
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }];
        return;
    }
    //self.activity.totalRecipients = (int)self.activity.recipients.count;
    self.activity.full = ((int)self.activity.recipients.count >= self.activity.spots);
    //self.activity.matchOn = (self.activity.totalRecipients >= self.activity.spots);
    //[self.activity.singlesMap removeObjectForKey:[PFUser currentUser].objectId];
    //[self.activity.doublesMap removeObjectForKey:[PFUser currentUser].objectId];
    [self.activity.ratingMap removeObjectForKey:[PFUser currentUser].objectId];
    [self.activity setRatingValues];
    [self.activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
         PFQuery *pushQuery = [PFInstallation query];
         [pushQuery whereKey:@"user" containedIn:self.activity.recipients];
        
        
        
        
        NSString *first = [[PFUser currentUser] objectForKey:@"first"];
        NSString *last = [[PFUser currentUser] objectForKey:@"last"];
        if(!first) {
            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        NSString *full = first;
        if(last && last.length > 0) {
            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
        }
        
        [Localytics tagEvent:@"Left from match"];
        
         NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ left your match!", full], @"badge" : @"Increment", @"a": self.activity.objectId};
         PFPush *push = [[PFPush alloc] init];
         [push setQuery:pushQuery];
         [push setData:data];
         [push sendPushInBackground];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        self.activity = nil;
    }];
}


#pragma mark - Save helper methods

- (void) saveActivity {
    BOOL edit = FALSE;
    if(self.activity && !self.fromStart) {
        edit = TRUE;
    } else {
        
        self.activity = [ActivityModel object];
        self.activity.user = [PFUser currentUser];
        self.activity.recipients = [[NSMutableArray alloc] initWithArray:@[[PFUser currentUser]]];
        //self.activity.totalRecipients = 1;
    }
    
    self.activity.note = curNote;
    
    if(edit && self.doubles != self.activity.doubles) {
        // Editing and it changed
        if(self.doubles) {
            // Changing to doubles
            
        } else {
            // Changing to singles
            if(self.activity.recipients.count > 2) {
                // Changing to singles and there are more than
                // 2 people in the match
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"There are too many people in this match for singles." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alertView show];
                return;
            }
        }
    }
    
    self.activity.spots = 2;
    if(self.doubles){
        self.activity.spots = 4;
    }
    
    if(self.activity.recipients.count >= self.activity.spots) {
        //self.activity.matchOn = TRUE;
        self.activity.full = TRUE;
    } else {
        //self.activity.matchOn = FALSE;
        self.activity.full = FALSE;
    }
    
    self.activity.sport = 1;
    self.activity.type = @"play";
    self.activity.doubles = self.doubles;
    self.activity.startTime = self.startTime;
    self.activity.geopoint = self.geopt;
    self.activity.location = self.location;
    self.activity.court = self.court;
    self.activity.active = TRUE;
    
    if(!self.location) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location required" message:@"Please add a match location." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        [alertView show];
        return;
    }
    
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Saving";
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    RatingModel *rating = [PFUser currentUser][@"rating"];
    [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        RatingModel *refreshed = (RatingModel *)object;
        if (!self.activity.ratingMap) {
            self.activity.ratingMap = [NSMutableDictionary dictionary];
        }
        /*if(!self.activity.singlesMap) {
            self.activity.singlesMap = [NSMutableDictionary dictionary];
        }
        if(!self.activity.doublesMap) {
            self.activity.doublesMap = [NSMutableDictionary dictionary];
        }*/
        
        
        NSLog(@"Setting ratings for map: %@",refreshed);
        /*NSNumber *s = [[NSNumber alloc] initWithInt:refreshed.singles];
        [self.activity.singlesMap setObject:s forKey:[PFUser currentUser].objectId];
        NSNumber *d = [[NSNumber alloc] initWithInt:refreshed.doubles];
        [self.activity.doublesMap setObject:d forKey:[PFUser currentUser].objectId];
        */
        
        NSNumber *r = [[NSNumber alloc] initWithInt:refreshed.rating];
        [self.activity.ratingMap setObject:r forKey:[PFUser currentUser].objectId];
        
        [self.activity setRatingValues];
        PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
        [query whereKey:@"user" equalTo:[PFUser currentUser]];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (objects.count == 0) {
                if (!refreshed.score) {
                    refreshed.score = 0;
                }
                refreshed.score += 2;
                //refreshed.curpoint = refreshed.score;
                [refreshed saveInBackground];
                PointsModel *pointsModel = [PointsModel object];
                pointsModel.user = refreshed.user;
                pointsModel.total = 2;
                pointsModel.activity = self.activity;
                [pointsModel saveInBackground];
            }
        }];
        NSLog(@"Ratings set for activity: %@",self.activity);
        [self.activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            NSLog(@"Error: %@",error);

            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if(succeeded) {
                
                
                if(edit) {
                    // Send push notifications
                    
                    NSMutableArray *recipients = [NSMutableArray array];
                    for(PFUser *user in self.activity.recipients){
                        if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
                        [recipients addObject:user];
                    }
                    
                    
                    PFQuery *pushQuery = [PFInstallation query];
                    [pushQuery whereKey:@"user" containedIn:recipients];
                    NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ made changes to your upcoming match!", [PFUser currentUser][@"name"]], @"badge" : @"Increment", @"a": self.activity.objectId};
                    PFPush *push = [[PFPush alloc] init];
                    [push setQuery:pushQuery];
                    [push setData:data];
                    [push sendPushInBackground];
                    
                    
                    self.activity = nil;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
                    [self dismissViewControllerAnimated:YES completion:^{
                        
                    }];
                } else {
                    
                    [Localytics tagEvent:@"Play Match Requested"];
                    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    InvitePopupViewController *invitePopup = (InvitePopupViewController *)[storyboard instantiateViewControllerWithIdentifier:@"InvitePopup"];
                    invitePopup.parent = self;
                    invitePopup.delegate = self;
                    CGRect frame = invitePopup.view.frame;
                    frame.size.height = 300;
                    frame.size.width = [UIScreen mainScreen].bounds.size.width - 20;
                    invitePopup.view.frame = frame;
                    [self presentPopupViewController:invitePopup animationType:MJPopupViewAnimationSlideBottomTop];
                    
                    /*self.activity = nil;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
                    [self.tabBarController setSelectedIndex:0];*/
                    /**/
                }
                
            }
        }];
    }];
}



#pragma mark - Segue method

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"location"]){
        LocationViewController *location = (LocationViewController *)[segue destinationViewController];
        location.delegate = self;
        location.geopt = self.geopt;
        location.courtName = self.court;
        self.locationEdit = TRUE;
    }else if ([segue.identifier isEqualToString:@"invite"]) {
        UINavigationController *nav = [segue destinationViewController];
        InviteViewController *vc = [nav.viewControllers firstObject];
        vc.delegate = self;
        vc.activity = self.activity;
    }
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"ADD NOTE"]) {
        textView.text = @"";
    }
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView
{
    curNote = textView.text;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    curNote = textView.text;
    if (textView.text.length == 0) {
        textView.text = @"ADD NOTE";
    }
    return YES;
}

-(void)answerForInvite:(BOOL)result
{
    if (result) {
        [self performSegueWithIdentifier:@"invite" sender:self];
        
    }else {
        self.activity = nil;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
        [self.tabBarController setSelectedIndex:0];
        if (self.fromStart) {
            [self updateShowStart:NO];
            [self dismissViewControllerAnimated:NO completion:^{
                [self.startPageDelegate matchRequested:YES];
            }];
        }
    }
}

-(void)dismissInviteViewController
{
    self.activity = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
    [self.tabBarController setSelectedIndex:0];
    if (self.fromStart) {
        [self updateShowStart:NO];
        [self dismissViewControllerAnimated:NO completion:^{
            [self.startPageDelegate matchRequested:YES];
        }];
    }
}

- (void)updateShowStart:(BOOL)showStart {
    // set the start switch
    [[NSUserDefaults standardUserDefaults] willChangeValueForKey:[PFUser currentUser].username];
    [[NSUserDefaults standardUserDefaults] setBool:showStart forKey: [PFUser currentUser].username];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)setLocation:(PFGeoPoint *)geopoint courtName:(NSString *)courtName
{
    self.geopt = geopoint;
    self.court = courtName;
    [self setLocationFromGeoPoint];
}

@end


