//
//  SupportViewController.h
//  Page
//
//  Created by Kevin Flynn on 1/2/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface SupportViewController : UIViewController
@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;
- (IBAction)done:(id)sender;

- (IBAction)submit:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *timeField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionField;

@end
