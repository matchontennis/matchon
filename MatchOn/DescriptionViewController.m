//
//  DescriptionViewController.m
//  MatchOn
//
//  Created by Sol on 5/8/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "DescriptionViewController.h"
#import "HomeViewController.h"

@interface DescriptionViewController ()

@end

@implementation DescriptionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.imgBack.image = self.imgBg;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBack:(id)sender {
    if (self.prevViewController != nil) {
        HomeViewController *homeViewController = (HomeViewController *)self.prevViewController;
        homeViewController.prevViewController = self;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
