//
//  LoginViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UIView *viewPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

- (IBAction)login:(id)sender;
- (IBAction)back:(id)sender;

@end
