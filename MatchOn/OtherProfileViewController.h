//
//  OtherProfileViewController.h
//  MatchOn
//
//  Created by Sol on 5/1/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>

@interface OtherProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrMain;
@property (nonatomic, strong) PFUser *curUser;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastPlayed;
@property (weak, nonatomic) IBOutlet UIImageView *imgGender;
@property (weak, nonatomic) IBOutlet UIView *viewBalls;
@property (weak, nonatomic) IBOutlet UIImageView *imgVerified;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileBg;
@property (weak, nonatomic) IBOutlet PFImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblHomeCourt;
@property (weak, nonatomic) IBOutlet UILabel *lblAboutMe;
@property (weak, nonatomic) IBOutlet UILabel *lblAllPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayedMatches;
@property (weak, nonatomic) IBOutlet UILabel *lblCompleted;
@property (weak, nonatomic) IBOutlet UITextView *txtAboutMe;
- (IBAction)back:(id)sender;
@end
