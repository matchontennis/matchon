//
//  PointPopupViewController.h
//  MatchOn
//
//  Created by Sol on 4/22/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Parse/Parse.h>

@protocol PointPopupViewControllerDelegate

-(void)dismissPointPopup;

@end

@interface PointPopupViewController : UIViewController

@property (nonatomic, strong) UIViewController *parent;

@property (weak, nonatomic) id<PointPopupViewControllerDelegate> delegate;

@property (nonatomic, strong) MFMailComposeViewController *mailViewController;

- (IBAction)onClose:(id)sender;
- (IBAction)onFeedback:(id)sender;


@end
