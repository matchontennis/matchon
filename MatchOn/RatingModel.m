//
//  RatingModel.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "RatingModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation RatingModel

@dynamic user;
/*@dynamic singles;
@dynamic doubles;*/
@dynamic rating;
@dynamic score;
@dynamic confidence_level;
//@dynamic curpoint;

+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Rating";
}

@end
