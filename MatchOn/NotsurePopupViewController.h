//
//  NotsurePopupViewController.h
//  MatchOn
//
//  Created by Sol on 4/24/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotsurePopupViewController : UIViewController

@property (nonatomic, strong) UIViewController *parent;

- (IBAction)onClose:(id)sender;

@end
