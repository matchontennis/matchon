//
//  ScoreModel.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/10/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "ScoreModel.h"
#import <Parse/PFObject+Subclass.h>

@implementation ScoreModel

@dynamic activity;
@dynamic scorer;
//@dynamic reviewer;
@dynamic firstScore;
@dynamic secondScore;
@dynamic firstTeam;
@dynamic secondTeam;
@dynamic winner; // On users team
@dynamic loser; // On opponents team
//@dynamic reviewers;
//@dynamic scorers;
@dynamic players;
@dynamic approved;
//@dynamic rejected;



+ (void)load {
    [self registerSubclass];
}

+ (NSString *)parseClassName {
    return @"Score";
}

- (void)createScoreForFirstScore:(NSMutableDictionary *)firstScore andSecondScore:(NSMutableDictionary *)secondScore withFirstTeam:(NSMutableArray *)firstTeam andSecondTeam:(NSMutableArray *)secondTeam andWinner:(NSMutableArray *)winner andLoser:(NSMutableArray *)loser withActivity:(ActivityModel *)activity{
    
    self.scorer = [PFUser currentUser];
    
    self.firstScore = firstScore;
    self.secondScore = secondScore;
    self.activity = activity;
    
    self.firstTeam = firstTeam;
    self.secondTeam = secondTeam;
    self.winner = winner;
    
    self.loser = loser;
    //self.reviewers = secondTeam;
    
    //self.scorers = firstTeam;
    
    for(PFUser *user in secondTeam) {
        if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
            //self.reviewers = firstTeam;
            //self.scorers = secondTeam;
            break;
        }
    }
    
    self.players = (NSMutableArray *)[self.firstTeam arrayByAddingObjectsFromArray:self.secondTeam];
    //self.rejected = FALSE;
    self.approved = FALSE;
}

@end
