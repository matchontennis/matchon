//
//  WizardViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "WizardViewController.h"
#import "PictureViewController.h"
#import <math.h>
#import "MBProgressHUD.h"
#import "NotsurePopupViewController.h"
#import "UIViewController+MJPopupViewController.h"
#import "SignupViewController.h"

@interface WizardViewController ()

@end

@implementation WizardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    
    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"ball"] forState:UIControlStateNormal];
    [[UISlider appearance] setThumbImage:[UIImage imageNamed:@"ball"] forState:UIControlStateHighlighted];
    self.scrollView.contentInset = UIEdgeInsetsMake(0, 0, 58, 0);
    
    self.btnNext.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnNext.layer.borderWidth = 1.0f;
    self.btnNext.layer.cornerRadius = 3.0f;
    self.btnNext.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setTranslucent:NO];
    
    if (![self.prevViewController isKindOfClass:[SignupViewController class]]) {
        self.navigationItem.leftBarButtonItem = nil;
    }
    
    if ([PFUser currentUser]) {
        
        RatingModel *rating = [[PFUser currentUser] objectForKey:@"rating"];
        NSDictionary *d;
        if ([[[PFUser currentUser] objectForKey:@"female"] boolValue]) {
            d = @{@76:@1.5,
                  @68:@2.0,
                  @60:@2.5,
                  @55:@3.0,
                  @50:@3.5,
                  @43:@4.0,
                  @35:@4.5,
                  @28:@5.0,
                  @21:@5.5 };
        }else {
            d = @{@76:@1.5,
                  @67:@2.0,
                  @58:@2.5,
                  @50:@3.0,
                  @44:@3.5,
                  @36:@4.0,
                  @28:@4.5,
                  @21:@5.0,
                  @14:@5.5 };
        }
        
        if (rating) {
            NSLog(@"%@",rating);
            [rating fetchIfNeeded];
            if ([d objectForKey:@(rating.rating)] != nil) {
                [self.slider setValue:[[d objectForKey:@(rating.rating)] floatValue] animated:NO];
            }
            
        }
    }
}


#pragma mark - IBActions


- (IBAction)rating:(id)sender {
    [self.slider setValue:((int)round(self.slider.value * 2.0) / 2.0) animated:NO];
    [self setLabels];
    //int rating = [self mapRatingFromSlider];
}

- (IBAction)next:(id)sender {
    PFUser *user = [PFUser currentUser];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    RatingModel *rating = [user objectForKey:@"rating"];
    if(!rating) {
        rating = [RatingModel object];
    }
    rating.user = [PFUser currentUser];
    rating.rating = [self mapRatingFromSlider];
    rating.confidence_level = 2;
    //rating.singles = [self mapRatingFromSlider];
    //rating.doubles = [self mapRatingFromSlider];
    //NSLog(@"Rating: %i",rating.singles);
    [rating saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        user[@"rating"] = rating;
        NSLog(@"Error: %@",error);
        if(!error) {
            [user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self performSegueWithIdentifier:@"picture" sender:self];
                
            }];
        } else {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
    }];
    
}

- (void)setLabels {
    NSDictionary *d = @{@1.5:@"You have limited experience and are working primarily on getting the ball in play.",
                        @2.0:@"You lack court experience and your strokes need developing.  You are familiar with the basic positions for singles and doubles play.",
                        @2.5:@"You are learning to judge where the ball is going, although your court coverage is limited.  You can sustain a short rally of slow pace with other players of the same ability.",
                        @3.0:@"You are fairly consistent when hitting medium-paced shots, but are not comfortable with all strokes and lack execution when trying for directional control, depth, or power. Your most common doubles formation is one-up, one-back.",
                        @3.5:@"You have achieved improved stroke dependability with directional control on moderate shots, but need to develop depth and variety. You exhibit more aggressive net play, have improved court coverage and are developing teamwork in doubles.",
                        @4.0:@"You have dependable strokes, including directional control and depth on both forehand and backhand sides on moderate-paced shots.  You can use lobs, overheads, approach shots and volleys with some success and occasionally force errors when serving. Rallies may be lost due to impatience. Teamwork in doubles is evident.",
                        @4.5:@"You have developed your use of power and spin and can handle pace. You have sound footwork, can control depth of shots, and attempt to vary game plan according to your opponents.  You can hit first serves with power and accuracy and place the second serve.  You tend to overhit on difficult shots. Aggressive net play is common in doubles.",
                        @5.0:@"You have good shot anticipation and frequently have an outstanding shot or attribute around which a game may be structured.  You can regularly hit winners or force errors off of short balls and can put away volleys.  You can successfully execute lobs, drop shots, half volleys, overhead smashes, and have good depth and spin on most second serves.",
                        @5.5:@"You have mastered power and/or consistency as a major weapon. You can vary strategies and styles of play in a competitive situation and hit dependable shots in a stress situation."};
    NSNumber *n = [NSNumber numberWithFloat:self.slider.value];
    self.explanation.text = [d objectForKey:n];
    CGSize s = [self.explanation sizeThatFits:CGSizeMake(272.0f, 1000.0f)];
    CGRect frame = self.explanation.frame;
    frame.size.height = s.height;
    
    //float diff = s.height-self.explanation.frame.size.height;
    
    self.explanation.frame = frame;
    //CGRect f = self.descriptionFrame.frame;
    //f.size = CGSizeMake(f.size.width, self.explanation.frame.size.height+self.label.frame.size.height+16.0f);
    //self.descriptionFrame.frame = f;
}

- (int)mapRatingFromSlider {
    NSNumber *key = [NSNumber numberWithFloat:self.slider.value];
    NSDictionary *d;
    if ([[[PFUser currentUser] objectForKey:@"female"] boolValue]) {
        d = @{@1.5:@76,
              @2.0:@68,
              @2.5:@60,
              @3.0:@55,
              @3.5:@50,
              @4.0:@43,
              @4.5:@35,
              @5.0:@28,
              @5.5:@21 };
    }else {
        d = @{@1.5:@76,
              @2.0:@67,
              @2.5:@58,
              @3.0:@50,
              @3.5:@44,
              @4.0:@36,
              @4.5:@28,
              @5.0:@21,
              @5.5:@14 };
    }
    
    NSNumber *val = [d objectForKey:key];
    return [val intValue];
}


- (IBAction)onNotSure:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotsurePopupViewController *notsurePopup = (NotsurePopupViewController *)[storyboard instantiateViewControllerWithIdentifier:@"NotsurePopup"];
    notsurePopup.parent = self;
    CGRect frame = notsurePopup.view.frame;
    frame.size.height = 340;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - 20;
    notsurePopup.view.frame = frame;
    [self presentPopupViewController:notsurePopup animationType:MJPopupViewAnimationSlideBottomTop];
}

- (IBAction)onback:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
