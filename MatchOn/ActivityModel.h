//
//  TestModel.h
//  Page
//
//  Created by Kevin Flynn on 11/16/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>

@interface ActivityModel : PFObject <PFSubclassing>

+ (NSString *)parseClassName;

// Class properties

// Type of activity
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *note;
@property (nonatomic, strong) NSString *court;

// User is the owner of the document/row - In this case, the creator
@property (nonatomic, strong) PFUser *user;
// Array of users
@property (nonatomic, strong) NSMutableArray *recipients;
@property (nonatomic, strong) NSMutableArray *declines;
@property (nonatomic, strong) NSMutableArray *messageRecipients;

// Array of searchable ratings
/*@property (nonatomic, strong) NSMutableArray *singlesRatings;
@property (nonatomic, strong) NSMutableArray *doublesRatings;*/

@property (nonatomic, strong) NSMutableArray *ratings;
// Ratings mapped to users
/*@property (nonatomic, strong) NSMutableDictionary *singlesMap;
@property (nonatomic, strong) NSMutableDictionary *doublesMap;*/

@property (nonatomic, strong) NSMutableDictionary *ratingMap;
// Average rating in map
/*@property (nonatomic, assign) float singlesAverage;
@property (nonatomic, assign) float doublesAverage;
@property (nonatomic, assign) float singlesHigh;
@property (nonatomic, assign) float doublesHigh;
@property (nonatomic, assign) float singlesLow;
@property (nonatomic, assign) float doublesLow;
@property (nonatomic, assign) float singlesMin;
@property (nonatomic, assign) float singlesMax;
@property (nonatomic, assign) float doublesMin;
@property (nonatomic, assign) float doublesMax;*/

//@property (nonatomic, assign) float ratingsAverage;
//@property (nonatomic, assign) float ratingsHigh;
//@property (nonatomic, assign) float ratingsLow;
@property (nonatomic, assign) float ratingsMin;
@property (nonatomic, assign) float ratingsMax;


// Count of all the recipients
//@property (nonatomic, assign) int totalRecipients;
// Number of available spots in the match
@property (nonatomic, assign) int spots;

// Start time for match
@property (nonatomic, strong) NSDate *startTime;
// Geo location
@property (nonatomic, strong) PFGeoPoint *geopoint;
// Locale information
@property (nonatomic, strong) NSDictionary *location;
// Doubles vs Singles Match
@property (nonatomic, assign) BOOL doubles;
//@property (nonatomic, assign) BOOL matchOn;

// 1 = Tennis
@property (nonatomic, assign) int sport;


@property (nonatomic, assign) BOOL active;

// Whether or not all spots have been filled
@property (nonatomic, assign) BOOL full;


// Invite only match - doesn't show up on browse feed
@property (nonatomic, assign) BOOL private;



// Whether or not they actually played
//@property (nonatomic, assign) BOOL played;

//@property (nonatomic, strong) NSMutableDictionary *score;
//@property (nonatomic, strong) NSMutableArray *winner;
//@property (nonatomic, strong) NSMutableArray *dictionary;


@property (nonatomic, strong) PFUser *scorer;
//@property (nonatomic, strong) PFUser *reviewer;
//@property (nonatomic, strong) NSMutableArray *reviewers;
@property (nonatomic, strong) NSMutableArray *invitations;
//@property (nonatomic, strong) NSMutableArray *scorers;

@property (nonatomic, strong) NSMutableArray *winner;
@property (nonatomic, strong) NSMutableArray *loser;

@property (nonatomic, strong) NSMutableArray *firstTeam;
@property (nonatomic, strong) NSMutableArray *secondTeam;


// If the score has been confirmed by at least one other
@property (nonatomic, assign) BOOL confirmed;
@property (nonatomic, assign) BOOL approved;

@property (nonatomic, strong) PFObject *score;
//@property (nonatomic, strong) ActivityModel *parent;


- (void)setRatingValues;

@end
