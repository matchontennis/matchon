//
//  PushNoAnimationSegue.h
//  wave
//
//  Created by Kevin Flynn on 4/16/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PushNoAnimationSegue :  UIStoryboardSegue

@end
