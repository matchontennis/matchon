//
//  HomeViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 11/22/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <CoreLocation/CoreLocation.h>
#import "ActivityModel.h"

@protocol HomeViewControllerDelegate <NSObject>

- (void)showView;

@end
@interface HomeViewController : UITableViewController<HomeViewControllerDelegate> {
    BOOL is_selected_score;
}

@property (nonatomic, strong) UIViewController *prevViewController;
@property (nonatomic,assign) BOOL pullToRefreshEnabled;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic,strong) NSMutableArray *objects1;
@property (nonatomic,strong) NSMutableArray *objects2;
@property (nonatomic,strong) NSMutableArray *objects3;
@property (nonatomic,strong) NSMutableArray *objects4;
@property (nonatomic,strong) NSMutableArray *objects;
@property (nonatomic,strong) NSMutableArray *defaults;
@property (nonatomic, assign) BOOL endOfResults;
@property (nonatomic, assign) BOOL endOfResults1;

// Query config
@property (nonatomic,assign) NSInteger limit;
@property (nonatomic,assign) NSInteger skip;


@property (nonatomic, assign) BOOL noResults;
@property (nonatomic, assign) BOOL noResults1;
@property (nonatomic, strong) PFGeoPoint *geopoint;
@property (nonatomic, strong) ActivityModel *selectedActivity;
@property (nonatomic, strong) NSIndexPath *selectedIndex;

- (IBAction)message:(id)sender;
- (IBAction)action:(id)sender;
- (IBAction)invite:(id)sender;
- (IBAction)map:(id)sender;
- (IBAction)notes:(id)sender;
- (IBAction)onClose:(id)sender;

-(void)refreshInbox;
- (IBAction)score:(id)sender;


@property (nonatomic, assign) int total;
@property (nonatomic, assign) int week;
@property (nonatomic, assign) int month;

@end
