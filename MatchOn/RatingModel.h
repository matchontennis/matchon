//
//  RatingModel.h
//  MatchOn
//
//  Created by Kevin Flynn on 12/2/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <Parse/Parse.h>

@interface RatingModel : PFObject <PFSubclassing>


@property (nonatomic, strong) PFUser *user;
/*@property (nonatomic, assign) int singles;
@property (nonatomic, assign) int doubles;*/
@property (nonatomic, assign) double rating;
@property (nonatomic, assign) double confidence_level;
@property (nonatomic, assign) int score;
//@property (nonatomic, assign) int curpoint;

@end
