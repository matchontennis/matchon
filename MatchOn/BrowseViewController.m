//
//  BrowseViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 10/23/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "BrowseViewController.h"
#import "BrowseViewCell.h"
#import "ActivityModel.h"
#import "DateTools.h"
#import "MessageViewController.h"
#import "MBProgressHUD.h"
#import "RatingModel.h"
#import "Constants.h"
#import "MapViewController.h"
#import "OtherProfileViewController.h"
#import "Localytics.h"

@interface BrowseViewController ()<CLLocationManagerDelegate> {
    int threshold;
}

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation BrowseViewController


#pragma mark - UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Config
    self.limit = 25;
    self.skip = 0;
    
    
    self.tableView.separatorInset = UIEdgeInsetsZero;
    self.tableView.separatorColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
    
    // Refresh
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshInbox) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    //self.objects = [NSMutableArray array];
    
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    threshold = 10;

    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    //[self.locationManager startUpdatingLocation];
    self.currentRating = [[PFUser currentUser] objectForKey:@"rating"];
    if(!self.currentRating) {
        self.currentRating = [RatingModel object];
        self.currentRating[@"user"] = [PFUser currentUser];
        //self.currentRating.singles = 80;
        //self.currentRating.doubles = 80;
        self.currentRating.rating = 80;
        [self.currentRating saveInBackgroundWithTarget:self selector:@selector(getLocation)];
        
    } else {
        [self.currentRating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            //self.currentRating = (RatingModel *)object;
            NSLog(@"%@",[object objectForKey:@"rating"]);
            [self getLocation];
        }];
    }
    self.tableView.separatorColor = [UIColor colorWithWhite:0.8f alpha:1.0f];

}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}



#pragma mark - Data for table

- (void)refreshInbox{
    if(![PFUser currentUser]) {
        [self.objects removeAllObjects];
        [self.tableView reloadData];
        return;
    }
    self.pullToRefreshEnabled = TRUE;
    self.skip = 0;
    [self loadObjects];
}

- (void)getLocation {
    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
        if (!error) {
            self.geopoint = geoPoint;
            [self refreshInbox];
        } else {
            [self refreshInbox];
        }
    }];
}

- (void)loadNextPage{
    self.skip += self.limit;
    if(!self.endOfResults) {
        [self loadObjects];
    }
}


- (void)loadObjects{
    PFQuery *query = [self queryForTable];
    if(self.pullToRefreshEnabled) {
        self.skip = 0;
    }
    query.limit = self.limit;
    query.skip = self.skip;
    __block int whichPass = 1;
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        
        if(objects.count == 0) {
            self.endOfResults = TRUE;
        }
        // Stop refresh control
        if ([self.refreshControl isRefreshing]) {
            [self.refreshControl endRefreshing];
        }
        // Remove objects, clear stale results
        if(self.pullToRefreshEnabled) {
            [self.objects removeAllObjects];
        }
        if(whichPass > 1) {
            [self.objects removeAllObjects];
        }
        if (self.objects == nil && objects != nil) {
            self.objects = [NSMutableArray array];
        }
        [self.objects addObjectsFromArray:objects];
        
        if(self.objects.count == 0) {
            if(!self.noResults) {
                self.noResults = TRUE;
            }
        } else {
            self.noResults = FALSE;
        }
        [self.tableView reloadData];
        
        
        self.pullToRefreshEnabled = FALSE;
        if(self.objects.count == 0 && whichPass > 1 && threshold < 20) {
            NSLog(@"Search more for %i",threshold);
            
            //threshold += 5;
            [self refreshInbox];
        }
            
        whichPass = whichPass + 1;
        
    }];
}




#pragma mark - QueryTableViewController

- (PFQuery *)queryForTable {
    //PFQuery *singlesQuery = [self queryForSingles];
    //PFQuery *doublesQuery = [self queryForSingles];
    //PFQuery *query =  [PFQuery orQueryWithSubqueries:@[singlesQuery,doublesQuery]];
    PFQuery *query = [self queryForRating];
    if (self.pullToRefreshEnabled) {
        query.cachePolicy = kPFCachePolicyNetworkOnly;
    }else if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    [query orderByAscending:@"startTime"];
    [query includeKey:@"recipients"];
    [query includeKey:@"user"];
    
    return query;
}


/*- (PFQuery *)queryForSingles {
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    [query whereKey:@"startTime" greaterThan:[NSDate date]];
    
    [query whereKey:@"sport" equalTo:@1];
    [query whereKey:@"type" equalTo:@"play"];
    [query whereKey:@"full" notEqualTo:@YES];
    [query whereKey:@"private" notEqualTo:@YES];
    
    
    if(self.currentRating) {
        NSLog(@"Current rating: %i",self.currentRating.singles);
        NSNumber *max = [NSNumber numberWithInt:(self.currentRating.singles + threshold)];
        NSNumber *min = [NSNumber numberWithInt:(self.currentRating.singles - threshold)];
        NSLog(@"Min: %@",min);
        NSLog(@"Max: %@",max);
        [query whereKey:@"singlesHigh" greaterThanOrEqualTo:min];
        [query whereKey:@"singlesLow" lessThanOrEqualTo:max];
    }
    [query whereKey:@"full" notEqualTo:@YES];
    
    if(self.geopoint) {
        [query whereKey:@"geopoint" nearGeoPoint:self.geopoint withinMiles:25];
    }
    // Turn back on *********************
    [query whereKey:@"recipients" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"invitations" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"active" equalTo:@YES];
    return query;
}*/

/*- (PFQuery *)queryForDoubles {
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    
    [query whereKey:@"startTime" greaterThan:[NSDate date]];
    
    [query whereKey:@"sport" equalTo:@1];
    [query whereKey:@"type" equalTo:@"play"];
    [query whereKey:@"full" notEqualTo:@YES];
    [query whereKey:@"private" notEqualTo:@YES];
    
    if(self.currentRating) {
        NSLog(@"Current rating: %i",self.currentRating.singles);
        NSNumber *max = [NSNumber numberWithInt:(self.currentRating.doubles + threshold)];
        NSNumber *min = [NSNumber numberWithInt:(self.currentRating.doubles - threshold)];
        NSLog(@"Min: %@",min);
        NSLog(@"Max: %@",max);
        [query whereKey:@"lowConstraint" greaterThanOrEqualTo:min];
        [query whereKey:@"highConstraint" lessThanOrEqualTo:max];
    }
    [query whereKey:@"full" notEqualTo:@YES];
    
    if(self.geopoint) {
        [query whereKey:@"geopoint" nearGeoPoint:self.geopoint withinMiles:25];
    }
    // Turn back on *********************
    [query whereKey:@"recipients" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"invitations" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"active" equalTo:@YES];
    //[query whereKey:@"active" equalTo:@NO];
    return query;
}*/

- (PFQuery *)queryForRating {
    PFQuery *query = [PFQuery queryWithClassName:@"Activity"];
    
    [query whereKey:@"startTime" greaterThan:[NSDate date]];
    
    [query whereKey:@"sport" equalTo:@1];
    [query whereKey:@"type" equalTo:@"play"];
    [query whereKey:@"full" notEqualTo:@YES];
    //[query whereKey:@"private" notEqualTo:@YES];
    
    if(self.currentRating) {
        NSLog(@"Current rating: %i",self.currentRating.rating);
        NSNumber *max = [NSNumber numberWithInt:(self.currentRating.rating + threshold)];
        NSNumber *min = [NSNumber numberWithInt:(self.currentRating.rating - threshold)];
        NSLog(@"Min: %@",min);
        NSLog(@"Max: %@",max);
        [query whereKey:@"ratingsMin" greaterThanOrEqualTo:min];
        [query whereKey:@"ratingsMax" lessThanOrEqualTo:max];
    }
    //[query whereKey:@"full" notEqualTo:@YES];
    
    if(self.geopoint) {
        [query whereKey:@"geopoint" nearGeoPoint:self.geopoint withinMiles:25];
    }
    // Turn back on *********************
    [query whereKey:@"recipients" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"invitations" notEqualTo:[PFUser currentUser]];
    [query whereKey:@"active" equalTo:@YES];
    //[query whereKey:@"active" equalTo:@NO];
    return query;
}


#pragma mark - Object at index

- (ActivityModel *)objectForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ActivityModel *object = [self.objects objectAtIndex:indexPath.row];
    return object;
}


#pragma mark - UITableView Data Source

/*- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    float height = 0.0f;
    if(self.objects.count == 0) {
        if (self.objects == nil) {
            return 0.01f;
        }
        height = self.tableView.frame.size.height;
    }
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *view;
    if (self.objects == nil) {
        return nil;
    }
    
    if(self.objects.count != 0) {
        self.tableView.separatorColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
        return view;
    }
    view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width,self.tableView.frame.size.height)];
    CGRect frame = view.frame;
    frame.size = CGSizeMake(frame.size.width-20.0f, frame.size.height);
    frame.origin= CGPointMake(10.0f, 0.0f);
    view.backgroundColor = [UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:246.0f/255.0f alpha:1.0f];
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = @"NEW PLAYERS ARE JOINING EVERY HOUR";
    label.textColor = [UIColor lightGrayColor];
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.numberOfLines = 0;
    [label setFont:[UIFont fontWithName:@"MyriadPro-Bold" size:24.0f]];
    label.textAlignment = NSTextAlignmentCenter;
    [view addSubview:label];
    return view;
}*/

/*- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}*/

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.objects.count == 0) {
        return tableView.frame.size.height;
    }
    return 191.0f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.objects == nil) {
        tableView.bounces = NO;
        return 1;
    }else if (self.objects.count == 0) {
        tableView.bounces = YES;
        return 1;
    }
    tableView.bounces = YES;
    return self.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    static NSString *CellIdentifier = @"Cell";
    
    if (self.objects == nil) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LogoCell" forIndexPath:indexPath];
        return cell;
    }else if (self.objects.count == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyCell" forIndexPath:indexPath];
        return cell;
    }
    ActivityModel *activity = [self objectForRowAtIndexPath:indexPath];
    BrowseViewCell *cell = (BrowseViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    PFUser *user = activity.user;
    
    cell.messageButton.layer.cornerRadius = 4.0f;
    cell.actionButton.layer.cornerRadius = 4.0f;
    cell.messageButton.clipsToBounds = YES;
    cell.actionButton.clipsToBounds = YES;
    cell.actionButton.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.actionButton.layer.borderWidth = 2.0f;
    cell.messageButton.layer.borderWidth = 2.0f;
    cell.messageButton.layer.borderColor = THEME_COLOR.CGColor;
    
    if (activity.note == nil || [activity.note isEqualToString:@""]) {
        cell.notesButton.hidden = YES;
    }else {
        cell.notesButton.hidden = NO;
    }
    
    UIView *view = [cell.contentView viewWithTag:187];
    [view removeFromSuperview];
    cell.actionButton.hidden = FALSE;
    cell.actionButton.enabled = TRUE;
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    
    NSString *type = @"SINGLES";
    if(activity.doubles) {
        type = @"DOUBLES";
    }
    cell.sportLabel.text = [NSString stringWithFormat:@"%@ MATCH",type];
    
    NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
    
    if(activity.startTime.year != [NSDate date].year) {
        dateTimeFormatter.dateFormat = @"EEE, MMM d, YYYY @h:mm a";
    } else {
        dateTimeFormatter.dateFormat = @"EEE, MMM d @h:mm a";
    }
    
    NSString *datetime = [dateTimeFormatter stringFromDate: activity.startTime];
    
    cell.fromNowLabel.text = activity.startTime.shortTimeAgoSinceNow;
    cell.timeLabel.text = datetime;
    
    if (activity.court != nil && ![activity.court isEqual:[NSNull null]] && ![activity.court isEqualToString:@""]) {
        cell.locationLabel.text = activity.court;
    }else {
        cell.locationLabel.text = [[NSString stringWithFormat:@"NEAR %@ %@",
                                    activity.location[@"Thoroughfare"],
                                    activity.location[@"City"]] uppercaseString];
        
        if(!activity.location[@"Thoroughfare"]) {
            cell.locationLabel.text = [[NSString stringWithFormat:@"NEAR %@",
                                        activity.location[@"City"]] uppercaseString];
        }
    }
    
    if([activity.startTime isEarlierThan:[NSDate date]]){
        // Date is in the past
        cell.openingsLabel.text = @"";
    } else {
        // Date is in the future
        
        if(activity.recipients.count >= activity.spots){
            //NSLog(@"Recipients count: %li",(long)activity.recipients.count);
            //NSLog(@"Spots: %li",(long)activity.spots);
            // FULL
            cell.openingsLabel.text = [NSString stringWithFormat:@"MATCH ON"];
            cell.openingsLabel.textColor = THEME_COLOR;

        } else {
            // OPENINGS
            cell.openingsLabel.text = [NSString stringWithFormat:@"%i OPENING",(int)(activity.spots-activity.recipients.count)];
            if((activity.spots-activity.recipients.count) != 1){
                cell.openingsLabel.text = [NSString stringWithFormat:@"%@s",cell.openingsLabel.text];
            }
            cell.openingsLabel.textColor = [UIColor redColor];
        }
    }
    
    for(int i=0;i<4;i++) {
        int tag = i+1;
        UIView *container = [cell.contentView viewWithTag:tag];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onProfile:)];
        [tap setCancelsTouchesInView:YES];
        [container addGestureRecognizer:tap];
        if(container) {
            UILabel *label = (UILabel *)[container viewWithTag:tag*13];
            label.backgroundColor = [UIColor clearColor];
            label.text = @"";
            PFImageView *imageView = (PFImageView *)[container viewWithTag:tag*10];
            imageView.image = nil;
            imageView.layer.borderColor = [UIColor clearColor].CGColor;
            UILabel *name = (UILabel *)[container viewWithTag:tag*11];
            name.text = @"";
            UILabel *host = (UILabel *)[container viewWithTag:tag*12];
            host.text = @"";
        }
    }
    
    // Set the players labels
    for(int i=0;i<activity.recipients.count;i++) {
        PFUser *recipient = [activity.recipients objectAtIndex:i];
        if(!recipient || [recipient isEqual:[NSNull null]] || !recipient.isDataAvailable)continue;
        int tag = i+1;
        UIView *container = [cell.contentView viewWithTag:tag];
        if(container) {
            PFImageView *imageView = (PFImageView *)[container viewWithTag:tag*10];
            if(imageView) {
                PFFile *file = [recipient objectForKey:@"image"];
                imageView.file = file;
                /*if([recipient[@"female"] boolValue]) {
                    imageView.image = [UIImage imageNamed:@"female"];
                } else {
                    imageView.image = [UIImage imageNamed:@"male"];
                }*/
                imageView.image = [UIImage imageNamed:@"default_profile"];
                [imageView loadInBackground];
                imageView.layer.cornerRadius = imageView.frame.size.height/2.0f;
                imageView.clipsToBounds = YES;
                imageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
                imageView.layer.borderWidth = 2.0f;
                imageView.contentMode = UIViewContentModeScaleAspectFill;
            }
            UILabel *name = (UILabel *)[container viewWithTag:tag*11];
            if(name) {
                
                
                NSString *first = [recipient objectForKey:@"first"];
                NSString *last = [recipient objectForKey:@"last"];
                
                if(!first) {
                    NSArray *name = [recipient[@"name"] componentsSeparatedByString:@" "];
                    first = [name firstObject];
                    if(!last) {
                        if(name.count > 1) {
                            last = [name lastObject];
                        }
                    }
                }
                name.text = first;
                if(last && last.length > 0) {
                    name.text = [NSString stringWithFormat:@"%@ %@.",name.text, [last substringToIndex:1]];
                }
                
            }
            UILabel *host = (UILabel *)[container viewWithTag:tag*12];
            if([recipient.objectId isEqualToString:user.objectId]) {
                host.text = @"HOST";
                if(imageView) {
                    imageView.layer.borderColor = THEME_COLOR.CGColor;
                }
            } else {
                host.text = @"";
            }
        }
    }
    
    if(self.geopoint && activity.geopoint) {
        CLLocation *location1 = [[CLLocation alloc] initWithLatitude:self.geopoint.latitude longitude:self.geopoint.longitude];
        CLLocation *location2 = [[CLLocation alloc] initWithLatitude:activity.geopoint.latitude longitude:activity.geopoint.longitude];
        CLLocationDistance distance = [location1 distanceFromLocation:location2];
        float miles = roundf(0.000621371*distance);
        cell.distanceLabel.text = [NSString stringWithFormat:@"%.1f mi",miles];
        
    } else {
        cell.distanceLabel.text = @"";
    }
    
    if (indexPath.row % 2 == 1) {
        cell.contentView.backgroundColor = [UIColor colorWithRed:235 / 255.0f green:235 / 255.0f blue:241 / 255.0f alpha:1.0f];
    }else {
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
    return cell;
}

-(void)onProfile:(UIGestureRecognizer *)gesture
{
    UIView *sender = gesture.view;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    self.selectedActivity = (ActivityModel *)[self.objects objectAtIndex:selectedIndex.row];

    UILabel *name = (UILabel*)[sender viewWithTag:sender.tag*11];
    if ([name.text isEqualToString:@""])
    {
        return;
    }
    [self performSegueWithIdentifier:@"profile" sender:sender];
}

#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView willDisplayCell:(BrowseViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;{
    
    if(!self.pullToRefreshEnabled &&  self.objects.count > 0 && !self.endOfResults){
        NSInteger total = (self.objects.count - 2);
        if (indexPath.row == total)
        {
            if ([tableView.indexPathsForVisibleRows containsObject:indexPath])
            {
                [self loadNextPage];
            }
        }
    }
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        NSLog(@"Set it");
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}



#pragma mark - Location Manager Delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    switch (status) {
        case kCLAuthorizationStatusDenied:
            NSLog(@"kCLAuthorizationStatusDenied");
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Not Enabled" message:@"Please Turn on Location Services\nFor this app to be useful you will need to turn on location services. We use your current location to show you only matches in your area.\nTo enable, go to the settings app on your phone, find the MatchOn app and enable." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alertView show];
        }
            break;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        {
            //NSLog(@"Location here!");
        }
            break;
        case kCLAuthorizationStatusAuthorizedAlways:
        {
            
        }
            break;
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"kCLAuthorizationStatusNotDetermined");
            break;
        case kCLAuthorizationStatusRestricted:
            NSLog(@"kCLAuthorizationStatusRestricted");
            break;
    }
}


#pragma mark - IBActions

- (IBAction)join:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    BrowseViewCell *cell = (BrowseViewCell *)[self.tableView cellForRowAtIndexPath:selectedIndex];
    ActivityModel *activity = (ActivityModel *)[self.objects objectAtIndex:selectedIndex.row];
    [self joinWithActivity:activity andCell:cell];
}

- (IBAction)message:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    self.selectedActivity = (ActivityModel *)[self.objects objectAtIndex:selectedIndex.row];
    [self performSegueWithIdentifier:@"message" sender:self];
}

- (IBAction)map:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    self.selectedActivity = (ActivityModel *)[self.objects objectAtIndex:selectedIndex.row];
    [self performSegueWithIdentifier:@"map" sender:self];
}

- (IBAction)notes:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *selectedIndex = [self.tableView indexPathForRowAtPoint:buttonPosition];
    self.selectedActivity = (ActivityModel *)[self.objects objectAtIndex:selectedIndex.row];
    [self showPopup:self.selectedActivity.note];
}

-(void)showPopup:(NSString *)note
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotePopup" object:nil userInfo:@{@"note":note}];
}

#pragma mark Helper

- (void)joinWithActivity:(ActivityModel *)activity andCell:(BrowseViewCell *)cell {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    
    [activity fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if(error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"An error occurred. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        } else {
            
            if(activity.recipients.count == activity.spots) {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Looks like this match is full!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            } else {
                for(PFUser *user in activity.recipients) {
                    if([user.objectId isEqualToString:[PFUser currentUser].objectId]) {
                        // Error Return
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops" message:@"Looks like you've already joined this match!" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                        [alertView show];
                        return;
                    }
                }
                [activity.recipients addObject:[PFUser currentUser]];
                //activity.totalRecipients = (int)activity.recipients.count;
                activity.full = (activity.recipients.count>=activity.spots);
                //activity.matchOn = (activity.totalRecipients >= activity.spots);
                
                if (activity.full && [activity.startTime compare:[NSDate date]] == NSOrderedDescending) {
                    PFUser *currentUser = [PFUser currentUser];
                    NSMutableArray *players = [currentUser objectForKey:@"players"];
                    if (![players containsObject:activity.user]) {
                        [players addObject:activity.user];
                    }
                    for (int i = 0; i < activity.recipients.count; i++) {
                        PFUser *user = (PFUser *)[activity.recipients objectAtIndex:i];
                        if (![players containsObject:user]) {
                            [players addObject:user];
                        }
                    }
                    if (players != nil) {
                        [currentUser setObject:players forKey:@"players"];
                        [currentUser saveInBackground];
                    }
                }
                
                RatingModel *rating = [PFUser currentUser][@"rating"];
                [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    RatingModel *refreshed = (RatingModel *)object;
                    /*if(!activity.singlesMap) {
                        activity.singlesMap = [NSMutableDictionary dictionary];
                    }
                    if(!activity.doublesMap) {
                        activity.doublesMap = [NSMutableDictionary dictionary];
                    }*/
                    
                    if(!activity.ratingMap) {
                        activity.ratingMap = [NSMutableDictionary dictionary];
                    }
                    //NSNumber *s = [[NSNumber alloc] initWithInt:refreshed.singles];
                    //NSNumber *d = [[NSNumber alloc] initWithInt:refreshed.singles];
                    
                    //[activity.singlesMap setObject:s forKey:[PFUser currentUser].objectId];
                    //[activity.doublesMap setObject:d forKey:[PFUser currentUser].objectId];
                    NSNumber *r = [[NSNumber alloc] initWithInt:refreshed.rating];
                    [activity.ratingMap setObject:r forKey:[PFUser currentUser].objectId];
                    [activity setRatingValues];
                    
                    
                    
                    [activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        
                        PFQuery *pushQuery = [PFInstallation query];
                        NSMutableArray *recipients = [NSMutableArray array];
                        for(PFUser *user in activity.recipients){
                            if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
                            [recipients addObject:user];
                        }
                        [pushQuery whereKey:@"user" containedIn:recipients];
                        
                        NSLog(@"Sending join push to recipients: %@",recipients);
                        
                        
                        NSString *first = [[PFUser currentUser] objectForKey:@"first"];
                        NSString *last = [[PFUser currentUser] objectForKey:@"last"];
                        if(!first) {
                            NSArray *name = [[PFUser currentUser][@"name"] componentsSeparatedByString:@" "];
                            first = [name firstObject];
                            if(!last) {
                                if(name.count > 1) {
                                    last = [name lastObject];
                                }
                            }
                        }
                        NSString *full = first;
                        if(last && last.length > 0) {
                            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
                        }
                        
                        
                        
                        
                        
                        NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ joined your match!", full], @"badge" : @"Increment", @"a": activity.objectId};
                        
                        
                        
                        
                        
                        PFPush *push = [[PFPush alloc] init];
                        [push setQuery:pushQuery];
                        [push setData:data];
                        [push sendPushInBackground];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"Match" object:self];
                        UILabel *label = [[UILabel alloc] initWithFrame:cell.actionButton.frame];
                        label.text = @"JOINED!";
                        label.tag = 187;
                        [cell.contentView addSubview:label];
                        cell.actionButton.hidden = TRUE;
                        cell.actionButton.enabled = FALSE;
                        
                        
                        if(activity.recipients.count == activity.spots) {
                            // MatchOn Notification.
                            PFQuery *pushMatch = [PFInstallation query];
                            
                            [pushMatch whereKey:@"user" containedIn:activity.recipients];
                            
                            NSDateFormatter *timeFormatter = [[NSDateFormatter alloc]init];
                            timeFormatter.dateFormat = @"h:mma";
                            NSDateFormatter *dateTimeFormatter = [[NSDateFormatter alloc]init];
                            dateTimeFormatter.dateFormat = @"EEE, MMM d, YYYY";
                            
                            
                            NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"Your Match is On at %@, on %@",[timeFormatter stringFromDate: activity.startTime],[dateTimeFormatter stringFromDate: activity.startTime]], @"badge" : @"Increment", @"a": activity.objectId};
                            PFPush *push = [[PFPush alloc] init];
                            [push setQuery:pushMatch];
                            [push setData:data];
                            [push sendPushInBackground];
                        }
                        
                        [self.tabBarController setSelectedIndex:0];
                        [Localytics tagEvent:@"Joined Match"];
                        
                    }];
                    
                }];
                
            }
        }
        
    }];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"message"]) {
        MessageViewController *message = [segue destinationViewController];
        message.recipients = self.selectedActivity.recipients;
        message.activity = self.selectedActivity;
    } else if([segue.identifier isEqualToString:@"map"]) {
        UINavigationController *nav = [segue destinationViewController];
        MapViewController *vc = [nav.viewControllers firstObject];
        vc.activity = self.selectedActivity;
    }  else if ([segue.identifier isEqualToString:@"profile"]) {
        OtherProfileViewController *vc = [segue destinationViewController];
        UIView *viewS = (UIView *)sender;
        vc.curUser = [self.selectedActivity.recipients objectAtIndex:viewS.tag - 1];
    }
}

@end
