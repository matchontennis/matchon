//
//  PlayViewController.h
//  MatchOn
//
//  Created by Kevin Flynn on 11/24/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <CoreLocation/CoreLocation.h>
#import "ActivityModel.h"
#import "InvitePopupViewController.h"
#import "InviteViewController.h"
#import "StartPageViewController.h"

@interface PlayViewController : UITableViewController <UITextViewDelegate, InvitePopupViewControllerDelegate, InviteViewControllerDelegate> {
    NSString *curNote;
    PFGeoPoint *currentPoint;
}

@property (nonatomic, strong) PFGeoPoint *geopt;

@property (nonatomic, strong) ActivityModel *activity;

// Cell to Model Props

// Default is singles.
@property (nonatomic, assign) BOOL doubles;
@property (nonatomic, assign) BOOL editing;
@property (nonatomic, assign) BOOL locationEdit;
@property (nonatomic, assign) BOOL fromStart;



@property (nonatomic, strong) NSDictionary *location;
@property (nonatomic, strong) NSString *court;

@property (weak) id <StartPageViewControllerDelegate> startPageDelegate;
- (IBAction)cancel:(id)sender;
- (IBAction)remove:(id)sender;
- (IBAction)done:(id)sender;


- (void)setLocationFromGeoPoint;

@end
