//
//  StartPageViewController.m
//  MatchOn
//
//  Created by Dandeljane Maraat on 4/18/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "StartPageViewController.h"
#import "PlayViewController.h"
#import "Constants.h"
#import "RatingModel.h"
#import "PointsModel.h"
#import "DateTools.h"
#import "ScoreModel.h"
#import "DescriptionViewController.h"

@interface StartPageViewController ()

@end

@implementation StartPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor colorWithRed:0.88 green:0.88 blue:0.92 alpha:1.0f]];
    [self.view addSubview:[self userDetailsView]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)userDetailsView {
    if(![PFUser currentUser]) return nil;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, 85.0f)];
    view.backgroundColor = [UIColor colorWithRed:100.0f/255.0f green:101.0f/255.0f blue:125.0f/255.0f alpha:1.0f];
    PFImageView *imageView = [[PFImageView alloc] initWithFrame:CGRectMake(10.0f, 10.0f, 60.0f, 60.0f)];
    PFFile *file = [PFUser currentUser][@"image"];
    [imageView setFile:file];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    /*if([PFUser currentUser] && [[PFUser currentUser][@"female"] boolValue]) {
        imageView.image = [UIImage imageNamed:@"female"];
    } else {
        imageView.image = [UIImage imageNamed:@"male"];
    }*/
    imageView.image = [UIImage imageNamed:@"default_profile"];
    [imageView loadInBackground];
    imageView.layer.cornerRadius = 30.0f;
    imageView.clipsToBounds = YES;
    imageView.layer.borderWidth = 2.0f;
    imageView.layer.borderColor = THEME_COLOR.CGColor;
    [view addSubview:imageView];
    
    CGRect frame = view.frame;
    frame.size = CGSizeMake(110.0f, 28.0f);
    frame.origin= CGPointMake(85.0f, 10.0f);
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.text = @"My Scoreboard";//[NSString stringWithFormat:@"%@'s \rScoreboard",[PFUser currentUser][@"first"]];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"MyriadPro-Cond" size:24.0f];
    label.numberOfLines = 1;
    
    // Add a rightBorder.
    CALayer *scoreBorder = [CALayer layer];
    scoreBorder.frame = CGRectMake(0, label.frame.size.height - 1.0f, label.frame.size.width + 23.0f, 1.0f);
    scoreBorder.backgroundColor = [UIColor colorWithRed:145 / 255.0f green:147 / 255.0f blue:165 / 255.0f alpha:1.0f].CGColor;
    [label.layer addSublayer:scoreBorder];
    [view addSubview:label];
    
    CGRect bf = view.frame;
    bf.size = CGSizeMake(141.0f, 72.0f);
    bf.origin= CGPointMake(75.0f, 10.0f);
    UIButton *btnHelp = [[UIButton alloc] initWithFrame:bf];
    [btnHelp addTarget:self action:@selector(showHelp:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:btnHelp];
    
    /*CGRect f = view.frame;
     f.size = CGSizeMake(138.0f, 30.0f);
     f.origin= CGPointMake(182.0f, 3.0f);
     UILabel *l = [[UILabel alloc] initWithFrame:f];
     l.textColor = THEME_COLOR;
     l.text = @"POINTS EARNED";
     l.textAlignment = NSTextAlignmentCenter;
     l.font = [UIFont boldSystemFontOfSize:13.0f];
     [view addSubview:l];*/
    
    if(!self.total)self.total = 0;
    CGRect fTotal = view.frame;
    fTotal.size = CGSizeMake(15.0f, 30.0f);
    fTotal.origin= CGPointMake(85.0f, 40.0f);
    UILabel *lTotal = [[UILabel alloc] initWithFrame:fTotal];
    lTotal.textColor = [UIColor whiteColor];
    lTotal.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:30.0f];
    lTotal.numberOfLines = 1;
    lTotal.textAlignment = NSTextAlignmentLeft;
    //lTotal.text = [@(self.total) stringValue];
    [view addSubview:lTotal];
    CGRect fSTotal = view.frame;
    fSTotal.size = CGSizeMake(40.0f, 35.0f);
    fSTotal.origin= CGPointMake(100.0f, 37.0f);
    UILabel *lSTotal = [[UILabel alloc] initWithFrame:fSTotal];
    lSTotal.textColor = THEME_COLOR;
    NSMutableAttributedString *attrTotalPoints = [[NSMutableAttributedString alloc] initWithString:@"TOTAL\nPOINTS"];
    [attrTotalPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Cond" size:15.0f]} range:NSMakeRange(0, 5)];
    [attrTotalPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-BoldCond" size:15.0f]} range:NSMakeRange(6, 6)];
    lSTotal.attributedText = attrTotalPoints;
    lSTotal.textAlignment = NSTextAlignmentLeft;
    lSTotal.numberOfLines = 2;
    [view addSubview:lSTotal];
    
    
    //NSDate *month = [NSDate dateWithTimeIntervalSinceNow:(-60*60*24*7*30)];
    
    if(!self.week)self.week = 0;
    if(!self.month)self.month = 0;
    
    
    CGRect fm = view.frame;
    fm.size = CGSizeMake(15.0f, 30.0f);
    fm.origin= CGPointMake(140.0f, 40.0f);
    UILabel *lm = [[UILabel alloc] initWithFrame:fm];
    lm.textColor = [UIColor whiteColor];
    lm.text = @"0";//[@(self.month) stringValue];
    lm.font = [UIFont fontWithName:@"MyriadPro-BoldCond" size:30.0f];
    lm.numberOfLines = 1;
    lm.textAlignment = NSTextAlignmentLeft;
    [view addSubview:lm];
    
    NSMutableAttributedString *attrCurPoints = [[NSMutableAttributedString alloc] initWithString:@"PRIZES\nWON"];
    [attrCurPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-Cond" size:15.0f]} range:NSMakeRange(0, 6)];
    [attrCurPoints addAttributes:@{NSFontAttributeName:[UIFont fontWithName:@"MyriadPro-BoldCond" size:15.0f]} range:NSMakeRange(7, 3)];
    
    CGRect ftm = view.frame;
    ftm.size = CGSizeMake(45.0f, 35.0f);
    ftm.origin= CGPointMake(155.0f, 37.0f);
    UILabel *ltm = [[UILabel alloc] initWithFrame:ftm];
    ltm.textColor = THEME_COLOR; //[UIColor colorWithWhite:0.8f alpha:1.0f];
    ltm.attributedText = attrCurPoints;
    ltm.textAlignment = NSTextAlignmentLeft;
    //ltm.font = [UIFont boldSystemFontOfSize:12.0f];
    ltm.numberOfLines = 2;
    [view addSubview:ltm];
    
    RatingModel *rating = [PFUser currentUser][@"rating"];
    [rating fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        RatingModel *refreshed = (RatingModel *)object;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.total = refreshed.score;
            lTotal.text = [@(self.total) stringValue];
            CGRect fTotal = lTotal.frame;
            CGRect fSTotal = lSTotal.frame;
            CGRect fm = lm.frame;
            CGRect ftm = ltm.frame;
            if (self.total == 0) {
                fTotal.size = CGSizeMake(15.0f, 30.0f);
            }else {
                fTotal.size = CGSizeMake(15.0f * ((int)log10(self.total) + 1), 30.0f);
            }
            /*if (refreshed.curpoint == 0) {
                fm.size = CGSizeMake(15.0f, 30.0f);
            }else {
                fm.size = CGSizeMake(15.0f * ((int)log10(refreshed.curpoint) + 1), 30.0f);
            }*/
            fSTotal.origin.x = fTotal.origin.x + fTotal.size.width;
            if (self.total < 10) {
                fm.origin.x = fSTotal.origin.x + fSTotal.size.width + 15;
            }else {
                fm.origin.x = fSTotal.origin.x + fSTotal.size.width + 5;
            }
            ftm.origin.x = fm.origin.x + fm.size.width;
            lTotal.frame = fTotal;
            lSTotal.frame = fSTotal;
            lm.frame = fm;
            ltm.frame = ftm;
            //lm.text = [@(refreshed.curpoint) stringValue];
            NSLog(@"Total: %i",self.total);
            //NSLog(@"Total: @",[@(refreshed.score) stringValue]);
        });
    }];
    
    
    CGRect fball1 = view.frame;
    fball1.size = CGSizeMake(20.0f, 15.0f);
    fball1.origin= CGPointMake(240.0f, 30.0f);
    UIImageView *iball1 = [[UIImageView alloc] initWithFrame:fball1];
    iball1.image = [UIImage imageNamed:@"ball_off"];
    iball1.hidden = YES;
    [view addSubview:iball1];
    
    CGRect fball2 = view.frame;
    fball2.size = CGSizeMake(20.0f, 15.0f);
    fball2.origin= CGPointMake(265.0f, 30.0f);
    UIImageView *iball2 = [[UIImageView alloc] initWithFrame:fball2];
    iball2.image = [UIImage imageNamed:@"ball_off"];
    iball2.hidden = YES;
    [view addSubview:iball2];
    
    CGRect fball3 = view.frame;
    fball3.size = CGSizeMake(20.0f, 15.0f);
    fball3.origin= CGPointMake(290.0f, 30.0f);
    UIImageView *iball3 = [[UIImageView alloc] initWithFrame:fball3];
    iball3.image = [UIImage imageNamed:@"ball_off"];
    iball3.hidden = YES;
    [view addSubview:iball3];
    
    CGRect fv = view.frame;
    fv.size = CGSizeMake(34.0f, 42.0f);
    fv.origin= CGPointMake(270.0f, 15.0f);
    UIImageView *iv = [[UIImageView alloc] initWithFrame:fv];
    iv.image = [UIImage imageNamed:@"v_badge"];
    iv.hidden = YES;
    [view addSubview:iv];
    iball1.hidden = NO;
    iball2.hidden = NO;
    iball3.hidden = NO;
    iball1.image = [UIImage imageNamed:@"ball_off"];
    iball2.image = [UIImage imageNamed:@"ball_off"];
    iball3.image = [UIImage imageNamed:@"ball_off"];
    iv.hidden = YES;
    
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
    [tap1 setCancelsTouchesInView:YES];
    iball1.userInteractionEnabled = YES;
    [iball1 addGestureRecognizer:tap1];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
    [tap2 setCancelsTouchesInView:YES];
    iball2.userInteractionEnabled = YES;
    [iball2 addGestureRecognizer:tap2];
    
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
    [tap3 setCancelsTouchesInView:YES];
    iball3.userInteractionEnabled = YES;
    [iball3 addGestureRecognizer:tap3];
    
    UITapGestureRecognizer *tap4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showDescView:)];
    [tap4 setCancelsTouchesInView:YES];
    iv.userInteractionEnabled = YES;
    [iv addGestureRecognizer:tap4];
    
    /*PFQuery *query = [PFQuery queryWithClassName:@"Score"];
    [query whereKey:@"players" equalTo:[PFUser currentUser]];
    [query whereKey:@"approved" equalTo:@YES];
    [query orderByDescending:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            return;
        }
        int ball = 0;
        if (objects.count <= 3 && objects.count >= 1) {
            ball = 1;
        }
        NSMutableArray *opponents = [[NSMutableArray alloc] init];
        for (int i = 0; i < objects.count; i++) {
            ScoreModel *score = (ScoreModel *)[objects objectAtIndex:i];
            for (int j = 0; j < score.players.count; j++) {
                PFUser *player = (PFUser *)[score.players objectAtIndex:j];
                if ([opponents containsObject:player.objectId]) {
                    continue;
                }
                if ([player.objectId isEqualToString:[PFUser currentUser].objectId]) {
                    continue;
                }
                [opponents addObject:player.objectId];
            }
        }
        if (objects.count >= 4 && objects.count <= 8 && opponents.count >= 3) {
            ball = 2;
        }
        if (objects.count >= 9 && objects.count <= 12 && opponents.count >= 4) {
            ball = 3;
        }
        if (objects.count >= 12 && opponents.count >= 5) {
            ball = 4;
        }
        if (ball < 4) {
            iball1.hidden = NO;
            iball2.hidden = NO;
            iball3.hidden = NO;
            iv.hidden = YES;
            if (ball == 0) {
                iball1.image = [UIImage imageNamed:@"ball_off"];
                iball2.image = [UIImage imageNamed:@"ball_off"];
                iball3.image = [UIImage imageNamed:@"ball_off"];
            }else if (ball == 1) {
                iball1.image = [UIImage imageNamed:@"ball_on"];
                iball2.image = [UIImage imageNamed:@"ball_off"];
                iball3.image = [UIImage imageNamed:@"ball_off"];
            }else if (ball == 2) {
                iball1.image = [UIImage imageNamed:@"ball_on"];
                iball2.image = [UIImage imageNamed:@"ball_on"];
                iball3.image = [UIImage imageNamed:@"ball_off"];
            }else {
                iball1.image = [UIImage imageNamed:@"ball_on"];
                iball2.image = [UIImage imageNamed:@"ball_on"];
                iball3.image = [UIImage imageNamed:@"ball_on"];
            }
        }else {
            iball1.hidden = YES;
            iball2.hidden = YES;
            iball3.hidden = YES;
            iv.hidden = NO;
        }
    }];*/
    
    /*CGRect fw = view.frame;
     fw.size = CGSizeMake(46.0f, 30.0f);
     fw.origin= CGPointMake(274.0f, 25.0f);
     UILabel *lw = [[UILabel alloc] initWithFrame:fw];
     lw.textColor = [UIColor whiteColor];
     lw.text = [@(self.week) stringValue];
     lw.font = [UIFont boldSystemFontOfSize:28.0f];
     lw.numberOfLines = 1;
     lw.textAlignment = NSTextAlignmentCenter;
     [view addSubview:lw];
     CGRect ftw = view.frame;
     ftw.size = CGSizeMake(46.0f, 25.0f);
     ftw.origin= CGPointMake(274.0f, 50.0f);
     UILabel *ltw = [[UILabel alloc] initWithFrame:ftw];
     ltw.textColor = [UIColor colorWithWhite:0.8f alpha:1.0f];
     ltw.text = @"Week";
     ltw.textAlignment = NSTextAlignmentCenter;
     ltw.font = [UIFont boldSystemFontOfSize:12.0f];
     ltw.numberOfLines = 1;
     [view addSubview:ltw];
     
     PFQuery *query = [PFQuery queryWithClassName:@"Points"];
     [query whereKey:@"user" equalTo:[PFUser currentUser]];
     [query whereKey:@"createdAt" greaterThanOrEqualTo:month];
     [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
     int thisMonth = 0;
     int thisWeek = 0;
     NSDate *week = [NSDate dateWithTimeIntervalSinceNow:(-60*60*24*7)];
     for(PointsModel *points in objects){
     if([points.createdAt isLaterThanOrEqualTo:week]){
     thisWeek += points.total;
     }
     thisMonth += points.total;
     }
     dispatch_async(dispatch_get_main_queue(), ^{
     self.month = thisMonth;
     self.week = thisWeek;
     lm.text = [@(self.month) stringValue];
     lw.text = [@(self.week) stringValue];
     });
     
     }];*/
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height-2.0f, view.frame.size.width, 2.0f);
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.2f alpha:1.0f].CGColor;
    [view.layer addSublayer:bottomBorder];
    
    return view;
}

#pragma mark - IBAction feed cells

- (IBAction)play:(id)sender {
    // go to match request page
    [self performSegueWithIdentifier:@"play" sender:self];
}

- (IBAction)home:(id)sender {
    // go back to home page
    [self updateShowStart:NO];
    self.activity = nil;
    [self.homeDelegate showView];
    [self dismissViewControllerAnimated:NO completion:^{
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"play"]) {
        UINavigationController *nav = [segue destinationViewController];
        PlayViewController *play = [nav.viewControllers firstObject];
        play.activity = self.activity;
        play.activity.type = @"play";
        play.editing = NO;
        play.fromStart = YES;
        play.startPageDelegate = self;
        [self.view setHidden:YES];
        [self.homeDelegate showView];
    }else if ([segue.identifier isEqualToString:@"description"]) {
        DescriptionViewController *descriptionViewController = (DescriptionViewController *)segue.destinationViewController;
        if ([sender isKindOfClass:[UIButton class]]) {
            descriptionViewController.imgBg = [UIImage imageNamed:@"score_desc"];
        }else {
            descriptionViewController.imgBg = [UIImage imageNamed:@"badge_desc"];
        }
    }
}

-(void)showDescView:(UIGestureRecognizer *)gesture
{
    [self performSegueWithIdentifier:@"description" sender:gesture.view];
}

-(void)showHelp:(UIButton *)sender
{
    [self performSegueWithIdentifier:@"description" sender:sender];
}

- (void)updateShowStart:(BOOL)showStart {
    // set the start switch
    [[NSUserDefaults standardUserDefaults] willChangeValueForKey:[PFUser currentUser].username];
    [[NSUserDefaults standardUserDefaults] setBool:showStart forKey:[PFUser currentUser].username];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)matchRequested:(BOOL)requested {
//    NSLog(@"MATCH REQUESTED");
    self.activity = nil;
    [self dismissViewControllerAnimated:NO completion:^{
        
    }];
}

- (void)hideStartPage:(BOOL)show {
    [self.view setHidden:show];
}

@end
