//
//  MessageViewCell.h
//  wave
//
//  Created by Kevin Flynn on 8/26/14.
//  Copyright (c) 2014 Kevin Flynn. All rights reserved.
//

#import <ParseUI/ParseUI.h>

@interface MessageViewCell : PFTableViewCell

@property (nonatomic, strong) UILabel *senderLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *commentLabel;
@property (nonatomic, strong) PFImageView *avatarView;

@end
