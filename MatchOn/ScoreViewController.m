//
//  ScoreViewController.m
//  MatchOn
//
//  Created by Kevin Flynn on 12/4/14.
//  Copyright (c) 2014 Wave Labs, Inc. All rights reserved.
//

#import "ScoreViewController.h"
#import "MBProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "PointPopupViewController.h"
#import <ParseUI/ParseUI.h>
#import "Constants.h"
#import "Localytics.h"
#import "UIViewController+MJPopupViewController.h"
#import "HitPopupViewController.h"

@interface ScoreViewController () <UITextFieldDelegate, MFMailComposeViewControllerDelegate,PointPopupViewControllerDelegate,HitPopupViewControllerDelegate>

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTask;

@end

@implementation ScoreViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.xButton.layer.cornerRadius = self.xButton.frame.size.width/2.0f;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"Will appear");
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"matchon_logo_header"]];
    int a = 1;
    for(UIView *view in self.firstTeamContainer.subviews) {
        UITextField *text = (UITextField *)view;
        text.tag = a;
        a++;
        text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        text.returnKeyType = UIReturnKeyDone;
        text.delegate = self;
        if([self.action isEqualToString:@"view"]) {
            text.enabled = NO;
        }
    }
    int b = 1;
    for(UIView *view in self.secondTeamContainer.subviews) {
        UITextField *text = (UITextField *)view;
        text.tag = b;
        b++;
        text.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        text.returnKeyType = UIReturnKeyDone;
        text.delegate = self;
        if([self.action isEqualToString:@"view"]) {
            text.enabled = NO;
        }
    }
    
    if(self.activity && self.activity.score) {
        self.btnJustHit.hidden = YES;
        self.firstTeam = self.activity.firstTeam;
        self.secondTeam = self.activity.secondTeam;
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 80.0f, self.navigationController.view.frame.size.height)];
        NSLog(@"%@",self.activity);
        [self.activity.scorer fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            title.text = [object objectForKey:@"name"];
        }];
        title.text = @"";
        title.textAlignment = NSTextAlignmentCenter;
        title.textColor = [UIColor whiteColor];
        //self.navigationItem.titleView = title;
        [self.navigationController.navigationBar setTranslucent:NO];
        
        self.navigationController.navigationBarHidden = NO;
        [self.navigationController.navigationBar setShadowImage:nil];
        
        ScoreModel *score = (ScoreModel *)self.activity.score;
        [score fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            ScoreModel *score = (ScoreModel *)object;
            for(NSNumber *round in score.firstScore) {
                UILabel *label = (UILabel *)[self.firstTeamContainer viewWithTag:[round intValue]];
                label.text = [score.firstScore objectForKey:round];
            }
            for (NSNumber *round in score.secondScore) {
                UILabel *label = (UILabel *)[self.secondTeamContainer viewWithTag:[round intValue]];
                label.text = [score.secondScore objectForKey:round];
            }
        }];
        
        if([self.action isEqualToString:@"pending"]) {
            self.approveButton.hidden = YES;
            self.approveButton.enabled = NO;
            self.rejectButton.enabled = NO;
            self.rejectButton.hidden = YES;
        } else if([self.action isEqualToString:@"view"]){
            self.approveButton.hidden = YES;
            self.approveButton.enabled = NO;
            self.rejectButton.enabled = NO;
            self.rejectButton.hidden = YES;
            self.navigationItem.rightBarButtonItem = nil;
        } else {
            self.approveButton.hidden = NO;
            self.approveButton.enabled = YES;
            self.rejectButton.enabled = YES;
            self.rejectButton.hidden = NO;
            self.navigationItem.rightBarButtonItem = nil;
        }
        
    } else {
        self.approveButton.hidden = YES;
        self.approveButton.enabled = NO;
        self.rejectButton.enabled = NO;
        self.rejectButton.hidden = YES;
        self.btnJustHit.hidden = NO;
        
        self.firstTeam = [[NSMutableArray alloc] initWithArray:@[[PFUser currentUser]]];
        self.secondTeam = [NSMutableArray array];
        
        for(PFUser *user in self.activity.recipients){
            
            if([user.objectId isEqualToString:[PFUser currentUser].objectId]){
                continue;
            }
            if(self.teammate && [user.objectId isEqualToString:self.teammate.objectId]){
                [self.firstTeam addObject:user];
            } else {
                [self.secondTeam addObject:user];
            }
        }
        
    }
    NSMutableArray *firstNames = [NSMutableArray array];
    NSMutableArray *secondNames = [NSMutableArray array];
    for(PFUser *user in self.firstTeam) {
        NSString *first = [user objectForKey:@"first"];
        NSString *last = [user objectForKey:@"last"];
        if(!first) {
            NSArray *name = [user[@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        NSString *full = first;
        if(last && last.length > 0) {
            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
        }
        
        [firstNames addObject:full];
    }
    for(PFUser *user in self.secondTeam) {
        NSString *first = [user objectForKey:@"first"];
        NSString *last = [user objectForKey:@"last"];
        if(!first) {
            NSArray *name = [user[@"name"] componentsSeparatedByString:@" "];
            first = [name firstObject];
            if(!last) {
                if(name.count > 1) {
                    last = [name lastObject];
                }
            }
        }
        NSString *full = first;
        if(last && last.length > 0) {
            full = [NSString stringWithFormat:@"%@ %@.",full, [last substringToIndex:1]];
        }
        [secondNames addObject:full];
    }
    if (self.firstTeam.count > 1) {
        PFUser *firstMan = (PFUser *)[self.firstTeam objectAtIndex:0];
        if (![firstMan.objectId isEqualToString:[PFUser currentUser].objectId]) {
            [self.firstTeam setObject:[self.firstTeam objectAtIndex:1] atIndexedSubscript:0];
            [self.firstTeam setObject:firstMan atIndexedSubscript:1];
        }
    }
    for (int i = 0; i < self.firstTeam.count; i++) {
        PFUser *teamUser = [self.firstTeam objectAtIndex:i];
        UIView *userView = [self.view viewWithTag:i + 101];
        userView.hidden = NO;
        PFImageView *imgPhoto = (PFImageView *)[userView viewWithTag:(i + 1) * 10];
        PFFile *file = [teamUser objectForKey:@"image"];
        imgPhoto.file = file;
        /*if([teamUser[@"female"] boolValue]) {
            imgPhoto.image = [UIImage imageNamed:@"female"];
        } else {
            imgPhoto.image = [UIImage imageNamed:@"male"];
        }*/
        imgPhoto.image = [UIImage imageNamed:@"default_profile"];
        //imgPhoto.image = [UIImage imageNamed:@"appIcon"];
        [imgPhoto loadInBackground];
        imgPhoto.layer.cornerRadius = imgPhoto.frame.size.height/2.0f;
        imgPhoto.clipsToBounds = YES;
        imgPhoto.layer.borderColor = [UIColor darkGrayColor].CGColor;
        imgPhoto.layer.borderWidth = 2.0f;
        imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
        
        UILabel *name = (UILabel *)[userView viewWithTag:(i + 1)*11];
        if(name) {
            
            NSString *first = [teamUser objectForKey:@"first"];
            NSString *last = [teamUser objectForKey:@"last"];
            
            if(!first) {
                NSArray *name = [teamUser[@"name"] componentsSeparatedByString:@" "];
                first = [name firstObject];
                if(!last) {
                    if(name.count > 1) {
                        last = [name lastObject];
                    }
                }
            }
            name.text = first;
            if(last && last.length > 0) {
                name.text = [NSString stringWithFormat:@"%@ %@.",name.text, [last substringToIndex:1]];
            }
            
        }
        UILabel *host = (UILabel *)[userView viewWithTag:(i + 1)*12];
        if([teamUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
            host.text = @"HOST";
            if(imgPhoto) {
                imgPhoto.layer.borderColor = THEME_COLOR.CGColor;
            }
        } else {
            host.text = @"";
        }
    }
    
    for (int i = 0; i < self.secondTeam.count; i++) {
        PFUser *teamUser = [self.secondTeam objectAtIndex:i];
        UIView *userView = [self.view viewWithTag:i + 103];
        userView.hidden = NO;
        PFImageView *imgPhoto = (PFImageView *)[userView viewWithTag:(i + 3) * 10];
        PFFile *file = [teamUser objectForKey:@"image"];
        imgPhoto.file = file;
        /*if([teamUser[@"female"] boolValue]) {
            imgPhoto.image = [UIImage imageNamed:@"female"];
        } else {
            imgPhoto.image = [UIImage imageNamed:@"male"];
        }*/
        imgPhoto.image = [UIImage imageNamed:@"default_profile"];
        //imgPhoto.image = [UIImage imageNamed:@"appIcon"];
        [imgPhoto loadInBackground];
        imgPhoto.layer.cornerRadius = imgPhoto.frame.size.height/2.0f;
        imgPhoto.clipsToBounds = YES;
        imgPhoto.layer.borderColor = [UIColor darkGrayColor].CGColor;
        imgPhoto.layer.borderWidth = 2.0f;
        imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
        
        UILabel *name = (UILabel *)[userView viewWithTag:(i + 3)*11];
        if(name) {
            
            NSString *first = [teamUser objectForKey:@"first"];
            NSString *last = [teamUser objectForKey:@"last"];
            
            if(!first) {
                NSArray *name = [teamUser[@"name"] componentsSeparatedByString:@" "];
                first = [name firstObject];
                if(!last) {
                    if(name.count > 1) {
                        last = [name lastObject];
                    }
                }
            }
            name.text = first;
            if(last && last.length > 0) {
                name.text = [NSString stringWithFormat:@"%@ %@.",name.text, [last substringToIndex:1]];
            }
            
        }
        UILabel *host = (UILabel *)[userView viewWithTag:(i + 3)*12];
        if([teamUser.objectId isEqualToString:[PFUser currentUser].objectId]) {
            host.text = @"HOST";
            if(imgPhoto) {
                imgPhoto.layer.borderColor = THEME_COLOR.CGColor;
            }
        } else {
            host.text = @"";
        }
    }
    
    self.firstTeamLabel.text = [[self.firstTeam valueForKey:@"name"] componentsJoinedByString:@" and "];
    self.secondTeamLabel.text = [[self.secondTeam valueForKey:@"name"] componentsJoinedByString:@" and "];
    
    if(![self.action isEqualToString:@"view"] && ![self.action isEqualToString:@"review"]) {
        UITextField *text = (UITextField *)self.firstTeamContainer.subviews[0];
        [text becomeFirstResponder];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

}


#pragma mark - Text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([self.action isEqualToString:@"review"]) {
        self.approveButton.hidden = YES;
        self.approveButton.enabled = NO;
        self.rejectButton.enabled = NO;
        self.rejectButton.hidden = YES;
        UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(save:)];
        self.navigationItem.rightBarButtonItem = saveButton;
    }
    NSScanner *scanner = [NSScanner scannerWithString:string];
    BOOL isNumeric = [scanner scanInteger:NULL] && [scanner isAtEnd];
    if(!isNumeric && string.length > 0) {
        return NO;
    }
    if(textField.text.length > 1 && string.length > 0) {
        return NO;
    }
    UIView *parent = [textField superview];
    if(parent == self.firstTeamContainer) {
        // Team A - score Team B
        UITextField *next = (UITextField *)[self.secondTeamContainer viewWithTag:textField.tag];
        if(next) {
            NSLog(@"Next Text %@",next.text);
            [next becomeFirstResponder];
        }
        
    } else {
        // Team B - Move up to Team A
        UITextField *next = (UITextField *)[self.firstTeamContainer viewWithTag:textField.tag+1];
        if(next) {
            NSLog(@"Next Text %@",next.text);
            [next becomeFirstResponder];
        } else {
            [textField resignFirstResponder];
        }
    }
    textField.text = string;
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}




#pragma mark - IBActions

- (IBAction)save:(id)sender {
    
    NSMutableDictionary *firstScore = [NSMutableDictionary dictionary];
    NSMutableDictionary *secondScore = [NSMutableDictionary dictionary];
    for (int i=0;i<self.firstTeamContainer.subviews.count;i++) {
        UITextField *text = (UITextField *)self.firstTeamContainer.subviews[i];

        if(text.text.length > 0) {
            [firstScore setObject:text.text forKey:[[NSNumber numberWithInt:i+1] stringValue]];
        }
    }
    for (int i=0;i<self.secondTeamContainer.subviews.count;i++) {
        UITextField *text = (UITextField *)self.secondTeamContainer.subviews[i];
        if(text.text.length > 0) {
            [secondScore setObject:text.text forKey:[[NSNumber numberWithInt:i+1] stringValue]];
        }
    }
    
    if(firstScore.count == 0 || firstScore.count == 0){
        // Scores dont equal
        NSLog(@"Error first %i",(int)firstScore.count);
        NSLog(@"Error second %i",(int)secondScore.count);
        return;
    }
    
    int firstTotal = 0;
    int secondTotal = 0;
    for(NSNumber *round in firstScore){
        NSNumber *first = [firstScore objectForKey:round];
        NSNumber *second = [secondScore objectForKey:round];
        
        if(!second) {
            return;
        }

        if([first intValue]>[second intValue]){
            firstTotal ++;
        } else if([second intValue]>[first intValue]) {
            secondTotal ++;
        }
    }
    
    NSMutableArray *winner = [NSMutableArray array];
    NSMutableArray *loser = [NSMutableArray array];
    
    if(firstTotal > secondTotal) {
        winner = self.firstTeam;
        loser = self.secondTeam;
    } else if (secondTotal > firstTotal) {
        winner = self.secondTeam;
        loser = self.firstTeam;

    }
    
    [self.view endEditing:TRUE];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
   
    
    ScoreModel *score = (ScoreModel *)self.activity.score;
    BOOL edited = TRUE;
    if(!self.activity.score) {
        edited = FALSE;
        score = [ScoreModel object];
    }
    [score createScoreForFirstScore:firstScore andSecondScore:secondScore withFirstTeam:self.firstTeam andSecondTeam:self.secondTeam andWinner:winner andLoser:loser withActivity:self.activity];
    
    [score saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        self.activity.scorer = [PFUser currentUser];
        //self.activity.played = TRUE;
        self.activity.score = score;
        self.activity.approved = FALSE;
        //self.activity.reviewer = NULL;
        //self.activity.reviewers = score.reviewers;
        //self.activity.scorers = score.scorers;
        self.activity.firstTeam = score.firstTeam;
        self.activity.secondTeam = score.secondTeam;
        
        [self.activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            PFQuery *pushQuery = [PFInstallation query];
            
            NSMutableArray *recipients = [NSMutableArray array];
            for(PFUser *user in self.activity.recipients){
                if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
                [recipients addObject:user];
            }
            
            if(recipients.count > 0) {
                NSLog(@"Send push");
                [pushQuery whereKey:@"user" containedIn:recipients];
                NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ submitted a score for your match", [PFUser currentUser][@"name"]], @"badge" : @"Increment", @"a":self.activity.objectId, @"s": score.objectId};
                if(edited) {
                    data = @{@"alert": [NSString stringWithFormat:@"%@ edited a score for your match", [PFUser currentUser][@"name"]], @"badge" : @"Increment", @"a":self.activity.objectId, @"s": score.objectId};
                }
                
                PFPush *push = [[PFPush alloc] init];
                [push setQuery:pushQuery];
                [push setData:data];
                [push sendPushInBackground];
            }
            
            [Localytics tagEvent:@"Score Submitted"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"Scored" object:self];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                self.feedbackView.hidden = FALSE;
            });
        }];
    }];
}

- (IBAction)approve:(id)sender {
    //self.activity.reviewer = [PFUser currentUser];
    ScoreModel *old = (ScoreModel *)self.activity.score;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    
    
    // Request a background execution task to allow us to finish uploading the photo even if the app is backgrounded
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
    }];
    
    [old fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        NSLog(@"Fetched %@",object);
        ScoreModel *score = (ScoreModel *)object;
        //score.reviewer = [PFUser currentUser]; // self.activity.reviewer;
        score.approved = TRUE;
        self.activity.approved = TRUE;
        [self.activity saveInBackground];
        
        PFUser *currentUser = [PFUser currentUser];
        NSMutableArray *players = [currentUser objectForKey:@"players"];
        if (players == nil) {
            players = [[NSMutableArray alloc] init];
        }
        NSMutableArray *playerIds = [[NSMutableArray alloc] init];
        for (int j = 0; j < players.count; j++) {
            PFUser *player = (PFUser *)[players objectAtIndex:j];
            [playerIds addObject:player.objectId];
        }
        if (![playerIds containsObject:self.activity.user.objectId] && ![self.activity.user.objectId isEqualToString:currentUser.objectId]) {
            [players addObject:self.activity.user];
            [playerIds addObject:self.activity.user.objectId];
        }
        for (int j = 0; j < self.activity.recipients.count; j++) {
            PFUser *tempUser = (PFUser *)[self.activity.recipients objectAtIndex:j];
            if ([tempUser.objectId isEqualToString:currentUser.objectId]) {
                continue;
            }
            if (![playerIds containsObject:tempUser.objectId]) {
                [players addObject:tempUser];
                [playerIds addObject:tempUser.objectId];
            }
        }
        if (players != nil) {
            [currentUser setObject:players forKey:@"players"];
            [currentUser saveInBackground];
        }
     
        [score saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            NSLog(@"Score: %@",score);
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            PFQuery *pushQuery = [PFInstallation query];
            
            NSMutableArray *recipients = [NSMutableArray array];
            for(PFUser *user in self.activity.recipients){
                if([user.objectId isEqualToString:[PFUser currentUser].objectId])continue;
                [recipients addObject:user];
            }
            NSLog(@"Push recipients: %@",recipients);
            
            
            
            if(recipients.count > 0) {
                [pushQuery whereKey:@"user" containedIn:recipients];
                NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ approved your score!", [PFUser currentUser][@"name"]], @"badge" : @"Increment", @"s": score.objectId};
                PFPush *push = [[PFPush alloc] init];
                [push setQuery:pushQuery];
                [push setData:data];
                [push sendPushInBackground];
            }
            
            NSMutableArray *ratings = [NSMutableArray array];
            for(PFUser *user in self.activity.recipients) {
                [ratings addObject:user[@"rating"]];
            }
            NSMutableArray *scores = [NSMutableArray array];
            [RatingModel fetchAllInBackground:ratings block:^(NSArray *objects, NSError *error) {
                /*double firstRating = 0, secondRating = 0;
                NSMutableArray *firstTeam = score.firstTeam;
                NSMutableArray *secondTeam = score.secondTeam;
                NSMutableDictionary *firstScore = score.firstScore;
                NSMutableDictionary *secondScore = score.secondScore;
                BOOL firstWon = 0, firstLost = 0;
                for (int i = 0; i < 5; i++) {
                    int firstTeamScore = 0;
                    if ([firstScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]]) {
                        firstTeamScore = [[firstScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                    }
                    int secondTeamScore = 0;
                    if ([secondScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]]) {
                        secondTeamScore = [[secondScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                    }
                    if (firstTeamScore > secondTeamScore) {
                        firstWon++;
                    }else if (firstTeamScore < secondTeamScore) {
                        firstLost++;
                    }
                    
                }
                for (RatingModel *rating in objects) {
                    for (PFUser *firstUser in firstTeam) {
                        if ([firstUser.objectId isEqualToString:rating.user.objectId]) {
                            firstRating += rating.rating;
                            break;
                        }
                    }
                    for (PFUser *secondUser in secondTeam) {
                        if ([secondUser.objectId isEqualToString:rating.user.objectId]) {
                            secondRating += rating.rating;
                            break;
                        }
                    }
                }
                BOOL bEnabled = YES;
                firstRating /= firstTeam.count;
                secondRating /= secondTeam.count;
                if (fabs(firstRating - secondRating) >= 10 && ((firstRating > secondRating && firstWon < firstLost) || (firstRating < secondRating && firstWon > firstLost))) {
                    bEnabled = NO;
                }*/
                int iuser = 0;
                NSMutableArray *updatedRatings = [[NSMutableArray alloc] init];
                /*NSMutableArray *temp_ratings = [NSMutableArray arrayWithArray:objects];
                NSMutableArray *calculations = [NSMutableArray array];*/
                for(RatingModel *rating in objects) {
                    if(!rating.score) {
                        rating.score = 0;
                    }
                    /*if (!rating.confidence_level) {
                        rating.confidence_level = 2;
                    }*/
                    int points = 1;
                    for(PFUser *user in score.winner){
                        if([rating.user.objectId isEqualToString:user.objectId]) {
                            // On the winning team - add a point
                            points += 1;
                            NSLog(@"Add point for winner!");
                        }
                    }
                    
                    NSArray *userF = [[self.activity.recipients objectAtIndex:iuser] objectForKey:@"players"];
                    BOOL bNew = YES;
                    if (userF != nil) {
                        int juser = 0;
                        for (RatingModel *rating1 in objects) {
                            NSArray *userF1 = [[self.activity.recipients objectAtIndex:juser] objectForKey:@"players"];
                            if (![rating.user.objectId isEqualToString:rating1.user.objectId]) {
                                
                                for (int i = 0; i < userF.count; i++) {
                                    PFUser *player = (PFUser *)[userF objectAtIndex:i];
                                    if ([player.objectId isEqualToString:rating1.user.objectId]) {
                                        bNew = NO;
                                        break;
                                    }
                                }
                                if (!bNew) {
                                    break;
                                }
                                for (int i = 0; i < userF1.count; i++) {
                                    PFUser *player = (PFUser *)[userF1 objectAtIndex:i];
                                    if ([player.objectId isEqualToString:rating.user.objectId]) {
                                        bNew = NO;
                                        break;
                                    }
                                }
                                if (!bNew) {
                                    break;
                                }
                            }
                            juser++;
                        }
                    }
                    /*if([rating.user.objectId isEqualToString:self.activity.user.objectId]) {
                        // Match creator
                        points += 1;
                        NSLog(@"Add point for creator!");
                    }*/
                    if (bNew) {
                        points += 1;
                    }
                    NSLog(@"points: %i",points);
                    rating.score += points;
                    //rating.curpoint = rating.score;
                    PointsModel *pointsModel = [PointsModel object];
                    pointsModel.user = rating.user;
                    pointsModel.total = points;
                    pointsModel.activity = self.activity;
                    [scores addObject:pointsModel];
                    
                    /*if (bEnabled) {
                        //Calculate New Ratings
                        NSArray *firstTeam = score.firstTeam;
                        NSMutableDictionary *firstScore = score.firstScore;
                        NSArray *secondTeam = score.secondTeam;
                        NSMutableDictionary *secondScore = score.secondScore;
                        NSMutableArray *homeTeam = [NSMutableArray array];
                        NSMutableDictionary *homeScore = [NSMutableDictionary dictionary];
                        NSMutableArray *awayTeam = [NSMutableArray array];
                        NSMutableDictionary *awayScore = [NSMutableDictionary dictionary];
                        for (int i = 0; i < firstTeam.count; i++) {
                            PFUser *curUser = (PFUser *)[firstTeam objectAtIndex:i];
                            if ([curUser.objectId isEqualToString:rating.user.objectId]) {
                                homeTeam = [NSMutableArray arrayWithArray:firstTeam];
                                homeScore = [NSMutableDictionary dictionaryWithDictionary:firstScore];
                                awayTeam = [NSMutableArray arrayWithArray:secondTeam];
                                awayScore = [NSMutableDictionary dictionaryWithDictionary:secondScore];
                                break;
                            }
                        }
                        for (int i = 0; i < secondTeam.count; i++) {
                            PFUser *curUser = (PFUser *)[secondTeam objectAtIndex:i];
                            if ([curUser.objectId isEqualToString:rating.user.objectId]) {
                                homeTeam = [NSMutableArray arrayWithArray:secondTeam];
                                homeScore = [NSMutableDictionary dictionaryWithDictionary:secondScore];
                                awayTeam = [NSMutableArray arrayWithArray:firstTeam];
                                awayScore = [NSMutableDictionary dictionaryWithDictionary:firstScore];
                                break;
                            }
                        }
                        int sets_played = (int)homeScore.allKeys.count;
                        int sets_won = 0;
                        double avg_won = 0;
                        double avg_lost = 0;
                        for (int i = 0; i < 5; i++) {
                            if ([homeScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] == nil) {
                                continue;
                            }
                            int home_score = [[homeScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                            avg_won += home_score;
                            if ([awayScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] == nil) {
                                sets_won++;
                                continue;
                            }
                            int away_score = [[awayScore objectForKey:[NSString stringWithFormat:@"%d",i + 1]] intValue];
                            avg_lost += away_score;
                            if (home_score > away_score) {
                                sets_won++;
                            }
                        }
                        if (sets_played > 0) {
                            avg_won /= sets_played;
                            avg_lost /= sets_played;
                        }else {
                            avg_won = 0;
                            avg_lost = 0;
                        }
                        double match_weight = 0;
                        for (RatingModel *rating1 in temp_ratings) {
                            if ([rating.user.objectId isEqualToString:rating1.user.objectId]) {
                                continue;
                            }
                            if (!rating1.confidence_level) {
                                match_weight += 2;
                            }else {
                                match_weight += rating1.confidence_level;
                            }
                        }
                        match_weight /= (ratings.count - 1);
                        if (rating.confidence_level) {
                            match_weight /= rating.confidence_level;
                        }else {
                            match_weight /= 2;
                        }
                        
                        match_weight *= (double)sets_played / 2;
                        [calculations addObject:@{@"match_weight":@(match_weight),
                                                  @"avg_won":@(avg_won),
                                                  @"avg_lost":@(avg_lost),
                                                  @"sets_won":@(sets_won),
                                                  @"sets_played":@(sets_played)}];
                    }*/
                    [updatedRatings addObject:rating];
                    iuser++;
                }
                /*if (bEnabled) {
                    NSLog(@"%@",calculations);
                    for (int i = 0; i < updatedRatings.count; i++) {
                        RatingModel *rating = (RatingModel *)[updatedRatings objectAtIndex:i];
                        NSArray *firstTeam = score.firstTeam;
                        NSMutableDictionary *firstScore = score.firstScore;
                        NSArray *secondTeam = score.secondTeam;
                        NSMutableDictionary *secondScore = score.secondScore;
                        NSMutableArray *awayTeam = [NSMutableArray array];
                        NSMutableDictionary *awayScore = [NSMutableDictionary dictionary];
                        for (int j = 0; j < firstTeam.count; j++) {
                            PFUser *curUser = (PFUser *)[firstTeam objectAtIndex:j];
                            if ([curUser.objectId isEqualToString:rating.user.objectId]) {
                                awayTeam = [NSMutableArray arrayWithArray:secondTeam];
                                awayScore = [NSMutableDictionary dictionaryWithDictionary:secondScore];
                                break;
                            }
                        }
                        for (int j = 0; j < secondTeam.count; j++) {
                            PFUser *curUser = (PFUser *)[secondTeam objectAtIndex:j];
                            if ([curUser.objectId isEqualToString:rating.user.objectId]) {
                                awayTeam = [NSMutableArray arrayWithArray:firstTeam];
                                awayScore = [NSMutableDictionary dictionaryWithDictionary:firstScore];
                                break;
                            }
                        }
                        NSMutableDictionary *calculation = [NSMutableDictionary dictionaryWithDictionary:[calculations objectAtIndex:i]];
                        int homeWon = [[calculation objectForKey:@"sets_won"] intValue];
                        int awayWon = 0;
                        for (int j = 0; j < updatedRatings.count; j++) {
                            if (i == j) {
                                continue;
                            }
                            RatingModel *ratingj = (RatingModel *)[updatedRatings objectAtIndex:j];
                            int k = 0;
                            for (k = 0; k < awayTeam.count; k++) {
                                PFUser *curUser = (PFUser *)[awayTeam objectAtIndex:k];
                                if ([curUser.objectId isEqualToString:ratingj.user.objectId]) {
                                    break;
                                }
                            }
                            if (k < awayTeam.count) {
                                awayWon = [[[calculations objectAtIndex:j] objectForKey:@"sets_won"] intValue];
                                break;
                            }
                        }
                        double played_like = 0;
                        if (self.activity.doubles) {
                            played_like = rating.rating - ([[calculation objectForKey:@"avg_won"] doubleValue] - [[calculation objectForKey:@"avg_lost"] doubleValue]) * 2 - (homeWon - awayWon) * 0.5;
                        }else {
                            RatingModel *otherRating;
                            for (int j = 0; j < updatedRatings.count; j++) {
                                RatingModel *jRating = (RatingModel *)[updatedRatings objectAtIndex:j];
                                if ([rating.objectId isEqualToString:jRating.objectId]) {
                                    if (j == 0) {
                                        otherRating = (RatingModel *)[updatedRatings objectAtIndex:1];
                                    }else {
                                        otherRating = (RatingModel *)[updatedRatings objectAtIndex:0];
                                    }
                                    break;
                                }
                            }
                            if (otherRating != nil) {
                                played_like = otherRating.rating - ([[calculation objectForKey:@"avg_won"] doubleValue] - [[calculation objectForKey:@"avg_lost"] doubleValue]) * 2 - (homeWon - awayWon) * 0.5;
                            }
                        }
                        [calculation setObject:@(played_like) forKey:@"played_like"];
                        [calculations setObject:calculation atIndexedSubscript:i];
                    }
                    NSLog(@"%@",calculations);
                    for (int i = 0; i < updatedRatings.count; i++) {
                        RatingModel *rating = (RatingModel *)[updatedRatings objectAtIndex:i];
                        NSMutableDictionary *calculation = [calculations objectAtIndex:i];
                        double played_like = [[calculation objectForKey:@"played_like"] doubleValue];
                        double match_weight = [[calculation objectForKey:@"match_weight"] doubleValue];
                        double newRating = 0;
                        if (rating.confidence_level) {
                            newRating = (rating.rating * rating.confidence_level + played_like * match_weight) / (rating.confidence_level + match_weight);
                        }else {
                            newRating = (rating.rating * 2 + played_like * match_weight) / (2 + match_weight);
                        }
                        if (newRating > 80) {
                            newRating = 80;
                        }
                        rating.rating = newRating;
                        
                        double avg_weight = 0;
                        for (RatingModel *rating1 in temp_ratings) {
                            if ([rating.user.objectId isEqualToString:rating1.user.objectId]) {
                                continue;
                            }
                            if (!rating1.confidence_level) {
                                avg_weight += 2;
                            }else {
                                avg_weight += rating1.confidence_level;
                            }
                        }
                        avg_weight /= (ratings.count - 1);
                        double newLevel = 0;
                        if (!rating.confidence_level) {
                            newLevel = rating.confidence_level + avg_weight / 10;
                        }else {
                            newLevel = 2 + avg_weight / 10;
                        }
                        
                        if (newLevel > 9.999) {
                            newLevel = 10;
                        }
                        rating.confidence_level = newLevel;
                        [updatedRatings setObject:rating atIndexedSubscript:i];
                    }
                }*/
                [PointsModel saveAllInBackground:scores block:^(BOOL succeeded, NSError *error) {
                    NSLog(@"Scores: %@",scores);
                    [RatingModel saveAllInBackground:updatedRatings
                                               block:^(BOOL succeeded, NSError *error) {
                                                   // End BG task
                                                   [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTask];
                                               }];
                    
                }];
                
            }];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PointPopupViewController *pointPopup = (PointPopupViewController *)[storyboard instantiateViewControllerWithIdentifier:@"PointPopup"];
            pointPopup.parent = self;
            pointPopup.delegate = self;
            CGRect frame = pointPopup.view.frame;
            frame.size.height = 360;
            frame.size.width = [UIScreen mainScreen].bounds.size.width - 40;
            pointPopup.view.frame = frame;
            [self presentPopupViewController:pointPopup animationType:MJPopupViewAnimationSlideBottomTop];
            
        }];
    }];
    
}

- (IBAction)back:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];

}

- (IBAction)feedback:(id)sender {
    [self showMail];
}

- (IBAction)justhit:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HitPopupViewController *hitPopup = (HitPopupViewController *)[storyboard instantiateViewControllerWithIdentifier:@"HitPopup"];
    hitPopup.parent = self;
    hitPopup.delegate = self;
    CGRect frame = hitPopup.view.frame;
    frame.size.height = 180;
    frame.size.width = [UIScreen mainScreen].bounds.size.width - 40;
    hitPopup.view.frame = frame;
    [self presentPopupViewController:hitPopup animationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)justHit
{
    if (self.activity) {
        //self.activity.matchOn = NO;
        self.activity.full = NO;
        [self.activity saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
    }
}

- (IBAction)reject:(id)sender {
    //self.activity.reviewer = [PFUser currentUser];
    
    ScoreModel *old = (ScoreModel *)self.activity.score;
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.margin = 20.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [old fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        ScoreModel *score = (ScoreModel *)object;
        //score.reviewer = [PFUser currentUser];//self.activity.reviewer;
        score.approved = FALSE;
        self.activity.approved = FALSE;
        [self.activity saveInBackground];
        [score saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            PFQuery *pushQuery = [PFInstallation query];
            [pushQuery whereKey:@"user" equalTo:score.scorer];

            NSDictionary *data = @{@"alert": [NSString stringWithFormat:@"%@ rejected your score.", [PFUser currentUser][@"name"]], @"badge" : @"Increment", @"s": score.objectId};
            PFPush *push = [[PFPush alloc] init];
            [push setQuery:pushQuery];
            [push setData:data];
            [push sendPushInBackground];
            
            [self.navigationController popViewControllerAnimated:YES];
        }];
    }];
}



#pragma mark - Mail delegate

- (void)showMail{
    NSDictionary *dimensions = @{ @"type": @"mail",
                                  @"objectId": [PFUser currentUser].objectId};
    // Send the dimensions to Parse along with the 'search' event
    [PFAnalytics trackEvent:@"share" dimensions:dimensions];

    self.mailViewController = [[MFMailComposeViewController alloc] init];
    self.mailViewController.mailComposeDelegate = self;
    [self.mailViewController setToRecipients:@[@"feedback@matchon.co"]];
    [self.mailViewController setSubject:@"MatchOn Feedback"];
    
    NSString *messageBody = @"";

    [self.mailViewController setMessageBody:messageBody isHTML:YES];
    
    [self presentViewController:self.mailViewController animated:YES completion:NULL];
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


-(void)dismissPointPopup
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
