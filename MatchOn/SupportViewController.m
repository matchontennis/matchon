//
//  SupportViewController.m
//  Page
//
//  Created by Kevin Flynn on 1/2/15.
//  Copyright (c) 2015 Wave Labs, Inc. All rights reserved.
//

#import "SupportViewController.h"
#import "Constants.h"

@interface SupportViewController ()

@end

@implementation SupportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (IBAction)done:(id)sender {
   [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)submit:(id)sender {
    UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
    view.backgroundColor = [UIColor colorWithWhite:0.4f alpha:0.5f];
    NSLog(@"Submitted");
    UIView *circle = [[UIView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - 20.0f)/2.0f, (self.view.frame.size.height - 20.0f)/3.0f, 20.0f, 20.0f)];
    circle.layer.cornerRadius = 10.0f;
    circle.layer.backgroundColor = THEME_COLOR.CGColor;
    
    [view addSubview:circle];
    [self.view addSubview:view];
    
    CABasicAnimation *theAnimation;
    theAnimation=[CABasicAnimation animationWithKeyPath:@"transform"];
    theAnimation.duration=0.3;
    theAnimation.repeatCount=HUGE_VALF;
    theAnimation.autoreverses=YES;
    theAnimation.fromValue=[NSValue valueWithCATransform3D:CATransform3DMakeScale(1.0f, 1.0f, 1.0f)];
    theAnimation.toValue=[NSValue valueWithCATransform3D:CATransform3DMakeScale(5.0f, 5.0f,5.0f)];
    [circle.layer addAnimation:theAnimation forKey:@"pulse"]; //myButton.layer instead of
    PFObject *complaint = [PFObject objectWithClassName:@"Complaint"];
    complaint[@"username"] = self.usernameField.text;
    complaint[@"datetime"] = self.timeField.text;
    complaint[@"description"] = self.descriptionField.text;
    complaint[@"user"] = [PFUser currentUser];
    [complaint saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [self.navigationController popViewControllerAnimated:NO];
    }];
}





@end
